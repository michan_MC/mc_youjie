//
//  MailListTableViewCell.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//



#import "MailListTableViewCell.h"

@interface MailListTableViewCell ()
{
    BOOL isAttention;
}
@end


@implementation MailListTableViewCell

+ (instancetype)mailList
{
    MailListTableViewCell *cell;
     cell = [[[NSBundle mainBundle]loadNibNamed:@"MailListTableViewCell" owner:nil options:nil]lastObject];
    
    return cell;
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

    }
    return self;
}

- (IBAction)yaoqing:(id)sender {
    
    
    [_Delegate yaoqing:_personInfo];
    
//    [self.inventBtn setSelected:YES];
//    [self.inventBtn setTitle:@"已邀请" forState:UIControlStateSelected];
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setValue:@"已邀请" forKey:@"已邀请"];
//    [defaults synchronize];
//    
//    [self.inventBtn setUserInteractionEnabled:NO];
//
    
    
//    
//    if (_isMailVC) {
//        [self.inventBtn setSelected:YES];
//        [self.inventBtn setTitle:@"已邀请" forState:UIControlStateSelected];
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        [defaults setValue:@"已邀请" forKey:@"已邀请"];
//        [defaults synchronize];
//        
//        [self.inventBtn setUserInteractionEnabled:NO];
//        
//    }else{
//        isAttention = !isAttention;
//        [self.inventBtn setSelected:isAttention];
//        [self.inventBtn setTitle:@"已关注" forState:UIControlStateSelected];
//        [self.inventBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
//        [self.inventBtn setTitle:@"关注" forState:UIControlStateNormal];
//    }
    
  
}


- (void)awakeFromNib
{
    isAttention = NO;
    
   // [self setupViews];
    

}

- (void)setupViews
{
    [self.inventBtn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
}
- (void)click
{
    
    if (_isMailVC) {
        [self.inventBtn setSelected:YES];
        [self.inventBtn setTitle:@"已邀请" forState:UIControlStateSelected];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"已邀请" forKey:@"已邀请"];
        [defaults synchronize];
        
        [self.inventBtn setUserInteractionEnabled:NO];
        
    }else{
        isAttention = !isAttention;
        [self.inventBtn setSelected:isAttention];
        [self.inventBtn setTitle:@"已关注" forState:UIControlStateSelected];
        [self.inventBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
        [self.inventBtn setTitle:@"关注" forState:UIControlStateNormal];
    }
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
