//
//  MailListTableView.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/19.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MailListTableView.h"
#import "MJRefresh.h"
#import "MailListTableViewCell.h"
#import "MailListUser.h"
#import <AddressBook/AddressBook.h>
#import "JXAddressBook.h"
#include "UIViewController+HUD.h"
#import "CommeHelper.h"
#import "NetworkManager.h"
#import "WMLabelAlertView.h"

@interface MailListTableView ()<MailListTableViewCellDelegate>
{
    NSMutableArray *mailListDataSource;
    
    NSArray *_dataArray;
    NSArray *_searchArray;
    NSMutableArray *_tempData;
    UITableView * table;
    NSMutableArray *iphoneArray;
    WMLabelAlertView *alert;


}
@end

@implementation MailListTableView

- (instancetype)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.dataSource = self;
        self.delegate = self;
        _tempData=[NSMutableArray arrayWithCapacity:20];
        iphoneArray = [NSMutableArray array];
        [self getAddressBook];

    }
    
    return self;
}



- (void)getAddressBook
{
    _dataArray = [NSArray array];
    
    [JXAddressBook getPersonInfo:^(NSArray *personInfos) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            // 对获取数据进行排序
            _dataArray = [JXAddressBook sortPersonInfos:personInfos];
            
            [self reloadData];
            
        });
    }];
}

#pragma mark - TableViewDelegate & TableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self) {
        return _dataArray.count;

    }
    else{
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self) {
        return JXSpellFromIndex(section);
        
    }
    else{
        return nil;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self) {
        return ((NSArray *)[_dataArray objectAtIndex:section]).count;
        
    }
    else{
        return iphoneArray.count;
    }

}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"mailLiistCell";
   
    if (tableView == self) {
        

    MailListTableViewCell *cell = (MailListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MailListTableViewCell" owner:self options:nil] lastObject];
        
        cell.isMailVC = YES;
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }    
    JXPersonInfo *personInfo = nil;
    cell.Delegate = self;

    
    NSArray *subArr = [_dataArray objectAtIndex:indexPath.section];
    
    personInfo = [subArr objectAtIndex:indexPath.row];
    
   // [cell.inventBtn setTitle:<#(NSString *)#> forState:UIControlStateNormal];
    
//     MailListUser *mailUser = mailListDataSource[indexPath.section];
//    cell.iconImage.image = mailUser.icon;
        
    cell.personInfo = personInfo;
    cell.name.text = personInfo.fullName;
   
    
    //[self setData:personInfo];
    
    return cell;
    }
    else{
        
        UITableViewCell *cell = nil;
        static NSString *CellIdentifier = @"ContactListCell";
        cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        cell.textLabel.text = [iphoneArray objectAtIndex:indexPath.row];
        return cell;
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == table) {
        [self yaoqinghaoyou:[iphoneArray objectAtIndex:indexPath.row]];
        [alert cancel:nil];
    }
}
-(void)yaoqing:(JXPersonInfo *)personInfo
{
   // NSLog(@"%@",personInfo.phone);
    //[iphoneArray removeAllObjects];
    if (personInfo.phone.count > 1) {
        table = [[UITableView alloc] initWithFrame:CGRectMake(10, 0, 260, 180) style:UITableViewStyleGrouped];
        table.delegate = self;
        table.dataSource = self;

        iphoneArray = (NSMutableArray *)personInfo.phone;
        alert = [[WMLabelAlertView alloc] initWithTitle:@"选择手机号码" contentView:table];
        [alert show];
        
        return;

    }
    [self yaoqinghaoyou:[personInfo.phone firstObject]];
    
}
-(void)yaoqinghaoyou:(NSString*)iphoneNum{
    NSLog(@"%@",iphoneNum);
    if (!iphoneNum) {
        return;
    }
    //[self showLoading:YES AndText:@"正在加载"];
    NetworkManager *requestManager;
    requestManager = [NetworkManager instanceManager];
    requestManager.needSeesion = YES;

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"mobilePhone"   : iphoneNum,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"invitationRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
       
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        
        
        
    }];
    

    
}
-(void)setData:(JXPersonInfo*)personInfo
{
    
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    [dict setObject:personInfo.fullName forKey:@"addressBookUser"];
    [dict setObject:personInfo.phone forKey:@"mobileNumber"];
    [_tempData addObject:dict];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self) {
        return 67;

        
    }
    else{
        return 40;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}



#pragma mark 右侧的索引方法
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i < 27; i++) {
        [arr addObject:JXSpellFromIndex(i)];
    }
    return arr;
}





@end
