//
//  ProjectListViewCell.m
//  StarGroups
//
//  Created by zijin on 15/4/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ProjectListViewCell.h"
#import "ProDetailsViewController.h"
#import "UIImageView+BFKit.h"
#import "UIColor+BFKit.h"
#import "UIImageView+EMWebCache.h"
#import "CommeHelper.h"
#import "EaseMob.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"
@implementation ProjectListViewCell

- (void)awakeFromNib {
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}
-(void)lodaMyFriView:(NSDictionary*)dict{
    for (UIView* obj in self.contentView.subviews) {
        [obj removeFromSuperview];
    }


    CGFloat y = 0;
    if (![[dict objectForKey:@"status"] isEqualToString:@"等待复核"] && [dict objectForKey:@"loannickname"]){
        y = 40;
        UIImageView *imageViewBottom1=[UIImageView initWithImage:[UIImage imageNamed:@"贷款个人列表底图"] frame:CGRectMake(7,0,306,60)];
        [self.contentView addSubview:imageViewBottom1];
        imageViewBottom1.layer.cornerRadius = 10;
        imageViewBottom1.layer.masksToBounds = YES;
        NSString * nickname = [dict objectForKey:@"nickname"];
        NSString * money = [dict objectForKey:@"money"];
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 10, 300, 20)];
        lbl.text = [NSString stringWithFormat:@"%@投资了%@元",nickname,money];
        [imageViewBottom1 addSubview:lbl];
        lbl.font = [UIFont systemFontOfSize:15];
        lbl.textColor = [UIColor grayColor];
        
        
    }
    

    
    
    UIImageView *imageViewTop=[UIImageView initWithImage:[UIImage imageNamed:@"首页-列表底图"] frame:CGRectMake(7,y,306,118)];
    [self.contentView addSubview:imageViewTop];
    UIImageView *imageViewBottom=[UIImageView initWithImage:[UIImage imageNamed:@"贷款个人列表底图"] frame:CGRectMake(7,103 + y,306,118)];
    [self.contentView addSubview:imageViewBottom];
       UIProgressView *progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(77, 163 + y, 150, 6)];
    //设置的是进度颜色
    progressView.progressTintColor = [UIColor grayColor];
    //设置背景颜色
    progressView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:progressView];
    
    
    [self setName:[dict objectForKey:@"name"]==nil ?@"水族箱用品设计建设":[dict objectForKey:@"name"] andFrame:CGRectMake(52,12 + y,178,21) andFont:[UIFont systemFontOfSize:16]withColor:[UIColor whiteColor]];
    
    
    UIButton *butGuarantyType=[self setBtn:[dict objectForKey:@"guarantyType"]==nil?@"人品贷":[dict objectForKey:@"guarantyType"] andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(244, 13 + y, 50, 18) andImageName:@"首页-标签"];
    
    
    UIButton *butShare=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(17, 182 + y, 20, 20) andImageName:@"首页-分享icon"];
    
    [butShare addTarget:self action:@selector(butShare)  forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *butPinLun=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(60, 183 + y, 20, 20) andImageName:@"首页-评论icon"];
    
    [butPinLun addTarget:self action:@selector(pinLun) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *butPi=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(102, 183 + y, 20, 20) andImageName:@"首页-点赞icon"];
    
    
    
    butPi.tag=0;
    [butPi addTarget:self action:@selector(setPraise:)  forControlEvents:UIControlEventTouchUpInside];
    
    if ([dict objectForKey:@"isPraise"]!=[NSNull null]) {
        
        if ([[dict objectForKey:@"isPraise"] boolValue]==true) {
            
            [butPi setBackgroundImage:[UIImage imageNamed:@"首页-点赞icon_按下状态"] forState:UIControlStateNormal];
            butPi.tag=1000;
        }
    }
    
    NSString *loanMoney;
    if ([dict objectForKey:@"loanMoney"]==[NSNull null]) {
        loanMoney=@"10000";
    }else
    {
        
        NSNumber *nuber=[dict objectForKey:@"loanMoney"];
        
        loanMoney= [nuber stringValue];
    }
    
    
    //NSString *loanMoney=[dict objectForKey:@"loanMoney"]==nil?@"10000":[NSString stringWithFormat:@"%ld",[[dict
    
    [self setName:[@"￥" stringByAppendingString:loanMoney ]andFrame:CGRectMake(70+7, 35 + y, 85, 27) andFont:[UIFont systemFontOfSize:15]withColor:[UIColor whiteColor]];
    
    NSString *deadline=[dict objectForKey:@"deadline"]==[NSNull null]?@"10":[[dict objectForKey:@"deadline"] stringValue];
    
    
    
    [self setName:[deadline stringByAppendingString:@"个月"] andFrame:CGRectMake(161,38 + y,63,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor whiteColor] ];
    
    UIImageView *imageViews=[[UIImageView alloc]initWithFrame:CGRectMake(8+7,85 + y,57,57)];
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    
    
    NSString *strUrl;
    if ([dict objectForKey:@"portrait"]!=[NSNull null]) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"portrait"]]];
        
    }
    
    
    [imageViews sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
    
    
    //    UIImageView *imageView=[UIImageView initWithImage:[UIImage imageNamed:@"首页-列表头像"] frame:CGRectMake(8+7,85,57,57)];
    
    [self.contentView bringSubviewToFront:imageViews];
    [self.contentView addSubview:imageViews];
    
    NSString *ratePercent=@"100";
    if ([dict objectForKey:@"rate"]!=[NSNull null]) {
        CGFloat f =  [[dict objectForKey:@"rate"] floatValue];
        f =  f * 100.0;
        
        ratePercent=[[NSString stringWithFormat:@"%.f",f]stringByAppendingString:@"%"];
    }else
    {
        ratePercent=[ratePercent stringByAppendingString:@"％"];
    }
    
    [self setName:ratePercent andFrame:CGRectMake(235,39 + y,62,21) andFont:[UIFont systemFontOfSize:13] withColor:[UIColor whiteColor]];
    
    
    if ([[dict objectForKey:@"status"]isEqualToString:@"等待复核"]){
    
    [self setName:[dict objectForKey:@"nickname"]==[NSNull null] ?@"爱飞的鸟":[[dict objectForKey:@"nickname"] stringValue] andFrame:CGRectMake(82,108 + y,78,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    }
    else{
        [self setName:[dict objectForKey:@"loannickname"]==[NSNull null] ?@"爱飞的鸟":[[dict objectForKey:@"loannickname"] stringValue] andFrame:CGRectMake(82,108 + y,78,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    }
    
    [self setName:[dict objectForKey:@"status"]==[NSNull null] ?@"还款中":[[dict objectForKey:@"status"] stringValue] andFrame:CGRectMake(237,112 + y,57,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    
    //    [self setName:[dict objectForKey:@"status"] andFrame:CGRectMake(252,155,30,21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor blackColor]];
    
    
    NSString *jd=@"0";
    if ([dict objectForKey:@"jd"]!=[NSNull null]) {
        jd=[[[dict objectForKey:@"jd"] stringValue] stringByAppendingString:@"%"];
        progressView.progress = [[dict objectForKey:@"jd"] floatValue] / 100.0;
        NSLog(@"%@",[[dict objectForKey:@"jd"] stringValue]);
        // NSLog(@"%d",[[dict objectForKey:@"jd"] intValue] / 100);
        
    }else
    {
        progressView.progress  = 0.0;
        jd= [jd stringByAppendingString:@"%"];
    }
    [self setName:jd andFrame:CGRectMake(252,155 + y,50,21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];//进度
    
    [self setName:@"借款金额"andFrame:CGRectMake(90, 64 + y, 59, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"期限"andFrame:CGRectMake(171, 64 + y, 41, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"年利率"andFrame:CGRectMake(244, 64+ y, 46, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    
    UILabel *label= [[UILabel alloc]initWithFrame:CGRectMake(160, 127 + y, 20, 21)];
    if ([dict objectForKey:@"friendsCount"]!=[NSNull null]) {
        
        [label setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"friendsCount"]]];
        
    }else
    {
        [label setText:@"10"];
        
    }
    
    //[label setText:@"10"];
    
    [self.contentView bringSubviewToFront:label];
    [self.contentView addSubview:label];
    NSString * str = @"间接朋友";
    if ([[dict objectForKey:@"friendsType"] integerValue] == 0){
        str = @"好友";
    }
    
    [self setName:str andFrame:CGRectMake(83, 127 + y, 51, 21) andFont:[UIFont systemFontOfSize:10]withColor:[UIColor darkGrayColor]];
    [self setName:@"个共同好友"andFrame:CGRectMake(181, 128 + y, 65, 21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];
    
    NSString * isAttention = @"关注";
    if ([dict objectForKey:@"isAttention"]) {
        isAttention = @"已关注";
    }
    
    [self setName:isAttention andFrame:CGRectMake(15+7, 148 + y, 42, 21) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor darkGrayColor]];
    
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    if (dict!=nil) {
        _dataDict=dict;
    }

    
    
    
    
    
}

-(void)lodaInvestView:(NSDictionary*)dict
{
    //初始化列表
    
    //    [self setBackgroundColor: ColorString(@"#E6E6E6")];

   [self setFrame:CGRectMake(0, 0, 320, 230)];
    
    
    UIImageView *imageViewTop=[UIImageView initWithImage:[UIImage imageNamed:@"首页-列表底图"] frame:CGRectMake(7,0,306,118)];
    [self.contentView addSubview:imageViewTop];
    UIImageView *imageViewBottom=[UIImageView initWithImage:[UIImage imageNamed:@"贷款个人列表底图"] frame:CGRectMake(7,103,306,118)];
    [self.contentView addSubview:imageViewBottom];
   UIProgressView *progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(77, 163, 150, 6)];
    //设置的是进度颜色
    progressView.progressTintColor = [UIColor grayColor];
    //设置背景颜色
    progressView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:progressView];
    
    [self setName:[dict objectForKey:@"loanName"]==[NSNull null] ?@"水族箱用品设计建设":[dict objectForKey:@"loanName"] andFrame:CGRectMake(52,12,178,21) andFont:[UIFont systemFontOfSize:16]withColor:[UIColor whiteColor]];
    
    
    UIButton *butGuarantyType=[self setBtn:[dict objectForKey:@"loanType"]==[NSNull null]?@"人品贷":[dict objectForKey:@"loanType"] andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(244, 13, 50, 18) andImageName:@"首页-标签"];
    
    
    UIButton *butShare=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(17, 182, 20, 20) andImageName:@"首页-分享icon"];
    
    [butShare addTarget:self action:@selector(butShare)  forControlEvents:UIControlEventTouchUpInside];
    UIButton *butPinLun=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(60, 183, 20, 20) andImageName:@"首页-评论icon"];
    
    [butPinLun addTarget:self action:@selector(pinLun) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *butPi=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(102, 183, 20, 20) andImageName:@"首页-点赞icon"];
    
    
    
    butPi.tag=0;
    [butPi addTarget:self action:@selector(setPraise:)  forControlEvents:UIControlEventTouchUpInside];
    
    if ([dict objectForKey:@"isPraise"]!=[NSNull null]) {
        
        if ([[dict objectForKey:@"isPraise"] boolValue]==true) {
            
            [butPi setBackgroundImage:[UIImage imageNamed:@"首页-点赞icon_按下状态"] forState:UIControlStateNormal];
            butPi.tag=1000;
        }
    }
    
    NSString *loanMoney;
    if ([dict objectForKey:@"investMoney"]==[NSNull null]) {
        loanMoney=@"10000";
    }else
    {
        
        NSNumber *nuber=[dict objectForKey:@"investMoney"];
        
        loanMoney= [nuber stringValue];
    }
    [self setName:[@"￥" stringByAppendingString:loanMoney ]andFrame:CGRectMake(70+7, 35, 85, 27) andFont:[UIFont systemFontOfSize:15]withColor:[UIColor whiteColor]];
    
    NSString *deadline=[dict objectForKey:@"deadline"]==[NSNull null]?@"10":[[dict objectForKey:@"deadline"] stringValue];
    
    [self setName:[deadline stringByAppendingString:@"个月"] andFrame:CGRectMake(161,38,63,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor whiteColor] ];
    
    UIImageView *imageViews=[[UIImageView alloc]initWithFrame:CGRectMake(8+7,85,57,57)];
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    
    
    NSString *strUrl;
    if ([dict objectForKey:@"portrait"]!=[NSNull null]) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"portrait"]]];
        
    }
    
    
    [imageViews sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
    
    [self.contentView bringSubviewToFront:imageViews];
    [self.contentView addSubview:imageViews];
    
    NSString *ratePercent=@"100";
    if ([dict objectForKey:@"ratePercent"]!=[NSNull null]) {
        ratePercent=[[[dict objectForKey:@"ratePercent"] stringValue]stringByAppendingString:@"%"];
    }else
    {
        ratePercent=[ratePercent stringByAppendingString:@"％"];
    }
    
    [self setName:ratePercent andFrame:CGRectMake(235,39,62,21) andFont:[UIFont systemFontOfSize:13] withColor:[UIColor whiteColor]];
    
    [self setName:[dict objectForKey:@"loanUserNickName"]==[NSNull null] ?@"爱飞的鸟":[[dict objectForKey:@"loanUserNickName"] stringValue] andFrame:CGRectMake(82,108,78,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    
    [self setName:[dict objectForKey:@"status"]==[NSNull null] ?@"还款中":[[dict objectForKey:@"status"] stringValue] andFrame:CGRectMake(237,112,57,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    
    NSString *jd=@"0";
    if ([dict objectForKey:@"jd"]!=nil) {
        jd=[[[dict objectForKey:@"jd"] stringValue] stringByAppendingString:@"%"];
        progressView.progress = [[dict objectForKey:@"jd"] floatValue] / 100.0;
    }else
    {
        progressView.progress = 0.0;
        jd= [jd stringByAppendingString:@"%"];
    }
    [self setName:jd andFrame:CGRectMake(252,155,50,21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];//进度
    
    [self setName:@"借款金额"andFrame:CGRectMake(90, 64, 59, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"期限"andFrame:CGRectMake(171, 64, 41, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"年利率"andFrame:CGRectMake(244, 64, 46, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    
    UILabel *label= [[UILabel alloc]initWithFrame:CGRectMake(160, 127, 20, 21)];
    if ([dict objectForKey:@"friendsCount"]!=[NSNull null]) {
        
        [label setText:[NSString stringWithFormat:@"%@",[dict objectForKey:@"friendsCount"]]];
 
    }else
    {
       [label setText:@"10"];

    }

    
    [self.contentView bringSubviewToFront:label];
    [self.contentView addSubview:label];
    NSString * str = @"间接朋友";
    if ([[dict objectForKey:@"friendsType"] integerValue] == 1){
        str = @"好友";
    }
    
    [self setName:str andFrame:CGRectMake(83, 127, 51, 21) andFont:[UIFont systemFontOfSize:10]withColor:[UIColor darkGrayColor]];
    [self setName:@"个共同好友"andFrame:CGRectMake(181, 128, 65, 21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];
    
    if ([dict objectForKey:@"isAttention"]!=[NSNull null] && [[dict objectForKey:@"isAttention"]boolValue ] == NO ) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(15+7, 148, 42, 21)];
        [btn setTitle:@"关注" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(payAtttention:)  forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        
    }
    else{
        [self setName:@"已关注" andFrame:CGRectMake(15+7, 148, 42, 21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    }
    

   // [self setName:@"关注"andFrame:CGRectMake(15+7, 148, 42, 21) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor darkGrayColor]];
    
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    if (dict!=nil) {
        _dataDict=dict;
    }
 
}




-(void)LodaView:(NSDictionary*)dict
{
    
    if (dict != nil) {
        _TempData = dict;
    }
    //初始化列表
    
    //    [self setBackgroundColor: ColorString(@"#E6E6E6")];
    [self setFrame:CGRectMake(0, 0, 320, 230)];
    
    
    UIImageView *imageViewTop=[UIImageView initWithImage:[UIImage imageNamed:@"首页-列表底图"] frame:CGRectMake(7,0,306,118)];
    [self.contentView addSubview:imageViewTop];
    UIImageView *imageViewBottom=[UIImageView initWithImage:[UIImage imageNamed:@"贷款个人列表底图"] frame:CGRectMake(7,103,306,118)];
    [self.contentView addSubview:imageViewBottom];
    UIProgressView *progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(77, 163, 150, 6)];
    //设置的是进度颜色
    progressView.progressTintColor = [UIColor grayColor];
    //设置背景颜色
    progressView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:progressView];

    
    [self setName:[dict objectForKey:@"name"]==nil ?@"水族箱用品设计建设":[dict objectForKey:@"name"] andFrame:CGRectMake(52,12,178,21) andFont:[UIFont systemFontOfSize:16]withColor:[UIColor whiteColor]];
    
    
    UIButton *butGuarantyType=[self setBtn:[dict objectForKey:@"guarantyType"]==nil?@"人品贷":[dict objectForKey:@"guarantyType"] andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(244, 13, 50, 18) andImageName:@"首页-标签"];
    
    
    UIButton *butShare=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(17, 182, 20, 20) andImageName:@"首页-分享icon"];
    
    [butShare addTarget:self action:@selector(butShare)  forControlEvents:UIControlEventTouchUpInside];

    UIButton *butPinLun=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(60, 183, 20, 20) andImageName:@"首页-评论icon"];
    
    [butPinLun addTarget:self action:@selector(pinLun) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *butPi=[self setBtn:@"" andFont:[UIFont systemFontOfSize:15] andFrame:CGRectMake(102, 183, 20, 20) andImageName:@"首页-点赞icon"];
    
    
    
    butPi.tag=0;
    [butPi addTarget:self action:@selector(setPraise:)  forControlEvents:UIControlEventTouchUpInside];
    
    if ([dict objectForKey:@"isPraise"]!=nil) {
        
        if ([[dict objectForKey:@"isPraise"] boolValue]==true) {
            
            [butPi setBackgroundImage:[UIImage imageNamed:@"首页-点赞icon_按下状态"] forState:UIControlStateNormal];
            butPi.tag=1000;
        }
    }
    
    NSString *loanMoney;
    if ([dict objectForKey:@"loanMoney"]==[NSNull null]) {
        loanMoney=@"10000";
    }else
    {
        
        NSNumber *nuber=[dict objectForKey:@"loanMoney"];
        
        loanMoney= [nuber stringValue];
    }
    [self setName:[@"￥" stringByAppendingString:loanMoney ]andFrame:CGRectMake(70+7, 35, 85, 27) andFont:[UIFont systemFontOfSize:15]withColor:[UIColor whiteColor]];
    
    NSString *deadline=[dict objectForKey:@"deadline"]==[NSNull null]?@"10":[[dict objectForKey:@"deadline"] stringValue];
    
    
    
    [self setName:[deadline stringByAppendingString:@"个月"] andFrame:CGRectMake(161,38,63,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor whiteColor] ];
    
    UIImageView *imageViews=[[UIImageView alloc]initWithFrame:CGRectMake(8+7,85,57,57)];
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    
    
    NSString *strUrl;
    if ([dict objectForKey:@"portrait"]!=[NSNull null]) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"portrait"]]];
        
    }
    
    
    [imageViews sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
        [self.contentView bringSubviewToFront:imageViews];
    [self.contentView addSubview:imageViews];
    
    NSString *ratePercent=@"100";
    if ([dict objectForKey:@"ratePercent"]!=[NSNull null]) {
        ratePercent=[[[dict objectForKey:@"ratePercent"] stringValue]stringByAppendingString:@"%"];
    }else
    {
        ratePercent=[ratePercent stringByAppendingString:@"％"];
    }
    
    [self setName:ratePercent andFrame:CGRectMake(235,39,62,21) andFont:[UIFont systemFontOfSize:13] withColor:[UIColor whiteColor]];
    
    [self setName:[dict objectForKey:@"nickname"]==[NSNull null] ?@"爱飞的鸟":[[dict objectForKey:@"nickname"] stringValue] andFrame:CGRectMake(82,108,78,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    
    [self setName:[dict objectForKey:@"status"]==[NSNull null] ?@"还款中":[[dict objectForKey:@"status"] stringValue] andFrame:CGRectMake(237,112,57,21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    NSString *jd=@"0";
    if ([dict objectForKey:@"jd"]!=[NSNull null]) {
        jd=[[[dict objectForKey:@"jd"] stringValue] stringByAppendingString:@"%"];
         progressView.progress = [[dict objectForKey:@"jd"] floatValue] / 100.0;
        NSLog(@"%@",[[dict objectForKey:@"jd"] stringValue]);
       // NSLog(@"%d",[[dict objectForKey:@"jd"] intValue] / 100);

    }else
    {
        progressView.progress  = 0.0;
        jd= [jd stringByAppendingString:@"%"];
    }
    [self setName:jd andFrame:CGRectMake(252,155,50,21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];//进度
    
    [self setName:@"借款金额"andFrame:CGRectMake(90, 64, 59, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"期限"andFrame:CGRectMake(171, 64, 41, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    [self setName:@"年利率"andFrame:CGRectMake(244, 64, 46, 25) andFont:[UIFont systemFontOfSize:12]withColor:[UIColor whiteColor]];
    if ([dict objectForKey:@"friendsCount"]!=nil) {
        
        [self setName:[NSString stringWithFormat:@"%@",[dict objectForKey:@"friendsCount"]] andFrame:CGRectMake(160, 127, 20, 21) andFont:[UIFont systemFontOfSize:12] withColor:[UIColor blackColor]];
        
    }else
    {
        [self setName:@"0" andFrame:CGRectMake(160, 127, 20, 21) andFont:[UIFont systemFontOfSize:12] withColor:[UIColor blackColor]];
    }

    //[label setText:@"10"];
    
    NSString * str = @"间接朋友";
    if ([[dict objectForKey:@"friendsType"] integerValue] == 1){
        str = @"好友";
    }
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *_userID = [loginInfo objectForKey:kSDKUsername];

    if ([dict objectForKey:@"isAttention"]!=[NSNull null] && [[dict objectForKey:@"isAttention"]boolValue ] == NO ) {
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(15+7, 148, 42, 21)];
        [btn setTitle:@"关注" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(payAtttention:)  forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        if ([_userID isEqualToString:[[dict objectForKey:@"userId"] stringValue]]){
            //[self showAllTextDialog:@"不能关注自己"];
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [btn setUserInteractionEnabled:NO];
            [btn setTitle:@"自己" forState:UIControlStateNormal];
            str= @"";
        }
        
        
    }
    else{
        [self setName:@"已关注" andFrame:CGRectMake(15+7, 148, 42, 21) andFont:[UIFont systemFontOfSize:13]withColor:[UIColor darkGrayColor]];
    }


    [self setName:str andFrame:CGRectMake(83, 127, 51, 21) andFont:[UIFont systemFontOfSize:10]withColor:[UIColor darkGrayColor]];
    [self setName:@"个共同好友"andFrame:CGRectMake(181, 128, 65, 21) andFont:[UIFont systemFontOfSize:11]withColor:[UIColor darkGrayColor]];
    //isAttention
   


    
    
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    if (dict!=nil) {
        _dataDict=dict;
    }
    
}
-(void)butShare{
    [_delegate Share];
    
}

-(void)pinLun
{
    
    [_delegate CommentsDidDone:_dataDict];
    
}

-(void)setPraise:(UIButton*)btn
{
    
    
    if (btn.tag==0) {
        [btn setBackgroundImage:[UIImage imageNamed:@"首页-点赞icon_按下状态"] forState:UIControlStateNormal];
        btn.tag=1000;
        [_dataDict setValue:@"dianzan" forKey:@"isPaise2"];
        
    }else
    {
        btn.tag=0;
        [btn setBackgroundImage:[UIImage imageNamed:@"首页-点赞icon"] forState:UIControlStateNormal];
        [_dataDict setValue:@"cancel" forKey:@"isPaise2"];
    }
    
    [_delegate GetPraise:_dataDict];
}




-(void)setName:(NSString*)labelText andFrame:(CGRect)rect andFont:(UIFont*)font withColor:(UIColor*)color
{
    
    UILabel *lable=[[UILabel alloc]initWithFrame:rect];
    [lable setText:labelText==nil?@" ":labelText];
    [lable setFont:font];
    
    [lable setTextColor:color];
    [self.contentView addSubview:lable];
    
    [self.contentView bringSubviewToFront:lable];
}



-(UIButton*)setBtn:(NSString*)title andFont:(UIFont*)font andFrame:(CGRect)frame andImageName:(NSString*)imageName
{
    
    
    
    UIButton *guarantyType=[[UIButton alloc]initWithFrame:frame];
    
    [guarantyType setTitle:title==nil?@"人品贷":title forState:UIControlStateNormal];
    [guarantyType setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [guarantyType setTintColor:[UIColor whiteColor]];
    guarantyType.titleLabel.font=font;
    
    [self.contentView addSubview:guarantyType];
    return guarantyType;
    
}
-(void)payAtttention:(UIButton*)btn
{
    
   NetworkManager* requestManager = [NetworkManager instanceManager];
    requestManager.needSeesion = YES;

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *_userID = [loginInfo objectForKey:kSDKUsername];

    if (![btn.titleLabel.text isEqual:@"已关注"]) {
        NSDictionary *dic = @{
                              @"userId" : _userID,
                              @"beAttentionUser" : [[_dataDict objectForKey:@"userId"] stringValue],
                              @"op": @"guanzhu",
                              @"index" : @"1",
                              @"length"    :   @"10"
                              };
        
        NSDictionary *dict = @{
                               @"method" : @"attentionRequestHandler",
                               @"value"  :[CommeHelper getEncryptWithString:dic]
                               };
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            
            btn.titleLabel.font = [UIFont systemFontOfSize:13];
            [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [btn setUserInteractionEnabled:NO];
            [btn setTitle:@"已关注" forState:UIControlStateNormal];
            if (_delegate) {
                [_delegate showAllTextlog:@"关注成功"];

            }
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            //[self showLoading:NO AndText:nil];
            
        }];
        
        
    }
    
}



@end
