//
//  FriendMoreDataTableViewCell.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendMoreDataTableViewCell : UITableViewCell

//@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (weak, nonatomic) IBOutlet UIImageView *UserImage;//用户背景图片

@property (weak, nonatomic) IBOutlet UILabel *Realname;//真实姓名
@property (weak, nonatomic) IBOutlet UILabel *LoabMoney;

//@property (weak, nonatomic) IBOutlet UILabel *Money;//借款金额
@property (weak, nonatomic) IBOutlet UILabel *name;//标题

@property (weak, nonatomic) IBOutlet UILabel *status;//还款状态（还款中）

@property (weak, nonatomic) IBOutlet UILabel *deadline;//期限
@property (weak, nonatomic) IBOutlet UILabel *actualRate;//年利率
@property (weak, nonatomic) IBOutlet UILabel *friendsType;//间接朋友
@property (weak, nonatomic) IBOutlet UILabel *jd;//进度

@property (weak, nonatomic) IBOutlet UIButton *guarantyType;//贷款方式
+ (instancetype)friendMoreData;
-(void)loadCellViewWithAryy:(NSDictionary*)arry;
@end
