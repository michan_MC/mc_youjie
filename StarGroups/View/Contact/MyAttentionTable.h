//
//  MyAttentionTable.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/26.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewController.h"
@protocol myAttentionTableDegate <NSObject>
- (void)attentionPushWithController:(NSString*)sendToUser;
-(void)MyAttentionHeaderRefresh:(NSInteger)page;

@end
@interface MyAttentionTable : UITableView <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic)NSMutableArray *myAttentionDataSource;
@property (weak, nonatomic) id <myAttentionTableDegate> attentionDalegat;
@property (strong, nonatomic)NSMutableArray *dataSources;
@end
