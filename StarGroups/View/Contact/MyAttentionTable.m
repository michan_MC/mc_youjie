//
//  MyAttentionTable.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/26.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MyAttentionTable.h"
#import "MJRefresh.h"
#import "ContactTableViewCell.h"
#import "NetworkManager.h"
#import "CommeHelper.h"
#import "FriendDataViewController.h"
#import "UIImageView+EMWebCache.h"

/*我的关注*/

@interface MyAttentionTable ()

@end
@implementation MyAttentionTable

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style
{
    
    
    self = [super initWithFrame:frame style:style];
    
    if (self) {
        
        //         _mailListDataSource = [NSMutableArray array];
        _dataSources=[NSMutableArray array];
        self.tableFooterView = [[UIView alloc] init];
        self.dataSource = self;
        self.delegate = self;
        [self addHeaderWithTarget:self action:@selector(myAttentionHeaderRefresh)];
//        [self addFooterWithTarget:self action:@selector(myAttentionFooterRefresh)];
   
    }
    
    return self;
}

-(void)myAttentionHeaderRefresh{
    [self.attentionDalegat MyAttentionHeaderRefresh:1];
}

- (NSMutableArray *)myAttentionDataSource
{
    return _myAttentionDataSource;
    
}



#pragma mark - TableViewDelegate & TableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return  self.myFriendDataSource.count;
    return _dataSources.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"ContactTableViewCell";
    ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];

        
    }
    if (_dataSources.count > 0) {
        NSDictionary * dic = [_dataSources objectAtIndex:indexPath.row];
        NSString *imgUrl = @"";
        if ([dic objectForKey:@"portrait"] != [NSNull null]){
            imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
        }
        
        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"attentionUser"]:[dic objectForKey:@"nickname"];
        cell.userName.text = namrStr;
        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
        

//        
//        
//        
//        
//    NSDictionary * dic = _dataSources[[indexPath row]];
//    NSString *imgUrl = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
//        NSString *namrStr = [dic objectForKey:@"nickname"];
//        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
//        cell.userName.text= namrStr;
//        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self.attentionDalegat attentionPushWithController:[[_dataSources objectAtIndex:[indexPath row]] objectForKey:@"beAttentionUser"]];
   

}


@end
