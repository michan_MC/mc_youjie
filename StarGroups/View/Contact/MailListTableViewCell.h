//
//  MailListTableViewCell.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MailListUser.h"
#import "JXPersonInfo.h"
@protocol  MailListTableViewCellDelegate<NSObject>

-(void)yaoqing:(JXPersonInfo*)personInfo;

@end




@interface MailListTableViewCell : UITableViewCell
@property(nonatomic,weak) id<MailListTableViewCellDelegate>Delegate;
@property (weak, nonatomic) IBOutlet UIButton *inventBtn;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
// 判断是否是联系人界面通讯录cell
@property (assign, nonatomic) BOOL isMailVC;
@property (strong, nonatomic) MailListUser *mailUser;
@property (weak,nonatomic)JXPersonInfo *personInfo;
+ (instancetype)mailList;
@end
