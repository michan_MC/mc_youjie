//
//  FriendMoreDataTableViewCell.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "FriendMoreDataTableViewCell.h"
#import "UIImageView+EMWebCache.h"
static NSString *url=@"http://121.201.16.96:80/app/test";

@implementation FriendMoreDataTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
 //   [_moreButton setUserInteractionEnabled:NO];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
      self= [[[NSBundle mainBundle] loadNibNamed:@"FriendMoreDataTableViewCell" owner:nil options:nil] lastObject];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (instancetype)friendMoreData
{
    return [[[NSBundle mainBundle] loadNibNamed:@"FriendMoreDataTableViewCell" owner:nil options:nil] lastObject];
}

-(void)loadCellViewWithAryy:(NSDictionary*)dict
{

    self.name.text=[NSString stringWithString:dict[@"name"]];
    
    self.status.text=[NSString stringWithString:dict[@"status"]];
    
    self.LoabMoney.text=[NSString stringWithFormat:@"￥%@",dict[@"loanMoney"]];
    
    self.deadline.text=[ NSString  stringWithFormat:@"%@月",[dict objectForKey:@"deadline"] ];
    
    self.actualRate.text=[ NSString  stringWithFormat:@"%@月",[dict objectForKey:@"actualRate"] ];
    
    UIImageView *imageView=[[UIImageView alloc]init];
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    NSString *strUrl=[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"userPhoto"]]];
    [imageView sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
    self.UserImage=imageView;
     self.Realname.text=[NSString stringWithString:dict[@"realname"]];


//    self.friendsType.text=[NSString stringWithString:dict[@"friendsType"]]==nil?0:[NSString stringWithString:dict[@"friendsType"]];
    
     NSInteger f=[[ NSString  stringWithFormat:@"%@",[dict objectForKey:@"jd"]] integerValue] ;
    
    self.jd.text=[[ NSString  stringWithFormat:@"%d",f ] stringByAppendingString:@"%"];

   self.guarantyType.titleLabel.text=[NSString  stringWithFormat:@"%@",[dict objectForKey:@"guarantyType"]];
   
   
    UIView *viewJd=[[UIView alloc]initWithFrame:CGRectMake(79, 165, 150*(f/100), 8)];
    [viewJd setBackgroundColor:[UIColor grayColor]];
    
    [self.contentView addSubview:viewJd];
    
    
}


@end
