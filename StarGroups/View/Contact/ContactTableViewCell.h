//
//  ContactTableViewCell.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol BaseTableCellDelegate;

@interface ContactTableViewCell : UITableViewCell
{
    UILongPressGestureRecognizer *_headerLongPress;
}

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) id<BaseTableCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) UIView *bottomLineView;
@end

@protocol BaseTableCellDelegate <NSObject>

- (void)cellImageViewLongPressAtIndexPath:(NSIndexPath *)indexPAth;

@end
