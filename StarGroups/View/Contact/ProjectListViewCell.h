//
//  ProjectListViewCell.h
//  StarGroups
//
//  Created by zijin on 15/4/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ProjectListViewCell;
@protocol  ProjectListViewCellDelegate<NSObject>

-(void)Share;//:(NSDictionary*)dataDict;

-(void)CommentsDidDone:(NSDictionary* )dictData;
-(void)GetPraise:(NSDictionary*)praise;
- (void)showAllTextlog:(NSString *)title;

@end

@interface ProjectListViewCell : UITableViewCell
//我关注的
-(void)lodaMyFriView:(NSDictionary*)dict;
//全部
-(void)LodaView:(NSDictionary*)dict;
//我投资的
-(void)lodaInvestView:(NSDictionary*)dict;
@property(nonatomic,weak)NSDictionary *dataDict;
@property(nonatomic,strong)NSDictionary *TempData;

@property(weak,nonatomic)id<ProjectListViewCellDelegate> delegate;



@end
