//
//  MePhotoView.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/16.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MePhotoView : UIView
- (void) enableEditing;
- (void) disableEditing;
@end
