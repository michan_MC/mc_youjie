//
//  MyPhotoController.h
//  StarGroups
//
//  Created by zijin on 15/4/24.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"
#import "MePhotoView.h"
@interface MyPhotoController : BaseViewController
@property (strong, nonatomic) MePhotoView *mePhotoView;

@end
