//
//  MyPhotoController.m
//  StarGroups
//
//  Created by zijin on 15/4/24.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MyPhotoController.h"
#import "ZYQAssetPickerController.h"
#import "CommeHelper.h"
#define kbutWith 71
#define KbutHeigth 71
#define Duration 0.2
@interface MyPhotoController ()<UIImagePickerControllerDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate>
{
    UIButton *btnAddButtn;
    
    UIButton *buttonNormal;
    
    BOOL contain;
    CGPoint startPoint;
    CGPoint originPoint;
}
@property (strong , nonatomic) NSMutableArray *itemArray;
@property (strong , nonatomic) NSMutableArray *delteItemArray;

@end

@implementation MyPhotoController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.itemArray = [NSMutableArray array];
    self.delteItemArray=[NSMutableArray array];
    self.mePhotoView = [[MePhotoView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72*2+50)];
    [self.view addSubview:self.mePhotoView];
    self.title=@"我的相册";
    [self addImageBtn];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


/**------------------------------拖动效果--------------------------------------------**/

-(void)addImageBtn
{
    
    btnAddButtn=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnAddButtn setImage:[UIImage imageNamed:@"个人资料页-添加图片icon-1"] forState:UIControlStateNormal];
    [btnAddButtn setTag:0];
    [btnAddButtn setFrame:[self setNumber:0]];//第一个添加按钮
    [btnAddButtn addTarget:self action:@selector(addOtherButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.itemArray addObject:btnAddButtn];
    [self.mePhotoView addSubview:btnAddButtn];
    
}


-(void)addOtherButton:(UIButton*)btn
{
    
    
    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                    initWithTitle:nil
                                    delegate:self
                                    cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles: @"从相册选择", @"拍照",nil];
    
    [myActionSheet showInView:self.view];
    
    
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self localPhto];
            break;
        case 1:
            [self takePhoto];
            
            break;
        default:
            break;
    }
    
}

-(void)localPhto
{
    
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
    picker.maximumNumberOfSelection = 8;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate=self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}


#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSInteger count=assets.count;
    
    if ((self.itemArray.count+assets.count)>9) {
        count=8-self.itemArray.count+1;
        //[self showMessge:@"所选的图片超过了8张"];
    }
    
    for (int i=0; i<count; i++) {
        ALAsset *asset=assets[i];
        
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        UIButton *btn=[self addNormalBtn];
        //图片显示在界面上
        [btn setImage:tempImg forState:UIControlStateNormal];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}






-(void)takePhoto{
    //资源类型为照相机
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否有相机
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        //资源类型为照相机
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
        
    }else {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:@"该设备无摄像头" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alter show];
    }
    
    
}



-(UIButton *)addNormalBtn
{
    
    NSInteger count=self.itemArray.count;
    buttonNormal=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonNormal.tag=count;
    [buttonNormal setImage:[UIImage imageNamed:@"个人资料页-图片缩略图】"] forState:UIControlStateNormal];
    //[buttonNormal setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    [buttonNormal setFrame:[self setNumber:self.itemArray.count]];
    [self.mePhotoView addSubview:buttonNormal];
    UIButton  *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    float w = 20;
    float h = 20;
    
    
    [deleteButton setImage:[UIImage imageNamed:@"deletbutton.png"] forState:UIControlStateNormal];
    deleteButton.backgroundColor = [UIColor clearColor];
    
    [deleteButton addTarget:self action:@selector(removeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[deleteButton setHidden:YES];
    deleteButton.tag=buttonNormal.tag ;
    [buttonNormal addSubview:deleteButton];
    
    [self.delteItemArray addObject:deleteButton];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonLongPressed:)];
    [buttonNormal addGestureRecognizer:longGesture];
    //[deleteButton addGestureRecognizer:longGesture];
    
    [self.itemArray addObject:buttonNormal];
    
    //点击后切换添加按钮的位置
    
    CGRect tempRect=btnAddButtn.frame;
    [UIView animateWithDuration:0.2 animations:^{
        if (count==8) {
            CGFloat x=btnAddButtn.frame.origin.x+KbutHeigth+16;
            CGFloat y=btnAddButtn.frame.origin.y;
            
            btnAddButtn.frame=CGRectMake(x, y, kbutWith, KbutHeigth);
        }else
        {
            
            [btnAddButtn setFrame:buttonNormal.frame];
            
        }
        [buttonNormal setFrame:tempRect];
        
        [deleteButton setFrame:CGRectMake(0,0, w, h)];
        
    } completion:^(BOOL finished) {
        return ;
    }];
    
    return buttonNormal;
}


-(void)removeButtonClicked:(UIButton *)btn
{
    
    
    UIButton *btnToBeDelete=(UIButton*)[btn superview];
    int index=(int)[_itemArray indexOfObject:btnToBeDelete]-1;
    
    UIButton *item=btnToBeDelete;
    [_itemArray removeObject:btnToBeDelete];
    [UIView animateWithDuration:0.2 animations:^{
        
        if (_itemArray.count==1) {
            [btnAddButtn setFrame:[self setNumber:0]];
            
        }else{
            
            CGRect lastFrame=item.frame;
            CGRect curFrame;
            for (int i=(index+1) ; i<self.itemArray.count; i++) {
                UIButton *temp=[self.itemArray objectAtIndex:i];
                curFrame=temp.frame;
                [temp setFrame:lastFrame];
                lastFrame=curFrame;
            }
            
            [btnAddButtn setFrame:lastFrame];
            
        }
        
    } completion:^(BOOL finished) {
        return ;
    }];
    
    [item removeFromSuperview];
    item=nil;
    
    
    
}

-(CGRect)setNumber:(NSInteger) number
{
    
    CGFloat y;
    CGFloat x;
    
    if (number>=4) {
        y=16+KbutHeigth;
        x=8+6*(number%4)+kbutWith*(number%4);
    }else
    {
        
        y=8;
        x=8+6*number+kbutWith*number;
    }
    
    CGRect rect=CGRectMake(x, y, kbutWith, KbutHeigth);
    
    
    
    return rect;
}



- (void)buttonLongPressed:(UILongPressGestureRecognizer *)sender
{
    
    UIButton *btn = (UIButton *)sender.view;
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        startPoint = [sender locationInView:sender.view];
        originPoint = btn.center;
        [UIView animateWithDuration:Duration animations:^{
            
            btn.transform = CGAffineTransformMakeScale(1.1, 1.1);
            btn.alpha = 0.7;
        }];
        
    }
    else if (sender.state == UIGestureRecognizerStateChanged)
    {
        
        CGPoint newPoint = [sender locationInView:sender.view];
        CGFloat deltaX = newPoint.x-startPoint.x;
        CGFloat deltaY = newPoint.y-startPoint.y;
        btn.center = CGPointMake(btn.center.x+deltaX,btn.center.y+deltaY);
        
        NSInteger index = [self indexOfPoint:btn.center withButton:btn];
        
        if (index<0)
        {
            contain = NO;
        }
        else
        {
            [UIView animateWithDuration:Duration animations:^{
                CGPoint temp = CGPointZero;
                UIButton *button = _itemArray[index];
                temp = button.center;
                button.center = originPoint;
                btn.center = temp;
                
                
                originPoint = btn.center;
                contain = YES;
                
                NSInteger indexBtn=[_itemArray indexOfObject:btn];
                UIButton *tempBtn=  _itemArray[index];
                
                _itemArray[index]=btn;
                _itemArray[indexBtn]=tempBtn;
                
                
            }];
        }
        
        
    }
    else if (sender.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:Duration animations:^{
            
            btn.transform = CGAffineTransformIdentity;
            btn.alpha = 1.0;
            if (!contain)
            {
                btn.center = originPoint;
            }
        }];
    }
}

- (NSInteger)indexOfPoint:(CGPoint)point withButton:(UIButton *)btn
{
    for (NSInteger i = 0;i<_itemArray.count;i++)
    {
        UIButton *button = _itemArray[i];
        if (button != btn&&button!=btnAddButtn)
        {
            if (CGRectContainsPoint(button.frame, point))
            {
                return i;
            }
        }
        
    }
    return -1;
}


/**-------------------------------上传图片-------------------------------------------***/
//上传照片第一步获取Token
-(void)getToken
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"image":@"",
                        @"image":@"",
                        @"fileName":@"",
                        @"type":@"",
                        @"op":@"getToken"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"appUploadPhotoRequest",
                         //@"value"  :[CommeHelper getEncryptWithString:dic]
                         @"value":[CommeHelper getJson:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        //NSLog(@"resultDic=================>%@",resultDic);
        NSDictionary *dict=[CommeHelper getDecryptWithString:[resultDic objectForKey:@"result"]];
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"获取Token失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        //[self alterMessge:description];
        
    }];
    
}

-(void)uplodaPhoto
{
    //开启后台线程上传图片
    //[NSThread detachNewThreadSelector:@selector(updataImageToSenve) toTarget:self withObject:nil];
    
    
}

//把照片上传到 7牛云端
-(void)updataImageToSenve
{
    
    //先获取Token（上传7牛云端需此参数） 此处要集成7牛云端 直接调用七牛的sdk方法循环的上传8张图片
    
    //每次图片修改或者排序改变需要把排序改变的顺序上传到友借 如果照片修改了需要上传到7牛并把修改的图片顺序重新上传到友借
}

-(void)savePhote
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"key":@"",//文件名称
                        @"op":@"save"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"uploadPhotoRequestHandler",
                         @"value"  :[CommeHelper getEncryptWithString:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        NSLog(@"resultDic=================>%@",resultDic);
        
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"上传图片失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        
    }];
}

@end
