//
//  SearchController.m
//  StarGroups
//
//  Created by zijin on 15/4/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "SearchController.h"
#import "ProjectListViewCell.h"
#import "CommeHelper.h"
#import "CommentsController.h"
#import "ProDetailsViewController.h"
@interface SearchController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ProjectListViewCellDelegate>
{
    UITextField *searchTextF;
    UIImageView *searchImageView;
    UITableView *tabeleData;
}
@property(nonatomic,strong)NSMutableArray *TempValueAryy;
@end
@implementation SearchController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"搜索"];
    [self.view setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    [self loadSearchView];
    _TempValueAryy=[NSMutableArray arrayWithCapacity:10];
    [self loadTableData:_searchText];
}


-(void)loadTableData:(NSString*)search
{
    
    
   [self showLoading:YES AndText:@"正在加载"];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    
    
    NSDictionary *dic = @{
                          @"userId" : username,
                          @"serchParam": search,
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"serchLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
   
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        for (NSDictionary *dict in aryy) {
            
            [_TempValueAryy addObject:dict];
        }
        
        [tabeleData reloadData];
        
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"点赞失败"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];

    
}


-(void)loadSearchView
{

    searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, Main_Screen_Width - 60, 30)];
    [searchImageView setUserInteractionEnabled:YES];
    [searchImageView setImage:[UIImage imageNamed:@"搜索好友页-搜索框"]];
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 16, 16)];
    [searchIcon setImage:[UIImage imageNamed:@"添加朋友页-搜索icon"]];
    searchTextF = [[UITextField alloc] initWithFrame:CGRectMake(32, 7, searchImageView.width - 32, 16)];
    [searchTextF setPlaceholder:@"搜索"];
    [searchTextF setTextColor:[UIColor colorWithHexString:@"#666666"]];
    [searchTextF setFont:[UIFont systemFontOfSize:13]];
    [searchTextF setBorderStyle:UITextBorderStyleNone];
    [searchTextF setTextAlignment:NSTextAlignmentLeft];
    [searchTextF setClearButtonMode:UITextFieldViewModeAlways];
    searchTextF.returnKeyType = UIReturnKeySearch;
    searchTextF.delegate = self;
    [self.view addSubview:searchImageView];
    [searchImageView addSubview:searchIcon];
    [searchImageView addSubview:searchTextF];
    
    
    UITableView *table=[[UITableView alloc]initWithFrame:CGRectMake(0, searchImageView.frame.size.height+20, Main_Screen_Width, Main_Screen_Height-110)];
    
    [table setDataSource:self];
    [table setDelegate:self];
    table.separatorStyle = UITableViewCellSelectionStyleNone;
    table.tableFooterView=[[UIView alloc] init];
    [table setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    tabeleData=table;
    [self.view addSubview:table];

}

#pragma mark-数据源
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    static NSString *dentifier=@"myCell";
    
    ProjectListViewCell *cell=(ProjectListViewCell*)[tableView dequeueReusableCellWithIdentifier:dentifier];
    
    if (cell==nil) {
        
        cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
        
    }
    if (_TempValueAryy.count==0) {
        [cell LodaView:nil];
    }else
    {
        [cell LodaView:_TempValueAryy[indexPath.row]];
    }
    cell.delegate = self;
    cell.backgroundColor= [self stringTOColor:@"#E6E6E6"];
    return cell;

}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _TempValueAryy.count;
}
#pragma mark-行高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 231;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //ProDetailsViewController *pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKey:@"id" ],@"1"];
    ProDetailsViewController *pro;
    
    if (![_TempValueAryy isEqual:[NSNull null]]) {
        
        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
        NSString *username = [loginInfo objectForKey:kSDKUsername];
        pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
    }else
    {
        
        pro=[[ProDetailsViewController alloc]initWithData:nil  withUserID:nil];
    }
    
    [self.navigationController pushViewController:pro animated:YES];
    
}


#pragma mark- search
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self loadTableData:textField.text==nil?@"":textField.text];
    return YES;
}


#pragma mark-cell代理
-(void)CommentsDidDone:(NSDictionary *)dictData
{
    
    CommentsController *comments=[[CommentsController alloc]init];
    
    comments.dataDict=dictData;
    [self.navigationController pushViewController:comments animated:YES];
    
}


-(void)GetPraise:(NSDictionary *)praise
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    
    
    NSDictionary *dic = @{
                          @"userId" : username,
                          @"loanId" : [praise objectForKeyedSubscript:@"id"],
                          @"op": [praise objectForKey:@"isPaise"],
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"praiseRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        if([[praise objectForKey:@"isPaise"] isEqual:@"cancel"])
        {
            [self showHint:@"取消点赞成功"];
            
        }else
        {
            [self showHint:@"点赞成功"];
            
        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"已经点赞过了"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
    
}




- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}
@end
