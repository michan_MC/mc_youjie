//
//  SearchController.h
//  StarGroups
//
//  Created by zijin on 15/4/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchController : BaseViewController
@property(strong,nonatomic)NSString *searchText;
@end
