//
//  Comment.m
//  StarGroups
//
//  Created by zijin on 15/4/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "Comment.h"
#import "UIColor+BFKit.h"
#import "UIImageView+EMWebCache.h"
static NSString *const url = @"http://120.25.218.167";
@implementation Comment

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //[self setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 150)];
    
    }
    return self;
}
-(void)LodaView:(NSDictionary*)dict
{
    
    //UIImageView *image=[UIImageView initWithImage:[UIImage imageNamed:@"@评价页面_头像"] frame:(0, 0, Main_Screen_Width, 150)];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(11, 17, 56, 56)];
    //[imageView setImage:[UIImage imageNamed:@"评价页面_头像"]];
    
    UIImage *imge=[UIImage imageNamed:@"评价页面_头像"];
    NSString *strUrl;
    if ([dict objectForKey:@"userPhoto"]!=nil) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"userPhoto"]]];
    }
    
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:imge];
    
    [self addSubview:imageView];
    UILabel *labelUserName=[[UILabel alloc]initWithFrame:CGRectMake(imageView.frame.size.width+imageView.frame.origin.x+13, imageView.frame.origin.y+25, 90, 30)];
    [labelUserName setFont:[UIFont systemFontOfSize:13]];
    [labelUserName setTextColor:[UIColor colorWithHexString:@"#666666"]];
    NSString *title;
    if ([dict objectForKey:@"nickname"]!=nil) {
        title=[NSString stringWithFormat:@"%@",[dict objectForKey:@"nickname"]];
    }else
    {
        title=@"大头菜";
    }
    labelUserName.text=@"";
    
    [labelUserName setText:title];

    [self addSubview:labelUserName];
    
    UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width-70-11, imageView.frame.origin.y+25, 90, 30)];
    [timeLabel setTextColor:[UIColor colorWithHexString:@"#666666"]];
    [timeLabel setFont:[UIFont systemFontOfSize:13]];
    NSString *createTime;
    if ([dict objectForKey:@"createTime"]!=nil) {
        createTime=[NSString stringWithFormat:@"%@",[dict objectForKey:@"createTime"]];
        createTime=[createTime substringWithRange:NSMakeRange(0,10)];
    }else
    {
        createTime=@"2015-15-18";
    }
    
    
    [timeLabel setText:createTime];
    UITextView *textView=[[UITextView  alloc ]initWithFrame:CGRectMake(11, timeLabel.frame.origin.y+27, [[UIScreen mainScreen] bounds].size.width-22, 90)];
    
    [textView setTextColor:[UIColor colorWithHexString:@"#333333"]];
    [textView setEditable:NO];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;// 字体的行间距
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:12],
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    
    NSString *body;
    if ([dict objectForKey:@"body"]!=nil) {
        body=[NSString stringWithFormat:@"%@",[dict objectForKey:@"body"]];
    }
    else
    {
        body=@"";
    }
    textView.attributedText= [[NSAttributedString alloc] initWithString:body attributes:attributes];
   // [textView setText:@"xsadasdasfadsffsdf丰富的是非得失丰富的是非的广东分公司的广告法国大使馆的法国电视d对方的是非得失负担收费"];
    [self addSubview:textView];
    
    [self addSubview:timeLabel];
    
    
}

@end
