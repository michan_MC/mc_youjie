//
//  RepaymentViewController.m
//  StarGroups
//
//  Created by zijin on 15/4/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "RepaymentViewController.h"
#import "CommeHelper.h"
#import "UIColor+BFKit.h"
/**还款计划*/
@interface RepaymentViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tabelView;
}
@property (nonatomic,strong) NSString *loanId;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSMutableArray *tempData;
@property(nonatomic,strong)NSString *userName;

@end

@implementation RepaymentViewController


-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID andUserName:(NSString *)userName
{
    
    
    self= [super init];
    
    if (self) {
        
        _loanId=loanId;
        _userID=userID;
        _userName=userName;
    }
    
    return self;
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _tempData=[[NSMutableArray alloc]init];
    self.title=@"还款计划";
    
    self.view.backgroundColor=[UIColor colorWithHexString:@"#E6E6E6"];
    [self addTopView];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)loadData{


    
    
    
    [self showLoading:YES AndText:@"正在加载"];
    //[_tempData removeAllObjects];
    NSDictionary *dic = @{
                          
//                          @"loanId" : _loanId,
//                          @"op":@"repayPlan",
//                          @"userId" :_userID
                          
                          @"loanId" : @"20150413000001",
                          @"op":@"repayPlan",
                          @"userId" :@"18927557274"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"repayMoneyRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
       
        for (NSDictionary *dict in aryy) {
            [_tempData addObject:dict];
        }
        [tabelView reloadData];
        
       
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];

    
}

-(void)setLable:(NSDictionary*)dict andView:(UITableViewCell*)cell andFontSize:(UIFont*)font
{
    
    
    CGFloat w=60;
    UILabel *lab1=[[UILabel alloc]initWithFrame:CGRectMake(7, 2, 60, 44)];
    
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    [lab1 setFont:font];
    [lab1 setTextColor:[self stringTOColor:@"#666666"]];
    [cell.contentView addSubview:lab1];
  
    
    if ([dict objectForKey:@"repayDay"]==nil) {
        [lab1 setText:@"2014-05-06"];
    }else
    {
        
    NSString *repayDay=[NSString stringWithFormat:@"%@",[dict objectForKey:@"repayDay"]];
    repayDay =[repayDay substringWithRange:NSMakeRange(0, 10)];
        
    [lab1 setText:repayDay];
    lab1.lineBreakMode=0;
    lab1.numberOfLines=0;
    }
    
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab1.frame.origin.x+w+5, 2, w, 44)];
   
    [lab2 setFont:font];
    [lab2 setTextColor:[self stringTOColor:@"#666666"]];
    [lab2 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab2];
    
    if ([dict objectForKey:@"repayType"]==nil) {
        [lab2 setText:@"付息"];
    }else
    {
        [lab2 setText:[dict objectForKey:@"repayType"]];
        lab2.lineBreakMode=0;
        lab2.numberOfLines=0;
    }

    
    UILabel *lab3=[[UILabel alloc]initWithFrame:CGRectMake(lab2.frame.origin.x+w+5, 2, w, 44)];
    
    [lab3 setFont:font];
    [lab3 setTextColor:[self stringTOColor:@"#01903A"]];
    [lab3 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab3];
    
    if ([dict objectForKey:@"interest"]==[NSNull null]) {
        [lab3 setText:@"50000"];
    }else
    {
        NSString *interest=[NSString stringWithFormat:@"%f",[[dict objectForKey:@"interest"] doubleValue ]];
        interest =[interest substringWithRange:NSMakeRange(0, 5)];
         [lab3 setText:interest];
    }
    
//
//
    UILabel *lab4=[[UILabel alloc]initWithFrame:CGRectMake(lab3.frame.origin.x+w, 2, w, 44)];
    
    [lab4 setFont:font];
    [lab4 setTextColor:[self stringTOColor:@"#FF0A44"]];
    [lab4 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab4];
    
    if ([dict objectForKey:@"time"]==[NSNull null]) {
        [lab4 setText:@"未还款"];
    }else
    {
        NSString *time=[NSString stringWithFormat:@"%@",[dict objectForKey:@"time"]];
        time =[time substringWithRange:NSMakeRange(0, 10)];
        [lab4 setText:time];
    }
     UILabel *lab5=[[UILabel alloc]initWithFrame:CGRectMake(lab4.frame.origin.x+w, 2, w, 44)];
    
    [lab5 setFont:font];
    [lab5 setTextColor:[self stringTOColor:@"#666666"]];
    [lab5 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab5];
    
//    if ([dict objectForKey:@"guarantyType"]==[NSNull null]) {
//        [lab5 setText:@"无法发"];
//    }else
//    {
        [lab5 setText:_userName];
//    }
    
    
    
}

-(void)addTopView
{
    
    UIView  *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,  [[UIScreen mainScreen] bounds].size.width, 44)];

    
    UIFont *font= [UIFont systemFontOfSize:12];
    
    CGFloat w=60;
    UILabel *lab1=[[UILabel alloc]initWithFrame:CGRectMake(7, 2, 60, 44)];
    
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    [lab1 setFont:font];
    [lab1 setTextColor:[self stringTOColor:@"#666666"]];
    
    [lab1 setText:@"本期起息日"];
 
    [topView addSubview:lab1];
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab1.frame.origin.x+w, 2, w, 44)];
    
    [lab2 setFont:font];
    [lab2 setTextColor:[self stringTOColor:@"#666666"]];
    [lab2 setTextAlignment:NSTextAlignmentCenter];
  
    
    
    [lab2 setText:@"付款类型"];
    [topView addSubview:lab2];
    
    
    UILabel *lab3=[[UILabel alloc]initWithFrame:CGRectMake(lab2.frame.origin.x+w, 2, w, 44)];
    
    [lab3 setFont:font];
    [lab3 setTextColor:[self stringTOColor:@"#666666"]];
    [lab3 setTextAlignment:NSTextAlignmentCenter];
    
   
    [lab3 setText:@"金额"];
    [topView addSubview:lab3];
    
    
    
    UILabel *lab4=[[UILabel alloc]initWithFrame:CGRectMake(lab3.frame.origin.x+w, 2, w, 44)];
    
    [lab4 setFont:font];
    [lab4 setTextColor:[self stringTOColor:@"#666666"]];
    [lab4 setTextAlignment:NSTextAlignmentCenter];
   
    
    
    [lab4 setText:@"还款日期"];
    [topView addSubview:lab4];
 
    UILabel *lab5=[[UILabel alloc]initWithFrame:CGRectMake(lab4.frame.origin.x+w, 2, w, 44)];
    
    [lab5 setFont:font];
    [lab5 setTextColor:[self stringTOColor:@"#666666"]];
    [lab5 setTextAlignment:NSTextAlignmentCenter];


    [lab5 setText:@"还款人"];
    [topView addSubview:lab5];
    [topView setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    [self.view addSubview:topView];
    
    
    
    
    UITableView *tabel=[[UITableView alloc]initWithFrame:CGRectMake(0, topView.frame.size.height, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height-topView.frame.size.height-10)];
    [tabel setDataSource:self];
    [tabel setDelegate:self];
    tabel.tableFooterView = [[UIView alloc] init];
    tabelView=tabel ;
    [self.view addSubview:tabel];
}

#pragma mark-数据源方法
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

     static NSString *dentifier=@"myRepaymentCell";

    UITableViewCell *cell;
  
    
    cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    if (cell==nil) {
      
      cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
    }
    

        cell.selectionStyle=UITableViewScrollPositionNone;
    
    [self setLable:_tempData[[indexPath row]] andView:cell andFontSize:[UIFont systemFontOfSize:12]];
    
    

    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tempData.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 44;
}

- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}



@end
