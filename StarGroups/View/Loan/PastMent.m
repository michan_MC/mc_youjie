//
//  PastMent.m
//  StarGroups
//
//  Created by zijin on 15/4/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "PastMent.h"

#import "CommeHelper.h"
@interface PastMent ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tabelView;
}
@property(nonatomic,strong)NSMutableArray *tempData;
@end

@implementation PastMent

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"以往还款记录";
    _tempData=[NSMutableArray arrayWithCapacity:20];
    [self addTopView];
    [self loadData:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)loadData:(NSInteger)curPage
{
    
    [self showLoading:YES AndText:@"正在加载"];
    //[_tempData removeAllObjects];
    NSDictionary *dic = @{

                          @"userId" : _userID,
                          @"op":@"all",
                          @"curPage" :[NSString stringWithFormat:@"%ld",curPage],
                          @"size" :@"20"

                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        
       NSArray *aryy= [[CommeHelper getDecryptWithString:resultDic[@"result"]]objectForKey:@"data"];
        
        for (NSDictionary *dict in aryy) {
            [_tempData addObject:dict];
        }
        [tabelView reloadData];
        
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    
    
}

-(void)addTopView
{
    
    UIView  *topView=[[UIView alloc]initWithFrame:CGRectMake(0, 0,  [[UIScreen mainScreen] bounds].size.width, 44)];
    
    
    UIFont *font= [UIFont systemFontOfSize:12];
    
    CGFloat w=60;
    UILabel *lab1=[[UILabel alloc]initWithFrame:CGRectMake(7, 2, 60, 44)];
    
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    [lab1 setFont:font];
    [lab1 setTextColor:[self stringTOColor:@"#666666"]];
    
    [lab1 setText:@"标题"];
    
    [topView addSubview:lab1];
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab1.frame.origin.x+w, 2, w, 44)];
    
    [lab2 setFont:font];
    [lab2 setTextColor:[self stringTOColor:@"#666666"]];
    [lab2 setTextAlignment:NSTextAlignmentCenter];
    
    
    
    [lab2 setText:@"年利率"];
    [topView addSubview:lab2];
    
    
    UILabel *lab3=[[UILabel alloc]initWithFrame:CGRectMake(lab2.frame.origin.x+w, 2, w, 44)];
    
    [lab3 setFont:font];
    [lab3 setTextColor:[self stringTOColor:@"#666666"]];
    [lab3 setTextAlignment:NSTextAlignmentCenter];
    
    
    [lab3 setText:@"借款金额"];
    [topView addSubview:lab3];
    
    
    
    UILabel *lab4=[[UILabel alloc]initWithFrame:CGRectMake(lab3.frame.origin.x+w, 2, w, 44)];
    
    [lab4 setFont:font];
    [lab4 setTextColor:[self stringTOColor:@"#666666"]];
    [lab4 setTextAlignment:NSTextAlignmentCenter];
    
    
    
    [lab4 setText:@"期限"];
    [topView addSubview:lab4];
    
    UILabel *lab5=[[UILabel alloc]initWithFrame:CGRectMake(lab4.frame.origin.x+w, 2, w, 44)];
    
    [lab5 setFont:font];
    [lab5 setTextColor:[self stringTOColor:@"#666666"]];
    [lab5 setTextAlignment:NSTextAlignmentCenter];
    
    
    [lab5 setText:@"状态"];
    [topView addSubview:lab5];
    [topView setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    [self.view addSubview:topView];
    
    
    
    
    UITableView *tabel=[[UITableView alloc]initWithFrame:CGRectMake(0, topView.frame.size.height, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height-topView.frame.size.height-10)];
    [tabel setDataSource:self];
    [tabel setDelegate:self];
    tabel.tableFooterView = [[UIView alloc] init];
    tabelView=tabel ;
    [self.view addSubview:tabel];
}

#pragma mark-数据源方法
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myRepaymentCell";
    
    UITableViewCell *cell;
    
    
    cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
    }
    
    
    cell.selectionStyle=UITableViewScrollPositionNone;
    
    [self setLable:_tempData[[indexPath row]] andView:cell andFontSize:[UIFont systemFontOfSize:12]];
    
    
    
    return cell;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tempData.count;
}

-(void)setLable:(NSDictionary*)dict andView:(UITableViewCell*)cell andFontSize:(UIFont*)font
{
    
    
    CGFloat w=60;
    UILabel *lab1=[[UILabel alloc]initWithFrame:CGRectMake(7, 2, 60, 44)];
    
    [lab1 setTextAlignment:NSTextAlignmentCenter];
    [lab1 setFont:font];
    [lab1 setTextColor:[self stringTOColor:@"#666666"]];
    [cell.contentView addSubview:lab1];
    
    
    if ([dict objectForKey:@"name"]==nil) {
        [lab1 setText:@""];
    }else
    {
        
//        NSString *repayDay=[NSString stringWithFormat:@"%@",[dict objectForKey:@"repayDay"]];
//        repayDay =[repayDay substringWithRange:NSMakeRange(0, 10)];
        
        [lab1 setText:[[dict objectForKey:@"name"] stringValue]];
        lab1.lineBreakMode=0;
        lab1.numberOfLines=0;
    }
    
    
    UILabel *lab2=[[UILabel alloc]initWithFrame:CGRectMake(lab1.frame.origin.x+w+5, 2, w, 44)];
    
    [lab2 setFont:font];
    [lab2 setTextColor:[self stringTOColor:@"#666666"]];
    [lab2 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab2];
    
    if ([dict objectForKey:@"rate"]==nil) {
        [lab2 setText:@""];
    }else
    {
        
        double rate=[[dict objectForKey:@"rate"] doubleValue ];
        rate=rate*100;
        NSString *strRate=[NSString stringWithFormat:@"%f",rate];
         strRate =[[strRate substringWithRange:NSMakeRange(0, 4)] stringByAppendingString:@"%"];
     
        [lab2 setText:strRate];
//        lab2.lineBreakMode=0;
//        lab2.numberOfLines=0;
    }
    
    
    UILabel *lab3=[[UILabel alloc]initWithFrame:CGRectMake(lab2.frame.origin.x+w+5, 2, w, 44)];
    
    [lab3 setFont:font];
    [lab3 setTextColor:[self stringTOColor:@"#01903A"]];
    [lab3 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab3];
    
    if ([dict objectForKey:@"money"]==[NSNull null]) {
        [lab3 setText:@""];
    }else
    {
         NSString *interest=[NSString stringWithFormat:@"%f",[[dict objectForKey:@"money"] doubleValue ]];
        interest =[interest substringWithRange:NSMakeRange(0, 8)];
        lab3.lineBreakMode=0;
        lab3.numberOfLines=0;
        [lab3 setText:interest];
    }
    
    
    UILabel *lab4=[[UILabel alloc]initWithFrame:CGRectMake(lab3.frame.origin.x+w, 2, w, 44)];
    
    [lab4 setFont:font];
    [lab4 setTextColor:[self stringTOColor:@"#FF0A44"]];
    [lab4 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab4];
    
    if ([dict objectForKey:@"deadline"]==[NSNull null]) {
        [lab4 setText:@""];
    }else
    {
        NSString *time=[NSString stringWithFormat:@"%ld",[[dict objectForKey:@"deadline"] integerValue]];
//        time =[time substringWithRange:NSMakeRange(0, 10)];
        [lab4 setText:time];
    }
    UILabel *lab5=[[UILabel alloc]initWithFrame:CGRectMake(lab4.frame.origin.x+w, 2, w, 44)];
    
    [lab5 setFont:font];
    [lab5 setTextColor:[self stringTOColor:@"#666666"]];
    [lab5 setTextAlignment:NSTextAlignmentCenter];
    [cell.contentView addSubview:lab5];
    
    if ([dict objectForKey:@"status"]==[NSNull null]) {
        [lab4 setText:@""];
    }else
    {
        NSString *status=[NSString stringWithFormat:@"%@",[[dict objectForKey:@"status"] stringValue]];
        //        time =[time substringWithRange:NSMakeRange(0, 10)];
        [lab5 setText:status];
    }
   

    
    
    
}
- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}

@end
