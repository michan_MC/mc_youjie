//
//  CommentsControllerViewController.h
//  StarGroups
//
//  Created by zijin on 15/4/16.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface CommentsController : BaseViewController
@property(strong,nonatomic)NSDictionary *dataDict;
@end
