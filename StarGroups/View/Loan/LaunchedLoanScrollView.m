//
//  LaunchedLoanScrollView.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LaunchedLoanScrollView.h"

@interface LaunchedLoanScrollView ()
{
    UIButton *button1;
    UIButton *button2;
    UIButton *button3;
}

@property (nonatomic, assign) CGFloat photoWidth;
@end
@implementation LaunchedLoanScrollView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addPhotoView];
        
// [self addUserInfomation];
        
    }
    return self;
}

- (CGFloat)photoWidth
{
    return (Main_Screen_Width - 20 - 30 - 30 - 20)/3;
}

- (void)addPhotoView
{
    button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setFrame:CGRectMake(20, 15, self.photoWidth, self.photoWidth)];
    [button1 setBackgroundImage:[UIImage imageNamed:@"发起借款页-图片默认图"] forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(clickButton1) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button1];

    
    button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setFrame:CGRectMake(button1.width + 20 + 30, 15, self.photoWidth, self.photoWidth)];
    [button2 setBackgroundImage:[UIImage imageNamed:@"发起借款页-图片默认图"] forState:UIControlStateNormal];
    [button2 addTarget:self action:@selector(clickButton2) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button2];
    
    button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button3 setFrame:CGRectMake(20 + button1.width + 30 + button1.width + 30, 15, self.photoWidth, self.photoWidth)];
    [button3 setBackgroundImage:[UIImage imageNamed:@"发起借款页-图片默认图"] forState:UIControlStateNormal];
    [button3 addTarget:self action:@selector(clickButton3) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button3];
}

- (void)clickButton1
{
    
}

- (void)clickButton2
{
    
}

- (void)clickButton3
{
    
}

//- (void)addUserInfomation
//{
//    UIImageView *imageV = [UIImageView alloc] initWithFrame:CGRectMake(7, 15 + button1.width + , <#CGFloat width#>, <#CGFloat height#>)
//}
@end
