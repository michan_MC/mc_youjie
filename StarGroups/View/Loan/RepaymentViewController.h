//
//  RepaymentViewController.h
//  StarGroups
//
//  Created by zijin on 15/4/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface RepaymentViewController : BaseViewController
-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID andUserName:(NSString *)userName;
@end
