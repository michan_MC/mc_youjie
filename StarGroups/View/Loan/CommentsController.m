//
//  CommentsControllerViewController.m
//  StarGroups
//
//  Created by zijin on 15/4/16.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "CommentsController.h"
#import "Comment.h"
#import "CommeHelper.h"
@interface CommentsController ()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tabeView;
    UITextField *commentsText;
}
@property(nonatomic,strong)NSMutableArray *TempValueAryy;
@end

@implementation CommentsController

- (void)viewDidLoad {
    [super viewDidLoad];
   

    [self addDottom];
    
  
 
    self.title=@"评论";
    _TempValueAryy=[NSMutableArray arrayWithCapacity:10];
    [self lodaDataAndSendComment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

-(void)addDottom
{

    UIView *bottomView=[[UIView alloc]initWithFrame:CGRectMake(0,Main_Screen_Height-150, Main_Screen_Width, 130)];

    UIImageView *imageView=[[UIImageView alloc]initWithFrame:bottomView.bounds];
    [imageView setImage:[UIImage imageNamed:@"评价页面_-输入框"]];

    [bottomView addSubview:imageView];
    
    UIImageView *userImage=[[UIImageView alloc]initWithFrame:CGRectMake(bottomView.bounds.origin.x+8, bottomView.bounds.origin.y+8+30, 44, 44)];
    [userImage setImage:[UIImage imageNamed:@"评价页面_头像"]];
    
    UITextField *textField=[[UITextField alloc]initWithFrame:CGRectMake(userImage.frame.origin.x+15+userImage.frame.size.width, userImage.frame.origin.y, 200, 44)];
    [textField setText:@"在这说点什么吧 "];
    
    [textField setTextColor:[UIColor colorWithHexString:@"#cccccc"]];
    [textField setDelegate:self];
    commentsText =textField;

    
    UIButton *btnSendPinLun=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnSendPinLun setFrame:CGRectMake(Main_Screen_Width-40, textField.frame.origin.y+5, 20, 20)];
  
    [btnSendPinLun setImage: [UIImage imageNamed:@"评价页面_评论icon"] forState:UIControlStateNormal];
    [btnSendPinLun addTarget:self action:@selector(sendComment) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btnSendPinLun];
    
    [bottomView addSubview:textField];
    [bottomView addSubview:userImage];
    
    UITableView *table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height-bottomView.frame.size.height)];
  
    [table setDataSource:self];
    [table setDelegate:self];
    [table setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:table];
    tabeView=table;

    [self.view addSubview:bottomView];
}

#pragma mark-TextFile
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

    [textField setText:@""];
    return YES;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // UITableViewCell *cell=[[UITableViewCell  alloc]init];
    
    static  NSString *dentifer=@"MyCellDif";
    Comment *cell=(Comment*)[tableView dequeueReusableCellWithIdentifier:dentifer];
    
    if (cell==nil) {
        cell=[[Comment alloc]init];
    }
    
    [cell LodaView:_TempValueAryy[[indexPath row]]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _TempValueAryy.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 150;
}


-(void)lodaDataAndSendComment
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
  
    NSString *descriptions;

    
 
    [self showLoading:YES AndText:@"正在加载"];
    NSDictionary *dic = @{
            
            @"userId" : username,
            @"content" : @"all",
            @"ip": username,
            @"loanId" :[_dataDict objectForKey:@"id"] ,
            @"index" : @"1",
            @"length":@"10",
            @"op":@"watchComment"
            };
    
    descriptions=@"数据为空";

    
    NSDictionary *dict = @{
                           @"method" : @"commentRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
       
        
        [self showLoading:NO AndText:@""];
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        [_TempValueAryy removeAllObjects];
        for (NSDictionary *dict in aryy) {
            
            
           
            [_TempValueAryy addObject:dict];
        }
        
        [tabeView reloadData];
        
        //[_allListTable reloadData];
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:descriptions];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    
}

-(void)sendComment
{
    
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSString *descriptions;
  
    
    
   // [self showLoading:YES AndText:@"正在发送"];
    NSDictionary *dic = @{
                          
                          @"userId" : username,
                          @"content" : commentsText.text==nil?@"  ":commentsText.text,
                          @"ip": username,
                          @"loanId" :[_dataDict objectForKey:@"id"] ,
                          @"index" : @"1",
                          @"length":@"10",
                          @"op":@"saveComment"
                          };
    
    descriptions=@"数据为空";
    
    
    NSDictionary *dict = @{
                           @"method" : @"commentRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
       // NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"resultMsg"];
        
       // [self showAllTextDialog:resultDic[@"resultMsg"]];
        
        [self showAllTextDialog:@"发表评论成功"];
        [commentsText setText:@""];
    
        [self lodaDataAndSendComment];
        //[_allListTable reloadData];
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:descriptions];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    

    
}


@end
