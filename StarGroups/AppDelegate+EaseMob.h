//
//  AppDelegate+EaseMob.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/24.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "AppDelegate.h"
#import "EaseMob.h"


@interface AppDelegate (EaseMob)


- (void)easemobApplication:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions ;
@end
