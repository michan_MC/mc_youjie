//
//  ViewController.m
//  StarGroups
//
//  Created by fenguo on 15-1-19.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "MainTableViewController.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initLanuchView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initLanuchView
{
   // 没有注销且seesion没过期才可以直接进入主界面
    NSNumber *logout = [[NSUserDefaults standardUserDefaults] objectForKey:@"isLogOut"];
    
    if ([MyTools getTheSeesionId] && logout && (logout.integerValue == 0)) {
        
        MainTableViewController *main = [[MainTableViewController alloc] init];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = main;
        
    }else{
        UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *loginVC = [storeBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = loginVC;
    }
}
@end
