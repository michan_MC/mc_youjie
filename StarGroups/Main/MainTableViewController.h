//
//  MainTableViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/12.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMSDKFull.h"

@interface MainTableViewController : UITabBarController
{
    EMConnectionState _connectionState;
}

- (void)jumpToChatList;

- (void)setupUntreatedApplyCount;

- (void)networkChanged:(EMConnectionState)connectionState;
@end
