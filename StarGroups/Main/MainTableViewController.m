//
//  MainTableViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/12.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MainTableViewController.h"
#import "LoanTableViewController.h"
#import "ChatTableViewController.h"
#import "ContactViewController.h"
#import "MeViewController.h"
#import "NavViewController.h"
#import "ApplyViewController.h"
#import "ContactViewController.h"
#import "ChatTableViewController.h"
#import "ContactsViewController.h"

//#import "MyFriendTableView.h"

@interface MainTableViewController () <IChatManagerDelegate>
{
    ContactViewController *_contactsVC;
    ChatTableViewController *_chatListVC;
    
    ContactsViewController *_contasctsVC;
    
}
@end

@implementation MainTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupViews];
    
    
    
// 获取未读消息数，此时并没有把self注册为SDK的delegate，读取出的未读数是上次退出程序的
    [self didUnreadMessagesCountChanged];
    
//    把self注册为sdk的delegate
    [self registerNotifications];
    
    
}

- (void)setupViews
{
    LoanTableViewController *loan = [[LoanTableViewController alloc]init];
    [self setUpChildController:loan title:@"友借" imageName:@"首页" selectedImageName:@"首页-选中状态"];
    
//    _contasctsVC = [[ContactsViewController alloc]init];
    
    ChatTableViewController *chatList= [[ChatTableViewController alloc] init];
//    [_chatListVC networkChanged:_connectionState];
    [self setUpChildController:chatList title:@"聊天" imageName:@"聊天" selectedImageName:@"聊天-选中状态"];
    
    ContactViewController *catact = [[ContactViewController alloc]init];
    [self setUpChildController:catact title:@"联系人" imageName:@"联系人" selectedImageName:@"联系人-选中状态"];

    
    
    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MeViewController *me = [storeBoard instantiateViewControllerWithIdentifier:@"MeViewController"];
    [self setUpChildController:me title:@"我" imageName:@"我的" selectedImageName:@"我的-选中状态"];
    
}

- (void)setUpChildController:(UIViewController *)controller title:(NSString *)title imageName:(NSString *)image selectedImageName:(NSString *)selectedImage
{
    controller.title = title;
    // 底部字体颜色
    self.tabBar.tintColor = RGBCOLOR(68, 68, 68);
    //默认的图片
    [controller.tabBarItem setImage:[UIImage imageNamed:image]];
    
    //选择时的图片,将其渲染方式设置为原始的颜色
    [controller.tabBarItem setSelectedImage:[[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    NavViewController *nav = [[NavViewController alloc]initWithRootViewController:controller];
    [self addChildViewController:nav];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

# pragma mark - 环信
// 未读消息数量变化回调
- (void)didUnreadMessagesCountChanged
{
    [self setupUntreatedApplyCount];
}

- (void)setupUntreatedApplyCount
{
    NSInteger unreadCount = [[[ApplyViewController shareController] dataSources] count];
    if (_contasctsVC) {
        if (unreadCount > 0) {
            //_contasctsVC.tabBarItem.badgeValue = [NSString stringWithFormat:@"%d",unreadCount];
        }else{
            //_contasctsVC.tabBarItem.badgeValue = nil;
        }
    }
}

// 把self注册为sdk的delegate
- (void)registerNotifications
{
    [self unregisterNotifications];
    
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    [[EMSDKFull sharedInstance].callManager removeDelegate:self];

}

- (void)unregisterNotifications
{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[EMSDKFull sharedInstance].callManager removeDelegate:self];
}


- (void)networkChanged:(EMConnectionState)connectionState
{
    _connectionState = connectionState;
    [_chatListVC networkChanged:connectionState];
}



@end
