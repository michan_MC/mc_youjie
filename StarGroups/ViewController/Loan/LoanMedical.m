//
//  LoanMedical.m
//  StarGroups
//
//  Created by zijin on 15/3/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LoanMedical.h"
#import "loanCell.h"
#import "CommeHelper.h"
#import "EaseMob.h"
#import "commonDefs.h"

static NSString *MyCell=@"cells";
@interface LoanMedical()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    UITableView *tableView;
    UIScrollView *scrollView;
    NSArray *_aryy;
    
    
}

@property(strong,nonatomic)NSMutableDictionary *dictLsit;//所有要提交的数据
@property(strong ,nonatomic)NSDictionary  *arryUserList;


@end


@implementation LoanMedical

-(void)viewDidLoad{
    [super viewDidLoad];
    
    
    
    self.title=@"发起借款";
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height*1.3)];
    scrollView.backgroundColor= [self stringTOColor:@"#E6E6E6"];
    [scrollView  setShowsVerticalScrollIndicator :NO];
    [scrollView setBounces:NO];
    [scrollView setUserInteractionEnabled:YES];
    [self.view addSubview:scrollView];
    
    [self setData];
 
  
    [self addTabeleView];
  
    
    UINib *nib = [UINib nibWithNibName:@"loanCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:MyCell];
     [self loadData];//加载个人信息
    
}

-(void)setData
{
    //初始化数据
    // NSMutableArray *mutableAryy=[[NSMutableArray alloc]initWithCapacity:8];
    NSArray *arry= [[NSArray alloc]initWithObjects:
                    @"借款人姓名"
                    ,@"借款人电话"
                    ,nil];
    
    NSArray *arry1= [[NSArray alloc]initWithObjects:
                     @"借款标题",
                     @"意向借款金额",
                     @"预期借款期限",
                     @"抵押物种类"
                     ,@"实际经营地址"
                     ,@"借款用途"
                     ,@"借款描述"
                     ,nil];
    
    NSMutableArray *arrys=[[NSMutableArray  alloc]initWithCapacity:2];
    [arrys addObject:arry];
    [arrys addObject:arry1];
    _aryy= arrys;
    
}

-(void)addTabeleView
{
    
    CGFloat h=9*44+8+150;
    
    UITableView *tabel=[[UITableView alloc]initWithFrame:CGRectMake(7, 16, 305, h)];
    [tabel setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    [tabel setDataSource:self];
    [tabel setDelegate:self];
    tabel.scrollEnabled=NO;
    tableView=tabel;
    [scrollView addSubview:tabel];
    
    UIButton *btnSubmit=[UIButton  buttonWithType:UIButtonTypeCustom];
    [btnSubmit setFrame: CGRectMake(tabel.frame.origin.x+10, tabel.frame.origin.y+tabel.frame.size.height, 282, 44)];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    [btnSubmit setTitle:@"发起借款" forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(subMit) forControlEvents:UIControlEventTouchUpInside];
    scrollView.userInteractionEnabled=YES;
    [btnSubmit addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btnSubmit];
    
}

-(void)loadData
{
    
    [self showLoading:YES AndText:@"正在加载"];
    
    _arryUserList=[[NSMutableDictionary alloc]init];
    NSDictionary *dict1=@{@"userId":@15813351726};
    NSDictionary *dict2=@{
                          @"method" : @"userInfoRequestHandler",
                          //@"userId":@15813351726
                          //@"value"  :[CommeHelper getEncryptWithString:dict1]
                          @"value"  : @"Au9DZrxGKqtTLL5Dr6qOfP31lTq09GmpECuM4UucXTY="
                          };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSLog(@"resultDic=========>%@",resultDic);
        _arryUserList=[CommeHelper getDecryptWithString:resultDic[@"result"]];
       [tableView reloadData];
        
        
        [self showLoading:NO AndText:nil];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        //  DLog(@"error==%@",error);
       // NSLog(@"description ==%@", description);
    }];
    
    
    
}

#pragma mark-textView代理
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    
    textView.text=@"";
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    
    if ([textView.text length]==0) {
        [self alterMessge:@"借款描述不能为空"];
    }
    [_dictLsit  setObject:textView.text forKey:@"description"];
}

-(void)alterMessge:(NSString*)message
{
    
    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
    
    [alter show];
}
-(void)subMit
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    [_dictLsit setObject:username forKey:@"userId"];
    if ([_dictLsit count]<4) {
        
        if (![[_dictLsit objectForKey:@"title"] isEqualToString:@""]) {
            
            [self alterMessge:@"标题不能为空"];
            return;
        }
        
        if (![[_dictLsit objectForKey:@"money"] isEqualToString:@""]) {
            
            [self alterMessge:@"借款金额不能为空"];
            return;
        }
        
        if (![[_dictLsit objectForKey:@"deadline"] isEqualToString:@""]) {
            
            [self alterMessge:@"意向借款期限不能为空"];
            return;
        }
        if ([[_dictLsit objectForKey:@"description"] isEqualToString:@""]) {
            
            [self alterMessge:@"借款描述"];
            return;
        }
        
        //            if ([[_dictLsit objectForKey:@"rate"] isEqual:@""]) {
        //
        //                 [self alterMessge:@"利率不能为空"];
        //                return;
        //            }
        //
        
        //        if ([[_dictLsit objectForKey:@"repayType"] isEqual:@""]) {
        //
        //           [self alterMessge:@"还款方式"];
        //            return;
        //        }
        // [self alterMessge:@"请仔细填写借款的类容"];
        return;
    }
    
    
    NSDictionary *dict=@{
                         // loanRpbReleaseRequest
                         @"method" : @"userInfoRequestHandler",
                         @"value"  :[CommeHelper getEncryptWithString:_dictLsit]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        NSLog(@"resultDic=========>%@",resultDic);
        if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
        {
            [self alterMessge:@"提交借款成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        [self alterMessge:description];
        
    }];
    
    
    
    
    
}

#pragma mark-数据源
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section=[indexPath section];
    NSInteger row=[indexPath row];
    
    
    
    UITableViewCell *tabelCell;
    if (section==1&&row==6) {
        tabelCell=[[UITableViewCell alloc]init];
        UITextView *text=[[UITextView alloc]initWithFrame:CGRectMake(0, 0, 305, 150)];
        text.text=_aryy[section][row];
        [text setDelegate:self];
        [tabelCell addSubview:text];
    }else
    {
        loanCell *loan=[tableView dequeueReusableCellWithIdentifier:MyCell];
        if (loan==nil) {
            loan =[[loanCell alloc]init];
        }
        loan.selectionStyle=UITableViewCellSelectionStyleNone;
        
        if (section==0) {
         
            
            if ([indexPath row]==0)
            {
                [loan loadCellView:_aryy[section][row] withTextVlue:[_arryUserList objectForKey:@"realname"]];
                
            }else if([indexPath row]==1)
            {
                [loan loadCellView:_aryy[section][row] withTextVlue:[_arryUserList objectForKey:@"mobileNumber"]];
                
            }
               loan.TextValue.enabled=NO;
        }else
        {
            
            [loan loadCellViewWithArry:_aryy[section][row]];
        }
        
        loan.TextValue.delegate=self;
        loan.tag=[indexPath row];


        tabelCell=loan;
    }
    
    
    
    return tabelCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0;
    }
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (1==[indexPath section]&&[indexPath row]==6) {
        return 150;
    }
    return 44;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(section==0)
    {
        return 2;
    }
    return 7;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}
@end
