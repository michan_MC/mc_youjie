//
//  SubMitLoan.m
//  StarGroups
//
//  Created by zijin on 15/4/30.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "SubMitLoan.h"
#import "CommeHelper.h"
@interface SubMitLoan ()
{
    UILabel *labeMoney;
    UITextField *fieldInvest;
}

@property (nonatomic,strong) NSString *loanId;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSString *investMoney;
@end

@implementation SubMitLoan

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title=@"项目详情";
     UITapGestureRecognizer *recongnizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap)];
    [self.view addGestureRecognizer:recongnizer];
    [self addInvestmentView];
    [self.view setBackgroundColor:[UIColor colorWithHexString: @"#E6E6E6"]];
    [self loadBalcance];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID andInvestMoney:(NSString*)investMoney
{
    self= [super init];
    
    if (self) {
        
        _loanId=loanId;
        _userID=userID;
        _investMoney=investMoney;
    }
    
    return self;
}

-(void)addInvestmentView
{

    UIView *viewTop=[[UIView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 100)];
    [viewTop setBackgroundColor:[UIColor whiteColor]];
    
    UILabel *labe=[[UILabel alloc]initWithFrame:CGRectMake(7, 7, 200, 60) ];
    [labe setText:@"当前余额（元）"];
    [labe setFont:[UIFont systemFontOfSize:12]];
    labeMoney  =[[UILabel alloc]initWithFrame:CGRectMake(7, 20, 200, 60) ];
    [labeMoney setText:@"￥500000"];
    [labeMoney setTextColor:[UIColor redColor]];
    [labeMoney setFont:[UIFont systemFontOfSize:12]];
    [viewTop addSubview:labeMoney];
    
    [viewTop addSubview:labe];
    
    UIView *invest=[[UIView alloc]initWithFrame:CGRectMake(7, viewTop.frame.size.height+7, Main_Screen_Width-14, 44) ];
    [invest setBackgroundColor:[UIColor whiteColor]];
    UILabel *lableinvest=[[UILabel alloc] initWithFrame:CGRectMake(7, 0, 50, 44)];
    [lableinvest setText:@"我要投资"];
    [lableinvest setFont:[UIFont systemFontOfSize:12]];
    UIView *middle = [[UIView alloc] initWithFrame:CGRectMake(lableinvest.frame.origin.x+lableinvest.frame.size.width+4, 13, 1, 19)];
    [middle setBackgroundColor:[UIColor colorWithHexString: @"#E6E6E6"]];
    fieldInvest=[[UITextField alloc] initWithFrame:CGRectMake(middle.frame.origin.x+middle.frame.size.width+10, 0, 200, 44)];
    [fieldInvest setText:_investMoney];
    [invest addSubview:lableinvest];
    [invest addSubview:middle];
    [invest addSubview:fieldInvest];
    [self.view addSubview:invest];
    
    UIButton *butSubMit=[UIButton buttonWithType:UIButtonTypeCustom];
    [butSubMit setFrame:CGRectMake((Main_Screen_Width/2)-100, invest.frame.origin.y+invest.frame.size.height+7, 200, 44)];
    [butSubMit setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    [butSubMit setTitle:@"投资" forState:UIControlStateNormal];
    [butSubMit addTarget:self action:@selector(subMit) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:butSubMit];
    [self.view addSubview:viewTop];
}
-(void)loadBalcance
{
    [self showLoading:@"正在加载" AndText:nil];
    NSDictionary *dic = @{
                          @"userId" : _userID
                      
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"centerHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];
        NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]];
        
        if ([dict objectForKey:@"balcance"]!=[NSNull null]) {
            
            labeMoney.text=[@"￥"  stringByAppendingString:[[dict objectForKey:@"balcance"] stringValue]];
        }
        

        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        
    }];
    

    
}

-(void)handleSingleTap
{
     [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}
-(void)subMit
{
    
  [self showLoading:YES AndText:@"正在加载"];
    
    if ([fieldInvest.text doubleValue]<50) {
        [self showHint:@"你投资的金额不是50的整倍"];
        [self showLoading:NO AndText:nil];
        return;
    }
    NSDictionary *dic = @{
                          @"userId" : _userID,
                          @"loanId" : _loanId,
                          @"investMoney" : fieldInvest.text,
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"pnrpayInvestRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];
        
        [self showHint:@"投资成功"];
        NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSLog(@"%@",dict);

        [self showLoading:NO AndText:nil];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        NSString  * str = [NSString stringWithFormat:@"%@",error];
        [self showHint:str];
        
    }];
    

    
}
@end
