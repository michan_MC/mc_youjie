//
//  CharacterLoanViewController.m
//  StarGroups
//
//  Created by zijin on 15/3/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

/*人品*/
#import "CharacterLoanViewController.h"
#import "loanCell.h"
#import "LoanCellDate.h"
#import "CommeHelper.h"
#import "NetworkManager.h"
#import "UIButton+EMWebCache.h"
#import "MF_Base64Additions.h"
#import "LoanTableViewController.h"

static NSString *MyCell=@"cells";
@interface CharacterLoanViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>
{
    UILabel *labelLifepPhotos;
    UITableView *tableView;
    UIScrollView *scrollView;
    NSArray *_aryy;
    BOOL isCheckBox;
    UIButton *tempBtn;
    UITextView *textDescrption;
    UIButton *clickCheckBox;
    
    UIButton *btnPositive;
    UIButton *btnBackID;
    UIButton *btnLifepPhotos;

}

@property (nonatomic,strong) IBOutlet UIView *topView;

//@property (strong, nonatomic) IBOutlet UILabel *PositiveID;//正面身份证号码
//@property (strong, nonatomic) IBOutlet UILabel *BackID;//背面身份证号码
//@property (strong, nonatomic) IBOutlet UILabel *LifepPhotos;//生活照

@property(strong,nonatomic)NSMutableDictionary *dictLsit;//所有要提交的数据
@property(strong ,nonatomic)NSDictionary  *arryUserList;

@end


@implementation CharacterLoanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isCheckBox = NO;
    
    
    scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height*1.7)];
        if(!isiPhone5){
        [scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height*1.7 + 30)];
        }

    scrollView.backgroundColor=[self stringTOColor:@"#E6E6E6"];
    
    //showsVerticalScrollIndicator
    [scrollView  setShowsVerticalScrollIndicator :NO];
    scrollView.userInteractionEnabled=YES;
    // NSLog(@"%@")
    [self.view addSubview:scrollView];
    self.title=@"发起借款";
    // self.view.backgroundColor=[self stringTOColor:@"#E6E6E6"];
    
    [self addTopView];
    
   
    [self addTableView];
    
    [self loadData];//加载个人信息
    //注册xib
    
    UINib *nib = [UINib nibWithNibName:@"loanCell" bundle:nil];
    [tableView registerNib:nib forCellReuseIdentifier:MyCell];
    
    [self setData];
    
    
    _dictLsit =[[NSMutableDictionary alloc]init];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)setData
{
    //初始化数据
    // NSMutableArray *mutableAryy=[[NSMutableArray alloc]initWithCapacity:8];
    NSArray *arry= [[NSArray alloc]initWithObjects:
                    @"借款人姓名"
                    ,@"借款人电话"
                    ,@"身份证号码"
                    
                    ,nil];
    
    
    NSArray *arry1= [[NSArray alloc]initWithObjects:
                     
                     @"借款标题"
                     ,@"借款金额"
                     ,@"借款期限"
                     ,@"年利率"
                     ,@"还款方式"
                     
                     ,nil];
    
    NSMutableArray *aryys=[[NSMutableArray alloc]initWithCapacity:2];
    [aryys addObject:arry];
    [aryys addObject:arry1];
    
    _aryy= aryys;
    
}
-(void)addTopView
{
    
    CGFloat w=72;
    
    
    
    btnPositive= [self setBtnIcon:@"Potos" andFrame:CGRectMake(20,15,72,72)];
    [btnPositive setTag:101];
    [btnPositive addTarget:self action:@selector(uplodeImage:)  forControlEvents:UIControlEventTouchUpInside];
    [btnPositive setTag:0];
    [scrollView addSubview:btnPositive];
    
     btnBackID= [self setBtnIcon:@"Potos" andFrame:CGRectMake(btnPositive.frame.origin.x+30+w,15,72,72)];
    [btnBackID setTag:1];
    [btnBackID addTarget:self action:@selector(uplodeImage:) forControlEvents:UIControlEventTouchUpInside];
    
    [scrollView addSubview:btnBackID];
    
    
    btnLifepPhotos= [self setBtnIcon:@"Potos" andFrame:CGRectMake(btnBackID.frame.origin.x+30+w,15,72,72)];
    [btnLifepPhotos setTag:2];
    [btnLifepPhotos addTarget:self action:@selector(uplodeImage:)  forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:btnLifepPhotos];
    
    UILabel *labelPositive=[self setLabelIcon:@"身份证正面" andFrame:CGRectMake(btnPositive.frame.origin.x-5, btnPositive.frame.origin.y+15+
                                                                           btnPositive.frame.size.height, 90, 72)];
    
    [scrollView addSubview:labelPositive];
    [labelPositive setTextColor:[self stringTOColor:@"#666666"]];
    
    UILabel *labelBackID=[self setLabelIcon:@"身份证背面" andFrame:CGRectMake(btnBackID.frame.origin.x-5, btnBackID.frame.origin.y+btnBackID.frame.size.height+15, 90, 72)];
    [labelBackID setTextColor:[self stringTOColor:@"#666666"]];
    [scrollView addSubview:labelBackID];
    labelLifepPhotos=[self setLabelIcon:@"生活照" andFrame:CGRectMake(btnLifepPhotos.frame.origin.x-5, btnLifepPhotos.frame.origin.y+btnLifepPhotos.frame.size.height+15, 90, 72)];
    [labelLifepPhotos setTextColor:[self stringTOColor:@"#666666"]];
    [scrollView addSubview:labelLifepPhotos];
    
    
    
}





#pragma mark-imagepiker代理
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo{
    
    if (image!=nil) {
        [tempBtn setImage:image forState:UIControlStateNormal];
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


-(void)addTableView{
    
    CGFloat h=8*44+8;
    
    UITableView *tabel=[[UITableView alloc]initWithFrame:CGRectMake(7, labelLifepPhotos.frame.origin.y+16+labelLifepPhotos.frame.size.height, 305, h)];
    
    tabel.delegate= self;
    tabel.dataSource=self;
    tabel.scrollEnabled=NO;
    [scrollView setBounces:NO];
    [scrollView setBackgroundColor:[ self stringTOColor:@"#E6E6E6"]];
    [tabel setBackgroundColor:[ self stringTOColor:@"#E6E6E6"]];
    tableView=tabel;
    [scrollView addSubview:tabel];
    

    
    UITextView *text=[[UITextView alloc]initWithFrame:CGRectMake(tableView.frame.origin.x, tableView.origin.y+h+10, 305, 100
                                                                 )];
    [text setText:@"借款描述"];
    [text setDelegate:self];
    [scrollView  addSubview:text];
    
    textDescrption=text;
    
    UIButton* checkbox=[[UIButton alloc]initWithFrame:CGRectMake(text.frame.origin.x, text.frame.size.height+text.frame.origin.y+8, 20, 20)];
    //    [checkbox setImage:[UIImage imageNamed:@"发起借款页-未选中"] forState:UIControlStateApplication];
    [checkbox setImage:[UIImage imageNamed:@"发起借款页-未选中"] forState:UIControlStateNormal];
    [checkbox addTarget:self action:@selector(chekboxSelectt:) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:checkbox];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(checkbox.frame.origin.x+10+checkbox.bounds.size.width, checkbox.frame.origin.y-checkbox.bounds.size.height/2, self.view.bounds.size.width-16-20, 44)];
    [label setText:@"同意有利可图《网站服务协议》和个人借款协议"];
    [label setFont:[UIFont systemFontOfSize:12]];
    [label setTextColor:[self stringTOColor:@"#1a681a"]];
    [scrollView addSubview:label];
    
    
    UIButton *btnSubmit=[UIButton  buttonWithType:UIButtonTypeCustom];
    [btnSubmit setFrame: CGRectMake(checkbox.frame.origin.x+10, checkbox.frame.origin.y+checkbox.frame.size.height+33, 282, 44)];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    
    [btnSubmit setTitle:@"提交" forState:UIControlStateNormal];
    
    
    [btnSubmit addTarget:self action:@selector(subMit) forControlEvents:UIControlEventTouchUpInside];
    
    [scrollView addSubview:btnSubmit];
}




-(void)chekboxSelectt:(UIButton*)btn
{
    if (isCheckBox==NO) {
        [btn setImage:[UIImage imageNamed:@"发起借款页-选中"] forState:UIControlStateNormal];

               isCheckBox=YES;
    }else
    {
        [btn setImage:[UIImage imageNamed:@"发起借款页-未选中"] forState:UIControlStateNormal];
        isCheckBox=NO;
    }
    
    
}

-(UILabel *)setLabelIcon:(NSString *)title andFrame:(CGRect)frame
{
    UILabel *label=[[UILabel alloc]initWithFrame:frame];
    label.textAlignment=NSTextAlignmentCenter;
    
    [label setText:title];
    return label;
}
-(UIButton *)setBtnIcon:(NSString *)imageName andFrame:(CGRect)frame
{
    UIImage *image=[UIImage imageNamed:imageName];
    UIButton *btn=  [[UIButton alloc] initWithFrame:frame];
    [btn setImage:image forState:UIControlStateNormal];
    //[btn setImage:image forState:UIControlStateHighlighted];
    return  btn;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}



#pragma mark-textField代理
-(void)textFieldDidBeginEditing:(UITextField *)textField
{

   textField.text=@"";
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{

    textView.text=@"";

    
}
-(void)textViewDidEndEditing:(UITextView *)textView
{

//    [textView resignFirstResponder];
//    if ([textView.text length]==0) {
//        [self alterMessge:@"借款描述不能为空"];
//    }
     [_dictLsit  setObject:textDescrption.text forKey:@"description"];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
     return YES;
}
-(void)alterMessge:(NSString*)message
{

    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
    
    [alter show];
}

-(void)subMit
{
    
    
    if (!isCheckBox) {
        [self alterMessge:@"请同意有利可图协议条款"];
        return;
    }
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    [_dictLsit setObject:username forKey:@"userId"];
         [_dictLsit  setObject:textDescrption.text forKey:@"description"];
//    if ([_dictLsit count]<4) {
//        
//        if (![[_dictLsit objectForKey:@"title"] isEqualToString:@""]) {
//            
//            [self alterMessge:@"标题不能为空"];
//            return;
//        }
//        
//        if (![[_dictLsit objectForKey:@"money"] isEqualToString:@""]) {
//            
//            [self alterMessge:@"借款金额不能为空"];
//            return;
//        }
//        
//        if (![[_dictLsit objectForKey:@"deadline"] isEqualToString:@""]) {
//            
//            [self alterMessge:@"意向借款期限不能为空"];
//            return;
//        }
//        if ([[_dictLsit objectForKey:@"description"] isEqualToString:@""]) {
//            
//            [self alterMessge:@"借款描述"];
//            return;
//        }
//    }

    if ([_dictLsit count]<7) {
        
        if ([[_dictLsit objectForKey:@"title"] isEqualToString:@""] || [_dictLsit objectForKey:@"title"] == nil) {
            
            [self alterMessge:@"标题不能为空"];
            return;
        }
        
        if ([[_dictLsit objectForKey:@"money"] isEqualToString:@""]|| [_dictLsit objectForKey:@"money"] == nil) {
            
            [self alterMessge:@"借款金额不能为空"];
            return;
        }
        
        if ([[_dictLsit objectForKey:@"deadline"] isEqualToString:@""] || [_dictLsit objectForKey:@"deadline"] == nil) {
            
            [self alterMessge:@"借款期限不能为空"];
            return;
        }
        if ([[_dictLsit objectForKey:@"description"] isEqualToString:@""] || [_dictLsit objectForKey:@"description"] == nil) {
            
            [self alterMessge:@"借款描述"];
            return;
        }
        
       if ([[_dictLsit objectForKey:@"rate"] isEqualToString:@""] || [_dictLsit objectForKey:@"rate"] == nil) {
        
                         [self alterMessge:@"利率不能为空"];
                        return;
       }
        
        
      if ([[_dictLsit objectForKey:@"repayType"] isEqualToString:@""] || [_dictLsit objectForKey:@"repayType"] == nil) {
        
                   [self alterMessge:@"还款方式"];
                    return;
        }
       // [self alterMessge:@"请仔细填写借款的类容"];
        return;
    }
    
    
    NSDictionary *dict=@{
                        // loanRpbReleaseRequest
                         @"method" : @"loanRpbReleaseRequest",
                         @"value"  :[CommeHelper getEncryptWithString:_dictLsit]
                        };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
    
        NSLog(@"resultDic=========>%@",resultDic);
        if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
        {
            NSString *decryptString = [AA3DESManager getDecryptWithString:[resultDic objectForKey:@"resultMsg"] keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
            //[self alterMessge:decryptString];
            [_Delegate alterMessge:decryptString];
            //[self alterMessge:@"提交借款成功"];
         [self.navigationController popViewControllerAnimated:YES];
        }
       
       
     
       } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
         [self showLoading:NO AndText:nil];
         [self showAllTextDialog:description];
//[_Delegate alterMessge:@"失败"];

        [self alterMessge:description];
  
    }];

    
   

    
}
-(void)loadData
{

    [self showLoading:YES AndText:@"正在加载"];
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    _arryUserList=[[NSMutableDictionary alloc]init];
    NSDictionary *dict1=@{@"userId":strUserName};
    NSDictionary *dict2=@{
                          @"method" : @"userInfoRequestHandler",
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {

        _arryUserList=[CommeHelper getDecryptWithString:resultDic[@"result"]];
        UIImage *image=[UIImage imageNamed:@"Potos"];
        NSURL *strUrl;
        if ([_arryUserList objectForKey:@"idCardPic"]!=[NSNull null]) {
            strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[_arryUserList objectForKey:@"idCardPic"]]]];
        }

      [btnPositive sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];
        
        if ([_arryUserList objectForKey:@"idCardPic2"]!=[NSNull null]) {
            strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[_arryUserList objectForKey:@"idCardPic2"]]]];
        }

        [btnBackID sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];

        
        if ([_arryUserList objectForKey:@"photo"]!=[NSNull null]) {
            strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[_arryUserList objectForKey:@"photo"]]]];
        }
        [btnLifepPhotos sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];

        
        [tableView reloadData];

      
     [self showLoading:NO AndText:nil];

    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        //  DLog(@"error==%@",error);
        NSLog(@"description ==%@", description);
    }];
    
    
    
}

-(void)uplodeImage:(UIButton*)btn
{
    tempBtn=btn;

    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                    initWithTitle:nil
                                    delegate:self
                                    cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles: @"从相册选择", @"拍照",nil];
    
    [myActionSheet showInView:self.view];

}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self localPhto];
            break;
        case 1:
            [self takePhoto];
            
            break;
        default:
            break;
    }
    
}
-(void)localPhto
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    //资源类型为图片库
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    // [self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:nil];
}


-(void)takePhoto{
    //资源类型为照相机
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否有相机
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        //资源类型为照相机
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
        
    }else {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:@"该设备无摄像头" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alter show];
    }
    
    
}

//图像选取器的委托方法，选完图片后回调该方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage *image=[info objectForKey:@"UIImagePickerControllerEditedImage"];

        //当图片不为空时显示图片并保存图片
        if (image != nil) {
    
            UIButton *btn=tempBtn;
        
            //图片显示在界面上
            [btn setImage:image forState:UIControlStateNormal];
            [self UploadGeneralLighting:image];
    
            //上传图片到友借
    
    //        [self UploadGeneralLighting:image];
    
            //        //以下是保存文件到沙盒路径下
            //        //把图片转成NSData类型的数据来保存文件
            //        NSData *data;
            //        //判断图片是不是png格式的文件
            //        if (UIImagePNGRepresentation(image)) {
            //            //返回为png图像。
            //            data = UIImagePNGRepresentation(image);
            //        }else {
            //            //返回为JPEG图像。
            //            data = UIImageJPEGRepresentation(image, 1.0);
            //        }
            //        //保存
            //        [[NSFileManager defaultManager] createFileAtPath:self.imagePath contents:data attributes:nil];
    
        }
        //关闭相册界面
        [picker dismissViewControllerAnimated:YES completion:nil];

}


//上传身份证
-(void)UploadGeneralLighting:(UIImage*)image
{
    
    
    NSData *data;
    NSString *fileName;
    if (UIImagePNGRepresentation(image)) {
        //返回为png图像。
        data = UIImagePNGRepresentation(image);
        fileName=[@"123" stringByAppendingString:@".png"];
    }else {
        //返回为JPEG图像。
        data = UIImageJPEGRepresentation(image, 1.0);
        fileName=[@"123" stringByAppendingString:@".jpeg"];
    }
    NSString *base64Image=[MF_Base64Codec base64StringFromData:data];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"image":base64Image,
                        @"fileName":fileName,
                        @"type":[NSString stringWithFormat:@"%ld",tempBtn.tag ],
                        @"op":@"uploadPic"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"appUploadPhotoRequest",
                         @"value":[CommeHelper getJson:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        // NSDictionary *dict=[CommeHelper getDecryptWithString:[resultDic objectForKey:@"result"]];
        //         NSLog(@"resultDic=================>%@",resultDic);
        [self showAllTextDialog:@"上传照片成功"];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"上传图片失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
    }];
    
}

#pragma mark-数据源
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   static NSString *flag=@"cacheCell";
    NSInteger section=[indexPath section];
    NSInteger row=[indexPath row];
    loanCell *loan=(loanCell*)[tableView dequeueReusableCellWithIdentifier:MyCell];
    if (loan==nil) {
        loan =[[loanCell alloc]init];
    }
    
    if (section==0) {
        loan.TextValue.enabled=NO;
  
        if ([indexPath row]==0) {
            [loan loadCellView:_aryy[section][row] withTextVlue:[_arryUserList objectForKey:@"realname"]];
        }else if([indexPath row]==1)
        {
            [loan loadCellView:_aryy[section][row] withTextVlue:[_arryUserList objectForKey:@"mobileNumber"]];

        }else
        {
            [loan loadCellView:_aryy[section][row] withTextVlue:[_arryUserList objectForKey:@"idCard"]];

        }
    
    }else
    {
        if (row==2) {
            [loan loadCellView:_aryy[section][row] withTextVlue:@"请输入6-10个月"];
            loan.TextValue.tag=2;
            [loan.TextValue  addTarget:self action:@selector(textFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
            loan.TextValue.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
            loan.TextValue.delegate=self;
        }else if(row==3)
        {
          [loan loadCellView:_aryy[section][row] withTextVlue:@"101"];
            loan.TextValue.tag=3;
            [loan.TextValue  addTarget:self action:@selector(textFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
            loan.TextValue.delegate=self;
            loan.TextValue.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
        }
        else if(row==4)
        {
        
            UITableViewCell *cell=[[UITableViewCell  alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:flag];
            [cell setFrame:loan.frame];
            UILabel *label=[[UILabel alloc]initWithFrame:loan.LabelName.frame];
            
            label.text=_aryy[section][row];
            [label setFont:[UIFont systemFontOfSize:13]];
            [label setTextColor:loan.LabelName.textColor];
            
            UIView *middle = [[UIView alloc] initWithFrame:CGRectMake(label.frame.origin.x+label.frame.size.width-4, 13, 1, 19)];
            [middle setBackgroundColor:ColorString(@"#e6e6e6")];
            [cell addSubview:middle];
            [cell addSubview: label];
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeSystem];
            [btn setFrame:loan.TextValue.frame];
            [btn setTitle:@"" forState:UIControlStateNormal];
            [btn setTitleColor:loan.LabelName.textColor];
            [btn addTarget:self action:@selector(clickCheckBox) forControlEvents:UIControlEventTouchUpInside];
            
            [cell addSubview:btn];
            

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
  
            clickCheckBox=btn;
            
            return cell;
        }
            
        else
        {
            
            
            [loan loadCellViewWithArry:_aryy[section][row]];
      
            if (row==1) {
                NSLog(@"========>%@",_aryy[section][row]);
                loan.TextValue.tag=1;

                loan.TextValue.delegate=self;
                loan.TextValue.keyboardType=UIKeyboardTypeNumbersAndPunctuation;
            }
          
            
        }
        loan.TextValue.delegate=self;
        [loan.TextValue  addTarget:self action:@selector(textFieldEditChanged:) forControlEvents:UIControlEventEditingChanged];
      
    
    }


   
    
    loan.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return loan;
}
-(void)textFieldEditChanged:(UITextField *)textField
{
    if (textField.tag==1||textField.tag==2||textField.tag==3) {
        NSString *regex=@"\\d+";
        
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        
        BOOL isMatch=[pred evaluateWithObject:textField.text];
        if (!isMatch) {
            if (textField.tag==1) {
                [self alterMessge: @"请输入正确的金额"];
            }else if(textField.tag==2)
            {
                [self alterMessge: @"请输入6-10个月的整数"];
            }else
            {
                [self alterMessge: @"请输入整数的利率"];
            }
            
            textField.text=@"";
            // [textField  becomeFirstResponder];
            return ;
        }
        if (textField.tag==1) {
            [_dictLsit  setObject:textField.text forKey:@"money"];
        }else if(textField.tag==2)
        {
            [_dictLsit  setObject:textField.text forKey:@"deadline"];
        }
    else if(textField.tag==3)
    {
        [_dictLsit  setObject:textField.text forKey:@"rate"];
        
        
    }
    }else
    {
        [_dictLsit  setObject:textField.text forKey:@"title"];
        
        
    }
    
    
}
/*
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    // UITableViewCell *cell=(UITableViewCell*)[[textField superview]superview];
    
    if (textField.tag==1||textField.tag==2||textField.tag==3) {
        NSString *regex=@"\\d+";
        
        NSPredicate *pred=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        
        BOOL isMatch=[pred evaluateWithObject:textField.text];
        if (!isMatch) {
            if (textField.tag==1) {
                [self alterMessge: @"请输入正确的金额"];
            }else if(textField.tag==2)
            {
                [self alterMessge: @"请输入6-10个月的整数"];
            }else
            {
                [self alterMessge: @"请输入整数的利率"];
            }
            
            textField.text=@"";
            // [textField  becomeFirstResponder];
            return ;
        }
        if (textField.tag==1) {
            [_dictLsit  setObject:textField.text forKey:@"money"];
        }else if(textField.tag==2)
        {
            [_dictLsit  setObject:textField.text forKey:@"deadline"];
        }
    }else if(textField.tag==3)
    {
        [_dictLsit  setObject:textField.text forKey:@"rate"];
        
        
    }else
    {
        [_dictLsit  setObject:textField.text forKey:@"title"];
        
        
    }

}
*/



-(void)clickCheckBox
{

    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"等额本息",@"按月付息到期还本", nil];
    alter.tag=1000;
    [alter show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag==1000) {


        if (buttonIndex==0) {
            
            // clickCheckBox.titleLabel.text=@"xxx";
            [clickCheckBox setTitle: @"等额本息" forState:UIControlStateNormal];
            [_dictLsit  setObject:@"等额本息" forKey:@"repayType"];
        }else
        {
            [clickCheckBox setTitle:@"按月付息到期还本" forState:UIControlStateNormal];
            [_dictLsit  setObject:@"按月付息到期还本" forKey:@"repayType"];

        }
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0;
    }
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(section==0)
    {
        return 3;
    }
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}


@end
