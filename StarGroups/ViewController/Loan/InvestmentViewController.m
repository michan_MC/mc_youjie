//
//  InvestmentViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "InvestmentViewController.h"

@interface InvestmentViewController ()<UITextFieldDelegate, UIAlertViewDelegate>
{
    UITextField *textFieldinvestment;
    UITextField *textFieldVerification;
    
    UILabel *balanceLabel;
    
    
    double investmentValue;
}

/**
 *  投标密码这个view的y
 */
@property (assign, nonatomic) CGFloat viewY;
@property (nonatomic,strong) NSString *loanId;
@property(nonatomic,strong)NSString *userID;
@end

@implementation InvestmentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"项目详情";
    
    [self.view setBackgroundColor:ColorString(@"#e6e6e6")];

    [self headView];
    
    
    [self addInvestmentBtn];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)headView
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 100)];
    [headView setBackgroundColor:[UIColor whiteColor]];
    UIView *bottonView = [[UIView alloc] initWithFrame:CGRectMake(0, headView.height, Main_Screen_Width, 1)];
    [bottonView setBackgroundColor:[UIColor colorWithHexString:@"#666666"]];
    
    [self.view addSubview:headView];
    [self.view addSubview:bottonView];
    
    UILabel *balanceL = [[UILabel alloc] initWithFrame:CGRectMake(35, 20, 200, 21)];
    [balanceL setFont:[UIFont systemFontOfSize:15]];
    [balanceL setText:@"当前余额（元）"];
    [balanceL setTextColor:[UIColor colorWithHexString:@"#cccccc"]];
    
    balanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 20 + 21 + 15, 200, 30)];
    [balanceLabel setFont:[UIFont systemFontOfSize:30]];
    [balanceLabel setTextColor:[UIColor colorWithHexString:@"#ff0a44"]];
    [balanceLabel setText:@"$300000.00"];
    
    [headView addSubview:balanceL];
    [headView addSubview:balanceLabel];
    
    
    UIView *backgroundView1 = [[UIView alloc] initWithFrame:CGRectMake(7, headView.height + 1 + 12, Main_Screen_Width - 14, 1)];
    [backgroundView1 setBackgroundColor:ColorString(@"#666666")];
    

    
    UIView *backgroundView3 = [[UIView alloc] initWithFrame:CGRectMake(7, headView.height + 1 + 12 + 1 + 47  + 47, Main_Screen_Width - 14, 1)];
    [backgroundView3 setBackgroundColor:ColorString(@"#666666")];
    
    self.viewY = backgroundView3.bottom;
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(7, headView.height + 1 + 12, 1, 95)];
    [leftView setBackgroundColor:ColorString(@"#666666")];
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(Main_Screen_Width - 7 - 1, headView.height + 1 + 12, 1, 95)];
    [rightView setBackgroundColor:ColorString(@"#666666")];
    
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(8, backgroundView1.bottom, Main_Screen_Width - 16, 47)];
    [view1 setBackgroundColor:[UIColor whiteColor]];
    
    UIView *backgroundView2 = [[UIView alloc] initWithFrame:CGRectMake(7, view1.bottom, Main_Screen_Width - 14, 1)];
    [backgroundView2 setBackgroundColor:ColorString(@"#666666")];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(6, 13, 70, 21)];
    [label1 setText:@"我要投资"];
    [label1 setTextColor:ColorString(@"#666666")];
    [label1 setFont:[UIFont systemFontOfSize:13]];
    [view1 addSubview:label1];
    
    UIView *middle = [[UIView alloc] initWithFrame:CGRectMake(86, 13, 1, 19)];
    [middle setBackgroundColor:ColorString(@"#e6e6e6")];
    [view1 addSubview:middle];
    
   textFieldinvestment = [[UITextField alloc] initWithFrame:CGRectMake(107, 13, Main_Screen_Width - 120, 21)];
    [textFieldinvestment setPlaceholder:@"50元以内，1000内免输投标密码"];
    [textFieldinvestment setTextColor:ColorString(@"#cccccc")];
    [textFieldinvestment setFont:[UIFont systemFontOfSize:13]];
    [view1 addSubview:textFieldinvestment];
    [textFieldinvestment setKeyboardType:UIKeyboardTypeNumberPad];
    [textFieldinvestment setDelegate:self];
    textFieldinvestment.clearButtonMode = UITextFieldViewModeAlways;
    
    
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(8, view1.bottom + 1, Main_Screen_Width - 16, 47)];
    [view2 setBackgroundColor:[UIColor whiteColor]];
    
    UIView *middle2 = [[UIView alloc] initWithFrame:CGRectMake(86, 13, 1, 19)];
    [middle2 setBackgroundColor:ColorString(@"#e6e6e6")];
    [view2 addSubview:middle2];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(6, 13, 70, 21)];
    [label2 setText:@"投标密码"];
    [label2 setTextColor:ColorString(@"#666666")];
    [label2 setFont:[UIFont systemFontOfSize:13]];
    [view2 addSubview:label2];

    
    textFieldVerification = [[UITextField alloc] initWithFrame:CGRectMake(107, 13, Main_Screen_Width - 120, 21)];
    [textFieldVerification setPlaceholder:@"投标密码为借款人真实名字"];
    [textFieldVerification setTextColor:ColorString(@"#cccccc")];
    [textFieldVerification setFont:[UIFont systemFontOfSize:13]];
    [view2 addSubview:textFieldVerification];
    textFieldVerification.clearButtonMode = UITextFieldViewModeAlways;
    [textFieldVerification setUserInteractionEnabled:NO];
    
    [self.view addSubview:backgroundView1];
    [self.view addSubview:backgroundView2];
    
    [self.view addSubview:leftView];
    [self.view addSubview:rightView];
    
    [self.view addSubview:view1];
    [self.view addSubview:view2];
    [self.view addSubview:backgroundView3];
}

- (void)addInvestmentBtn
{
    UIButton *invertment = [[UIButton alloc] initWithFrame:CGRectMake(20, self.viewY + 45, Main_Screen_Width - 40, 43)];
    [invertment setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    [invertment setBackgroundImage:[UIImage imageNamed:@"确定按钮-按下状态"] forState:UIControlStateHighlighted];
    [invertment setTintColor:[UIColor whiteColor]];
    [invertment setTitle:@"投资" forState:UIControlStateNormal];
    [invertment.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [invertment addTarget:self action:@selector(clickInvertmentBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:invertment];
    
}

- (void)clickInvertmentBtn
{
    NSString *balance = [balanceLabel.text substringFromIndex:1];
    double balanceValue = [balance doubleValue];
    
    if (investmentValue >  balanceValue) {
        UIAlertView *alter = [[UIAlertView alloc] initWithTitle:nil message:@"您的余额不足，是否充值？" delegate:self cancelButtonTitle:@"是" otherButtonTitles:@"否", nil];
        [alter show];
    }
}

#pragma mark - uialertview代理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        DLog(@"shi");
    }
}




#pragma mark - textField代理
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    double investmentVal = [textField.text longLongValue];
    investmentValue = investmentVal;
    
    if (investmentValue <= 1000) {
        [textFieldVerification setUserInteractionEnabled:NO];
    }else{
        [textFieldVerification setUserInteractionEnabled:YES];
    }
    
}

@end
