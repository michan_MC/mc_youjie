//
//  RealNameCertification.m
//  StarGroups
//
//  Created by zijin on 15/4/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "RealNameCertification.h"
#import "CommeHelper.h"
/*实名认证*/
@interface RealNameCertification ()<UIWebViewDelegate>
{
    UIWebView *_webView;
}


@end

@implementation RealNameCertification

- (void)viewDidLoad {
    [super viewDidLoad];
        
    _webView=[[UIWebView alloc]initWithFrame:[UIScreen mainScreen].applicationFrame];
    [self.view addSubview:_webView];


//    NSString *urlStr =@"http://mertest.chinapnr.com/muser/publicRequests";
//    NSURL *url = [NSURL URLWithString:urlStr];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    if (_lodaHtml!=nil) {
      
        [_webView loadHTMLString:_lodaHtml baseURL:nil];
    }
    
    _webView.scalesPageToFit=YES;
    //[_webView loadRequest:request];
    
    // 2.设置代理
    _webView.delegate = self;
//
}

 -(void)webViewDidStartLoad:(UIWebView *)webView
{
  
    [self showLoading:YES AndText:@"正在加载"];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
   [self showLoading:NO AndText:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
