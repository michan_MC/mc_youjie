//
//  SubMitLoan.h
//  StarGroups
//
//  Created by zijin on 15/4/30.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface SubMitLoan : BaseViewController
-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID andInvestMoney:(NSString*)investMoney;
@end
