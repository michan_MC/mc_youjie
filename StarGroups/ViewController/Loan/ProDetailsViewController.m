//
//  ProDetailsViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ProDetailsViewController.h"
#import "AuthenticationLoanViewController.h"
#import "CommeHelper.h"
#import "NetworkManager.h"
#import "UIImageView+EMWebCache.h"
#import "RepaymentViewController.h"
#import "CommentsController.h"
#import "PastMent.h"
#import "HaikuanViewController.h"
@interface ProDetailsViewController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UIButton *btnTemp;
    NSArray *_detailsArray;
    UIButton *_haikuanBtn;
    UIView *myView;
    UILabel * lbl_comment;
}
@property (strong, nonatomic) UIView *suspensionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, assign) CGFloat contentOffsetY;
@property (nonatomic,strong) NSString *loanId;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSDictionary *TempData;


@end

@implementation ProDetailsViewController


-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID
{
    
    
    self= [super init];
    
    if (self) {
        
        _loanId=loanId;
        _userID=userID;
        _isDetails = NO;
        _isMyProject = NO;
        _detailsArray = [NSArray array];
    }
    
    return self;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"项目详情";
    self.contentOffsetY = 0;
    self.PayInvestment.delegate = self;
    if(!IS_IPHONE_5){
        [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1300 + 4 + 20 )];
    }
    else{
        [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1250 + 4 + 20 )];
    }
    self.scrollView.delegate = self;
    [self.scrollView setBounces:NO];
    
    //    self.scrollView setc
    self.pinlun.tag=101;
    [self.pinlun  addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.PayAttention addTarget:self action:@selector(payAtttention:) forControlEvents:UIControlEventTouchUpInside];
    if (_isMyProject){
        self.PayAttention.hidden = YES;
    }
    else{
        self.PayAttention.hidden = NO;
    }
    if (!_isDetails){
        [self addSuspensionButton];
    }
    UITapGestureRecognizer *recongnizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap)];
    [self.view addGestureRecognizer:recongnizer];
    if (_isDetails){
        
        UIView * mc_view = [[UIView alloc]initWithFrame:CGRectMake(0, 622, self.view.frame.size.width, self.scrollView.frame.size.height - 622 + 44)];
        mc_view.backgroundColor = [UIColor yellowColor];
        [self.scrollView addSubview:mc_view];
        _table_View = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, mc_view.frame.size.width, mc_view.frame.size.height) style:UITableViewStyleGrouped];
        _table_View.delegate = self;
        _table_View.dataSource = self;
        
        [mc_view addSubview:_table_View];
        
    }
    myView = [[UIView alloc]initWithFrame:CGRectMake(0, Main_Screen_Height - 40 - 64, Main_Screen_Width, 40)];
    
    myView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    _haikuanBtn = [[UIButton alloc ]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 40)];
    _haikuanBtn.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [_haikuanBtn setTitle:@"还款" forState:UIControlStateNormal];
    [_haikuanBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _haikuanBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [_haikuanBtn addTarget:self action:@selector(kaikuang:) forControlEvents:UIControlEventTouchUpInside];
    [myView addSubview:_haikuanBtn];
    // [self.view bringSubviewToFront:_haikuanBtn];
    [self.view addSubview:myView];
    
    [self lodata];
    [self lodPraise];
    [self commentRequestHandler];
    
}
#pragma mark-代理
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableVie{
    return 2;
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    }else if (section == 1){
        return _detailsArray.count + 1 ;
    }
    return 0;
    
}
//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
//    if (section == 1) {
//        return @"ewqewqewqeqwewq";
//    }
//    return nil;
//}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid=@"mc";
    // UITableViewCell *cell=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    //    if (cell){
    //    [cell.contentView removeFromSuperview];
    //    }
    for (UIView* obj in cell.contentView.subviews) {
        [obj removeFromSuperview];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    if (indexPath.section == 0){
        //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 100, 40)];
        lbl_comment = lbl;
        lbl.text = @"评论";//[NSString stringWithFormat:@"评论   100"]
        
        
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height)];
        btn.tag = 101;
        [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btn];
        return cell;
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 3, 40)];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"投标记录";
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3, 0, self.view.frame.size.width / 3, 40)];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"投标金额";
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3 *2 -10, 0, self.view.frame.size.width / 3, 40)];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"投标时间";
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        
        //return cell;
    }
    else if (indexPath.section == 1 && indexPath.row > 0){
               
        NSDictionary * dic = [_detailsArray objectAtIndex:indexPath.row - 1];
        
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 3, 40)];
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        
        NSMutableString *  str = [NSMutableString stringWithString:[dic objectForKey:@"userId"]];//[dic objectForKey:@"userId"];
        //  NSRange range = {2,6};
        [str deleteCharactersInRange:NSMakeRange(2, 6)];
        [str insertString:@"***" atIndex:2];
        // [str stringByReplacingCharactersInRange:NSMakeRange(2, 7) withString:@"***"];
        lbl.text = str;//[dic objectForKey:@"userId"];//@"投标记录";
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3, 0, self.view.frame.size.width / 3, 40)];
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.textColor = [UIColor grayColor];
        lbl.textAlignment = NSTextAlignmentCenter;
        NSString * str2 = [NSString stringWithFormat:@"￥%@",[dic objectForKey:@"investMoney"]];
        lbl.text = str2;//[dic objectForKey:@"investMoney"];//@"投标金额";
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 3 *2 - 15, 0, self.view.frame.size.width / 3 + 15, 40)];
        lbl.font = [UIFont systemFontOfSize:12];
        lbl.textColor = [UIColor grayColor];
        //lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = [dic objectForKey:@"time"];//@"投标时间";
        [cell.contentView addSubview:lbl];
        
        // }
    }
    return cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)handleSingleTap
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}
-(void)payAtttention:(UIButton*)btn
{
    
    
    if (![btn.titleLabel.text isEqual:@"已关注"]) {
        NSDictionary *dic = @{
                              @"userId" : _userID,
                              @"beAttentionUser" : [[_TempData objectForKey:@"userId"] stringValue],
                              @"op": @"guanzhu",
                              @"index" : @"1",
                              @"length"    :   @"10"
                              };
        
        NSDictionary *dict = @{
                               @"method" : @"attentionRequestHandler",
                               @"value"  :[CommeHelper getEncryptWithString:dic]
                               };
        if ([_userID isEqualToString:[[_TempData objectForKey:@"userId"] stringValue]]){
            [self showAllTextDialog:@"不能关注自己"];
            
        }
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            
            [btn setImage:nil forState:UIControlStateNormal];
            [btn setTitle:@"已关注" forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:13];
            [btn setTitleColor:[UIColor blackColor]];
            [btn setUserInteractionEnabled:NO];
            [btn setTitle:@"已关注" forState:UIControlStateNormal];
            [self showAllTextDialog:@"关注成功"];
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            [self showLoading:NO AndText:nil];
            
        }];
        
        
    }
    
    
    
    
}

-(void)lodPraise
{
    
    NSDictionary *dic = @{
                          @"userId" : _userID,
                          @"loanId" : _loanId,
                          @"op": @"query",
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"praiseRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSString  *totalPraise ;
        if ([dict objectForKey:@"totalPraise"]!=[NSNull null]) {
            totalPraise=[[dict objectForKey:@"totalPraise"] stringValue];
        }else
        {
            totalPraise=@"0";
        }
        
        [self setTiteValue:totalPraise andBtn:btnTemp];
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"已经点赞过了"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
    
    
    
}
-(void)commentRequestHandler
{
    
    if (_loanId==nil||_userID==nil) {
        return;
    }
    
   // [self showLoading:YES AndText:@"正在加载"];
    NSDictionary *dic = @{
                          
                          @"loanId" : _loanId,
                          @"length" :@"10",
                          @"content":@"",
                          @"index" :@"1",
                          @"op":@"watchComment",
                          @"ip":_userID,
                          @"userId":_userID
                          
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"commentRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]];
        if ([dict objectForKey:@"count"] != [NSNull null]) {
            if (_isDetails) {
                lbl_comment.text = [NSString stringWithFormat:@"评论  %@",[[dict objectForKey:@"count"] stringValue]];
            }
            else
            _comment.text = [[dict objectForKey:@"count"] stringValue];
           // NSLog(@"%@",_comment.text);
        }
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
    }];
    
    
    
    
    
}

-(void)lodata
{
    
    if (_loanId==nil||_userID==nil) {
        return;
    }
    
    [self showLoading:YES AndText:@"正在加载"];
    NSDictionary *dic = @{
                          
                          @"loanId" : _loanId,
                          @"objName" :@"loan,invests,user",
                          @"userId":_userID
                          //[ @"loan" stringByAppendingFormat:@",%@",_userID ]
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"loanIdFindRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    //WjyFQ4F/xAQu0kN9zuzAVXRQA3LbEZwTwzySzfoxa+WgDZANAExNFdA9obFoGSZaixfFWd1rwZilUTh/X3Y+AogEd2Z28H0nfcKWUvE9k/9TBxrVmkxVCw==
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"loan"];
        _detailsArray=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"invests"];
        
        [self loadDict:dict];
        [_table_View reloadData];
        
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
        NSLog(@"description ==%@", description);
    }];
    
}

-(void)loadDict:(NSDictionary*)dic
{//20150429000002
    //15603064130
    //13533973991
    //    NSLog(@"%@",[dic objectForKey:@"description"]);//5212
    //    NSLog(@"%@",[dic objectForKey:@"guarantyType"]);//人品贷
    //    NSLog(@"%@",[dic objectForKey:@"realname"]);//黄志
    //    NSLog(@"%@",[dic objectForKey:@"repayTimeUnit"]);//天
    //    NSLog(@"%@",[dic objectForKey:@"repayType"]);//等额本息
    //    NSLog(@"%@",[dic objectForKey:@"status"]);//完成
    //    NSLog(@"%@",[dic objectForKey:@"verified"]);//通过
    
    
    if ([dic objectForKey:@"status"]!=[NSNull null] && [[dic objectForKey:@"status"] isEqualToString:@"还款中"] && _isMyProject) {
        myView.hidden = NO;
        if(!IS_IPHONE_5){
            [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1300 + 4 + 20 )];
        }
        else{
            [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1250 + 4 + 20 )];
        }
        
    }
    else{
    myView.hidden = YES;
        if(!IS_IPHONE_5){
            [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1300 + 4 + 20 )];
        }
        else{
            [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, 1250 + 4 + 20 )];
        }
    }
    
    
    if ([dic objectForKey:@"isAttention"]!=[NSNull null] && [[dic objectForKey:@"isAttention"]boolValue ] == YES) {
        [_PayAttention setImage:nil forState:UIControlStateNormal];
        _PayAttention.titleLabel.font = [UIFont systemFontOfSize:13];
        [_PayAttention setTitleColor:[UIColor blackColor]];
        [_PayAttention setUserInteractionEnabled:NO];
        [_PayAttention setTitle:@"已关注" forState:UIControlStateNormal];
    }
    if ([[dic objectForKey:@"friendsType"]integerValue] == 2){
        
        _friendTey.text = @"间接朋友";
    }
    else{
        _friendTey.text = @"好友";
    }
    
    if ([dic objectForKey:@"loanMoney"]!=[NSNull null]) {
        self.money.text=[NSString stringWithFormat:@"￥%@",[[dic objectForKey:@"loanMoney"] stringValue]];
        
    }
    
    NSLog(@"%@",[dic objectForKey:@"name"]);
    if ([dic objectForKey:@"name"]!=nil) {
        self.tite.text=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"name"] stringValue]];
        self.tite2.text = self.tite.text;
        
    }
    
    if ([dic objectForKey:@"deadline"]!=[NSNull null]) {
        NSString *giveMoneyTime=[NSString stringWithFormat:@"%@",[dic objectForKey:@"deadline"] ];
        
        self.giveMoneyTime.text=[giveMoneyTime stringByAppendingFormat:@"个月"];
    }
    
    if ([dic objectForKey:@"repayType"]!=[NSNull null]) {
        self.repayType.text=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"repayType"] stringValue]];
    }
    
    if ([dic objectForKey:@"guarantyType"]!=[NSNull null]) {
        [self.guarantyType setTitle:[NSString stringWithFormat:@"%@",[[dic objectForKey:@"guarantyType"] stringValue]] forState:UIControlStateNormal];
        
    }
    if ([dic objectForKey:@"jd"]!=[NSNull null]) {
        self.jd.text=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"jd"] stringValue]];
        _progressView.progress = [[dic objectForKey:@"jd"] floatValue] / 100.0;
    }
    else{
        _progressView.progress = 0.0;
    }
    
    //设置的是进度颜色
    _progressView.progressTintColor = [UIColor grayColor];
    //设置背景颜色
    _progressView.backgroundColor = [UIColor whiteColor];
    //[self addSubview:progressView];
    
    //restOfTimw
    
    NSString *restOfTimw=@"剩余";
    if ([dic objectForKey:@"jd"]!=[NSNull null]) {
        
        double money=0;
        if ([dic objectForKey:@"money"]!=[NSNull null]) {
            money=[[dic objectForKey:@"money"] doubleValue]*(1-([[dic objectForKey:@"jd"]doubleValue]/100));
        }
        
        restOfTimw=[restOfTimw stringByAppendingString:[ NSString stringWithFormat:@"%.2f", money]];
        //        if ([dic objectForKey:@"userPhoto"]!=[NSNull null]) {
        //            restOfTimw=[restOfTimw stringByAppendingString:@","];
        //            NSString *giveMoneyTime=[NSString stringWithFormat:@"%@",[dic objectForKey:@"deadline"] ];
        //
        //            self.giveMoneyTime.text=[giveMoneyTime stringByAppendingFormat:@"天"];
        //            restOfTimw=[restOfTimw stringByAppendingString:giveMoneyTime];
        //        }
    }
    
    self.restOfTimw.text=restOfTimw;
    
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    
    UIImageView *imageViews=[[UIImageView alloc]initWithFrame:CGRectMake(8, 320, 57, 57)];
    
    NSString *strUrl;
    if ([dic objectForKey:@"portrait"]!=nil) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
    }
    
    
    [imageViews sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
    
    [self.userPhoto setImage:imageViews.image];
    
    
    if ([dic objectForKey:@"ratePercent"]!=[NSNull null]) {
        NSInteger ratePercent=  [[dic objectForKey:@"ratePercent"]integerValue ];
        if (ratePercent<100) {
            NSString *str=[[NSString stringWithFormat:@"%ld" ,(long)ratePercent] stringByAppendingString:@".0"];
            self.ratePercent.text= str;
            
        }else{
            self.ratePercent.text=[NSString stringWithFormat:@"%ld",(long)ratePercent];
        }
    }
    
    
    if ([dic objectForKey:@"description"]!=[NSNull null]) {
        self.desques.text=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"description"] stringValue]];
    }
    
    
    //     self.ratePercent.text=[NSString stringWithFormat:@"%@",[dic objectForKey:@"ratePercent"]];
    
    if ([dic objectForKey:@"verified"]!=[NSNull null]) {
        
        [self.verified setTitle:[NSString stringWithFormat:@"%@",[[dic objectForKey:@"verified"] stringValue]] forState:UIControlStateNormal];
    }
    
    if ([dic objectForKey:@"friendsCount"]!=[NSNull null]) {
        NSString *friendsType=[NSString stringWithFormat:@"%ld",[[dic objectForKey:@"friendsCount"] integerValue]];
        self.friendsType.text=friendsType!=nil?friendsType:@"0";
        
    }
    
    if ([dic objectForKey:@"realname"]!=[NSNull null]) {
        self.userName.text=[NSString stringWithFormat:@"%@",[[dic objectForKey:@"realname"] stringValue]];
    }
    
    _TempData=dic;
    
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark -计划按钮
- (IBAction)plan:(UIButton *)sender {
    
    RepaymentViewController *repay=[[RepaymentViewController alloc]initWithData:_loanId withUserID:[_TempData objectForKey:@"userId"] andUserName:[_TempData objectForKey:@"realname"]];
    [self.navigationController pushViewController:repay animated:YES];
}


#pragma mark -记录按钮  手
- (IBAction)record:(UIButton *)sender {
    
    
    //#import "PastMent.h"
    PastMent *pas=[[PastMent alloc] init];
    pas.userID=[_TempData objectForKey:@"userId"];
    [self.navigationController pushViewController:pas animated:YES];
    
    
}


#pragma mark -评论按钮 ------这段是垃圾代码不要管
- (IBAction)comment:(UIButton *)sender {
    
}

#pragma mark - 添加分享，评论，点赞
- (void)addSuspensionButton
{
    
    
    _suspensionView = [[UIView alloc] initWithFrame:CGRectMake(Main_Screen_Width - (35 + 20)*3, Main_Screen_Height - 35 - 40 - 64, 165, 35)];
    [self.view addSubview:_suspensionView];
    
    [self buttonWithFrame:CGRectMake(0, 0, 35, 35) image:[UIImage imageNamed:@"发起借款详情页-分享icon"] withTag:100];
    
    [self buttonWithFrame:CGRectMake((35 + 20)*1, 0, 35, 35) image:[UIImage imageNamed:@"发起借款详情页-评论icon"] withTag:101];
    
    [self buttonWithFrame:CGRectMake((35 + 20)*2, 0, 35, 35) image:[UIImage imageNamed:@"发起借款详情页-点赞icon"] withTag:102];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"发起借款页面-喜欢数提示底图"] forState:UIControlStateNormal];
    [btn setTitle:@"0"  forState:UIControlStateNormal];
    btn.titleLabel.font=[UIFont systemFontOfSize:10];
    [btn setFrame:CGRectMake((35 + 20)*2+20, 2, 20, 10)];
    [btn setUserInteractionEnabled:NO];
    btnTemp=btn;
    [_suspensionView addSubview:btn];
    
    
}
-(void)setTiteValue:(NSString*)strValue andBtn:(UIButton*)btn
{
    
    if (btn!=nil) {
        [btn setTitle:strValue  forState:UIControlStateNormal];
    }
    
}



- (void)buttonWithFrame:(CGRect)frame image:(UIImage *)image withTag:(int)tag
{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTag:tag];
    [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_suspensionView addSubview:btn];
    
    //    if (tag == 102) {
    //
    //    }
    
}

#pragma mark - 点击分享，评论，点赞
- (void)clickBtn:(UIButton *)sender
{
    
    if (sender.tag==100) {
        //分享
        
        [self ShareCtlView:self];
        
        
        
    }else if(sender.tag==101)
    {
        //评论
        CommentsController *comments=[[CommentsController alloc]init];
        
        comments.dataDict=_TempData;
        [self.navigationController pushViewController:comments animated:YES];
    }else
    {
        
        //点赞
        
        NSDictionary *dic = @{
                              @"userId" : _userID,
                              @"loanId" : _loanId,
                              @"op":@"dianzan" ,
                              };
        
        NSDictionary *dict = @{
                               @"method" : @"praiseRequestHandler",
                               @"value"  :[CommeHelper getEncryptWithString:dic]
                               };
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            
            
            [self showHint:@"点赞成功"];
            // [self lodPraise];
            
            [self showLoading:NO AndText:nil];
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            [self showLoading:NO AndText:nil];
            if (description==nil) {
                [UIView animateWithDuration:0.5f animations:^{
                    [self showAllTextDialog:@"已经点赞过了"];
                }];
            }else
            {
                [UIView animateWithDuration:0.5f animations:^{
                    [self showAllTextDialog:description];
                }];
            }
            
        }];
        
        [self lodPraise];
    }
    
}
//还款
-(void)kaikuang:(UIButton *)btn{
    
    HaikuanViewController * Controller = [[HaikuanViewController alloc]init];
    [self.navigationController pushViewController:Controller animated:YES];
    
    
}
//投资按钮
- (IBAction)investmentBtn:(UIButton *)sender
{
    
    NSString *payInvestmentMoney=self.PayInvestment.text;
    if (payInvestmentMoney==nil) {
        
        [self showAllTextDialog:@"请输入正确的金额"];
        return;
    }
    
    AuthenticationLoanViewController *autthen=[[AuthenticationLoanViewController alloc] initWithData:_loanId withUserID:_userID andInvestMoney:payInvestmentMoney];
    [self pushNewViewController:autthen];
    
    /**下面是垃圾代码可以忽略不管*/
    //    InvestmentViewController *invesVC = [[InvestmentViewController alloc] init];
    //    [self pushNewViewController:invesVC];
}


#pragma  mark - uiscrollview的代理
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    // NSLog(@"scrollView.contentOffset.y22222222==%f    %f",scrollView.contentOffset.y, scrollView.contentSize.height);
    if (scrollView.contentOffset.y >= self.contentOffsetY) {
        if (scrollView.contentOffset.y == 0) {
            [_suspensionView setHidden:NO];
        }else{
            [_suspensionView setHidden:YES];
        }
    }else{
        [_suspensionView setHidden:NO];
    }
    self.contentOffsetY = scrollView.contentOffset.y;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.scrollView.scrollEnabled = NO;
    
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    self.scrollView.scrollEnabled = YES;
    [textField resignFirstResponder];
    return YES;

}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.scrollView.scrollEnabled = YES;

}

@end
