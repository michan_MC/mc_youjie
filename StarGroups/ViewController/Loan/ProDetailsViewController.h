//
//  ProDetailsViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface ProDetailsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *tite2;
@property (weak, nonatomic) IBOutlet UILabel *friendTey;

@property (weak, nonatomic) IBOutlet UILabel *comment;//评论

@property (weak, nonatomic) IBOutlet UILabel *money;//借款金额
@property (weak, nonatomic) IBOutlet UILabel *tite;//借款标题
@property (weak, nonatomic) IBOutlet UILabel *giveMoneyTime;//借款期限
@property (weak, nonatomic) IBOutlet UILabel *repayType; //还款方式
@property (weak, nonatomic) IBOutlet UIButton *guarantyType;//贷款（人品贷）

@property (weak, nonatomic) IBOutlet UILabel *jd;//进度
@property (weak, nonatomic) IBOutlet UILabel *desques;//描述
@property (weak, nonatomic) IBOutlet UILabel *restOfTimw;//剩余 时间

@property (weak, nonatomic) IBOutlet UIButton *verified;
@property (weak, nonatomic) IBOutlet UILabel *ratePercent;
@property (weak, nonatomic) IBOutlet UILabel *friendsType;
@property (weak, nonatomic) IBOutlet UIView *JdLength;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *surplus;//剩余时间
@property (weak, nonatomic) IBOutlet UIButton *pinlun;
@property (weak, nonatomic) IBOutlet UITextField *PayInvestment;

@property (weak, nonatomic) IBOutlet UIButton *PayAttention;//关注
-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID;

@property(nonatomic,assign)BOOL isDetails;//是否需要加载Details的table_View
@property(nonatomic,assign)BOOL isMyProject;//判断加载的内容是否“我的项目”
@property(nonatomic,retain)UITableView* table_View;
@end
