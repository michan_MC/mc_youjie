//
//  LoanCellDate.h
//  StarGroups
//
//  Created by zijin on 15/3/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoanCellDate : NSObject
@property(nonatomic,strong) NSString* Name;
@property(nonatomic,strong) NSString* NameValue;

+(id)initWithDict:(NSDictionary *)dict;
@end
