//
//  LoanTableViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/12.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LoanTableViewController.h"
#import "HMSegmentedControl.h"
#import "CharacterLoanViewController.h"
#import "LoanMedical.h"
#import "MedicineCredit.h"
#import "FriendMoreDataTableViewCell.h"
#import "CommeHelper.h"
#import "ProDetailsViewController.h"
#import "ProjectListViewCell.h"
#import "CommentsController.h"
#import "SearchController.h"
#import "RealNameCertification.h"
#import "LoginViewController.h"
#import "AppDelegate.h"

#define IMAGE_NAME @"sharesdk_img"
#define IMAGE_EXT @"jpg"

#define pushCotroller self pushNewViewController:
#define navheight self.navigationController.navigationBar.frame.size.height
#define KBtnW 70
#define KBtnH 13

//
static NSString *MyCell=@"cells";
@interface LoanTableViewController ()<UITableViewDataSource,UITableViewDelegate,ProjectListViewCellDelegate,UITextFieldDelegate,UIScrollViewDelegate>
{
    //UITableView *allListTable;
    UIScrollView *mainScrollView;
    UIAlertView *alter;
    
    HMSegmentedControl *titleSegment;
    
    UIImageView *iamgeView;
    UIView *locaView;
    BOOL isClik;
    BOOL isQuery;
    CGFloat viewHeigh;
    
    __block UIImageView *searchImageView;
    __block UITextField *searchTextF;
    
    UIView *bacgroundView;
    UIButton *btnAlter;
    UIButton *btnAlter1;
    UIButton *btnAlter2;
    NSInteger _allCurPage;
    NSInteger _friCurPage;
    NSInteger _myCurPage;
    NSInteger _allSize;
    NSInteger _friSzie;
    NSInteger _myCurSzie;

    NSInteger _oldOffsef;
    
    
}
@property (strong,nonatomic)UIScrollView *mainScroll;
@property(strong,nonatomic)NSMutableArray *TempValueAryy;//暂时存储数据
@property(strong,nonatomic)NSMutableArray *TempMyLoanValueAryy;//暂时存储数据(我的)
@property(strong,nonatomic)NSMutableArray *MyFriLoanValueAryy;//暂时存储数据(好友)
@property(strong,nonatomic)UITableView *allListTable;
@property(strong,nonatomic)UITableView *DynamicTabele;//朋友动态
@property(strong,nonatomic)UITableView *MyLoanTabele;//我
@property(strong,nonatomic)UIScrollView *dataMianScorView;//中间滚动视图


@end

@implementation LoanTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _oldOffsef = 0;
    _allCurPage=0;
    _friCurPage = 1;
    _myCurPage = 0;
    _allSize = 20;
    _friSzie = 20;
    _myCurSzie = 20;

    
    self.view.backgroundColor=[self stringTOColor:@"#E6E6E6"];
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName:[UIColor whiteColor]
                                                                      // UITextAttributeTextColor :[UIColor whiteColor] ios7 之前用这个
                                                                      }];
    
    _TempValueAryy=[NSMutableArray arrayWithCapacity:20];
    _TempMyLoanValueAryy=[NSMutableArray arrayWithCapacity:20];
    _MyFriLoanValueAryy = [NSMutableArray array];
    //添加item
    [self addNagetionItem];
    //添加选择框
    [self addAllSelect];
    
    [self refesh:0];
    [self myLoanLoadeDate:0];
    [self myFirend:1];
    
}
-(void)myFirend:(NSInteger)curPage{
    
   // [self showLoading:YES AndText:@"正在加载"];
    NSString *szie = [NSString stringWithFormat:@"%ld",(long)_friSzie];

    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    //{"length":"10","index":"1","beAttentionUser":"","op":"loanAndInvest","userId":"18927557274"}

    NSDictionary *dic = @{
                          
                          @"length" : szie,
                          @"index" : page,
                          @"beAttentionUser": @"",
                          @"op"   : @"loanAndInvest",
                          @"userId":username//@"18820786541"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"attentionRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
      //  NSArray * aryy = [CommeHelper getDecryptWithString:resultDic[@"result"][@"data"]];
          NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        if (![aryy isEqual:[NSNull null]]) {
            [_MyFriLoanValueAryy removeAllObjects];
            for (NSDictionary *dict in aryy) {
                
                [_MyFriLoanValueAryy addObject:dict];
            }

           // _MyFriLoanValueAryy =  array;//[NSMutableArray arrayWithArray:array];
            [_DynamicTabele reloadData];

        }
        [_DynamicTabele footerEndRefreshing];
        [_DynamicTabele headerEndRefreshing];

        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [_DynamicTabele footerEndRefreshing];

        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
    }];

}

- (void)refesh:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];
    
    NSString *page= [NSString stringWithFormat:@"%ld",(long)curPage ];
    NSString *szie = [NSString stringWithFormat:@"%ld",(long)_allSize];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    if (!username) {
        UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        LoginViewController *loginVC = [storeBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.window.rootViewController = loginVC;
    }
    NSDictionary *dic = @{
                          
                          @"isRecommend" : @"0",
                          @"status" : @"all",
                          @"curPage": page,
                          @"size"   : szie ,
                          @"businessType" : @"1",
                          @"userId":username//@"18820786541"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"loanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        
        if (![aryy isEqual:[NSNull null]]) {
            [_TempValueAryy removeAllObjects];
            for (NSDictionary *dict in aryy) {
                
                [_TempValueAryy addObject:dict];
            }
            _allCurPage++;
            _allSize = _allSize + 20;
            [_allListTable reloadData];
        }
        [_allListTable footerEndRefreshing];
        [_allListTable headerEndRefreshing];
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [_allListTable footerEndRefreshing];

        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
    }];
}

-(void)myLoanLoadeDate:(NSInteger)curPage
{
    
    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSString *szie = [NSString stringWithFormat:@"%ld",(long)_myCurSzie];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    

    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : szie ,
                          @"userId":username//@"18820786541"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        
        if (![aryy isEqual:[NSNull null]]) {
            [_TempMyLoanValueAryy removeAllObjects];

            for (NSDictionary *dict in aryy) {
                
                [_TempMyLoanValueAryy addObject:dict];
            }
            _myCurPage++;
            _myCurSzie = _myCurSzie + 20;
            [_MyLoanTabele reloadData];
        }
        [_MyLoanTabele footerEndRefreshing];
            [_MyLoanTabele headerEndRefreshing];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [_MyLoanTabele footerEndRefreshing];

        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
    }];
    

}

-(void)addNagetionItem
{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"我要借款" forState:0];
    btn.frame = CGRectMake(100, 0, 80, 60);
    
    
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn setTitleColor:[UIColor whiteColor]];
    
    [btn addTarget:self action:@selector(myLoan) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *bar=[[UIBarButtonItem alloc]initWithCustomView: btn];
    
    self.navigationItem.rightBarButtonItem=bar;
//    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"我要借款" style:UIBarButtonItemStyleDone target:self action:@selector(myLoan)];
    
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchLoan)];
    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
}

-(void)addAllSelect
{
    
    mainScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,self.view.frame.size.height - 111)];
    UIImageView *imageLogoView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 160)];
    [imageLogoView setImage:[UIImage imageNamed:@"首页-banner-.png"]];
    
    [mainScrollView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height)];
    [mainScrollView setShowsVerticalScrollIndicator:NO];
    
    [mainScrollView setBounces:NO];
    
    [mainScrollView addSubview:imageLogoView];
    
    
    
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, imageLogoView.bounds.origin.y+imageLogoView.frame.size.height, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"全部", @"我关注的", @"我的项目"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
    titleSegment.textColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.selectedTextColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.font = [UIFont systemFontOfSize:13];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-9"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    
    
    [mainScrollView addSubview:titleSegment];
    
    _dataMianScorView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 160+44, Main_Screen_Width,Main_Screen_Height-160)];
    _dataMianScorView.pagingEnabled = YES;
    _dataMianScorView.delegate = self;
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        
        [weakSelf.dataMianScorView scrollRectToVisible:CGRectMake(Main_Screen_Width * index, 44, Main_Screen_Width, weakSelf.view.height-44) animated:YES];
        
        
    }];
    
    [_dataMianScorView setContentSize:CGSizeMake(Main_Screen_Width*3, Main_Screen_Height-160)];
   // [_dataMianScorView setScrollEnabled:NO];
    
    
    [mainScrollView addSubview:_dataMianScorView];
    
    
    //    UITableView *table=[[UITableView alloc]initWithFrame:CGRectMake(0, titleSegment.frame.origin.y+titleSegment.frame.size.height, Main_Screen_Width,mainScrollView.height-titleSegment.height)];
    UITableView *table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,mainScrollView.height-titleSegment.height)];
    
    [table setDataSource:self];
    [table setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    table.userInteractionEnabled=YES;
    table.separatorColor=[UIColor clearColor];
    [table addFooterWithTarget:self action:@selector(_allAttentionFooterRefresh)];
    [table addHeaderWithTarget:self action:@selector(_allAttentionHeaderRefresh)];
    
    _allListTable=table;
    [_dataMianScorView addSubview:table];
    
    
    _DynamicTabele=[[UITableView alloc]initWithFrame:CGRectMake(Main_Screen_Width, 0, Main_Screen_Width,mainScrollView.height-titleSegment.height)];//朋友动态
    [_DynamicTabele setDataSource:self];
    [_DynamicTabele setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _DynamicTabele.userInteractionEnabled=YES;
    _DynamicTabele.separatorColor=[UIColor clearColor];
    [_DynamicTabele addFooterWithTarget:self action:@selector(_friAttentionFooterRefresh)];
     [_DynamicTabele addHeaderWithTarget:self action:@selector(_friAttentionHeaderRefresh)];
    [_dataMianScorView addSubview:_DynamicTabele];
    
    
    _MyLoanTabele=[[UITableView alloc]initWithFrame:CGRectMake(Main_Screen_Width*2, 0, Main_Screen_Width,mainScrollView.height-titleSegment.height)];//自己的项目
    [_MyLoanTabele setDataSource:self];
    [_MyLoanTabele setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _MyLoanTabele.userInteractionEnabled=YES;
    _MyLoanTabele.separatorColor=[UIColor clearColor];
    [_MyLoanTabele addFooterWithTarget:self action:@selector(_myAttentionFooterRefresh)];
    [_MyLoanTabele addHeaderWithTarget:self action:@selector(_myAttentionHeaderRefresh)];
    [_dataMianScorView addSubview:_MyLoanTabele];
    
    [self.view addSubview:mainScrollView];
    
    
}



-(void)myLoan
{
    
    if (!isQuery) {
        
        //        UITapGestureRecognizer *recongnizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap)];
        //
        //
        //        bacgroundView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        //        [bacgroundView setBackgroundColor:[UIColor grayColor]];
        //        bacgroundView.alpha=0.5;
        //        [bacgroundView addGestureRecognizer:recongnizer];
        //        [self.view addSubview:bacgroundView];
        //
        //        CGFloat width=44;
        //        CGFloat y=(self.view.frame.size.height-3*width)/3;
        //
        //        btnAlter=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        //        [btnAlter setTitle:@"人品贷" forState:UIControlStateNormal];
        //        btnAlter.tag=0;
        //        [btnAlter setBackgroundColor:[UIColor whiteColor]];
        //        [btnAlter setFrame:CGRectMake(20, y, 280, 44)];
        //        [btnAlter addTarget:self action:@selector(alertBtn:) forControlEvents:UIControlEventTouchUpInside];
        //        [self.view addSubview:btnAlter];
        
        
        //        btnAlter1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        //        [btnAlter1 setFrame:CGRectMake(20, y+width+0.5, 280, 44)];
        //
        //        btnAlter1.tag=1;
        //        [btnAlter1 setTitle:@"融易贷" forState:UIControlStateNormal];
        //        [btnAlter1 setBackgroundColor:[UIColor whiteColor]];
        //        [btnAlter1 addTarget:self action:@selector(alertBtn:) forControlEvents:UIControlEventTouchUpInside];
        //        [self.view addSubview:btnAlter1];
        //        btnAlter2=[UIButton buttonWithType:UIButtonTypeRoundedRect];
        //        [btnAlter2 setFrame:CGRectMake(20, y+2*width+1, 280, 44)];
        //
        //        [btnAlter2 setBackgroundColor:[UIColor whiteColor]];
        //        btnAlter2.tag=2;
        //        [btnAlter2 setTitle:@"医贷通" forState:UIControlStateNormal];
        //        [btnAlter2 addTarget:self action:@selector(alertBtn:) forControlEvents:UIControlEventTouchUpInside];
        //
        //        [self.view addSubview:btnAlter2];
        
        [self showLoading:YES AndText:@"正在加载"];
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        NSString *strUserName=[defaults objectForKey:@"UserName"];
        
        NSDictionary *dic=@{
                            @"userId":strUserName,
                            @"op":@"checkRealName"
                            };
        
        NSDictionary *dict=@{
                             
                             @"method" : @"realNameCertificationHandler",
                             
                             @"value":[CommeHelper getEncryptWithString:dic]
                             };
        
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            [self showLoading:NO AndText:nil];
            
            CharacterLoanViewController *character=[[CharacterLoanViewController alloc]init];
            character.Delegate = self;
            [self.navigationController pushViewController:character animated:YES];
            
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            
            [self showLoading:NO AndText:nil];
            [self isVerification:strUserName];
            
        }];
        
        isQuery=YES;
    }else
    {
        isQuery =NO;
        
    }
    
    
    
    
    
}

-(void)isVerification:(NSString*)strUserName
{
    
    [self showLoading:YES AndText:@"正在加载"];
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"op":@"realNameCertification"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"realNameCertificationHandler",
                         
                         @"value":[CommeHelper getEncryptWithString:dic]
                         };
    
    
    [requestManager requestWebWithParaWithURL_NotResponseJson:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic){
        
        
        [self showLoading:NO AndText:nil];
        
        
        RealNameCertification *rel=[[RealNameCertification alloc]init];
        rel.title=@"实名认证";

        rel.lodaHtml=[resultDic objectForKey:@"html"];
        
        [self.navigationController pushViewController:rel animated:YES];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
    }];
    [UIView animateWithDuration:0.2 animations:^{
        [self handleSingleTap];
    }];
    
    
}


-(void)handleSingleTap{
    
    
    [bacgroundView removeFromSuperview];
    bacgroundView=nil;
    [btnAlter isHidden];
    [btnAlter removeFromSuperview];
    [btnAlter1 removeFromSuperview];
    [btnAlter2 removeFromSuperview];
    btnAlter=nil;
    btnAlter1=nil;
    btnAlter2=nil;
    
    isQuery=NO;
    
    
}
-(void)alertBtn:(UIButton*)btn
{
    
    //   NSLog(@"%ld",(long)buttonIndex);
    
    if(0==btn.tag )
    {
        CharacterLoanViewController *character=[[CharacterLoanViewController alloc]init];
        [self.navigationController pushViewController:character animated:YES];
    }else if(1==btn.tag)
    {
        
        LoanMedical *lo=[[LoanMedical alloc]init];
        [self.navigationController pushViewController:lo animated:YES];
    }else
    {
        MedicineCredit *med=[[MedicineCredit alloc]init];
        [self.navigationController pushViewController:med animated:YES];
        
    }
    [UIView animateWithDuration:0.2 animations:^{
        [self handleSingleTap];
    }];
    
}

-(void)searchLoan
{
    if (!isClik) {
        [UIView animateWithDuration:0.1 animations:^{
            CGFloat Y=mainScrollView.frame.origin.y+46;
            // NSLog(@"%f",Y);
            mainScrollView.frame=(CGRect){CGPointMake(mainScrollView.frame.origin.x, Y),mainScrollView.frame.size};
            
        } completion:^(BOOL finished) {
            searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 8, Main_Screen_Width - 60, 30)];
            [searchImageView setUserInteractionEnabled:YES];
            [searchImageView setImage:[UIImage imageNamed:@"搜索好友页-搜索框"]];
            UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 16, 16)];
            [searchIcon setImage:[UIImage imageNamed:@"添加朋友页-搜索icon"]];
            searchTextF = [[UITextField alloc] initWithFrame:CGRectMake(32, 7, searchImageView.width - 32, 16)];
            [searchTextF setPlaceholder:@"搜索"];
            [searchTextF setTextColor:[UIColor colorWithHexString:@"#666666"]];
            [searchTextF setFont:[UIFont systemFontOfSize:13]];
            [searchTextF setBorderStyle:UITextBorderStyleNone];
            [searchTextF setTextAlignment:NSTextAlignmentLeft];
            [searchTextF setClearButtonMode:UITextFieldViewModeAlways];
            searchTextF.returnKeyType = UIReturnKeySearch;
            searchTextF.delegate = self;
            [self.view addSubview:searchImageView];
            [searchImageView addSubview:searchIcon];
            [searchImageView addSubview:searchTextF];
        }];
        
        isClik=YES;
        
        
        
    }else
    {
        [UIView animateWithDuration:0.1 animations:^{
            CGFloat Y=mainScrollView.frame.origin.y-46;
            
            mainScrollView.frame=(CGRect){CGPointMake(mainScrollView.frame.origin.x, Y),mainScrollView.frame.size};
            [searchImageView removeFromSuperview];
            
        } completion:^(BOOL finished) {
            isClik=NO;
        }];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}

#pragma mark - 上拉刷新
- (void)_allAttentionFooterRefresh
{
        [self refesh:_allCurPage];
}
- (void)_friAttentionFooterRefresh
{
    [self myFirend:_friCurPage];
}
- (void)_myAttentionFooterRefresh
{
   [self myLoanLoadeDate:_myCurPage];
}
#pragma mark - 下拉刷新


- (void)_allAttentionHeaderRefresh
{
    _allCurPage=0;
    _allSize = 20;
    [self refesh:_allCurPage];
}
- (void)_friAttentionHeaderRefresh
{
    _friCurPage = 1;
    _friSzie = 20;

    [self myFirend:_friCurPage];
}
- (void)_myAttentionHeaderRefresh
{
    _myCurPage = 0;
    _myCurSzie = 20;

    [self myLoanLoadeDate:_myCurPage];
}

#pragma mark - Share
-(void)Share//分享
{

    
    [self ShareCtlView:self];
    
}
#pragma mark- search
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    SearchController *controller=[[SearchController alloc]init];
    controller.searchText=textField.text==nil?@"":textField.text;
    [self.navigationController pushViewController:controller animated:YES];
    return YES;
}

#pragma mark-评论代理实现



-(void)CommentsDidDone:(NSDictionary *)dictData
{
    
    CommentsController *comments=[[CommentsController alloc]init];
    
    comments.dataDict=dictData;
    [self.navigationController pushViewController:comments animated:YES];
    
}
-(void)GetPraise:(NSDictionary *)praise
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    
    
    NSDictionary *dic = @{
                          @"userId" : username,
                          @"loanId" : [praise objectForKeyedSubscript:@"id"],
                          @"op": [praise objectForKey:@"isPaise2"],
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"praiseRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        if([[praise objectForKey:@"isPaise2"] isEqual:@"cancel"])
        {
            [self showHint:@"取消点赞成功"];
            
        }else
        {
            [self showHint:@"点赞成功"];
            
        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"已经点赞过了"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
    
}

#pragma mark - Table view data source

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *dentifier=@"myCell";
    static NSString *DynamicDentifier=@"DynamicDentifier";
    static NSString *myfir=@"myfri";

    if (_MyLoanTabele==tableView) {
        
        ProjectListViewCell *cell= [tableView dequeueReusableCellWithIdentifier:dentifier];
        
        if (cell==nil) {
            
            cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
            
        }
        if (_TempMyLoanValueAryy.count==0) {
            [cell LodaView:nil];
        }else
        {
            [cell LodaView:_TempMyLoanValueAryy[indexPath.row]];
        }
        cell.delegate = self;
        
        cell.backgroundColor= [  self stringTOColor:@"#E6E6E6"];
        return cell;

    }else if(_allListTable==tableView)
    {
        
        
        
        ProjectListViewCell *cell=[tableView dequeueReusableCellWithIdentifier:DynamicDentifier];
        
        if (cell==nil) {
            
            cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DynamicDentifier];
            
        }
        if (_TempValueAryy.count==0) {
            [cell LodaView:nil];
        }else
        {
            [cell LodaView:_TempValueAryy[indexPath.row]];
        }
        cell.delegate = self;
        
        cell.backgroundColor= [  self stringTOColor:@"#E6E6E6"];
        return cell;
        
    }else if(tableView == _DynamicTabele)
    {
        
        
        ProjectListViewCell *cell=[tableView dequeueReusableCellWithIdentifier:myfir];
        
        if (cell==nil) {
            
            cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:myfir];
            
        }

        if (_MyFriLoanValueAryy.count==0) {
            [cell LodaView:nil];
        }else
        {
            [cell lodaMyFriView:_MyFriLoanValueAryy[indexPath.row]];
          //  [cell LodaView:_TempValueAryy[indexPath.row]];
        }
        cell.delegate = self;
        
        cell.backgroundColor= [  self stringTOColor:@"#E6E6E6"];
        return cell;
    }
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    ProDetailsViewController *pro;
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    if (tableView == _allListTable){
        if (![_TempValueAryy isEqual:[NSNull null]]) {//全部
            
            
            pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
            
            pro.isMyProject = NO;
            pro.isDetails = NO;
        }
    }
    else if (tableView == _DynamicTabele){//朋友
        if (![_MyFriLoanValueAryy isEqual:[NSNull null]]) {//全部
            
            
            pro=[[ProDetailsViewController alloc]initWithData:[_MyFriLoanValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
            pro.isMyProject = NO;
            pro.isDetails = NO;
        }
    }
    else if (tableView == _MyLoanTabele){//我的
        if (![_TempMyLoanValueAryy isEqual:[NSNull null]]) {
            
            pro=[[ProDetailsViewController alloc]initWithData:[_TempMyLoanValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
            pro.isMyProject = YES;
            pro.isDetails = YES;
        }
        
    }

    
    [self.navigationController pushViewController:pro animated:YES];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_DynamicTabele==tableView)
    {
        return _MyFriLoanValueAryy.count;
        
    }else if(_MyLoanTabele==tableView)
    {
        
        if (![_TempMyLoanValueAryy isEqual:[NSNull null]])
        {
            
            return  _TempMyLoanValueAryy.count;
        }else
        {
            return 0 ;
        }
    }else
    {
    
        if (![_TempValueAryy isEqual:[NSNull null]])
        {
            
            return  _TempValueAryy.count;
        }else
        {
            return 0 ;
        }

        
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _DynamicTabele)
    {
        if (_MyFriLoanValueAryy.count > indexPath.row)
        {
           NSDictionary * dic = _MyFriLoanValueAryy[indexPath.row];
            if (![[dic objectForKey:@"status"] isEqualToString:@"等待复核"] && [dic objectForKey:@"loannickname"]){
                return 271;
            }
                return 231;
        }
        
    }
    return 231;
    
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [titleSegment setSelectedSegmentIndex:page animated:YES];
    
//    if (page == 1 && _investArray.count < 1){
//        
//        [self myInvesLoadDate:0];
//    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (scrollView == _allListTable || scrollView == _DynamicTabele || scrollView == _MyLoanTabele ) {
        
   if( scrollView.contentOffset.y > _oldOffsef )
   {
       
      // [mainScrollView setContentOffset:CGPointMake(0, 0)];
       [mainScrollView setContentOffset:CGPointMake(0, 111 + 48) animated:YES];
        }
        else if (_oldOffsef <= 50)
        {
            [mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            
        }
        _oldOffsef = scrollView.contentOffset.y;
    }
    
    
    
}
-(void)alterMessge:(NSString*)message
{
    
    UIAlertView *alter1=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
    
    [alter1 show];
}
-(void)showAllTextlog:(NSString *)title{
    [self showAllTextDialog:title];
}




@end
