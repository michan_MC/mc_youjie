//
//  LoanCellTableViewCell.m
//  StarGroups
//
//  Created by zijin on 15/3/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "loanCell.h"
#import "LoanCellDate.h"

@implementation loanCell

- (void)awakeFromNib {
    UIView *middle = [[UIView alloc] initWithFrame:CGRectMake(self.LabelName.frame.origin.x+self.LabelName.frame.size.width-4, 13, 1, 19)];
    [middle setBackgroundColor:ColorString(@"#e6e6e6")];
    [self addSubview:middle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

  
}
#pragma mark-加载数据
-(id)loadCellViewWithArry:(NSString *)arry
{

        self.LabelName.text=arry;
        self.TextValue.text=@"";
        //self.TextValue.enabled=NO;

    return self;
  
}

-(id)loadCellView:(NSString *)str withTextVlue:(NSString *)userVlue
{
    self.LabelName.text=str;
    
   
    if ([userVlue isEqual:@"101"]) {
        UILabel *label=[[UILabel alloc ]initWithFrame:CGRectMake(self.TextValue.frame.origin.x+self.TextValue.frame.size.width-10, self.TextValue.frame.origin.y, 20, 30)];
        [label setText:@"%"];
        [self addSubview:label];
    }
    else
    {
        self.TextValue.text= ([userVlue isEqual:[NSNull null]])?@"":userVlue;
    
    }
    return self;
}


@end
