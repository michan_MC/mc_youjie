//
//  AuthenticationLoanViewController.m
//  StarGroups
//
//  Created by zijin on 15/4/30.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "AuthenticationLoanViewController.h"
#import "CommeHelper.h"
#import "UIImageView+EMWebCache.h"
#import "SubMitLoan.h"
#define imageViewHiegth 200
#define imageViewPace  10


@interface AuthenticationLoanViewController ()
{
    UIImage *imag;
    UIImageView *idCardPic;
    UIImageView *idCardPic2;
    UIImageView *photo;
}
@property (nonatomic,strong) NSString *loanId;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSString *investMoney;
@end
/*借款认证*/
@implementation AuthenticationLoanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title=@"借款认证";
    [self addImageView];//加载图片
    [self addItemBar];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
}

-(id)initWithData:(NSString *)loanId withUserID:(NSString*)userID andInvestMoney:(NSString*)investMoney
{
    
    
    self= [super init];
    
    if (self) {
        
        _loanId=loanId;
        _userID=userID;
        _investMoney=investMoney;
    }
    
    return self;
    
    
}

-(void)addItemBar
{
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"提交" forState:0];
    btn.frame = CGRectMake(100, 0, 80, 60);
    
    
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn setTitleColor:[UIColor whiteColor]];
    
    [btn addTarget:self action:@selector(subMit) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *bar=[[UIBarButtonItem alloc]initWithCustomView: btn];
    
    self.navigationItem.rightBarButtonItem=bar;
    

}
-(void)addImageView
{

    UIScrollView *mainScroView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height) ];
    [mainScroView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height*1.3)];

    
    imag=[UIImage imageNamed:@"Potos"];
    idCardPic =  [[UIImageView alloc] initWithFrame:CGRectMake(7, imageViewPace, Main_Screen_Width-14,imageViewHiegth)];
    [idCardPic setImage:imag];
    idCardPic2=  [[UIImageView alloc] initWithFrame:CGRectMake(7, imageViewPace+imageViewHiegth+idCardPic.frame.origin.y, Main_Screen_Width-14,imageViewHiegth)];
    [idCardPic2 setImage:imag];
    photo=  [[UIImageView alloc] initWithFrame:CGRectMake(7, imageViewPace+imageViewHiegth+idCardPic2.frame.origin.y, Main_Screen_Width-14,imageViewHiegth)];
    [photo setImage:imag];
    
    [mainScroView addSubview:idCardPic];
    [mainScroView addSubview:idCardPic2];
    [mainScroView addSubview:photo];
    
    [self.view addSubview:mainScroView];
}
-(void)loadData
{

    NSDictionary *dict1=@{@"userId":_userID};
    NSDictionary *dict2=@{
                      @"method" : @"userInfoRequestHandler",
                      @"value"  :[CommeHelper getEncryptWithString:dict1]
                      };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
    
    NSDictionary *dict=[CommeHelper getDecryptWithString:resultDic[@"result"]];
    NSURL *strUrl;
        
        
        
    if ([dict objectForKey:@"idCardPic"]!=[NSNull null]) {
        strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"idCardPic"]]]];
    }
        
    [idCardPic sd_setImageWithURL:strUrl placeholderImage:imag];
        
        


    if ([dict objectForKey:@"idCardPic2"]!=[NSNull null]) {
        strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"idCardPic2"]]]];
    }

    [idCardPic2 sd_setImageWithURL:strUrl placeholderImage:imag];
    
    
    if ([dict objectForKey:@"photo"]!=[NSNull null]) {
        strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dict objectForKey:@"photo"]]]];
    }

    [photo sd_setImageWithURL:strUrl placeholderImage:imag];
    
    
    
    [self showLoading:NO AndText:nil];
    
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
    

    }];

}

-(void)subMit
{

    SubMitLoan *sub=[[SubMitLoan alloc]initWithData:_loanId withUserID:_userID andInvestMoney:_investMoney ];
    
    [self pushNewViewController:sub];
}


@end
