//
//  LoanCellTableViewCell.h
//  StarGroups
//
//  Created by zijin on 15/3/23.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface loanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *LabelName;
@property (weak, nonatomic) IBOutlet UITextField *TextValue;
-(id)loadCellViewWithArry:(NSString *)arry;
-(id)loadCellView:(NSString *)str withTextVlue:(NSString *)userVlue;
@end
