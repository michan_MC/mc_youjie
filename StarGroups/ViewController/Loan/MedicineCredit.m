#import "MedicineCredit.h"
#import "HMSegmentedControl.h"
#import "loanCell.h"
static NSString *leftCell=@"leftCell";
static NSString *rigthCell=@"rigthCell";
static NSString *MyCell=@"cells";
@interface MedicineCredit ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *LeftTableView;
    UITableView *rigthTableView;
    HMSegmentedControl *titleSegment;
    NSArray *_aryy;
    NSArray *_aryy2;
    
    BOOL isCheckBox;
}
@property(strong,nonatomic)UIScrollView *mainScroll;
@end

@implementation MedicineCredit
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title=@"发起借款";
    self.view.backgroundColor=[self stringTOColor:@"#E6E6E6"];
    
    [self setData];
    [self addTitleSegment];
    [self addScrollView];
    UINib *nib = [UINib nibWithNibName:@"loanCell" bundle:nil];
    [LeftTableView registerNib:nib forCellReuseIdentifier:MyCell];
    [rigthTableView registerNib:nib forCellReuseIdentifier:MyCell];
}


-(void)setData
{
    //初始化数据
    NSArray *arry= [[NSArray alloc]initWithObjects:
                    @"联系电话"
                    ,nil];
    
    NSArray *arry1= [[NSArray alloc]initWithObjects:
                     @"中标通知书编号",
                     @"实际经营地址",
                     @"借款标题",
                     @"意向借款金额"
                     ,@"借款描述"
                     ,nil];
    
    NSMutableArray *arrys=[[NSMutableArray  alloc]initWithCapacity:2];
    [arrys addObject:arry];
    [arrys addObject:arry1];
    _aryy= arrys;
    
    
    NSArray *arry3= [[NSArray alloc]initWithObjects:
                     @"借款人姓名"
                     ,@"联系电话"
                     ,nil];
    
    NSArray *arry4= [[NSArray alloc]initWithObjects:
                     @"采购方[医院]",
                     @"拟投标产品品牌",
                     @"产品型号",
                     @"借款标题"
                     ,@"意向借款金额"
                     ,@"预期借款期限"
                     ,@"实际经营地址"
                     ,@"借款描述"
                     ,nil];
    
    NSMutableArray *arry5=[[NSMutableArray  alloc]initWithCapacity:2];
    
    [arry5 addObject:arry3];
    [arry5 addObject:arry4];
    _aryy2=arry5;
}

- (void)addTitleSegment
{
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"中标", @"未中标"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
    titleSegment.textColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.selectedTextColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.font = [UIFont systemFontOfSize:13];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-9"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.mainScroll scrollRectToVisible:CGRectMake(Main_Screen_Width * index, 44, Main_Screen_Width, weakSelf.view.height-44) animated:YES];
    }];
    
    [self.view addSubview:titleSegment];
    
    
}

- (void)addScrollView
{
    self.mainScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, Main_Screen_Width, self.view.bounds.size.height*1.2)];
    self.mainScroll.contentSize = CGSizeMake(Main_Screen_Width * 2, self.view.frame.size.height*1.7);
    self.mainScroll.backgroundColor = [UIColor colorWithHexString:@"#e6e6e6"];
    self.mainScroll.delegate = self;
    self.mainScroll.pagingEnabled = YES;
    [self.mainScroll setShowsVerticalScrollIndicator :NO];
    self.mainScroll.bounces = NO;
    [self.view addSubview:self.mainScroll];
    
    [self addTabelView];
}

-(void)addTabelView
{
    
    LeftTableView=[[UITableView alloc]initWithFrame:CGRectMake(titleSegment.frame.origin.x+8,12, 305, 5*44+10+150)];
    [LeftTableView setDataSource:self];
    [LeftTableView setDelegate:self];
    [LeftTableView setScrollEnabled:NO];
    [LeftTableView setBackgroundColor:[self stringTOColor:@"#E6E6E6"]];
    
    [self.mainScroll addSubview:LeftTableView];
    
    UIButton* checkbox=[[UIButton alloc]initWithFrame:CGRectMake(LeftTableView.frame.origin.x, LeftTableView.frame.size.height+LeftTableView.frame.origin.y+8, 20, 20)];
    [checkbox setImage:[UIImage imageNamed:@"发起借款页-选中"] forState:UIControlStateNormal];
    [checkbox addTarget:self action:@selector(chekboxSelectt:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *label=[[UILabel alloc]initWithFrame:CGRectMake(checkbox.frame.origin.x+10+checkbox.bounds.size.width, checkbox.frame.origin.y-checkbox.bounds.size.height/2, self.view.bounds.size.width-16-20, 44)];
    [label setText:@"同意有利可图《网站服务协议》和个人借款协议"];
    [label setFont:[UIFont systemFontOfSize:12]];
    [label setTextColor:[self stringTOColor:@"#1a681a"]];
    
    [self.mainScroll addSubview:checkbox];
    [self.mainScroll addSubview:label];
    
    UIButton *btnSubmit=[UIButton  buttonWithType:UIButtonTypeCustom];
    [btnSubmit setFrame: CGRectMake(checkbox.frame.origin.x+10,checkbox.frame.origin.y+checkbox.frame.size.height+33, 282, 44)];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    
    [btnSubmit setTitle:@"提交" forState:UIControlStateNormal];
    
    [btnSubmit addTarget:self action:nil forControlEvents:UIControlEventTouchUpInside];
    
    [self.mainScroll addSubview:btnSubmit];
    
    
    rigthTableView=[[UITableView alloc]initWithFrame:CGRectMake(titleSegment.frame.origin.x+Main_Screen_Width+8,12, 302, 9*44+10+150)];
    [rigthTableView setDataSource:self];
    [rigthTableView setDelegate:self];
    [rigthTableView setScrollEnabled:NO];
    [rigthTableView setBackgroundColor:[UIColor whiteColor]];
    [self.mainScroll addSubview:rigthTableView];
    
    UIButton *btnSubmit2=[UIButton  buttonWithType:UIButtonTypeCustom];
    [btnSubmit2 setFrame: CGRectMake(Main_Screen_Width+20, rigthTableView.frame.origin.y+rigthTableView.frame.size.height+33, 282, 44)];
    [btnSubmit2 setBackgroundImage:[UIImage imageNamed:@"确定按钮"] forState:UIControlStateNormal];
    
    [btnSubmit2 setTitle:@"借款认证" forState:UIControlStateNormal];
    
    [self.mainScroll addSubview:btnSubmit2];
    
}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    [titleSegment setSelectedSegmentIndex:page];
    
}

-(void)chekboxSelectt:(UIButton*)btn
{
    if (isCheckBox==NO) {
        [btn setImage:[UIImage imageNamed:@"发起借款页-未选中"] forState:UIControlStateNormal];
        isCheckBox=YES;
    }else
    {
        [btn setImage:[UIImage imageNamed:@"发起借款页-选中"] forState:UIControlStateNormal];
        isCheckBox=NO;
    }
    
    
}
#pragma mark- 数据源


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger section=[indexPath section];
    
    NSInteger row=[indexPath row];
    if (tableView==LeftTableView)
    {
        
        if (section==1&&row==4) {
            UITableViewCell *tabelCell=[[UITableViewCell alloc]init];
            tabelCell.textLabel.font=[UIFont systemFontOfSize:8];
            UITextView *text=[[UITextView alloc]initWithFrame:CGRectMake(0, 0, 305, 150)];
            text.text=_aryy[section][row];
            [tabelCell addSubview:text];
            return  tabelCell;
        }else
        {
            loanCell *loan=[tableView dequeueReusableCellWithIdentifier:MyCell];
            if (loan==nil) {
                loan=[[loanCell alloc]init];
            }
            
            loan.textLabel.font=[UIFont systemFontOfSize:8];
            
            [loan loadCellViewWithArry:_aryy[section][row]];
            return loan;
        }
        
    }else{
        
        if (section==1&&row==7) {
            UITableViewCell *tabelCell=[[UITableViewCell alloc]init];
            tabelCell.textLabel.font=[UIFont systemFontOfSize:3];
            UITextView *text=[[UITextView alloc]initWithFrame:CGRectMake(0, 0, 305, 150)];
            text.text=_aryy2[section][row];
            [tabelCell addSubview:text];
            return  tabelCell;
        }else{
            
            
            loanCell *loan=[tableView dequeueReusableCellWithIdentifier:MyCell];
            if (loan==nil) {
                loan=[[loanCell alloc]init];
            }
            
            loan.textLabel.font=[UIFont systemFontOfSize:8];
            
            [loan loadCellViewWithArry:_aryy2[section][row]];
            
            return loan;
        }
        
        
    }
    // NSLog(@"%@",_aryy[section][row]);
    //return  loan;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView==LeftTableView)
    {
        if (section==0)
        {
            return 0;
        }
        return 10;
    }else{
        if (section==0) {
            return 2;
        }
        return 8;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView==LeftTableView) {
        if (1==[indexPath section]&&[indexPath row]==4) {
            return 150;
        }
        return 44;
    }
    else{
        
        if (1==[indexPath section]&&[indexPath row]==7) {
            return 150;
        }
        
        return 44;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView==LeftTableView) {
        if(section==0)
        {
            return 1;
        }
        return 5;
    }else
    {
        if(section==0)
        {
            return 2;
        }
        return 8;
    }
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}

@end