//
//  FriendMoreDataViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "FriendMoreDataViewController.h"
#import "HMSegmentedControl.h"
#import "UIViewExt.h"
#import "MJRefresh.h"
#import "MailListTableViewCell.h"
#import "ProDetailsViewController.h"
#import "CommeHelper.h"
#import "ProjectListViewCell.h"
#import "CommentsController.h"
#import "UIImageView+EMWebCache.h"
#import "ContactTableViewCell.h"
@interface FriendMoreDataViewController ()<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,ProjectListViewCellDelegate>
{
    HMSegmentedControl *titleSegment;
    
     CGFloat viewHeigh;
    //dispatch_queue_t _userQueue;

    
}
@property (nonatomic, strong) UIScrollView *mainScroll;
@property (nonatomic, strong) UITableView *projectTable;
@property (nonatomic, strong) UITableView *fansTable;
@property (nonatomic, strong) UITableView *attentionTable;
@property(nonatomic,strong)NSArray *TempValueAryy;
@property(nonatomic,strong)NSArray *attentionAryy;
@property(nonatomic,strong)NSArray *fansAryy;

@end

@implementation FriendMoreDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"更多";
    
    viewHeigh = Main_Screen_Height-64-44;
    
//    添加状态栏
    [self addTitleSegment];
    
    [self addScrollView];
    
    [self addProjectTable];
    
    [self addFansTable];
    
    [self addAttentionTable];
    [self refesh:0];
    [self himFeisi:0];
    [self lodeMyVermicelli];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addTitleSegment
{
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"他的项目", @"他的粉丝",  @"他关注的"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
    titleSegment.textColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.selectedTextColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.font = [UIFont systemFontOfSize:13];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-9"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.mainScroll scrollRectToVisible:CGRectMake(Main_Screen_Width * index, 44, Main_Screen_Width, weakSelf.view.height-44) animated:YES];
    }];
    
    [self.view addSubview:titleSegment];
}


- (void)addScrollView
{
    self.mainScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, Main_Screen_Width, viewHeigh)];
    self.mainScroll.contentSize = CGSizeMake(Main_Screen_Width * 3, viewHeigh);
    self.mainScroll.backgroundColor = [UIColor colorWithHexString:@"#e6e6e6"];
    self.mainScroll.delegate = self;
    self.mainScroll.pagingEnabled = YES;
    self.mainScroll.showsHorizontalScrollIndicator = NO;
    self.mainScroll.bounces = NO;
    [self.view addSubview:self.mainScroll];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [titleSegment setSelectedSegmentIndex:page animated:YES];
}
-(void)himFeisi:(NSInteger)curPage{

    NSString *page=[NSString stringWithFormat:@"%d",curPage ];
    NSDictionary *dic = @{
                          
                          @"op" : @"query2",
                          @"index": @"1",
                          @"length"   : @"10" ,
                          @"userId":_userID,
                          @"beAttentionUser":@""

                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"attentionRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];

        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        if (![aryy isEqual:[NSNull null]]) {
            
            _fansAryy = aryy;
            
            [_fansTable reloadData];
        }
        [self showLoading:NO AndText:nil];

       // [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
 
    
    
}
//加载关注
-(void)lodeMyVermicelli
{
    NSDictionary *song = @{@"userId" : _userID,
                           @"beAttentionUser" : @"",
                           @"op"   : @"query1",
                           @"index" : @"1",
                           @"length"    :   @"20"
                           };
    
    
    
    NSDictionary *dict = @{
                           @"method"   :  @"attentionRequestHandler",
                           @"value"    :  [CommeHelper getEncryptWithString:song]
                           };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
      //  NSMutableArray *dataAryy=[NSMutableArray arrayWithCapacity:20];
        if (![aryy isEqual:[NSNull null]]) {
            _attentionAryy = aryy;
        }
        
        [_attentionTable reloadData];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //[self.tableView headerEndRefreshing];
        
        
    }];
    
}

- (void)refesh:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];
    
    NSString *page=[NSString stringWithFormat:@"%d",curPage ];
    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : @"20" ,
                          @"userId":_userID
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };

    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        
        if (![aryy isEqual:[NSNull null]]) {
            
            _TempValueAryy=aryy;
            [_projectTable reloadData];
        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
}
#pragma mark - 项目
- (void)addProjectTable
{
    
    _projectTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, viewHeigh) style:UITableViewStylePlain];
    _projectTable.tableFooterView = [[UIView alloc] init];
    _projectTable.dataSource = self;
    _projectTable.delegate = self;
    _projectTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [_projectTable addHeaderWithTarget:self action:@selector(projectTableHeaderRefresh)];
//    [_projectTable addFooterWithTarget:self action:@selector(projectTableFooterRefresh)];
    
    [self.mainScroll addSubview:_projectTable];
}

- (void)projectTableHeaderRefresh
{
    
}


- (void)projectTableFooterRefresh
{
    
}

#pragma mark - 粉丝
- (void)addFansTable
{
    _fansTable = [[UITableView alloc] initWithFrame:CGRectMake(Main_Screen_Width, 0, Main_Screen_Width, viewHeigh) style:UITableViewStylePlain];
    _fansTable.tableFooterView = [[UIView alloc] init];
    _fansTable.dataSource = self;
    _fansTable.delegate = self;
    _fansTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [_fansTable addHeaderWithTarget:self action:@selector(fansTableHeaderRefresh)];
//    [_fansTable addFooterWithTarget:self action:@selector(fansTableFooterRefresh)];
    
    [self.mainScroll addSubview:_fansTable];

}

#pragma mark-评论代理实现



-(void)CommentsDidDone:(NSDictionary *)dictData
{
    
    CommentsController *comments=[[CommentsController alloc]init];
    
    comments.dataDict=dictData;
    [self.navigationController pushViewController:comments animated:YES];
    
}
-(void)GetPraise:(NSDictionary *)praise
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    
    
    NSDictionary *dic = @{
                          @"userId" : username,
                          @"loanId" : [praise objectForKeyedSubscript:@"id"],
                          @"op": [praise objectForKey:@"isPaise2"],
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"praiseRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        if([[praise objectForKey:@"isPaise2"] isEqual:@"cancel"])
        {
            [self showHint:@"取消点赞成功"];
            
        }else
        {
            [self showHint:@"点赞成功"];
            
        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"已经点赞过了"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
    
}


- (void)fansTableHeaderRefresh
{

}

- (void)fansTableFooterRefresh
{
    
}

#pragma mark - attention
- (void)addAttentionTable
{
    _attentionTable = [[UITableView alloc] initWithFrame:CGRectMake(Main_Screen_Width * 2, 0, Main_Screen_Width, viewHeigh) style:UITableViewStylePlain];
    _attentionTable.tableFooterView = [[UIView alloc] init];
    _attentionTable.dataSource = self;
    _attentionTable.delegate = self;
    _attentionTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [_attentionTable addHeaderWithTarget:self action:@selector(attentionTableHeaderRefresh)];
//    [_attentionTable addFooterWithTarget:self action:@selector(attentionTableFooterRefresh)];
//    
    [self.mainScroll addSubview:_attentionTable];
}

- (void)attentionTableHeaderRefresh
{

}

- (void)attentionTableFooterRefresh
{
    
}


#pragma mark - TableViewDelegate & TableViewDatasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifyPro = @"friendMoreDataCell";
    
    if(tableView==_projectTable)
    {
    
    ProjectListViewCell *cell=(ProjectListViewCell*)[tableView dequeueReusableCellWithIdentifier:identifyPro];
    
    if (cell==nil) {
        
        cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifyPro];
        
    }
    if (_TempValueAryy.count==0) {
        [cell LodaView:nil];
    }else
    {
        [cell LodaView:_TempValueAryy[indexPath.row]];
    }
    cell.delegate = self;
    cell.backgroundColor= [self stringTOColor:@"#E6E6E6"];
        

    return cell;
    }
   else if(tableView==_fansTable)
    {
        
        
        ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mc"];
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];
        }
        
        if (_fansAryy.count > 0) {
            
            NSDictionary * dic = [_fansAryy objectAtIndex:indexPath.row];
            NSString *imgUrl = @"";
            if ([dic objectForKey:@"portrait"] != [NSNull null]){
                imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
            }
            
            NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"attentionUser"]:[dic objectForKey:@"nickname"];
            cell.userName.text = namrStr;
            UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
            [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
            
            
        }
        return cell;

        
        
    }

    
    ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mc"];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];
    }
    
    if (_attentionAryy.count > 0) {
        
        NSDictionary * dic = [_attentionAryy objectAtIndex:indexPath.row];
        NSString *imgUrl = @"";
        if ([dic objectForKey:@"portrait"] != [NSNull null]){
            imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
        }
        
        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"attentionUser"]:[dic objectForKey:@"nickname"];
        cell.userName.text = namrStr;
        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
        
        
    }
    return cell;
   // return cell;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _projectTable) {
        return _TempValueAryy.count;
 
    }
    else if(tableView == _fansTable){
        return _fansAryy.count;
    }
    return _attentionAryy.count;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == _projectTable) {
        
    
    ProDetailsViewController *pro;
    if (_TempValueAryy.count!=0) {
   
        pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:self.userID];
    }else
    {
        
        pro=[[ProDetailsViewController alloc]initWithData:nil  withUserID:nil];
    }
    
    [self.navigationController pushViewController:pro animated:YES];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return tableView == _projectTable ? 231 : 67;
}


- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}
@end
