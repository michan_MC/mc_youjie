//
//  ContactSelectionViewController.h
//  ChatDemo
//
//  Created by fenguoxl on 15/3/12.
//  Copyright (c) 2015年 fenguoxl. All rights reserved.
//

#import "EMChooseViewController.h"

@interface ContactSelectionViewController : EMChooseViewController
//已有选中的成员username，在该页面，这些成员不能被取消选择
- (instancetype)initWithBlockSelectedUsernames:(NSArray *)blockUsernames;
@end
