//
//  ContactsContactsViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/26.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ContactsViewController.h"

#import "EaseMob.h"
#import "SRRefreshView.h"
#import "ApplyViewController.h"
#import "UIViewController+HUD.h"
#import "ChineseToPinyin.h"
#import "ChatViewController.h"
#import "FriendDataViewController.h"
#import "BaseTableViewCell.h"
#import "ContactTableViewCell.h"
#import "UIImageView+EMWebCache.h"

//@interface ContactsViewController ()<UITableViewDataSource, UITableViewDelegate, SRRefreshDelegate, BaseTableCellDelegate, UIActionSheetDelegate>
@interface ContactsViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    
}


@end

@implementation ContactsViewController


-(instancetype)initWithFrame:(CGRect)frame
{

    self=[super initWithFrame:frame];
    if (self) {
        _dataSources=[NSMutableArray array];
        self.tableFooterView = [[UIView alloc] init];
        self.dataSource = self;
        self.delegate = self;
        [self addHeaderWithTarget:self action:@selector(myVermicelliHeaderRefresh)];
    }
    
    return  self;
}
-(void)myVermicelliHeaderRefresh{
    [self.attentionDalegat  MyContactsHeaderRefresh: 1];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    return  self.myFriendDataSource.count;
    return _dataSources.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"ContactTableViewCells";
    
    ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];
        
    }
    
    if (_dataSources.count > 0) {
        
        
        NSDictionary * dic = [_dataSources objectAtIndex:indexPath.row];
        NSString *imgUrl = @"";
        if ([dic objectForKey:@"portrait"] != [NSNull null]){
            imgUrl  = [@"http://120.25.218.167" stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
        }
        
        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"attentionUser"]:[dic objectForKey:@"nickname"];
        cell.userName.text = namrStr;
        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
        

        
        
//        NSDictionary * dic = _dataSources[[indexPath row]];
//        NSString *imgUrl = [@"http://120.25.218.167" stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
//        NSString *namrStr = [dic objectForKey:@"nickname"];
//        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
//        cell.userName.text= namrStr;
//        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
    }

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.attentionDalegat ContactsPushWithController:[_dataSources[[indexPath row]] objectForKey:@"id"]==[NSNull null]?@"LE":[_dataSources[[indexPath row]] objectForKey:@"id"]];
    
}


//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        
//
//        _dataSource = [NSMutableArray array];
//        _contactsSource = [NSMutableArray array];
//        _sectionTitles = [NSMutableArray array];
//    }
//    return self;
//}

//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    
//    //    [self searchController];
//
//    
//    self.tableView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - 100);
//    //    self.tableView.frame = CGRectMake(0, 100, 320, self.view.frame.size.height - 200);
//    [self.view addSubview:self.tableView];
//    
//    [self.tableView addSubview:self.slimeView];
//    [self.slimeView setLoadingWithExpansion];
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}
//
//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    [self reloadApplyView];
//}

//- (UITableView *)tableView
//{
//    if (_tableView == nil)
//    {
//        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _tableView.backgroundColor = [UIColor whiteColor];
//        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.tableFooterView = [[UIView alloc] init];
//    }
//    
//    return _tableView;
//}

//- (SRRefreshView *)slimeView
//{
//    if (_slimeView == nil) {
//        _slimeView = [[SRRefreshView alloc] init];
//        _slimeView.delegate = self;
//        _slimeView.upInset = 0;
//        _slimeView.slimeMissWhenGoingBack = YES;
//        _slimeView.slime.bodyColor = [UIColor grayColor];
//        _slimeView.slime.skinColor = [UIColor grayColor];
//        _slimeView.slime.lineWith = 1;
//        _slimeView.slime.shadowBlur = 4;
//        _slimeView.slime.shadowColor = [UIColor grayColor];
//    }
//    
//    return _slimeView;
//}

//#pragma mark - slimeRefresh Delegate
//- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView
//{
//    __weak ContactsViewController *weakSelf = self;
//    
//    //    __unsafe_unretained ContactsViewController *weakSelf = self;
//    [[[EaseMob sharedInstance] chatManager] asyncFetchBuddyListWithCompletion:^(NSArray *buddyList, EMError *error) {
//        if (!error) {
//            [weakSelf reloadDataSource];
//        }
//        [weakSelf.slimeView endRefresh];
//    } onQueue:nil];
//}


#pragma mark - dataSource
//- (void)reloadDataSource
//{
////    [self showHudInView:self.view hint:NSLocalizedString(@"refreshData", @"Refresh data...")];
////    [self.dataSource removeAllObjects];
////    [self.contactsSource removeAllObjects];
////    
////    NSArray *buddyList = [[EaseMob sharedInstance].chatManager buddyList];
////    for (EMBuddy *buddy in buddyList) {
////        if (buddy.followState != eEMBuddyFollowState_NotFollowed) {
////            [self.contactsSource addObject:buddy];
////        }
////    }
////    
////    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
////    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
////    if (loginUsername && loginUsername.length > 0) {
////        EMBuddy *loginBuddy = [EMBuddy buddyWithUsername:loginUsername];
////        [self.contactsSource addObject:loginBuddy];
////    }
////    
////    [self.dataSource addObjectsFromArray:[self sortDataArray:self.contactsSource]];
////    
////    [_tableView reloadData];
////    [self hideHud];
//}

//- (NSMutableArray *)sortDataArray:(NSArray *)dataArray
//{
//    //建立索引的核心
//    UILocalizedIndexedCollation *indexCollation = [UILocalizedIndexedCollation currentCollation];
//    
//    [self.sectionTitles removeAllObjects];
//    [self.sectionTitles addObjectsFromArray:[indexCollation sectionTitles]];
//    
//    //返回27，是a－z和＃
//    NSInteger highSection = [self.sectionTitles count];
//    //tableView 会被分成27个section
//    NSMutableArray *sortedArray = [NSMutableArray arrayWithCapacity:highSection];
//    for (int i = 0; i <= highSection; i++) {
//        NSMutableArray *sectionArray = [NSMutableArray arrayWithCapacity:1];
//        [sortedArray addObject:sectionArray];
//    }
//    
//    //名字分section
//    for (EMBuddy *buddy in dataArray) {
//        //getUserName是实现中文拼音检索的核心，见NameIndex类
//        NSString *firstLetter = [ChineseToPinyin pinyinFromChineseString:buddy.username];
//        NSInteger section = [indexCollation sectionForObject:[firstLetter substringToIndex:1] collationStringSelector:@selector(uppercaseString)];
//        
//        NSMutableArray *array = [sortedArray objectAtIndex:section];
//        [array addObject:buddy];
//    }
//    
//    //每个section内的数组排序
//    for (int i = 0; i < [sortedArray count]; i++) {
//        NSArray *array = [[sortedArray objectAtIndex:i] sortedArrayUsingComparator:^NSComparisonResult(EMBuddy *obj1, EMBuddy *obj2) {
//            NSString *firstLetter1 = [ChineseToPinyin pinyinFromChineseString:obj1.username];
//            firstLetter1 = [[firstLetter1 substringToIndex:1] uppercaseString];
//            
//            NSString *firstLetter2 = [ChineseToPinyin pinyinFromChineseString:obj2.username];
//            firstLetter2 = [[firstLetter2 substringToIndex:1] uppercaseString];
//            
//            return [firstLetter1 caseInsensitiveCompare:firstLetter2];
//        }];
//        
//        
//        [sortedArray replaceObjectAtIndex:i withObject:[NSMutableArray arrayWithArray:array]];
//    }
//    
//    return sortedArray;
//}


//#pragma mark - action
//
//- (void)reloadApplyView
//{
//    NSInteger count = [[[ApplyViewController shareController] dataSource] count];
//    
//    
//    if (count == 0) {
//        self.unapplyCountLabel.hidden = YES;
//    }
//    else
//    {
//        NSString *tmpStr = [NSString stringWithFormat:@"%i", count];
//        CGSize size = [tmpStr sizeWithFont:self.unapplyCountLabel.font constrainedToSize:CGSizeMake(50, 20) lineBreakMode:NSLineBreakByWordWrapping];
//        CGRect rect = self.unapplyCountLabel.frame;
//        rect.size.width = size.width > 20 ? size.width : 20;
//        self.unapplyCountLabel.text = tmpStr;
//        self.unapplyCountLabel.frame = rect;
//        self.unapplyCountLabel.hidden = NO;
//    }
//}


//- (UILabel *)unapplyCountLabel
//{
//    if (_unapplyCountLabel == nil) {
//        _unapplyCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, 20, 20)];
//        _unapplyCountLabel.textAlignment = NSTextAlignmentCenter;
//        _unapplyCountLabel.font = [UIFont systemFontOfSize:11];
//        _unapplyCountLabel.backgroundColor = [UIColor redColor];
//        _unapplyCountLabel.textColor = [UIColor whiteColor];
//        _unapplyCountLabel.layer.cornerRadius = _unapplyCountLabel.frame.size.height / 2;
//        _unapplyCountLabel.hidden = YES;
//        _unapplyCountLabel.clipsToBounds = YES;
//    }
//    
//    return _unapplyCountLabel;
//}


//#pragma mark - Table view data source
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return [self.dataSource count] + 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    if (section == 0) {
//        return 0;
//    }
//    return [[self.dataSource objectAtIndex:(section - 1)] count];
//}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{

//   BaseTableViewCell *cell;
//    
//    if (indexPath.section == 0 && indexPath.row == 0) {
//        cell = (BaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"FriendCell"];
//        if (cell == nil) {
//            cell = [[BaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FriendCell"];
//        }
//      }
//    else{
//        static NSString *CellIdentifier = @"ContactListCell";
//        cell = (BaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//        // Configure the cell...
//        if (cell == nil) {
//            cell = [[BaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//            cell.delegate = self;
//
//        }
//        cell.indexPath = indexPath;
//        if (indexPath.section == 0 && indexPath.row == 1) {
//        }
//        else{
//            EMBuddy *buddy = [[self.dataSource objectAtIndex:(indexPath.section - 1)] objectAtIndex:indexPath.row];
//            cell.imageView.image = [UIImage imageNamed:@"chatListCellHead.png"];
//            cell.textLabel.text = buddy.username;
//            [cell setTintColor:ColorString(@"#333333")];
//            [cell.textLabel setFont:[UIFont systemFontOfSize:13]];
//        }
//    }
//    return cell;
//}



//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    if (indexPath.section == 0) {
//        if (indexPath.row == 0) {
//            [self.navigationController pushViewController:[ApplyViewController shareController] animated:YES];
//        }
//        else if (indexPath.row == 1)
//        {
//
//        }
//    }
//    else{
//        EMBuddy *buddy = [[self.dataSource objectAtIndex:(indexPath.section - 1)] objectAtIndex:indexPath.row];
//        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
//        NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
//        if (loginUsername && loginUsername.length > 0) {
//            if ([loginUsername isEqualToString:buddy.username]) {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", @"Prompt") message:NSLocalizedString(@"你不能和自己聊天", @"can't talk to yourself") delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//                [alertView show];
//                return;
//            }
//        }
//
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
//        friVC.buddy = buddy;
//        [self pushNewViewController:friVC];
//
//    }
    
    
//}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 67;
//}

#pragma mark - BaseTableCellDelegate

//- (void)cellImageViewLongPressAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.section == 0 && indexPath.row == 1) {
//        // 群组
//        return;
//    }
//    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
//    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
//    
//    EMBuddy *buddy = [[self.dataSource objectAtIndex:(indexPath.section - 1)] objectAtIndex:indexPath.row];
//    if ([buddy.username isEqualToString:loginUsername])
//    {
//        return;
//    }
//    
//    _currentLongPressIndex = indexPath;
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") destructiveButtonTitle:NSLocalizedString(@"friend.block", @"join the blacklist") otherButtonTitles:nil, nil];
//    [actionSheet showInView:[[UIApplication sharedApplication] keyWindow]];
//}
//
//
//#pragma mark - scrollView delegate
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    [_slimeView scrollViewDidScroll];
//}
//
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//    [_slimeView scrollViewDidEndDraging];
//}
@end
