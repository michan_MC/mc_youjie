//
//  ContactsContactsViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/26.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//
@protocol ContactsViewController <NSObject>
- (void)ContactsPushWithController:(NSString*)sendToUser;
-(void)MyContactsHeaderRefresh:(NSInteger)page;

@end

@interface ContactsViewController : UITableView
@property(nonatomic,strong)NSArray *dataSources;
@property (weak, nonatomic) id <ContactsViewController> attentionDalegat;
// 好友请求变化时，更新好友请求未处理的个数
- (void)reloadApplyView;

// 群组变化时，更新群组页面
- (void)reloadGroupView;

// 好友个数变化时，重新获取数据
- (void)reloadDataSource;

// 添加好友的操作被触发
- (void)addFriendAction;
@end
