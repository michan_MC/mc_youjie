//
//  ChatViewController+Category.m
//  ChatDemo
//
//  Created by fenguoxl on 15/3/11.
//  Copyright (c) 2015年 fenguoxl. All rights reserved.
//

#import "ChatViewController+Category.h"

@implementation ChatViewController (Category)
- (void)registerBecomeActive{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}

- (void)didBecomeActive{
    [self reloadData];
}

@end
