//
//  ChatViewController+Category.h
//  ChatDemo
//
//  Created by fenguoxl on 15/3/11.
//  Copyright (c) 2015年 fenguoxl. All rights reserved.
//

#import "ChatViewController.h"

@interface ChatViewController (Category)
- (void)registerBecomeActive;
- (void)didBecomeActive;
@end
