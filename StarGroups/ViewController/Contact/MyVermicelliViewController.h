//
//  MyVermicelliViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/4/3.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

@protocol MyVermicelliDeleget <NSObject>
- (void)MyVermicPushWithController:(NSString*)sendToUser;
-(void)MyVermicFooterRefresh:(NSInteger)page;
-(void)MyVermicHeaderRefresh:(NSInteger)page;

@end
@interface MyVermicelliViewController : UITableView
@property (strong, nonatomic)NSMutableArray *dataSources;
@property(assign,nonatomic)NSInteger pageIndex;
@property (weak, nonatomic) id <MyVermicelliDeleget> MyVermiceattentionDalegat;
@end
