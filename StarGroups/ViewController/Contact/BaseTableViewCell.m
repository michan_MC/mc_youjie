//
//  BaseTableViewCell.m
//  ChatDemo
//
//  Created by fenguoxl on 15/3/11.
//  Copyright (c) 2015年 fenguoxl. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_bottomLineView];
        
        self.textLabel.backgroundColor = [UIColor blueColor];
        
        _headerLongPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(headerLongPress:)];
        [self addGestureRecognizer:_headerLongPress];
        
        
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(20, 67 - 1, self.frame.size.width - 20, 1)];
        [bottomView setBackgroundColor:ColorString(@"#cccccc")];
        [self addSubview:bottomView];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)headerLongPress:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        if (_delegate && _indexPath && [_delegate respondsToSelector:@selector(cellImageViewLongPressAtIndexPath:)]) {
            [_delegate cellImageViewLongPressAtIndexPath:self.indexPath];
        }
    }
}
@end
