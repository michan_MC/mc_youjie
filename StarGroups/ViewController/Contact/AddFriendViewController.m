//
//  AddFriendViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "AddFriendViewController.h"
#import "UIViewExt.h"
#import "MailListTableViewCell.h"
#import "MJRefresh.h"
#import "ApplyViewController.h"
#import "InvitationManager.h"
#import "CommeHelper.h"
#import "JXAddressBook.h"
#import "UIImageView+EMWebCache.h"
#import "FriendDataViewController.h"
@interface AddFriendViewController ()<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>
{
    UIView *mailView;
    UITextField *searchTextF;
    
    BOOL isClickAttention;

}
@property (nonatomic, strong) UITableView *searchTable;
@property (nonatomic, strong) NSMutableArray *dataSources;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSArray *dataArry;

@end

@implementation AddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dataArry=[[NSMutableArray alloc]initWithCapacity:20];
    _dataSources = [NSMutableArray array];
    isClickAttention = 1;
    
    self.title = @"添加好友";
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];
    [self addSearchBar];
    
    [self addMailListView];
    
    [self addTable];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addSearchBar
{
    UIImageView *searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, Main_Screen_Width - 60, 30)];
    [searchImageView setUserInteractionEnabled:YES];
    [searchImageView setImage:[UIImage imageNamed:@"搜索好友页-搜索框"]];
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(8, 7, 16, 16)];
    [searchIcon setImage:[UIImage imageNamed:@"添加朋友页-搜索icon"]];
    searchTextF = [[UITextField alloc] initWithFrame:CGRectMake(32, 7, searchImageView.width - 32, 16)];
    [searchTextF setPlaceholder:@"搜索好友名称"];
    [searchTextF setTextColor:[UIColor colorWithHexString:@"#666666"]];
    [searchTextF setFont:[UIFont systemFontOfSize:13]];
    [searchTextF setBorderStyle:UITextBorderStyleNone];
    [searchTextF setTextAlignment:NSTextAlignmentLeft];
    [searchTextF setClearButtonMode:UITextFieldViewModeAlways];
    searchTextF.returnKeyType = UIReturnKeySearch;
    searchTextF.delegate = self;
    [self.view addSubview:searchImageView];
    [searchImageView addSubview:searchIcon];
    [searchImageView addSubview:searchTextF];
    
}

-(void)addMailListView
{
    mailView = [[UIView alloc] initWithFrame:CGRectMake(0, 10 + 30 + 10 , Main_Screen_Width, 48)];
    [mailView setBackgroundColor:[UIColor whiteColor]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 17, 14, 18)];
    [imageView setImage:[UIImage imageNamed:@"添加朋友页-通讯录icon"]];
    
    UILabel *mailLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 17, Main_Screen_Width - 30, 18)];
    [mailLabel setText:@"查看手机通讯录好友"];
    [mailLabel setFont:[UIFont systemFontOfSize:13]];
    [mailLabel setTextColor:[UIColor colorWithHexString:@"#333333"]];
    
    UIButton *mailBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 48)];
    [mailBtn addTarget:self action:@selector(mailBtn) forControlEvents:UIControlEventTouchUpInside];
    
   // [mailView addSubview:imageView];
    //[mailView addSubview:mailLabel];
    //[mailView addSubview:mailBtn];
    
    
    [self.view addSubview:mailView];
}



- (void)addTable
{
    _searchTable = [[UITableView alloc] initWithFrame:CGRectMake(0,  20 + 30  + 10, Main_Screen_Width, Main_Screen_Height - 64 - 52) style:UITableViewStylePlain];
    
    _searchTable.delegate = self;
    _searchTable.dataSource = self;
    _searchTable.tableFooterView = [[UIView alloc] init];
    [self.searchTable addHeaderWithTarget:self action:@selector(friendSearchHeaderRefresh)];
    [self.searchTable addFooterWithTarget:self action:@selector(friendSearchFooterRefresh)];
    
    [self.view addSubview:_searchTable];
}


#pragma mark - refresh
- (void)friendSearchHeaderRefresh
{
    [self.searchTable headerEndRefreshing];
}

- (void)friendSearchFooterRefresh
{
    [self.searchTable footerEndRefreshing];
}



- (void)mailBtn
{
   // [self showLoading:YES AndText:@"正在加载"];
    
}

#pragma make - textfield Dalegate0
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField.text.length > 0) {
//#warning 由用户体系的用户，需要添加方法在已有的用户体系中查询符合填写内容的用户
//#warning 以下代码为测试代码，默认用户体系中有一个符合要求的同名用户
//        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
//        NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
//        if ([textField.text isEqualToString:loginUsername]) {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notAddSelf", @"can't add yourself as a friend") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//            [alertView show];
//            
//            return NO;
//        }
//        
//        //判断是否已发来申请
//        NSArray *applyArray = [[ApplyViewController shareController] dataSources];
//        if (applyArray && [applyArray count] > 0) {
//            for (ApplyEntity *entity in applyArray) {
//                ApplyStyle style = [entity.style intValue];
//                BOOL isGroup = style == ApplyStyleFriend ? NO : YES;
//                if (!isGroup && [entity.applicantUsername isEqualToString:textField.text]) {
//                    NSString *str = [NSString stringWithFormat:NSLocalizedString(@"friend.repeatInvite", @"%@ have sent the application to you"), textField.text];
//                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:str delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
//                    [alertView show];
//                    
//                    return NO;
//                }
//            }
//        }
        
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];

        
        [self searchText:[defaults objectForKey:@"UserName"] andSearchFiled:textField.text];

       
//        [self.dataSources addObject:textField.text];
        //[self.searchTable reloadData];
        
    }
    return YES;
}






-(void)searchText:(NSString*)UserID andSearchFiled:(NSString*)SearchFiled
{
    
    
    [self showLoading:YES AndText:@"正在加载"];
    
    
    
    NSDictionary *dic = @{
                          @"userId" : UserID,
                          @"beUserId" : SearchFiled
                          
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"searchFriendsRequest",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        
        
        NSArray *arry=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
     
        _dataArry =arry;
        

        [_searchTable reloadData];
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
   
    }];
    
    
}
#pragma mark-数据源方法
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *identify = @"mailLiistCell";
    
    MailListTableViewCell *contactCell = (MailListTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identify];
    if (contactCell == nil) {
        contactCell = [MailListTableViewCell mailList];
        contactCell.isMailVC = NO;
    }
    
    
    
    //[contactCell.inventBtn setTitle:[[_dataArry[[indexPath row]] objectForKey:@"isAttention"] boolValue]==false?@"关注":@"已关注" forState:UIControlStateNormal];
    
    if ([[_dataArry[[indexPath row]] objectForKey:@"isAttention"] boolValue]==false) {
        [contactCell.inventBtn setTitle:@"关注" forState:UIControlStateNormal];
        [contactCell.inventBtn setSelected:NO];
    }else
    {
        
        [contactCell.inventBtn setTitle:@"已关注" forState:UIControlStateNormal];
        [contactCell.inventBtn setSelected:YES];
    }
    
    [contactCell.inventBtn addTarget:self action:@selector(addFriendBtn:) forControlEvents:UIControlEventTouchUpInside];
    [contactCell.inventBtn setTag:[indexPath row]];
    if (_dataArry.count > 0) {
        
        NSDictionary * dic = [[_dataArry objectAtIndex:indexPath.row] objectForKey:@"user"];
        
        NSString *imgUrl = @"";
        if ([dic objectForKey:@"portrait"] != [NSNull null]){
            imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
        }
        
        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"username"]:[dic objectForKey:@"nickname"];
        contactCell.name.text = namrStr;
        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
        [contactCell.iconImage sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
    }

    return contactCell;

}


#pragma mark - tableView delegate datasource
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_dataArry.count > 0) {
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
        NSDictionary * dic_data = [[_dataArry objectAtIndex:indexPath.row] objectForKey:@"user"];
        
        friVC.sendToUser=[[dic_data objectForKey:@"id"] stringValue];
        [self pushNewViewController:friVC];

        //beAttentionUser
//        if ([dic objectForKey:@"portrait"] != [NSNull null]){
//            imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
//        }
//        
//        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"username"]:[dic objectForKey:@"nickname"];
//        contactCell.name.text = namrStr;
//        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
//        [contactCell.iconImage sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
    }

    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArry.count;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}



- (BOOL)hasSendBuddyRequest:(NSString *)buddyName
{
    NSArray *buddyList = [[[EaseMob sharedInstance] chatManager] buddyList];
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:buddyName] &&
            buddy.followState == eEMBuddyFollowState_NotFollowed &&
            buddy.isPendingApproval) {
            return YES;
        }
    }
    return NO;
}


- (BOOL)didBuddyExist:(NSString *)buddyName
{
    NSArray *buddyList = [[[EaseMob sharedInstance] chatManager] buddyList];
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:buddyName] &&
            buddy.followState != eEMBuddyFollowState_NotFollowed) {
            return YES;
        }
    }
    return NO;
}

- (void)addFriendBtna:(NSString *)op andRow:(NSInteger)row
{

    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    
    NSDictionary *dic = @{ @"userId" : [defaults objectForKey:@"UserName"],
                            @"beAttentionUser" : [[_dataArry[row] objectForKey:@"user"]objectForKey:@"username"],
                            @"op"   : op,
                            @"index" : @"1",
                            @"length"    :   @"10"
                            };
    
    
    NSDictionary *dict = @{
                           @"method" : @"attentionRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {

        NSString *messge;
        if ([op isEqual:@"guanzhu"]) {
            messge=@"关注成功";
        }else
        {
             messge=@"取消关注成功";
        }
        
        [self showAllTextDialog:messge];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];

    

}

#pragma mark - 点击关注按钮
- (void)addFriendBtn:(UIButton*)btn
{

    if ([btn.titleLabel.text isEqual:@"关注"]) {
       
        [self addFriendBtna:@"guanzhu" andRow:btn.tag];
        [btn setTitle:@"已关注" forState:UIControlStateNormal];
        [btn setSelected:YES];
    }
    else
    {
            //取消关注
        [self addFriendBtna:@"cancelAtt" andRow:btn.tag];
        [btn setTitle:@"关注" forState:UIControlStateNormal];
        [btn setSelected:NO];

    }
    
    
}

@end
