//
//  NearbyPeopleTableViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "NearbyPeopleTableViewController.h"
#import "ContactTableViewCell.h"
#import "MJRefresh.h"
#import "FriendDataViewController.h"

@interface NearbyPeopleTableViewController ()
{
    UIButton *rightBtn;
    UIView *alterViewBackground;
    
    BOOL isClickBtn;
}
@end

@implementation NearbyPeopleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"附近的人";
    
    rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 27)];
    [rightBtn setBackgroundImage:[UIImage imageNamed:@"附近的人页-显示框"] forState:UIControlStateNormal];
    [rightBtn setTitle:@"全部" forState:UIControlStateNormal];
    [rightBtn.titleLabel setFont:[UIFont systemFontOfSize:13]];
    [rightBtn setTitleColor:[UIColor blackColor]];
    [rightBtn addTarget:self action:@selector(ViewGenderBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightBtn];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:rightBtn]];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.tableView addFooterWithTarget:self action:@selector(footerRefresh)];
    [self.tableView addHeaderWithTarget:self action:@selector(headerRefresh)];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)ViewGenderBtn
{
    isClickBtn = !isClickBtn;
    
    if (isClickBtn) {
        [self showMaskView];
        
    }else{
        [alterViewBackground setHidden:YES];
    }
    
    [self.tableView setUserInteractionEnabled:!isClickBtn];
}

- (void)headerRefresh
{

}

- (void)footerRefresh
{
    
}


- (void)showMaskView {
    
    alterViewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 100)];
    [alterViewBackground setBackgroundColor:RGB(44/225.0, 44/225.0, 52/225.0)];
    [alterViewBackground setAlpha:0.7];
    [alterViewBackground setHidden:NO];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAlterViewBackground)];
    tapRecognizer.numberOfTouchesRequired = 1;
    [alterViewBackground addGestureRecognizer:tapRecognizer];
    [self.view addSubview:alterViewBackground];
    
    
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 51)];
    [btn1 setTag:100];
    [btn1 setCenter:CGPointMake(Main_Screen_Width/6, 50)];
    [btn1 setBackgroundImage:[UIImage imageNamed:@"附近的人页-全部icon"] forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 21)];
//    [label1 setTextAlignment:NSTextAlignmentCenter];
//    [label1 setCenter:CGPointMake(Main_Screen_Width/6, 80)];
//    [label1 setText:@"全部"];
//    [label1 setTextColor:[UIColor whiteColor]];
    
    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 51)];
    [btn2 setTag:101];
    [btn2 setCenter:CGPointMake(Main_Screen_Width/6 * 3, 50)];
    [btn2 setBackgroundImage:[UIImage imageNamed:@"附近的人页-男icon"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 21)];
//    [label2 setTextAlignment:NSTextAlignmentCenter];
//    [label2 setCenter:CGPointMake(Main_Screen_Width/6 * 3, 80)];
//    [label2 setText:@"只看男"];
//    [label2 setTextColor:[UIColor whiteColor]];
//    
    
    
    UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 38, 51)];
    [btn3 setTag:102];
    [btn3 setCenter:CGPointMake(Main_Screen_Width/6 * 5, 50)];
    [btn3 setBackgroundImage:[UIImage imageNamed:@"附近的人页-女icon"] forState:UIControlStateNormal];
    [btn3 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 21)];
//    [label3 setTextAlignment:NSTextAlignmentCenter];
//    [label3 setCenter:CGPointMake(Main_Screen_Width/6 * 5, 80)];
//    [label3 setText:@"只看女"];
//    [label3 setTextColor:[UIColor whiteColor]];
//    
    
    [alterViewBackground addSubview:btn1];
    [alterViewBackground addSubview:btn2];
    [alterViewBackground addSubview:btn3];
//    [alterViewBackground addSubview:label1];
//    [alterViewBackground addSubview:label2];
//    [alterViewBackground addSubview:label3];
}

- (void)clickBtn:(UIButton *)sender
{
    [self ViewGenderBtn];
    if (sender.tag == 100) {
        [rightBtn setTitle:@"全部" forState:UIControlStateNormal];
    }else if (sender.tag == 101)
    {
         [rightBtn setTitle:@"只看男" forState:UIControlStateNormal];
    }else if (sender.tag == 102){
         [rightBtn setTitle:@"只看女" forState:UIControlStateNormal];
    }
    
}

- (void)tapAlterViewBackground
{
    [self ViewGenderBtn];
}

#pragma mark - tableview代理，数据源
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"ContactTableViewCell";
    UITableViewCell *nearCell = [tableView dequeueReusableCellWithIdentifier:identify];
    if (nearCell == nil) {
        nearCell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];
    }
    return nearCell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
    
    [self pushNewViewController:friVC];
}

@end
