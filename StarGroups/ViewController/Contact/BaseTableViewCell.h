//
//  BaseTableViewCell.h
//  ChatDemo
//
//  Created by fenguoxl on 15/3/11.
//  Copyright (c) 2015年 fenguoxl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BaseTableCellDelegate;

@interface BaseTableViewCell : UITableViewCell
{
    UILongPressGestureRecognizer *_headerLongPress;
}

@property (weak, nonatomic) id<BaseTableCellDelegate> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) UIView *bottomLineView;
@end

@protocol BaseTableCellDelegate <NSObject>

- (void)cellImageViewLongPressAtIndexPath:(NSIndexPath *)indexPAth;

@end
