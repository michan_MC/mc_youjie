//
//  FriendDataViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseTableViewController.h"

@interface FriendDataViewController : BaseViewController

@property (nonatomic, strong) EMBuddy *buddy;
@property(nonatomic,strong)NSString *sendToUser;
@property (weak, nonatomic) IBOutlet UILabel *UserName;
@property (weak, nonatomic) IBOutlet UILabel *RealName;
@property (weak, nonatomic) IBOutlet UIImageView *Sex;
@property (weak, nonatomic) IBOutlet UILabel *HomeAdress;

@end
