//
//  MyVermicelliViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/4/3.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MyVermicelliViewController.h"
#import "MJRefresh.h"
#import "ContactTableViewCell.h"
#import "FriendDataViewController.h"
#import "CommeHelper.h"
#import "NetworkManager.h"
#import "UIImageView+EMWebCache.h"

/*我的粉丝*/
@interface MyVermicelliViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation MyVermicelliViewController



-(instancetype)initWithFrame:(CGRect)frame
{
    self= [super initWithFrame:frame];
    if (self) {
        self.pageIndex=1;
        _dataSources = [NSMutableArray array];
        
        self.tableFooterView = [[UIView alloc] init];
        self.delegate = self;
        self.dataSource = self;
 
        [self addFooterWithTarget:self action:@selector(myVermicelliFooterRefresh)];
        [self addHeaderWithTarget:self action:@selector(myVermicelliHeaderRefresh)];
        
    }
    
    return self;
}



- (void)myVermicelliFooterRefresh
{
    NSInteger index=self.pageIndex+1;
    self.pageIndex=index;
    [self.MyVermiceattentionDalegat  MyVermicFooterRefresh: index];
}
-(void)myVermicelliHeaderRefresh{
    
    [self.MyVermiceattentionDalegat  MyVermicHeaderRefresh: 1];
}



#pragma mark - TableViewDelegate & TableViewDatasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSources.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identify = @"ContactTableViewCell";
    
    ContactTableViewCell *cell = (ContactTableViewCell *)[tableView dequeueReusableCellWithIdentifier:identify];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:self options:nil] lastObject];
    }
    
    
    
    
    
    
    if (_dataSources.count > 0) {
        
        NSDictionary * dic = [_dataSources objectAtIndex:indexPath.row];
        NSString *imgUrl = @"";
        if ([dic objectForKey:@"portrait"] != [NSNull null]){
            imgUrl  = [url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]];
        }
        
        NSString *namrStr = [dic objectForKey:@"nickname"] == [NSNull null]?[dic objectForKey:@"attentionUser"]:[dic objectForKey:@"nickname"];
        cell.userName.text = namrStr;
        UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
        [cell.userImageView sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:image];
        

    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 67;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    [self.MyVermiceattentionDalegat MyVermicPushWithController:[[_dataSources objectAtIndex:[indexPath row]] objectForKey:@"attentionUser"]];
}

@end
