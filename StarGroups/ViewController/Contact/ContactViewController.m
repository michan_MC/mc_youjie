//
//  ContactViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/16.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ContactViewController.h"
#import "UIViewExt.h"
#import "HMSegmentedControl.h"
#import "MJRefresh.h"
#import "MailListTableViewCell.h"
#import "NearbyPeopleTableViewController.h"
#import "FriendDataViewController.h"
#import <AddressBook/AddressBook.h>
#import "MailListUser.h"
#import "AddFriendViewController.h"
#import "ContactViewController.h"
#import "MailListTableView.h"
#import "MyVermicelliViewController.h"
#import "MyAttentionTable.h"
#import "JXAddressBook.h"
#import "CommeHelper.h"
#import "ContactsViewController.h"
#import "EaseMob.h"


@interface ContactViewController () <UITableViewDataSource, UITableViewDelegate, myAttentionTableDegate,ContactsViewController,MyVermicelliDeleget>
{
    UITableView *mailListTable;

    UIView *alterViewBackground;
    
    NSMutableDictionary *mailListDataSource;
    
    ContactsViewController *myFriendVC;
    
    CGFloat viewHeigh;
    
    BOOL isAddBarClick;
    
    HMSegmentedControl *titleSegment;
    
    MailListTableView *mailLitsTable;
    MyVermicelliViewController *myVermicelliTableVC;
    MyAttentionTable *myAttentionTable;
    
    UIImageView *VermicelliView;
    
    
    
}
@property(nonatomic,strong)UIScrollView *mainScroll;
@end

@implementation ContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addRightBar];
    
    mailListDataSource = [[NSMutableDictionary alloc] initWithCapacity:4];
    
    viewHeigh = Main_Screen_Height-64-49-44;
    
    // 切换栏
    [self addTitleSegment];
    
    //    添加滚动
    [self addScrollView];
    

    
    //    添加我的好友
    [self myFriend];
    
    //    添加我的粉丝
    [self myVermicelli];
    
    //    添加我的关注
    [self myAttention];
    
    // 添加通讯录
    [self MailList];

    //加载数据
    [self lodAllData];
    
    [self loadisHaveNewVermicelli];
}



- (void)viewWillAppear:(BOOL)animated
{
//    [self firstRequest];
    [self uploadAddressBook];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addRightBar
{
//    UIBarButtonItem *searchBar = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"联系人页-搜索icon"] style:UIBarButtonItemStylePlain  target:self action:@selector(searchBarClick)];
    
    UIBarButtonItem *addBar = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"联系人页-添加icon"] style:UIBarButtonItemStylePlain target:self action:@selector(addBarClick)];
    NSArray *array = @[addBar];
    [self.navigationItem setRightBarButtonItems:array animated:YES];
}


- (void)addTitleSegment
{
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"通讯录", @"我的好友", @"我的粉丝", @"我关注的"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithHexString:@"#f5f5f5"];
    titleSegment.textColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.selectedTextColor = [UIColor colorWithHexString:@"#333333"];
    titleSegment.font = [UIFont systemFontOfSize:13];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-9"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //为其添加红点
     VermicelliView=[[UIImageView alloc] initWithFrame:CGRectMake(174+52, 11, 5, 5)];
    
    [VermicelliView setImage:[UIImage imageNamed:@"tips"]];
    [titleSegment addSubview:VermicelliView];
    
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.mainScroll scrollRectToVisible:CGRectMake(Main_Screen_Width * index, 44, Main_Screen_Width, weakSelf.view.height-44) animated:YES];
       
    }];
    
    [self.view addSubview:titleSegment];
}


- (void)addScrollView
{
    self.mainScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, Main_Screen_Width, viewHeigh)];
    self.mainScroll.contentSize = CGSizeMake(Main_Screen_Width * 4, viewHeigh);
    self.mainScroll.backgroundColor = [UIColor colorWithHexString:@"#e6e6e6"];
    self.mainScroll.delegate = self;
    self.mainScroll.pagingEnabled = YES;
    self.mainScroll.showsHorizontalScrollIndicator = NO;
    self.mainScroll.bounces = NO;
    [self.view addSubview:self.mainScroll];
    
    
}


- (void)MailList
{
    mailLitsTable = [[MailListTableView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, viewHeigh)];

    [self.mainScroll addSubview:mailLitsTable];
    

}

#pragma mark-我的朋友
- (void)myFriend
{
    
    myFriendVC = [[ContactsViewController alloc] initWithFrame:CGRectMake(Main_Screen_Width, 0, Main_Screen_Width, viewHeigh)];
    myFriendVC.attentionDalegat=self;
    [self.mainScroll addSubview:myFriendVC];
}



#pragma  mark - 我的粉丝
- (void)myVermicelli
{
    myVermicelliTableVC = [[MyVermicelliViewController alloc] initWithFrame:CGRectMake(Main_Screen_Width * 2, 0, Main_Screen_Width, viewHeigh)];
    myVermicelliTableVC.MyVermiceattentionDalegat=self;
    [self.mainScroll addSubview:myVermicelliTableVC];
  
}

#pragma mark - 我的关注
- (void)myAttention
{
    myAttentionTable = [[MyAttentionTable alloc] initWithFrame:CGRectMake(Main_Screen_Width * 3, 0, Main_Screen_Width, viewHeigh) style:UITableViewStylePlain];
    myAttentionTable.attentionDalegat = self;
    [self.mainScroll addSubview:myAttentionTable];


}


#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [titleSegment setSelectedSegmentIndex:page animated:YES];
}

#pragma mark - rightBar
- (void)searchBarClick
{
    isAddBarClick = NO;
    [alterViewBackground setHidden:YES];
    [self.mainScroll setUserInteractionEnabled:YES];
    
    AddFriendViewController *addFriendVC = [[AddFriendViewController alloc] init];
    [self pushNewViewController:addFriendVC];
}



- (void)addBarClick
{
    isAddBarClick = !isAddBarClick;
    if (isAddBarClick) {
        [self showMaskView];
    }else{
        [alterViewBackground setHidden:YES];
        [self.mainScroll setUserInteractionEnabled:YES];
    }
}

- (void)showMaskView {
    
    [self.mainScroll setUserInteractionEnabled:NO];
    
    alterViewBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 100)];
    [alterViewBackground setBackgroundColor:RGB(44/225.0, 44/225.0, 52/225.0)];
    [alterViewBackground setAlpha:0.7];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAlterViewBackground)];
    tapRecognizer.numberOfTouchesRequired = 1;
    [alterViewBackground addGestureRecognizer:tapRecognizer];
    
    [self.view addSubview:alterViewBackground];
    
    
    UIButton *nearBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 51, 49)];
    [nearBtn setCenter:CGPointMake(Main_Screen_Width/4, 50)];
    [nearBtn setBackgroundImage:[UIImage imageNamed:@"联系人页-附近的人icon"] forState:UIControlStateNormal];
    [nearBtn addTarget:self action:@selector(clickNearBtn) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *friendBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 51, 49)];
    [friendBtn setCenter:CGPointMake(Main_Screen_Width/4 * 3, 50)];
    [friendBtn setBackgroundImage:[UIImage imageNamed:@"联系人页-添加好友icon"] forState:UIControlStateNormal];
    [friendBtn addTarget:self action:@selector(clickFriendBtn) forControlEvents:UIControlEventTouchUpInside];

    [alterViewBackground addSubview:friendBtn];
    [alterViewBackground addSubview:nearBtn];
}

- (void)clickNearBtn
{
    [self tapAlterViewBackground];
    
    NearbyPeopleTableViewController *near = [[NearbyPeopleTableViewController alloc] init];
    [self pushNewViewController:near];
}

- (void)clickFriendBtn
{
    [self tapAlterViewBackground];
    
    AddFriendViewController *addFriendVC = [[AddFriendViewController alloc] init];
    [self pushNewViewController:addFriendVC];

    
}

- (void)tapAlterViewBackground
{
    [self.mainScroll setUserInteractionEnabled:YES];
    [alterViewBackground setHidden:YES];
    isAddBarClick = !isAddBarClick;
    
}

#pragma mark - 我的好友跳转
- (void)friendPushWithController
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
    [self pushNewViewController:friVC];
}

#pragma mark - 我的粉丝跳转

-(void)ContactsPushWithController:(NSString *)sendToUser
{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
    friVC.sendToUser=sendToUser;
    [self pushNewViewController:friVC];
    
}

#pragma mark-跳转喝下拉刷新
//我的关注跳转
-(void)attentionPushWithController:(NSString *)sendToUser
{
    [self pushController:sendToUser];
}

//粉丝跳转
-(void)MyVermicPushWithController:(NSString *)sendToUser
{
    [self pushController:sendToUser];
}

-(void)pushController:(NSString*)sendToUser
{
    
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
    friVC.sendToUser=sendToUser;
    [self pushNewViewController:friVC];
}

//粉丝下来刷新
-(void)MyVermicFooterRefresh:(NSInteger)page
{
    
   
}
-(void)MyVermicHeaderRefresh:(NSInteger)page{
    [self loadAllMyVermicelli:page];

}
-(void)MyContactsHeaderRefresh:(NSInteger)page{
    [self loadMyFriend];
}
-(void)MyAttentionHeaderRefresh:(NSInteger)page{
    [self lodeMyVermicelli];
}

#pragma mark-加载所有页面数据

-(void)lodAllData
{

    [self loadAllMyVermicelli:0];
    [self lodeMyVermicelli];
    [self loadMyFriend];
}
//判断是否有新的粉丝
-(void)loadisHaveNewVermicelli
{

    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    NSDictionary *song = @{@"userId" : username,
                           @"beAttentionUser" : @"",
                           @"op"   : @"newAttentionUser",
                           @"index" : @"1" ,
                           @"length"    :   @"10"
                           };
    
    
    
    NSDictionary *dict = @{
                           @"method"   :  @"attentionRequestHandler",
                           @"value"    :  [CommeHelper getEncryptWithString:song]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
    

        NSInteger cont=[[CommeHelper getDecryptWithString:resultDic[@"result"]] objectForKey:@"total"]==[NSNull null]? 0 :[[[CommeHelper getDecryptWithString:resultDic[@"result"]] objectForKey:@"total"] integerValue];
        if (cont>0) {
            [VermicelliView setHidden:NO];
        }else
        {
             [VermicelliView setHidden:YES];
        }
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [VermicelliView setHidden:YES];
    }];

    
    
}

//加载粉丝
-(void)loadAllMyVermicelli:(NSInteger)pageIndex
{
    
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    

    
    NSDictionary *song = @{@"userId" : username,
                           @"beAttentionUser" : @"",
                           @"op"   : @"query2",
                           @"index" : @"1",//pageIndex
                           @"length"    :   @"20"
                           };
    
    
    
    NSDictionary *dict = @{
                           @"method"   :  @"attentionRequestHandler",
                           @"value"    :  [CommeHelper getEncryptWithString:song]
                           };
    //G75cA4D8k7A8VJ04kCyzGjE3uobb2wcT9yJLIAovF51yMfcaoFq0Yh2TS2mVxFPDDs61MADh4guqjTnp7N/BQx/DiE5Be6SaeZ0m9pQ+I2TX9h3OGdw9hw==
   // NSString *decryptString = [AA3DESManager getDecryptWithString:@"GLKQFUHCwtu2YBQQQRRBRsyLs4RCP+AiIa2aOwXM9bUKNi8o+tR/EA==" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
 
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        if (![aryy isEqual:[NSNull null]]) {
            [myVermicelliTableVC.dataSources removeAllObjects];
            for (NSDictionary *dict in aryy) {
                [myVermicelliTableVC.dataSources addObject:dict];
            }
            
            [myVermicelliTableVC reloadData];

        }

        [myVermicelliTableVC footerEndRefreshing];
        [myVermicelliTableVC headerEndRefreshing];
        [self showLoading:NO AndText:nil];
        

    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
      
    }];

    
   
}

//加载关注
-(void)lodeMyVermicelli
{
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *song = @{@"userId" : username,
                           @"beAttentionUser" : @"",
                           @"op"   : @"query1",
                           @"index" : @"1",
                           @"length"    :   @"20"
                           };
    
    
    
    NSDictionary *dict = @{
                           @"method"   :  @"attentionRequestHandler",
                           @"value"    :  [CommeHelper getEncryptWithString:song]
                           };
    
 
    //G75cA4D8k7A8VJ04kCyzGjE3uobb2wcT9yJLIAovF51yMfcaoFq0Yh2TS2mVxFPDDs61MADh4gucjGlmrfS0kLxBX6r0P/tZo7pKrZvuHo2aJ0Jqe7pQ6w==
   // NSString *decryptString = [AA3DESManager getDecryptWithString:@"G75cA4D8k7A8VJ04kCyzGjE3uobb2wcT9yJLIAovF51yMfcaoFq0Yh2TS2mVxFPDDs61MADh4gucjGlmrfS0kLxBX6r0P/tZo7pKrZvuHo2aJ0Jqe7pQ6w==" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];

    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        NSMutableArray *dataAryy=[NSMutableArray arrayWithCapacity:20];
       
        for (NSInteger i=0; i<aryy.count; i++) {
            [dataAryy addObject:aryy[i]];
           
        }
        myAttentionTable.dataSources=dataAryy;
        
        [myAttentionTable reloadData];
        [myAttentionTable headerEndRefreshing];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //[self.tableView headerEndRefreshing];
        
        
    }];
    
}

//加载好友
-(void)loadMyFriend
{

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    
    NSDictionary *song = @{@"userId" : username,
                           @"loanUser" : @"",
                           @"op"   : @"appRegFriends",
                           @"index":@"1",
                           @"length":@"10"
                           
                           };
    
    
    
    NSDictionary *dict = @{
                           @"method"   :  @"addressBookFriendsRequest",
                           @"value"    :  [CommeHelper getEncryptWithString:song]
                           };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
 
        
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"userList"];
        
        myFriendVC.dataSources=aryy;
        
        [myFriendVC reloadData];
        [myFriendVC headerEndRefreshing];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
     
        
        
    }];
}

//粉丝下拉刷新
-(void)FooterRefresh:(NSInteger)page
{

    [self loadAllMyVermicelli:page];
}

#pragma mark- 上传通讯录
-(void)uploadAddressBook
{
    
//    
//    [JXAddressBook getPersonInfo:^(NSArray *personInfos) {
//        NSMutableArray *aryyData=[NSMutableArray arrayWithCapacity:10];
//         JXPersonInfo *personInfo = nil;
//        for (NSInteger i=0; i<personInfos.count; i++) {
//            personInfo=[personInfos objectAtIndex:i];
//            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//            [dict setObject:[personInfo.phone objectAtIndex:0] forKey:@"mobileNumber"];
//       
//            [dict setObject:personInfo.fullName forKey:@"addressBookUser"];
//            [aryyData addObject:dict];
//        }
//   
//
//        
//                    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//                    NSString *strUserName=[defaults objectForKey:@"UserName"];
//        
//                    NSDictionary *dic = @{
//        
//                                          @"userId":strUserName,
//                                          @"op" : @"saveAddressBook",
//                                          @"addressBookList":aryyData,
//                                          @"loanUser":strUserName
//                                          };
//        
//        
//                    NSDictionary *dict = @{
//                                           @"method" : @"addressBookFriendsRequest",
//                                           @"value"  :[CommeHelper getEncryptWithString:dic]
//                                           };
//        
//
//       // NetworkManager *requestManager=[[NetworkManager alloc]init];
//        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
//            
//       #warning 此处后期有需求在添加处理的逻辑代码
//            NSLog(@"===========================>上传通讯录成功");
//          
//        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
//                NSLog(@"===========================>上传通讯录失败%@",description);
//        }];
//
// 
//    }];
}

@end
