//
//  FriendDataViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/17.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "FriendDataViewController.h"
#import "FriendMoreDataViewController.h"
#import "ProDetailsViewController.h"
#import "ChatViewController.h"
#import "ApplyViewController.h"
#import "InvitationManager.h"
#import "CommeHelper.h"
#import "MePhotoView.h"
#import "UIButton+EMWebCache.h"
#define kbutWith 71
#define KbutHeigth 71
#define Duration 0.2

@interface FriendDataViewController ()
{
    UIButton *attentionBtn;
    UILabel * lbl_attention;
    UIButton *buttonNormal;
    UIButton *btnAddButtn;


}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) MePhotoView *mePhotoView;
@property (strong , nonatomic) NSMutableArray *itemArray;

@end

@implementation FriendDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _itemArray = [NSMutableArray array];

    self.title = @"个人资料";
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];

    
    [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height - 64)];
    [self.scrollView setBounces:NO];
    if(!isiPhone5){
        [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height + 80 + 100)];
        _scrollView.scrollEnabled = YES;
    }
    
    
    
    self.mePhotoView = [[MePhotoView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72*2+50)];
    [_scrollView addSubview:_mePhotoView];
    btnAddButtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [btnAddButtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    // [btnAddButtn setFrame:[self setNumber:0]];//第一个添加按钮
    [self.mePhotoView addSubview:btnAddButtn];
    
    
    
    
    
    
//    添加底部view
    [self addBottonView];
    
    UIBarButtonItem *moreBar = [[UIBarButtonItem alloc] initWithTitle:@"更多" style:UIBarButtonItemStylePlain target:self action:@selector(moreBarClick)];
    [self.navigationItem setRightBarButtonItem:moreBar animated:YES];
    
    [self loadData];
    [self loadData2];
    [self loadPhoto];
    
}
-(void)loadData2{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dict1=@{@"userId": strUserName,
                          @"beAttentionUser" : self.sendToUser==nil ? strUserName:self.sendToUser,
                          @"op" :@"isAttention",
                          @"index" : @"1",
                          @"length" : @"10"
                          };
    NSDictionary *dict2=@{
                          @"method" : @"attentionRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          
                          };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSString  *stringJson=[AA3DESManager getDecryptWithString:resultDic[@"result"] keyString:@"p2p_standard2_base64_key"  ivString:@"p2p_s2iv"];
        if ([stringJson isEqualToString:@"true"]) {
            lbl_attention.hidden = NO;
            attentionBtn.hidden = YES;
        }
        else{
            lbl_attention.hidden = YES;
            attentionBtn.hidden = NO;

        }
        
//        
//        NSString *cityName=[dic objectForKey:@"homeAddress"]==[NSNull null]?@"":[dic objectForKey:@"homeAddress"];
//        self.UserName.text=self.sendToUser==nil ? strUserName:self.sendToUser;
//        self.HomeAdress.text=cityName;
//        self.RealName.text=[dic objectForKey:@"nickname"]==[NSNull null]?@"":[dic objectForKey:@"nickname"];
//        
//        NSString *sex=[dic objectForKey:@"sex"]==[NSNull null]?@"":[dic objectForKey:@"sex"];
//        
//        if ([sex isEqual:@"女"]) {
//            self.Sex.image=[UIImage imageNamed:@"女icon"];
//            
//        }
//        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
//        [self showLoading:NO AndText:nil];
//        [self showAllTextDialog:description];
        
        
    }];
  
}
-(void)loadData
{
    [self showLoading:YES AndText:@"正在加载"];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dict1=@{@"userId":self.sendToUser==nil ? strUserName:self.sendToUser};
    NSDictionary *dict2=@{
                          @"method" : @"userInfoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          
                          };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSDictionary *dic =[CommeHelper getDecryptWithString:resultDic[@"result"]];

         NSString *cityName=[dic objectForKey:@"homeAddress"]==[NSNull null]?@"":[dic objectForKey:@"homeAddress"];
        self.UserName.text=self.sendToUser==nil ? strUserName:self.sendToUser;
        self.HomeAdress.text=cityName;
        self.RealName.text=[dic objectForKey:@"nickname"]==[NSNull null]?@"":[dic objectForKey:@"nickname"];
        
         NSString *sex=[dic objectForKey:@"sex"]==[NSNull null]?@"":[dic objectForKey:@"sex"];
        
        if ([sex isEqual:@"女"]) {
            self.Sex.image=[UIImage imageNamed:@"女icon"];
          
        }
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addBottonView
{
    
    
    
        UIView *bottonView = [[UIView alloc] initWithFrame:CGRectMake(0, 450, Main_Screen_Width, 60)];
        [bottonView setBackgroundColor:[UIColor colorWithHexString:@"#4f5056"]];
        [self.scrollView addSubview:bottonView];
   
        attentionBtn = [[UIButton alloc] initWithFrame:CGRectMake(62, 13, 70, 17)];
    lbl_attention = [[UILabel alloc]initWithFrame:CGRectMake(62, 19, 70, 17)];
    lbl_attention.textColor = [UIColor whiteColor];
    lbl_attention.text = @"已关注";
    lbl_attention.hidden = YES;
    [bottonView addSubview:lbl_attention];
        [attentionBtn setCenter:CGPointMake(Main_Screen_Width/4 + 20, 30)];
        [attentionBtn setImage:[UIImage imageNamed:@"联系人页-添加icon"] forState:UIControlStateNormal];
    
        [attentionBtn setTitle:@"关注" forState:UIControlStateNormal];
        [attentionBtn addTarget:self action:@selector(sendAttentionBtn) forControlEvents:UIControlEventTouchUpInside];
        [attentionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [attentionBtn setTitleColor:[UIColor whiteColor]];
        [bottonView addSubview:attentionBtn];
    
        UIButton *sendLetterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 17)];
        [sendLetterBtn setCenter:CGPointMake(Main_Screen_Width/4 * 3 - 20 , 30)];
        [sendLetterBtn setImage:[UIImage imageNamed:@"联系人详情页-私信icon"] forState:UIControlStateNormal];
        [sendLetterBtn addTarget:self action:@selector(sendLetterBtn) forControlEvents:UIControlEventTouchUpInside];
        [sendLetterBtn setTitle:@"私信" forState:UIControlStateNormal];
        [sendLetterBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [sendLetterBtn setTitleColor:[UIColor whiteColor]];
        [bottonView addSubview:sendLetterBtn];
    
    
    
    
//    UIView *bottonView = [[UIView alloc] initWithFrame:CGRectMake(0, Main_Screen_Height  - 60, Main_Screen_Width, 60)];
//    [bottonView setBackgroundColor:[UIColor colorWithHexString:@"#4f5056"]];
//    [self.scrollView addSubview:bottonView];
//    
//    
//    UIButton *attentionBtn = [[UIButton alloc] initWithFrame:CGRectMake(62, 13, 70, 17)];
//    [attentionBtn setCenter:CGPointMake(Main_Screen_Width/4 + 20, 30)];
//    [attentionBtn setImage:[UIImage imageNamed:@"联系人页-添加icon"] forState:UIControlStateNormal];
//    [attentionBtn setTitle:@"关注" forState:UIControlStateNormal];
//    [attentionBtn addTarget:self action:@selector(sendAttentionBtn) forControlEvents:UIControlEventTouchUpInside];
//    [attentionBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
//    [attentionBtn setTitleColor:[UIColor whiteColor]];
//    [bottonView addSubview:attentionBtn];
//    
//    UIButton *sendLetterBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 17)];
//    [sendLetterBtn setCenter:CGPointMake(Main_Screen_Width/4 * 3 - 20 , 30)];
//    [sendLetterBtn setImage:[UIImage imageNamed:@"联系人详情页-私信icon"] forState:UIControlStateNormal];
//    [sendLetterBtn addTarget:self action:@selector(sendLetterBtn) forControlEvents:UIControlEventTouchUpInside];
//    [sendLetterBtn setTitle:@"私信" forState:UIControlStateNormal];
//    [sendLetterBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
//    [sendLetterBtn setTitleColor:[UIColor whiteColor]];
//    [bottonView addSubview:sendLetterBtn];

}

#pragma mark - 点击私信
- (void)sendLetterBtn
{
    

    ChatViewController *chatVC = [[ChatViewController alloc] initWithChatter:_buddy.username isGroup:NO];
    if (self.sendToUser!=nil) {

        
        chatVC.title =  self.RealName .text= nil ? _sendToUser : self.RealName.text ;
        chatVC.SendToUser=self.RealName .text= [NSNull null] ? self.sendToUser : self.RealName.text;
        
    }else
    {
    
        chatVC.title = _buddy.username;
    }
    [self pushNewViewController:chatVC];

}


#pragma mark - 发送关注按钮
- (void)sendAttentionBtn
{
    UITextField *textField = [[UITextField alloc] init];
    textField.text = self.UserName.text;
    
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
    
    if ([textField.text isEqualToString:loginUsername]) {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"friend.notAddSelf", @"can't add yourself as a friend") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        [alertView show];
        
        return;
    }

    
    
//    判断是否已发来申请
    NSArray *applyArray = [[ApplyViewController shareController] dataSources];
    
    if (applyArray && [applyArray count] > 0) {
        for (ApplyEntity *entity in applyArray) {
            ApplyStyle style = [entity.style intValue];
            BOOL isGroup = style == ApplyStyleFriend ? NO : YES;
            if (!isGroup && [entity.applicantUsername isEqualToString:textField.text]) {
                NSString *str = [NSString stringWithFormat:NSLocalizedString(@"friend.repeatInvite", @"%@ have sent the application to you"), textField.text];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:str delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
                [alertView show];
                
                return;
            }
        }
    }
    
    if ([self didBuddyExist:textField.text]) {
        
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"已关注", @"'%@'has been your friend!"), textField.text];
        [WCAlertView showAlertWithTitle:message
                                message:nil
                     customizationBlock:nil
                        completionBlock:nil
                      cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                      otherButtonTitles: nil];
        
    }
    else if ([self hasSendBuddyRequest:textField.text])
    {
        
        NSString *message = [NSString stringWithFormat:NSLocalizedString(@"等待同意", @"you have send fridend request to '%@'!"), textField.text];
        
        [WCAlertView showAlertWithTitle:message
                                message:nil
                     customizationBlock:nil
                        completionBlock:nil
                      cancelButtonTitle:NSLocalizedString(@"ok", @"OK")
                      otherButtonTitles: nil];
        
    }else{
        
        NSString *messageStr = @"";
        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
        NSString *username = [loginInfo objectForKey:kSDKUsername];
        
        EMError *error;
        [[EaseMob sharedInstance].chatManager addBuddy:textField.text message:messageStr error:&error];
        if (error) {
            
            [self showAllTextDialog:@"send application fails, please operate again"];
        }
        else{
            NSLog(@"%@",textField.text);
            
            [self showAllTextDialog:@"申请发送成功"];
            if( [[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
            [self popoverPresentationController];
        //[self p]
        }

    }
}



- (BOOL)hasSendBuddyRequest:(NSString *)buddyName
{
    NSArray *buddyList = [[[EaseMob sharedInstance] chatManager] buddyList];
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:buddyName] &&
            buddy.followState == eEMBuddyFollowState_NotFollowed &&
            buddy.isPendingApproval) {
            return YES;
        }
    }
    return NO;
}


- (BOOL)didBuddyExist:(NSString *)buddyName
{
    NSArray *buddyList = [[[EaseMob sharedInstance] chatManager] buddyList];
    for (EMBuddy *buddy in buddyList) {
        if ([buddy.username isEqualToString:buddyName] &&
            buddy.followState != eEMBuddyFollowState_NotFollowed) {
            return YES;
        }
    }
    return NO;
}



- (void)moreBarClick
{
    FriendMoreDataViewController *friendMoreVC = [[FriendMoreDataViewController alloc] init];
    friendMoreVC.userID=self.sendToUser;
    [self pushNewViewController:friendMoreVC];
    
}
-(void)loadPhoto{
    if (!_sendToUser) {
        return;
    }
    
    NSDictionary *dict1=@{@"userId":_sendToUser,
                          @"op" : @"getToken"
                          
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        // NSArray *dic =(NSArray*)[CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSString *decryptString = [AA3DESManager getDecryptWithString:resultDic[@"result"] keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
        [self loadPhoto2:decryptString];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
    }];
    
    
    
    
}
-(void)loadPhoto2:(NSString *)key{
    NSDictionary *dict1=@{@"userId":_sendToUser,
                          @"op" : @"getUserPhoto",
                          @"key" : key,
                          
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSMutableArray * arry =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        int j = 0;
        [_itemArray removeAllObjects];
        for (int i = arry.count; i > 8; i--)
        {
            [arry removeObjectAtIndex:j];
            
        }
        _itemArray = [NSMutableArray arrayWithArray:arry];
        if (_itemArray.count) {
            [self test];
            
        }
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
    }];
    
}
-(void)test{
    for (int i=0; i<_itemArray.count; i++) {
        //        ALAsset *asset=assets[i];
        //http://7wy48d.com2.z0.glb.qiniucdn.com/
        
        
        NSString * imgStr = [NSString stringWithFormat:@"http://7wy48d.com2.z0.glb.qiniucdn.com/%@",_itemArray[i][@"fileName"]];
        UIButton *btn=[self addNormalBtn:i];
        
        UIImage *image=[UIImage imageNamed:@""];
        [btn sd_setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholderImage:image];
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}
-(UIButton *)addNormalBtn:(int)index
{
    
    NSInteger count=self.itemArray.count;
    buttonNormal=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonNormal.tag=count;
    [buttonNormal setImage:[UIImage imageNamed:@"个人资料页-图片缩略图】"] forState:UIControlStateNormal];
    //[buttonNormal setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    [buttonNormal setFrame:[self setNumber:index]];
    
    [self.mePhotoView addSubview:buttonNormal];
    
    
    return buttonNormal;
}
-(CGRect)setNumber:(NSInteger) number
{
    
    CGFloat y;
    CGFloat x;
    
    if (number>=4) {
        y=16+KbutHeigth;
        x=8+6*(number%4)+kbutWith*(number%4);
    }else
    {
        
        y=8;
        x=8+6*number+kbutWith*number;
    }
    
    CGRect rect=CGRectMake(x, y, kbutWith, KbutHeigth);
    
    
    
    return rect;
}

@end
