//
//  MeDataViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/16.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"
@interface MeDataViewController : BaseViewController
{
    
    UITableView *table;
    
    
    NSArray *dataSource;
    NSArray *provinceArray;
    NSArray *cityArray;
    
    NSIndexPath *provinceIndex;
    NSIndexPath *industryIndex;
    
  
}

@property (weak, nonatomic) IBOutlet UILabel *numbelLabel;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UILabel *provinceLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong , nonatomic) NSMutableArray *itemArray;
@property (strong , nonatomic) NSMutableArray *currentimgArray;

@property (weak, nonatomic) IBOutlet UIButton *manButton;
@property (weak, nonatomic) IBOutlet UIButton *womanButton;
@property (weak, nonatomic) IBOutlet UIButton *subMit;
@property (weak, nonatomic) IBOutlet UITextView *signatures;

@end
