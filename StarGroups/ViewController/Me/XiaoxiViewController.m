//
//  XiaoxiViewController.m
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "XiaoxiViewController.h"
#import "CommeHelper.h"

@interface XiaoxiViewController ()

@end

@implementation XiaoxiViewController




-(void)loadXiaoxi{
    NSDate *datenow = [NSDate date];
NSString *timeSp = [NSString stringWithFormat:@"%.f", (NSTimeInterval)[datenow timeIntervalSince1970]];
    
    
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    NSDictionary *dic = @{
                          @"time":timeSp,
                          @"op" :@"query",
                          @"length" :@"10",
                          @"index" :@"1"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"noticeRequest",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray * dics = [CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        if (dics.count) {
            [self showAllTextDialog:[NSString stringWithFormat:@"%d",dics.count]];
            return;
        }
    [self showAllTextDialog:@"没数据"];

    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        
        
        
        
    }];
    
   
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"消息";
    [self loadXiaoxi];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
