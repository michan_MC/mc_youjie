//
//  aboutViewController.m
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "aboutViewController.h"

@interface aboutViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;
    
    
    
}
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSArray *imageArray;

@end

@implementation aboutViewController

-(void)prepareUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    if(isiPhone5){
//    _tableView.scrollEnabled  = NO;
//    }
//    else{
//        _tableView.scrollEnabled  = YES;
//
//    }
    [self.view addSubview:_tableView];

    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 120;
    }
    return 44;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"meCell"];
    UIImageView *imageViewcon;
    UILabel *labelText;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"meCell"];
       // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        imageViewcon = [[UIImageView alloc] initWithFrame:CGRectMake(20, 14, 19, 19)];
        labelText = [[UILabel alloc] initWithFrame:CGRectMake(51, 0, cell.contentView.frame.size.width - 51 - 5, 44)];
        labelText.font = [UIFont systemFontOfSize:15];
        [labelText setTextColor:ColorString(@"#333333")];
        labelText.numberOfLines = 0;
        [cell addSubview:imageViewcon];
        [cell addSubview:labelText];
        // [cell addSubview:bottonView];
        
    }
     if (indexPath.row == 0) {
         
         imageViewcon.frame = CGRectMake((cell.contentView.frame.size.width - 60) / 2, 10, 60, 60);
         imageViewcon.image = [UIImage imageNamed: _imageArray[indexPath.row]];
         labelText.frame = CGRectMake(0, 90, cell.contentView.frame.size.width, 20);
         labelText.font = [UIFont systemFontOfSize:18];
         labelText.textAlignment = NSTextAlignmentCenter;
         NSString *title = _titleArray[indexPath.row];
         [labelText setText:title];
         return cell;
         
     }
    NSString *con = _imageArray[indexPath.row];
    [imageViewcon setImage:[UIImage imageNamed:con]];
    
    NSString *title = _titleArray[indexPath.row];
    [labelText setText:title];
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"关于我们";
    _titleArray = @[@"联系我们",@"http://www.youliketoo.com/",@"400-080-2283", @"youliketoo-com",@"有利可图网",@"2316870136",@"广州市白云机场路1600号壹汇创业产业园B1栋202"];
    _imageArray = @[@"我的页-个人资料icon",@"我的页-我的项目icon", @"我的页-我的钱包icon" , @"我的页-消息icon" , @"我的页-更多icon", @"我的页-更多icon",@"我的页-更多icon"];
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
