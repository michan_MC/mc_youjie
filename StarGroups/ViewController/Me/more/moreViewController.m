//
//  moreViewController.m
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "moreViewController.h"
#import "helpViewController.h"
#import "aboutViewController.h"
#import "AppDelegate.h"
#import "WMLabelAlertView.h"
@interface moreViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView;

}

@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSArray *imageArray;
@end

@implementation moreViewController

-(void)prepareUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 250) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.bounces  = NO;
    [self.view addSubview:_tableView];
    
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, 300, Main_Screen_Width - 40, 40)];
    [btn setTitle:@"安全退出" forState:UIControlStateNormal];
    // [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
    [btn addTarget:self action:@selector(dengchu) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
    [self.view addSubview:btn];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableVie{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"meCell"];
    UIImageView *imageViewcon;
    UILabel *labelText;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"meCell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        imageViewcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 14, 19, 19)];
        labelText = [[UILabel alloc] initWithFrame:CGRectMake(41, 13, 80, 21)];
        labelText.font = [UIFont systemFontOfSize:13];
        [labelText setTextColor:ColorString(@"#333333")];
        [cell addSubview:imageViewcon];
        [cell addSubview:labelText];
        // [cell addSubview:bottonView];
        
    }
   // if (indexPath.section == 0) {
        NSString *con = _imageArray[indexPath.row];
        [imageViewcon setImage:[UIImage imageNamed:con]];
        
        NSString *title = _titleArray[indexPath.row];
        [labelText setText:title];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        helpViewController  * help = [[helpViewController alloc]init];
        [self.navigationController pushViewController:help animated:YES];
    }
    else if(indexPath.row == 1){
        aboutViewController * about = [[aboutViewController alloc]init];
        [self.navigationController pushViewController:about animated:YES];
    }
    else if(indexPath.row == 2){//分享
        [self ShareCtlView:self];
    }

    
}
-(void)dengchu{
    
    WMLabelAlertView * alertView = [[WMLabelAlertView alloc]initWithTitle:@"" content:@"是否确定退出账号?" sureButtonTitle:@"取消"];
    [alertView.leftButton setTitle:@"确定" forState:UIControlStateNormal];
//    alertView.leftButton.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
//    [alertView.leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    alertView.leftButton.layer.borderWidth = 0.5;
//    alertView.leftButton.layer.cornerRadius = 5;
//    alertView.leftButton.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
    [alertView.leftButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside ];
    [alertView showInView:self.view];
    
    
    
    
    
    
   // AppDelegate *app = [UIApplication sharedApplication].delegate;
    
}
-(void)back{
        UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
       // [UIView animateWithDuration:1.0f animations:^{
            window.alpha = 0;
            window.frame = CGRectMake(0, window.bounds.size.width, 0, 0);
       // } completion:^(BOOL finished) {
            exit(0);
       //}];
    
 
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"更多";
    _titleArray = @[@"使用帮助",@"关于我们", @"分享给朋友"];
    _imageArray = @[@"我的页-个人资料icon",@"我的页-我的项目icon", @"我的页-我的钱包icon" , @"我的页-消息icon" , @"我的页-更多icon"];//, @"我的页-我的相册icon"
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
