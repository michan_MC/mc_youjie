//
//  helpViewController.m
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "helpViewController.h"

@interface helpViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView * _tableView;
    CGFloat _lblHeight;
    UIView *_headView;

    
}

@end

@implementation helpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"帮助使用";
   _headView = [self headView];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    // Do any additional setup after loading the view.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return _lblHeight + 10;
    }
    return 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *dentifier=@"myCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
        
        
    }
    if (indexPath.row == 0) {
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 44)];
        lbl.text = @"帮助";
        [cell.contentView addSubview:lbl];
    }
    else{
        [cell.contentView addSubview:_headView];
        
    }
    
    return cell;
}
-(UIView *)headView{
    
    UILabel* lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 5,self.view.frame.size.width, 15)];
    lbl1.text = @"提示";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.font = [UIFont systemFontOfSize:12];
    lbl1.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
    //    [self.view addSubview:lbl];
    
    NSString *str = @"1.请您输入的提现金额，以及银行账号信息准确无误。\n2.如果你填写的提现信息不正确可能会导致提现失败，由此产生的提现费用将不予返还。\n3.在双休日和法定节假日期间，用户可以申请提现，汇付天下会在下一个工作日进行处理。由此造成的不便，请多多谅解!\n4.平台禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确定，将该账户的使用。";
    //获取系统版本
    // float system_Version = [[[UIDevice currentDevice] systemVersion] floatValue];
    //根据不同的系统版本使用不同的API
    //    if (system_Version >= 7.0) {
    //        _lblHeight = [str boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 10, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] context:nil].size.height;
    //
    //    }
    //    else {
    //        _lblHeight = [str sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(self.view.frame.size.width - 10, 1000)].height;
    //    }
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 25,self.view.frame.size.width - 10 , 0)];
    lbl.numberOfLines = 0;
    lbl.text = str;
    //lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont systemFontOfSize:12];
    lbl.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
    // [self.view addSubview:lbl];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:10];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    lbl.attributedText = attributedString;
    //[self.view addSubview:lbl];
    [lbl sizeToFit];
    
    _lblHeight = 20 + lbl.frame.size.height;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, _lblHeight)];
    [view addSubview:lbl1];
    [view addSubview:lbl];
    return view;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
