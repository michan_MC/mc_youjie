#import "MeDataViewController.h"
#import "MePhotoView.h"
#import "WMLabelAlertView.h"
#import "RegisterOrFindPwdViewController.h"
#import "ChangePwdViewController.h"
#import "UIButton+EMWebCache.h"
#import "CommeHelper.h"
#import "NetworkManager.h"
#import "CommeHelper.h"
#import "MyPhotoController.h"
#import "UIColor+BFKit.h"
#import "MF_Base64Additions.h"
#import "ZYQAssetPickerController.h"
#import "QiniuSimpleUploader.h"
#import "QiniuResumableUploader.h"
#import "AA3DESManager.h"
#define kbutWith 71
#define KbutHeigth 71
#define Duration 0.2

#define Main_Screen_Height      [[UIScreen mainScreen] bounds].size.height
#define Main_Screen_Width       [[UIScreen mainScreen] bounds].size.width

#define imageViewHeight ((self.view.frame.size.width - 8 * 3 ) / 4)
//UIImagePickerControllerDelegate,UIAlertViewDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,ZYQAssetPickerControllerDelegate
@interface MeDataViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate,UIAlertViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ZYQAssetPickerControllerDelegate,QiniuUploadDelegate>
{
    NSString *title;
    WMLabelAlertView *alert;
    
    BOOL contain;
    CGPoint startPoint;
    CGPoint originPoint;
    
    UIButton *btnAddButtn;
    
    UIButton *buttonNormal;
    NSMutableDictionary *dictData;//个人信息
    
    NSMutableArray *_imgarry;
    NSMutableArray *_imgKeyArray;
    NSInteger _index;
    
    
//    UIButton *btnAddButtn;
//    
//    UIButton *buttonNormal;
//    
//    BOOL contain;
//    CGPoint startPoint;
//    CGPoint originPoint;

    
}
@property (strong, nonatomic) MePhotoView *mePhotoView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *infomationView;
@property (strong , nonatomic) NSMutableArray *delteItemArray;
@property (retain, nonatomic)QiniuRioPutExtra *extra;
@property (retain, nonatomic)QiniuResumableUploader *rUploader;
@property (retain, nonatomic)QiniuSimpleUploader *sUploader;
@property (copy, nonatomic)NSString *filePath;
@property (copy, nonatomic)NSString *lastResumableKey;
@property (copy, nonatomic)NSString *token;


@end

@implementation MeDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //isfis = NO;
        dictData=[[NSMutableDictionary alloc]init];
        _imgarry = [NSMutableArray array];
        _currentimgArray = [NSMutableArray array];

    }
    return self;
}
-(void)test{
    for (int i=0; i<_itemArray.count; i++) {
        //        ALAsset *asset=assets[i];
        //http://7wy48d.com2.z0.glb.qiniucdn.com/
        
        
        NSString * imgStr = [NSString stringWithFormat:@"http://7wy48d.com2.z0.glb.qiniucdn.com/%@",_itemArray[i][@"fileName"]];
        
        
        //        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        UIButton *btn=[self addNormalBtn];
        
        //图片显示在界面上
        //[btn setImage:tempImg forState:UIControlStateNormal];
        // [btn sd_setBackgroundImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal];
        UIImage *image=[UIImage imageNamed:@"个人资料页-添加图片icon-1"];
        [btn sd_setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholderImage:image];
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    _index = 0;

    self.title = @"编辑资料";
    self.scrollView.delegate = self;
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];
    //self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(myPhoto)];
    
    
//    self.mePhotoView = [[MePhotoView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72*2+50)];
//    self.mePhotoView.backgroundColor = [UIColor yellowColor ];
//    
    self.itemArray = [NSMutableArray array];
    self.delteItemArray=[NSMutableArray array];
    _imgKeyArray = [NSMutableArray array];
    self.mePhotoView = [[MePhotoView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72*2+50)];
  //  [self.view addSubview:self.mePhotoView];

    
    
    
    
    
    [self.infomationView setFrame:CGRectMake(0, self.mePhotoView.frame.size.height + 20, Main_Screen_Width, self.infomationView.frame.size.height)];
    
    [self.scrollView setContentSize:CGSizeMake(Main_Screen_Width, self.mePhotoView.frame.size.height + 80 + 55  + self.infomationView.frame.size.height + 25 + 43 + 22)];
    [self.scrollView addSubview:self.mePhotoView];
    
    table = [[UITableView alloc] initWithFrame:CGRectMake(10, 0, 260, 180) style:UITableViewStylePlain];
    table.delegate = self;
    table.dataSource = self;
    [self addImageBtn];
    [self loadData];//加载数据
    [self.subMit addTarget:self action:@selector(subMitVule) forControlEvents:UIControlEventTouchUpInside];
  
}
-(void)loadPhoto{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    NSDictionary *dict1=@{@"userId":strUserName,
                          @"op" : @"getToken"
                          
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        // NSArray *dic =(NSArray*)[CommeHelper getDecryptWithString:resultDic[@"result"]];
       self.token = [AA3DESManager getDecryptWithString:resultDic[@"result"] keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
        //[self loadPhoto2:decryptString];
        [self upPhoto];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        //        [self showLoading:NO AndText:nil];
        //        [self showAllTextDialog:description];
        //
        //        //  DLog(@"error==%@",error);
        //        NSLog(@"description ==%@", description);
    }];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)changePwd:(UIButton *)sender {
    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePwdViewController *changeVC = [storeBoard instantiateViewControllerWithIdentifier:@"ChangePwdViewController"];
    [self pushNewViewController:changeVC];
    
   

}


- (IBAction)clickManBtn:(UIButton *)sender
{
    [_manButton setSelected:YES];
    [_womanButton setSelected:NO];
    
    [dictData setObject:@"M" forKey:@"sex"];
    
}
- (IBAction)clickWomanBtn:(UIButton *)sender
{
    [_manButton setSelected:NO];
    [_womanButton setSelected:YES];
    [dictData setObject:@"F" forKey:@"sex"];
}

- (IBAction)ProvinceBtn:(UIButton *)sender {
    
//        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 350, 0, 0)];
//        [self.view addSubview:textField];
//        UIPickerView *pickerView = [[UIPickerView alloc]init];
//       [self setUpProvinceData];
//        // 1.3 设置选择指示器
//        [pickerView setShowsSelectionIndicator:YES];
//        [textField setInputView:pickerView];
//        [textField becomeFirstResponder];
//        [pickerView setDataSource:self];
//        [pickerView setDelegate:self];
//        [textField setHidden:YES];
    
    switch (sender.tag) {
            
            
        case 100:{
            title = @"请选择省份";
            [self setUpProvinceData];
            WMLabelAlertView *alert = [[WMLabelAlertView alloc] initWithTitle:title contentView:table];
            [alert show];
            //            点击按钮的方法中创建一个textview控件、设置为隐藏、通过它来成为第一响应者。
            
        }
            break;
        case 200 :{
            title = @"请选择城市";
            [self setUpCityData:provinceIndex];
            WMLabelAlertView *alert = [[WMLabelAlertView alloc] initWithTitle:title contentView:table];
            [alert show];
        }
            break;
        default:
            break;
    }
}


- (void)setUpProvinceData
{
    if (provinceArray.count==0) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"provincelist" ofType:@"json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:path];
        provinceArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:nil];
    }
    
    dataSource = provinceArray;
}

- (void)setUpCityData:(NSIndexPath *)indexPath
{
    NSDictionary *provinces = [provinceArray objectAtIndex:[indexPath row]];
    //取出当前这个省里所有的市
    cityArray = [provinces objectForKey:@"city"];
    
    dataSource = cityArray;
}


#pragma mark - Table view data source & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *CellIdentifier = @"ContactListCell";
    cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
    
    if ([title isEqualToString:@"请选择省份"]||[title isEqualToString:@"请选择城市"]) {
        NSDictionary *dic = [dataSource objectAtIndex:indexPath.row];
        cell.textLabel.text = [dic objectForKey:@"title"];
        
       
    }
    else {
        cell.textLabel.text = [dataSource objectAtIndex:indexPath.row];
        
        
    }

    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([title isEqualToString:@"请选择省份"]) {
        _provinceLabel.text = cell.textLabel.text;
        [dictData setValue:  [cell.textLabel.text stringByAppendingString:@"-"] forKey:@"homeAddress"];
        provinceIndex = indexPath;
    }
    else if ([title isEqualToString:@"请选择城市"]){
        _cityLabel.text = cell.textLabel.text;
        
        NSString *strProvince=[dictData objectForKey:@"homeAddress"];
        if (strProvince!=nil) {
             strProvince=[strProvince stringByAppendingString:cell.textLabel.text];
            
            
             [dictData setValue:  strProvince  forKey:@"homeAddress"];
        }
    }
    
    
    UIWindow *appWindow = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    for (UIView *view in appWindow.subviews) {
        if ([view isKindOfClass:[WMLabelAlertView class]]) {
            WMLabelAlertView *wm = (WMLabelAlertView *)view;
            [wm cancel:nil];
        }
    }
}


-(void)loadData
{

    [self showLoading:YES AndText:@"正在加载"];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];

    [dictData setObject:strUserName forKey:@"userId"];
    
    NSDictionary *dict1=@{@"userId":strUserName};
    NSDictionary *dict2=@{
                          @"method" : @"userInfoRequestHandler",
                        
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSDictionary *dic =[CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSLog(@"%@",dic);
        self.numbelLabel.text=[dic objectForKey:@"id"]==[NSNull null]?@"":[dic objectForKey:@"id"];
        
        self.nameField.text=[dic objectForKey:@"nickname"]==[NSNull null]?@"":[dic objectForKey:@"nickname"];
        
        [dictData setObject:self.nameField.text forKey:@"nickname"];
        
        NSString *sex=[dic objectForKey:@"sex"]==[NSNull null]?@"":[dic objectForKey:@"sex"];
        if ([sex isEqual:@"女"]) {
            
            [_manButton setSelected:NO];
            [_womanButton setSelected:YES];
            [dictData setObject:@"F" forKey:@"sex"];
        }else
        {
            [_manButton setSelected:YES];
            [_womanButton setSelected:NO];
            [dictData setObject:@"M" forKey:@"sex"];
        }
        
        if ([dic objectForKey:@"homeAddress"]!=[NSNull null]) {

            NSString *cityName=[dic objectForKey:@"homeAddress"];

            NSRange rang=[cityName rangeOfString:@"-"];
            
            NSString *cityLabel=[cityName substringFromIndex:NSMaxRange(rang)];
            
            NSString *provinceLabel=[cityName substringWithRange:NSMakeRange(0,rang.location)];
            
            self.provinceLabel.text=provinceLabel;
            
            self.cityLabel.text=cityLabel;
            
            [dictData setObject:cityName forKey:@"homeAddress"];
        }
      
        
        NSString *signature=@"";
        if ([dic objectForKey:@"signature"]!=[NSNull null]) {
            signature=[dic objectForKey:@"signature"];
        }
 
        self.signatures.text=signature;
        
        UIImage *image=[UIImage imageNamed:@"个人资料页-添加图片icon-1"];
        [dictData setObject:signature forKey:@"signature"];
        
            NSURL *strUrl;
            if ([dic objectForKey:@"portrait"]!=[NSNull null]) {
                strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]]];
            }//http://120.25.218.167/upload/20150506/10981430890856209.png
        //[btnAddButtn sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];
        [self showLoading:NO AndText:nil];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        //  DLog(@"error==%@",error);
        NSLog(@"description ==%@", description);
    }];
}



-(void)subMitVule
{

    //_itemArray
    
    
    NSString *nickname=self.nameField.text;
    
    if ([nickname isEqual:@""]) {
  
        [self alterMessge:@"昵称不能为空"];
        return;
    }
    [dictData setObject:nickname forKey:@"nickname"];
    
      NSString *signature=self.signatures.text;
 
    if ([signature isEqual:@""]) {
        
        [self alterMessge:@"个性签名不能为空"];
        return;
    }
    NSString *homeAddress=[dictData objectForKey:@"homeAddress"];
    
    if (homeAddress==nil ||[homeAddress isEqual:@""]) {
        [self alterMessge:@"请选择你的地址"];
        return;
    }
    
    [dictData setObject:signature forKey:@"signature"];
    
    [dictData setObject:@"modifyPersonalInfo" forKey:@"op"];
    //[dictData setObject:@"M" forKey:@"sex"];
    
    NSDictionary *dict=@{
                   
                         @"method" : @"modifyPersonalInfoRequest",
                         @"value"  :[CommeHelper getEncryptWithString:dictData]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        

        if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
        {
            
//             [UIView animateWithDuration:0.5f animations:^{
//                 //上传图片
//                 
//                 //[self.navigationController popViewControllerAnimated:YES];
//             }];
            [self loadPhoto];
        }
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"修改个人资料失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];

        }];
        
    }];
    
    

}
#pragma mark-  开始上传
-(void)upPhoto{
    
    NSMutableArray *imgDataArray = [NSMutableArray array];
     _sUploader.token = self.token;
    NSLog(@"%@",_token);
    if (!_imgarry.count) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    for (ALAsset * result in _imgarry) {
        
         NSString *urlStr = [NSString stringWithFormat:@"%@",result.defaultRepresentation.url];
        //------------------------根据图片的url反取图片－－－－－
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        NSURL *url=[NSURL URLWithString:urlStr];
    
        [assetLibrary assetForURL:url resultBlock:^(ALAsset *asset)  {
            UIImage *image=[UIImage imageWithCGImage:asset.thumbnail];
           // cellImageView.image=image;
            
            NSData * data;// = UIImagePNGRepresentation(image);
            if (UIImagePNGRepresentation(image)) {
                //返回为png图像。
                data = UIImagePNGRepresentation(image);
//                fileName=[@"123" stringByAppendingString:@".png"];
            }else {
                //返回为JPEG图像。
                data = UIImageJPEGRepresentation(image, 1.0);
//                fileName=[@"123" stringByAppendingString:@".jpeg"];
            }
            
            
            [imgDataArray addObject:data];
            
            if (imgDataArray.count == _imgarry.count) {
                int i = 0;
                for (NSData * data1 in imgDataArray) {
                    
                    QiniuSimpleUploader *  sUploader = [QiniuSimpleUploader uploaderWithToken:self.token];
                    
                    sUploader.delegate = self;

                    [sUploader uploadFileData:data1 key:[NSString stringWithFormat:@"mcwqwqwqwww%d",arc4random() % 10000] extra:nil];
                    i++;
                }
  
            }
            
            
        }failureBlock:^(NSError *error) {
            NSLog(@"error=%@",error);
            [self showLoading:NO AndText:nil];

        }
         
         ];
    }
    
}
- (void)uploadSucceeded:(NSString *)theFilePath ret:(NSDictionary *)ret
{
    NSString *succeedMsg = [NSString stringWithFormat:@"Upload Succeeded: - Ret: %@\n", ret];
    NSLog(@"========%@",succeedMsg);
    [self upPhoto3:theFilePath];
    NSLog(@"%@",theFilePath);
    //[_imgKeyArray addObject:[ret objectForKey:@"key"]];

}
//-(void)upyoujiePhtoto:(NSString *)key{
//    [self showLoading:YES AndText:nil];
//
//    NetworkManager*  requestManager1 = [NetworkManager instanceManager];
//    requestManager1.needSeesion = YES;
//    NSString *indexStr = [NSString stringWithFormat:@"%ld",(long)_index];
//    _index++;
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    NSString *strUserName=[defaults objectForKey:@"UserName"];
//    
//    NSDictionary *dict1=@{@"userId":strUserName,
//                          @"op" : @"uploadPic",
//                          @"type" : @"2",
//                          @"fileName" :key
//                          };
//    NSDictionary *dict2=@{
//                          @"method" : @"appUploadPhotoRequest",
//                          
//                          @"value"  :[CommeHelper getEncryptWithString:dict1]
//                          };
//
//    
//    [requestManager1 requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
//        [self showAllTextDialog:@"修改个人资料成功"];
//        [self showLoading:NO AndText:nil];
//
//        //[self text];
//        NSLog(@"成功");
//    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
//        [self showLoading:NO AndText:nil];
//
//        
//        
//        
//    }];
//
//    
//    
//    
//}
-(void)upPhoto3:(NSString *)key {
    [self showLoading:YES AndText:nil];

  NetworkManager*  requestManager1 = [NetworkManager instanceManager];
    requestManager1.needSeesion = YES;
    NSString *indexStr = [NSString stringWithFormat:@"%ld",(long)_index];
    _index++;
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dict1=@{@"userId":strUserName,
                          @"op" : @"save",
                         // @"order" : indexStr,
                          @"key" :key
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    [requestManager1 requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];
[self showAllTextDialog:@"修改个人资料成功"];
        [self.navigationController popViewControllerAnimated:YES ];
        //[self text];
        NSLog(@"成功");
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {

    
        [self showLoading:NO AndText:nil];

    
    }];
    
    
}

// Upload failed
- (void)uploadFailed:(NSString *)theFilePath error:(NSError *)error
{
    NSString *failMsg = [NSString stringWithFormat:@"Upload Failed: %@  - Reason: %@", theFilePath, error];
    NSLog(@">>>>>>>%@",failMsg);
   // self.msg.text = [failMsg stringByAppendingString:self.msg.text];
    [self showLoading:NO AndText:nil];

}

- (NSString *) timeString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat: @"yyyyMMddHHmmss"];
    return [formatter stringFromDate:[NSDate date]];
}

-(void)alterMessge:(NSString*)message
{
    
    UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
    
    [alter show];
}


/*－－－－－－－－－－－－－－－－－－－－－－－－－－－－－上传生活照－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－*/
-(void)UploadGeneralLighting:(UIImage*)image
{

    
    NSData *data;
    NSString *fileName;
    if (UIImagePNGRepresentation(image)) {
                    //返回为png图像。
                    data = UIImagePNGRepresentation(image);
                    fileName=[@"123" stringByAppendingString:@".png"];
                }else {
                    //返回为JPEG图像。
                    data = UIImageJPEGRepresentation(image, 1.0);
                    fileName=[@"123" stringByAppendingString:@".jpeg"];
                }
    NSString *base64Image=[MF_Base64Codec base64StringFromData:data];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"image":base64Image,
                        @"fileName":fileName,
                        @"type":@"2",
                        @"op":@"uploadPic"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"appUploadPhotoRequest",
                         @"value":[CommeHelper getJson:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
       
       // NSDictionary *dict=[CommeHelper getDecryptWithString:[resultDic objectForKey:@"result"]];
//         NSLog(@"resultDic=================>%@",resultDic);
        [self showAllTextDialog:@"上传生活照成功"];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"上传图片失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
    }];

}



//-(void)addImageBtn
//{
//    
//    btnAddButtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    [btnAddButtn setImage:[UIImage imageNamed:@"个人资料页-添加图片icon-1"] forState:UIControlStateNormal];
//    [btnAddButtn setTag:0];
    //[btnAddButtn setFrame:CGRectMake(((self.mePhotoView.frame.size.width/2)-(kbutWith/2)), ((self.mePhotoView.frame.size.height/2)-(KbutHeigth/2)), kbutWith, KbutHeigth)];//第一个添加按钮
//    
//    [btnAddButtn addTarget:self action:@selector(addOtherButton:) forControlEvents:UIControlEventTouchUpInside];
//
//    [self.mePhotoView addSubview:btnAddButtn];
//    
//}

-(void)addOtherButton:(UIButton*)btn
{
    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                    initWithTitle:nil
                                    delegate:self
                                    cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                    otherButtonTitles: @"从相册选择", @"拍照",nil];
    
    [myActionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self localPhto];
            break;
        case 1:
            [self takePhoto];
            
            break;
        default:
            break;
    }
    
}
//-(void)localPhto
//{
//    
//    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//    //资源类型为图片库
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.delegate = self;
//    //设置选择后的图片可被编辑
//    picker.allowsEditing = YES;
//    // [self presentModalViewController:picker animated:YES];
//    [self presentViewController:picker animated:YES completion:nil];
//}


//-(void)takePhoto{
//    //资源类型为照相机
//    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
//    //判断是否有相机
//    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        //设置拍照后的图片可被编辑
//        picker.allowsEditing = YES;
//        //资源类型为照相机
//        picker.sourceType = sourceType;
//        [self presentViewController:picker animated:YES completion:nil];
//        
//    }else {
//        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:@"该设备无摄像头" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alter show];
//    }
//    
//    
//}
#pragma Delegate method UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image=[info objectForKey:@"UIImagePickerControllerEditedImage"];
    //当图片不为空时显示图片并保存图片
    if (image != nil) {
        
        UIButton *btn=btnAddButtn;
        //图片显示在界面上
        [btn setImage:image forState:UIControlStateNormal];
        
        //上传图片到友借
        
        [self UploadGeneralLighting:image];
#warning 以下代码视后期需求变化自己判断是否放出
        
        //        //以下是保存文件到沙盒路径下
        //        //把图片转成NSData类型的数据来保存文件
        //        NSData *data;
        //        //判断图片是不是png格式的文件
        //        if (UIImagePNGRepresentation(image)) {
        //            //返回为png图像。
        //            data = UIImagePNGRepresentation(image);
        //        }else {
        //            //返回为JPEG图像。
        //            data = UIImageJPEGRepresentation(image, 1.0);
        //        }
        //        //保存
        //        [[NSFileManager defaultManager] createFileAtPath:self.imagePath contents:data attributes:nil];
        
    }
    //关闭相册界面
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)myPhoto
{

    MyPhotoController *myPhoto=[[MyPhotoController alloc] init];
    
    [self.navigationController pushViewController:myPhoto animated:YES];
}
/**------------------------------拖动效果--------------------------------------------**/

-(void)addImageBtn
{
    
    btnAddButtn=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnAddButtn setImage:[UIImage imageNamed:@"个人资料页-添加图片icon-1"] forState:UIControlStateNormal];
    [btnAddButtn setTag:0];
    [btnAddButtn setFrame:[self setNumber:_currentimgArray.count]];//第一个添加按钮
    [btnAddButtn addTarget:self action:@selector(addOtherButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.itemArray addObject:btnAddButtn];
    [self.mePhotoView addSubview:btnAddButtn];
    
}


//-(void)addOtherButton:(UIButton*)btn
//{
//    
//    
//    UIActionSheet *myActionSheet = [[UIActionSheet alloc]
//                                    initWithTitle:nil
//                                    delegate:self
//                                    cancelButtonTitle:@"取消"
//                                    destructiveButtonTitle:nil
//                                    otherButtonTitles: @"从相册选择", @"拍照",nil];
//    
//    [myActionSheet showInView:self.view];
//    
//    
//}
//

//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    switch (buttonIndex) {
//        case 0:
//            [self localPhto];
//            break;
//        case 1:
//            [self takePhoto];
//            
//            break;
//        default:
//            break;
//    }
//    
//}

-(void)localPhto
{
    
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] init];
    picker.maximumNumberOfSelection = 8;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate=self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}


#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    _imgarry = (NSMutableArray*)assets;
    
    NSInteger count=assets.count;
    
    if ((self.itemArray.count+assets.count)>9) {
        count=8-self.itemArray.count+1;
        //[self showMessge:@"所选的图片超过了8张"];
    }
    
    for (int i=0; i<count; i++) {
        ALAsset *asset=assets[i];
        
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        UIButton *btn=[self addNormalBtn];
        //图片显示在界面上
        [btn setImage:tempImg forState:UIControlStateNormal];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}






-(void)takePhoto{
    //资源类型为照相机
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    //判断是否有相机
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        //资源类型为照相机
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
        
    }else {
        UIAlertView *alter=[[UIAlertView alloc]initWithTitle:@"提示" message:@"该设备无摄像头" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alter show];
    }
    
    
}



-(UIButton *)addNormalBtn
{
    
    NSInteger count=self.itemArray.count;
    buttonNormal=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonNormal.tag=count;
    [buttonNormal setImage:[UIImage imageNamed:@"个人资料页-图片缩略图】"] forState:UIControlStateNormal];
    //[buttonNormal setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    [buttonNormal setFrame:[self setNumber:self.itemArray.count]];
    [self.mePhotoView addSubview:buttonNormal];
    UIButton  *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    float w = 20;
    float h = 20;
    
    
    [deleteButton setImage:[UIImage imageNamed:@"deletbutton.png"] forState:UIControlStateNormal];
    deleteButton.backgroundColor = [UIColor clearColor];
    
    [deleteButton addTarget:self action:@selector(removeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //[deleteButton setHidden:YES];
    deleteButton.tag=buttonNormal.tag ;
    [buttonNormal addSubview:deleteButton];
    
    [self.delteItemArray addObject:deleteButton];
    
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(buttonLongPressed:)];
    [buttonNormal addGestureRecognizer:longGesture];
    //[deleteButton addGestureRecognizer:longGesture];
    
    [self.itemArray addObject:buttonNormal];
    
    //点击后切换添加按钮的位置
    
    CGRect tempRect=btnAddButtn.frame;
    [UIView animateWithDuration:0.2 animations:^{
        if (count==8) {
            CGFloat x=btnAddButtn.frame.origin.x+KbutHeigth+16;
            CGFloat y=btnAddButtn.frame.origin.y;
            
            btnAddButtn.frame=CGRectMake(x, y, kbutWith, KbutHeigth);
        }else
        {
            
            [btnAddButtn setFrame:buttonNormal.frame];
            
        }
        [buttonNormal setFrame:tempRect];
        
        [deleteButton setFrame:CGRectMake(0,0, w, h)];
        
    } completion:^(BOOL finished) {
        return ;
    }];
    
    return buttonNormal;
}


-(void)removeButtonClicked:(UIButton *)btn
{
    
    
    UIButton *btnToBeDelete=(UIButton*)[btn superview];
    int index=(int)[_itemArray indexOfObject:btnToBeDelete]-1;
    
    UIButton *item=btnToBeDelete;
    [_itemArray removeObject:btnToBeDelete];
    [UIView animateWithDuration:0.2 animations:^{
        
        if (_itemArray.count==1) {
            [btnAddButtn setFrame:[self setNumber:0]];
            
        }else{
            
            CGRect lastFrame=item.frame;
            CGRect curFrame;
            for (int i=(index+1) ; i<self.itemArray.count; i++) {
                UIButton *temp=[self.itemArray objectAtIndex:i];
                curFrame=temp.frame;
                [temp setFrame:lastFrame];
                lastFrame=curFrame;
            }
            
            [btnAddButtn setFrame:lastFrame];
            
        }
        
    } completion:^(BOOL finished) {
        return ;
    }];
    
    [item removeFromSuperview];
    item=nil;
    
    
    
}

-(CGRect)setNumber:(NSInteger) number
{
    
    CGFloat y;
    CGFloat x;
    
    if (number>=4) {
        y=16+KbutHeigth;
        x=8+6*(number%4)+kbutWith*(number%4);
    }else
    {
        
        y=8;
        x=8+6*number+kbutWith*number;
    }
    
    CGRect rect=CGRectMake(x, y, kbutWith, KbutHeigth);
    
    
    
    return rect;
}



- (void)buttonLongPressed:(UILongPressGestureRecognizer *)sender
{
    
    UIButton *btn = (UIButton *)sender.view;
    if (sender.state == UIGestureRecognizerStateBegan)
    {
        startPoint = [sender locationInView:sender.view];
        originPoint = btn.center;
        [UIView animateWithDuration:Duration animations:^{
            
            btn.transform = CGAffineTransformMakeScale(1.1, 1.1);
            btn.alpha = 0.7;
        }];
        
    }
    else if (sender.state == UIGestureRecognizerStateChanged)
    {
        
        CGPoint newPoint = [sender locationInView:sender.view];
        CGFloat deltaX = newPoint.x-startPoint.x;
        CGFloat deltaY = newPoint.y-startPoint.y;
        btn.center = CGPointMake(btn.center.x+deltaX,btn.center.y+deltaY);
        
        NSInteger index = [self indexOfPoint:btn.center withButton:btn];
        
        if (index<0)
        {
            contain = NO;
        }
        else
        {
            [UIView animateWithDuration:Duration animations:^{
                CGPoint temp = CGPointZero;
                UIButton *button = _itemArray[index];
                temp = button.center;
                button.center = originPoint;
                btn.center = temp;
                
                
                originPoint = btn.center;
                contain = YES;
                
                NSInteger indexBtn=[_itemArray indexOfObject:btn];
                UIButton *tempBtn=  _itemArray[index];
                
                _itemArray[index]=btn;
                _itemArray[indexBtn]=tempBtn;
                
                
            }];
        }
        
        
    }
    else if (sender.state == UIGestureRecognizerStateEnded)
    {
        [UIView animateWithDuration:Duration animations:^{
            
            btn.transform = CGAffineTransformIdentity;
            btn.alpha = 1.0;
            if (!contain)
            {
                btn.center = originPoint;
            }
        }];
    }
}

- (NSInteger)indexOfPoint:(CGPoint)point withButton:(UIButton *)btn
{
    for (NSInteger i = 0;i<_itemArray.count;i++)
    {
        UIButton *button = _itemArray[i];
        if (button != btn&&button!=btnAddButtn)
        {
            if (CGRectContainsPoint(button.frame, point))
            {
                return i;
            }
        }
        
    }
    return -1;
}


/**-------------------------------上传图片-------------------------------------------***/
//上传照片第一步获取Token
-(void)getToken
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"image":@"",
                        @"image":@"",
                        @"fileName":@"",
                        @"type":@"",
                        @"op":@"getToken"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"appUploadPhotoRequest",
                         //@"value"  :[CommeHelper getEncryptWithString:dic]
                         @"value":[CommeHelper getJson:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        //NSLog(@"resultDic=================>%@",resultDic);
        NSDictionary *dict=[CommeHelper getDecryptWithString:[resultDic objectForKey:@"result"]];
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"获取Token失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        //[self alterMessge:description];
        
    }];
    
}

-(void)uplodaPhoto
{
    //开启后台线程上传图片
    //[NSThread detachNewThreadSelector:@selector(updataImageToSenve) toTarget:self withObject:nil];
    
    
}

//把照片上传到 7牛云端
-(void)updataImageToSenve
{
    
    //先获取Token（上传7牛云端需此参数） 此处要集成7牛云端 直接调用七牛的sdk方法循环的上传8张图片
    
    //每次图片修改或者排序改变需要把排序改变的顺序上传到友借 如果照片修改了需要上传到7牛并把修改的图片顺序重新上传到友借
}

-(void)savePhote
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"key":@"",//文件名称
                        @"op":@"save"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"uploadPhotoRequestHandler",
                         @"value"  :[CommeHelper getEncryptWithString:dic]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        NSLog(@"resultDic=================>%@",resultDic);
        
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"上传图片失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        
    }];
}


@end