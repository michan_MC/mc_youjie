//
//  MCViewController.m
//  StarGroups
//
//  Created by michan on 15/5/12.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MCViewController.h"
#import "CommeHelper.h"
#import "ChangePwdViewController.h"
#import "MeDataViewController.h"
#import "MePhotoView.h"
#import "UIButton+EMWebCache.h"
#define kbutWith 71
#define KbutHeigth 71
#define Duration 0.2

@interface MCViewController ()<UIScrollViewDelegate>
{
    UIScrollView * _scrollview;
    UILabel * lbl_zhanghao;
    UILabel * lbl_name;
    UILabel * lbl_sex;
    UILabel * lbl_dizhi;
    UILabel * lbl_sing;
    UIButton *btnAddButtn;
    BOOL isfis;
    UIButton * btn;
    UIButton *buttonNormal;

}
@property (strong, nonatomic) MePhotoView *mePhotoView;
@property (strong , nonatomic) NSMutableArray *itemArray;

@end

@implementation MCViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isfis = NO;
        _itemArray = [NSMutableArray array];
        // _dicData = [NSDictionary dictionary];
    }
    return self;
}
-(void)prepareUI{
    isfis = YES;
    _scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height)];
    if (!IS_IPHONE_5) {
        [_scrollview setContentSize:CGSizeMake(0, Main_Screen_Height + 80)];
    }
    _scrollview.delegate = self;
    [self.view addSubview:_scrollview];
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width , 200)];
    imgView.image = [UIImage imageNamed:@"个人资料页-图片底图"];
    //[_scrollview addSubview:imgView];
        self.mePhotoView = [[MePhotoView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 72*2+50)];
    [_scrollview addSubview:_mePhotoView];
    btnAddButtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [btnAddButtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
   // [btnAddButtn setFrame:[self setNumber:0]];//第一个添加按钮
        [self.mePhotoView addSubview:btnAddButtn];
    
    
    imgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 210, Main_Screen_Width - 10, 179)];
    imgView.image = [UIImage imageNamed:@"我的页-列表"];
    [_scrollview addSubview:imgView];
    imgView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 210 + 179 - 0.5, Main_Screen_Width - 10, 100)];
    imgView.image = [UIImage imageNamed:@"个性签名输入框232"];
    [_scrollview addSubview:imgView];
    
    UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(28, 220, 42, 21)];
    lbl.text = @"账户";
    lbl.font = [UIFont systemFontOfSize:15];
    lbl.textAlignment = NSTextAlignmentCenter;
    [_scrollview addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(28, 220 + 47, 42, 21)];
    lbl.text = @"昵称";
    lbl.font = [UIFont systemFontOfSize:15];
    
    lbl.textAlignment = NSTextAlignmentCenter;
    [_scrollview addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(28, 220 + 47 * 2, 42, 21)];
    lbl.text = @"性别";
    lbl.font = [UIFont systemFontOfSize:15];
    
    lbl.textAlignment = NSTextAlignmentCenter;
    [_scrollview addSubview:lbl];
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(28, 220 + 47 * 3, 42, 21)];
    lbl.text = @"地址";
    lbl.font = [UIFont systemFontOfSize:15];
    
    lbl.textAlignment = NSTextAlignmentCenter;
    [_scrollview addSubview:lbl];
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 220 + 47 * 4 - 20, 42 + 40, 21)];
    lbl.text = @"个性签名:";
    lbl.font = [UIFont systemFontOfSize:13];
    [_scrollview addSubview:lbl];
    
    lbl_sing = [[UILabel alloc]initWithFrame:CGRectMake(20, 220 + 47 * 4 - 20 + 21, Main_Screen_Width - 20 - 10, 80)];
    lbl_sing.numberOfLines = 0;
    lbl_sing.font = [UIFont systemFontOfSize:13];
    [_scrollview addSubview:lbl_sing];
    
    
    lbl_zhanghao = [[UILabel alloc]initWithFrame:CGRectMake(108, 220, 116, 21)];
    lbl_zhanghao.font = [UIFont systemFontOfSize:15];
    [_scrollview addSubview:lbl_zhanghao];
    
    lbl_name = [[UILabel alloc]initWithFrame:CGRectMake(108, 220 + 47, 116, 21)];
    lbl_name.font = [UIFont systemFontOfSize:15];
    [_scrollview addSubview:lbl_name];
    lbl_sex = [[UILabel alloc]initWithFrame:CGRectMake(108, 220 + 47 * 2, 116, 21)];
    lbl_sex.font = [UIFont systemFontOfSize:15];
    [_scrollview addSubview:lbl_sex];
    lbl_dizhi = [[UILabel alloc]initWithFrame:CGRectMake(108, 220 + 47 * 3, 116, 21)];
    lbl_dizhi.font = [UIFont systemFontOfSize:15];
    [_scrollview addSubview:lbl_dizhi];
    
    btn = [[UIButton alloc]initWithFrame:CGRectMake(Main_Screen_Width - 5 - 79 - 5, 218, 79, 30)];
    //btn.backgroundColor = [UIColor redColor];
    [btn setBackgroundImage:[UIImage imageNamed:@"个人资料页-修改密码按钮" ] forState:UIControlStateNormal];
    [btn setTitle:@"修改密码" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    [btn addTarget:self action:@selector(changePwd:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollview addSubview:btn];
    [self loadData];
    [self loadPhoto];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的资料";
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(edit)];
    [self prepareUI];
    // Do any additional setup after loading the view.
}
-(void)edit{
    
            MeDataViewController *data=[[MeDataViewController alloc] init];
        //data.currentimgArray = _itemArray;
            [self.navigationController pushViewController:data animated:YES];

}
- (void)changePwd:(UIButton *)sender {
    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePwdViewController *changeVC = [storeBoard instantiateViewControllerWithIdentifier:@"ChangePwdViewController"];
    NSLog(@"%@",lbl_zhanghao.text);
    changeVC.iphoneNum =lbl_zhanghao.text;
    
    
    [self pushNewViewController:changeVC];
    
    
    
}
-(void)loadPhoto{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    NSDictionary *dict1=@{@"userId":strUserName,
                          @"op" : @"getToken"
                          
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
       // NSArray *dic =(NSArray*)[CommeHelper getDecryptWithString:resultDic[@"result"]];
         NSString *decryptString = [AA3DESManager getDecryptWithString:resultDic[@"result"] keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
        [self loadPhoto2:decryptString];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
    }];

    
    
    
}
-(void)loadPhoto2:(NSString *)key{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    NSDictionary *dict1=@{@"userId":strUserName,
                          @"op" : @"getUserPhoto",
                          @"key" : key,
                          
                          };
    NSDictionary *dict2=@{
                          @"method" : @"uploadPhotoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSMutableArray * arry =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        int j = 0;
        [_itemArray removeAllObjects];
        for (int i = arry.count; i > 8; i--)
        {
            [arry removeObjectAtIndex:j];
            
        }
        _itemArray = [NSMutableArray arrayWithArray:arry];
        if (_itemArray.count) {
            [self test];
 
        }
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
    }];
 
}
-(void)test{
    for (int i=0; i<_itemArray.count; i++) {
//        ALAsset *asset=assets[i];
        //http://7wy48d.com2.z0.glb.qiniucdn.com/
        
        
        NSString * imgStr = [NSString stringWithFormat:@"http://7wy48d.com2.z0.glb.qiniucdn.com/%@",_itemArray[i][@"fileName"]];
        UIButton *btn=[self addNormalBtn:i];

        UIImage *image=[UIImage imageNamed:@""];
        [btn sd_setImageWithURL:[NSURL URLWithString:imgStr] forState:UIControlStateNormal placeholderImage:image];
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }

}
-(UIButton *)addNormalBtn:(int)index
{
    
    NSInteger count=self.itemArray.count;
    buttonNormal=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonNormal.tag=count;
    [buttonNormal setImage:[UIImage imageNamed:@"个人资料页-图片缩略图】"] forState:UIControlStateNormal];
    //[buttonNormal setTitle:[NSString stringWithFormat:@"%d",count] forState:UIControlStateNormal];
    [buttonNormal setFrame:[self setNumber:index]];
    
    [self.mePhotoView addSubview:buttonNormal];
    
    
    return buttonNormal;
}
-(CGRect)setNumber:(NSInteger) number
{
    
    CGFloat y;
    CGFloat x;
    
    if (number>=4) {
        y=16+KbutHeigth;
        x=8+6*(number%4)+kbutWith*(number%4);
    }else
    {
        
        y=8;
        x=8+6*number+kbutWith*number;
    }
    
    CGRect rect=CGRectMake(x, y, kbutWith, KbutHeigth);
    
    
    
    return rect;
}

-(void)loadData
{
    
    [self showLoading:YES AndText:@"正在加载"];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    NSDictionary *dict1=@{@"userId":strUserName};
    NSDictionary *dict2=@{
                          @"method" : @"userInfoRequestHandler",
                          
                          @"value"  :[CommeHelper getEncryptWithString:dict1]
                          };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict2 Finish:^(NSDictionary *resultDic) {
        
        NSDictionary *dic =[CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSLog(@"%@",dic);
        lbl_zhanghao.text=[dic objectForKey:@"id"]==[NSNull null]?@"":[dic objectForKey:@"id"];
        
        lbl_name.text=[dic objectForKey:@"nickname"]==[NSNull null]?@"":[dic objectForKey:@"nickname"];
        
        NSString *sex =[dic objectForKey:@"sex"]==[NSNull null]?@"你猜猜":[dic objectForKey:@"sex"];
        if ([sex isEqualToString:@"M"]) {
            lbl_sex.text = @"男";
        }
       else if ([sex isEqualToString:@"F"]) {
            lbl_sex.text = @"女";
        }
       else{
           lbl_sex.text = sex;
       }
        
        
        if ([dic objectForKey:@"homeAddress"]!=[NSNull null]) {
            
            NSString *cityName=[dic objectForKey:@"homeAddress"];
            
            NSRange rang=[cityName rangeOfString:@"-"];
            
//            NSString *cityLabel=[cityName substringFromIndex:NSMaxRange(rang)];
//            
//            NSString *provinceLabel=[cityName substringWithRange:NSMakeRange(0,rang.location)];
            lbl_dizhi.text = cityName;
//            self.provinceLabel.text=provinceLabel;
//            
//            self.cityLabel.text=cityLabel;
//            
//            [dictData setObject:cityName forKey:@"homeAddress"];
        }
        
        
        NSString *signature=@"";
        if ([dic objectForKey:@"signature"]!=[NSNull null]) {
            signature=[dic objectForKey:@"signature"];
        }
        
        lbl_sing.text=signature;
        UIImage *image=[UIImage imageNamed:@"个人资料页-添加图片icon-1"];
        NSURL *strUrl;
        if ([dic objectForKey:@"portrait"]!=[NSNull null]) {
            strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"portrait"]]]];
        }//http://120.25.218.167/upload/20150506/10981430890856209.png
        [btnAddButtn sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];
//
//        UIImage *image=[UIImage imageNamed:@"个人资料页-添加图片icon-1"];
//        [dictData setObject:signature forKey:@"signature"];
//        
//        NSURL *strUrl;
//        if ([dic objectForKey:@"photo"]!=[NSNull null]) {
//            strUrl =[NSURL URLWithString:[url stringByAppendingString:[NSString stringWithFormat:@"%@",[dic objectForKey:@"photo"]]]];
//        }//http://120.25.218.167/upload/20150506/10981430890856209.png
//        [btnAddButtn sd_setImageWithURL:strUrl forState:UIControlStateNormal placeholderImage:image];
        [self showLoading:NO AndText:nil];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        [self showAllTextDialog:description];
        
        //  DLog(@"error==%@",error);
        NSLog(@"description ==%@", description);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (isfis) {
        [self loadData];
        [self loadPhoto];

    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
