//
//  LiushuiViewController.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LiushuiViewController.h"
#import "CommeHelper.h"
#import "LiushuiTableViewCell.h"
#import "MJRefresh.h"

@interface LiushuiViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    
    UITableView *_tableView;
    NSMutableArray *_myDataArray;
    NSInteger index;
    NSInteger size;
}

@end

@implementation LiushuiViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _myDataArray = [NSMutableArray array];
        index = 1;
        size = 20;
    }
    return self;
}
-(void)prepareUI{
    
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height - 22) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource =  self;
    __weak typeof(self) weakSelf = self;
    __weak typeof(NSInteger) weakindex = index;
    [_tableView addFooterWithCallback:^{
        
        [weakSelf querData:weakindex];
    }];
    [self.view addSubview:_tableView];
    
    [self querData:index];
    
}
-(void)querData:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];

    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSString *sizeStr=[NSString stringWithFormat:@"%ld",(long)size ];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];

    NSDictionary *dic = @{
                          
                          @"type" : @"",
                          @"typeInfo" : @"",
                          @"curPage": page,
                          @"size"   : sizeStr ,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"cashFlowRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    //8mZ4Ew4u9jddKgDTSKFkZNlunRWfc4mMtR/5/M1kxm9WVfXFzc54dTc6NhBXsRkFxoTNMav5qnHCGKi1zqZvSNkRXXvVEWTDQlPeeC2LqTc=
   // NSString *decryptString = [AA3DESManager getDecryptWithString:@"8mZ4Ew4u9jddKgDTSKFkZNlunRWfc4mMtR/5/M1kxm9WVfXFzc54dTc6NhBXsRkFxoTNMav5qnHCGKi1zqZvSNkRXXvVEWTDQlPeeC2LqTc=" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray * aryy =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
       // NSLog(@"%@",aryy);
        
        if (![aryy isEqual:[NSNull null]]) {
            index++;
            size = size + 20;
            [_myDataArray removeAllObjects];
            for (NSDictionary *dict in aryy) {
                [_myDataArray addObject:dict];
                // NSLog(@"%@",dict);
                
            }
            
            [_tableView reloadData];
            [_tableView footerEndRefreshing];
        }
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];

    
    
}
#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.view.frame.size.width / 4;
    CGFloat offx = width;
    CGFloat height = 44;
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"时间";
    [view addSubview:lbl];
    
    x = offx;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"类型";
    
    [view addSubview:lbl];
    x = offx * 2;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"金额";
    
    [view addSubview:lbl];
    x = offx * 3;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
        lbl.text = @"说明";
    
        [view addSubview:lbl];
    view.backgroundColor = [UIColor whiteColor];
    return view;
    
    
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myCell";
        
        LiushuiTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
        
        if (cell==nil) {
            
            cell=[[LiushuiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
            
        }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_myDataArray.count > indexPath.row)
        {
            NSDictionary * dic = [_myDataArray objectAtIndex:indexPath.row];
            NSString * time = [[dic objectForKey:@"time"] substringToIndex:10];
            NSString * type = [dic objectForKey:@"type"];
            NSString * moneyStr = [dic objectForKey:@"moneyStr"];
            NSString * typeInfo = [dic objectForKey:@"typeInfo"];
            cell.time = time;
            cell.tey = type;
            cell.jine = moneyStr;
            cell.shouming = typeInfo;
            
            
        }
        return cell;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _myDataArray.count;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"流水资金";
    
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
