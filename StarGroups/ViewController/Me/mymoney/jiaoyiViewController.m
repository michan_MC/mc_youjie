//
//  jiaoyiViewController.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "jiaoyiViewController.h"
#import "HMSegmentedControl.h"
#import "CommeHelper.h"
#import "touziTableViewCell.h"
#import "jiekuanTableViewCell.h"
#import "ProDetailsViewController.h"

@interface jiaoyiViewController ()<UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>
{
    UIScrollView *mainScrollView;
    HMSegmentedControl *titleSegment;
    UITableView *_myProjectTableView;
    UITableView *_myInvestTableView;
    NSMutableArray * _TempValueAryy;//项目
    NSMutableArray * _investArray;//投资

}
@property(strong,nonatomic)UIScrollView *dataMianScorView;//中间滚动视图

@end

@implementation jiaoyiViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
                _TempValueAryy = [NSMutableArray array];
                _investArray = [NSMutableArray array];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"交易记录";
    //添加选择框
    [self addAllSelect];

    // Do any additional setup after loading the view.
}
-(void)addAllSelect
{
    
    mainScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,self.view.frame.size.height)];
    
    [mainScrollView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height)];
    [mainScrollView setShowsVerticalScrollIndicator:NO];
    // mainScrollView.backgroundColor = [UIColor blueColor];
    [mainScrollView setBounces:NO];
    
    //  [mainScrollView addSubview:imageLogoView];
    
    
    
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"投资账户", @"借款账户"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithRed:44.0/255.0 green:45.0/255.0 blue:53.0/255.0 alpha:1];
    titleSegment.textColor = [UIColor whiteColor];
    titleSegment.selectedTextColor = [UIColor whiteColor];
    titleSegment.font = [UIFont boldSystemFontOfSize:15];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-10"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    
    
    [mainScrollView addSubview:titleSegment];
    
    _dataMianScorView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 44, Main_Screen_Width,Main_Screen_Height - 44 )];
    _dataMianScorView.delegate = self;
    _dataMianScorView.pagingEnabled = YES;
    _dataMianScorView.bounces = NO;
    //_dataMianScorView.backgroundColor = [UIColor redColor];
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.dataMianScorView setContentOffset:CGPointMake(Main_Screen_Width * index, 0) animated:YES];
        if (index == 1 && _TempValueAryy.count < 1){
            
            [weakSelf myLoanLoadeDate:0];
        }
        
    }];
    
    [_dataMianScorView setContentSize:CGSizeMake(Main_Screen_Width*2, 0)];
    // [_dataMianScorView setScrollEnabled:NO];
    
    
    [mainScrollView addSubview:_dataMianScorView];
    _myProjectTableView=  [[UITableView alloc]initWithFrame:CGRectMake(Main_Screen_Width*1, 0, Main_Screen_Width,_dataMianScorView.height - 64) style:UITableViewStyleGrouped];    
    [_myProjectTableView setDataSource:self];
    [_myProjectTableView setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _myProjectTableView.userInteractionEnabled=YES;
    
    [_dataMianScorView addSubview:_myProjectTableView];
    //
    
    _myInvestTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,_dataMianScorView.height - 64) style:UITableViewStyleGrouped];
    
    
    [_myInvestTableView setDataSource:self];
    [_myInvestTableView setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _myInvestTableView.userInteractionEnabled=YES;
   // _myInvestTableView.backgroundColor = [UIColor yellowColor];
    [_dataMianScorView addSubview:_myInvestTableView];
    
    [self.view addSubview:mainScrollView];
    [self myInvesLoadDate:0];
    
}
-(void)myLoanLoadeDate:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];

    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : @"20" ,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        if (![aryy isEqual:[NSNull null]]) {
            
            for (NSDictionary *dict in aryy) {
                
                [_TempValueAryy addObject:dict];
            }
            
            
        }
        [_myProjectTableView reloadData];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    
    
}

-(void)myInvesLoadDate:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];

    NSString *page=[NSString stringWithFormat:@"%d",curPage ];
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : @"20" ,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myInvestRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray * aryy =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        NSLog(@"%@",aryy);
        
        if (![aryy isEqual:[NSNull null]]) {
            for (NSDictionary *dict in aryy) {
                [_investArray addObject:dict];
                // NSLog(@"%@",dict);
                
            }
            
            [_myInvestTableView reloadData];
        }
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
}
#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.view.frame.size.width / 5;
    CGFloat offx = width;
    CGFloat height = 44;
    UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    if (tableView == _myInvestTableView) {
       lbl.text = @"投资项目";
    }
    else
        lbl.text = @"待还项目";
    [view addSubview:lbl];
    
    x = offx;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    if (tableView == _myInvestTableView) {
            lbl.text = @"总投金额";
    }
    else
        lbl.text = @"借款金额";

    [view addSubview:lbl];
    x = offx * 2;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    if (tableView == _myInvestTableView) {
        lbl.text = @"预期收益";
    }
    else
        lbl.text = @"待还金额";

    [view addSubview:lbl];
    x = offx * 3;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    if (tableView == _myInvestTableView) {
        lbl.text = @"年利率";
    }
    else
        lbl.text = @"逾期费用";

    [view addSubview:lbl];
    x = offx * 4;
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
    lbl.font = [UIFont systemFontOfSize:13];
    lbl.textColor = [UIColor grayColor];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.text = @"操作";
//    if (tableView == _myInvestTableView) {
//        lbl.text = @"投资项目";
//    }
//    else
//        lbl.text = @"投资项目";

    [view addSubview:lbl];
    view.backgroundColor = [UIColor whiteColor];
    return view;
    
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myCell";
    static NSString *DynamicDentifier=@"DynamicDentifier";
    if (_myInvestTableView==tableView) {
        
        touziTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
        
        if (cell==nil) {
            
            cell=[[touziTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
            
        }
        if (_investArray.count > indexPath.row)
        {
            NSDictionary * dic = [_investArray objectAtIndex:indexPath.row];
            NSString * titleStr = [dic objectForKey:@"loanName"];
             NSString * investMoney = [[dic objectForKey:@"investMoney"] stringValue];
            NSString * interest = [[dic objectForKey:@"interest"] stringValue];
            NSString * ratePercent = [[dic objectForKey:@"ratePercent"] stringValue];
            cell.title = titleStr;
            cell.touzijine = investMoney;
            cell.shouyi = interest;
            cell.lixi = ratePercent;
            
            
        }
        return cell;
        
    }
    
    jiekuanTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    
    if (cell==nil) {
        
        cell=[[jiekuanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
        
    }
    if (_TempValueAryy.count > indexPath.row)
    {
        NSDictionary * dic = [_TempValueAryy objectAtIndex:indexPath.row];
        NSString * titleStr = [dic objectForKey:@"name"];
        NSString * loanMoney = [[dic objectForKey:@"loanMoney"] stringValue];
        
        NSString *restOfTimw=@"";
        if ([dic objectForKey:@"jd"]!=[NSNull null]) {
            
            double money=0;
            if ([dic objectForKey:@"money"]!=[NSNull null]) {
                money=[[dic objectForKey:@"money"] doubleValue]*(1-([[dic objectForKey:@"jd"]doubleValue]/100));
            }
            
            restOfTimw=[restOfTimw stringByAppendingString:[ NSString stringWithFormat:@"%.2f", money]];
        }
        NSString * overMoney = [[dic objectForKey:@"overMoney"] stringValue];
        cell.title = titleStr;
       cell.zijine = loanMoney;
        cell.haijine = restOfTimw;
       cell.feiyong = overMoney;
        
        
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ProDetailsViewController *pro;
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    if (tableView == _myProjectTableView){
        
        if (![_TempValueAryy isEqual:[NSNull null]]) {
            
            
            pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
            pro.isMyProject = YES;
            pro.isDetails = YES;
        }
    }
    else if (tableView == _myInvestTableView){
        if (_investArray.count!=0) {
            
            
            pro=[[ProDetailsViewController alloc]initWithData:[_investArray[indexPath.row] objectForKey:@"loanId"]  withUserID:username];
            pro.isDetails = YES;
        }
        
    }
    
    [self.navigationController pushViewController:pro animated:YES];
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_myProjectTableView == tableView){
        if (![_TempValueAryy isEqual:[NSNull null]])
        {
            
            return  _TempValueAryy.count;
        }else
        {
            return 0 ;
        }
    }
    else{
        if (![_investArray isEqual:[NSNull null]])
        {
            NSLog(@"%d",_investArray.count);
            return  _investArray.count;
        }else
        {
            return 0 ;
        }
        
        
    }
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [titleSegment setSelectedSegmentIndex:page animated:YES];
    
    if (page == 1 && _TempValueAryy.count < 1){
        
        [self myLoanLoadeDate:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
