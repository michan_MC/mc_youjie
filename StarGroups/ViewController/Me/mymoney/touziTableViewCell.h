//
//  touziTableViewCell.h
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface touziTableViewCell : UITableViewCell

@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *touzijine;
@property(nonatomic,copy)NSString *shouyi;
@property(nonatomic,copy)NSString *lixi;

@end
