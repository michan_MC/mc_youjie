//
//  jiekuanTableViewCell.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "jiekuanTableViewCell.h"
@interface jiekuanTableViewCell (){
    
    UILabel * lbl_title;
    UILabel * lbl_feiyong;
    UILabel * lbl_haijine;
    UILabel * lbl_zijine;
    
    
}

@end

@implementation jiekuanTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = self.contentView.frame.size.width / 5;
        CGFloat offx = width;
        CGFloat height = self.contentView.frame.size.height;
        lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_title.textAlignment = NSTextAlignmentCenter;
        lbl_title.font = [UIFont systemFontOfSize:13];
        lbl_title.textColor = [UIColor grayColor];
        lbl_title.adjustsFontSizeToFitWidth = YES;

        [self.contentView  addSubview:lbl_title];
        x = offx;
        lbl_zijine = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_zijine.textAlignment = NSTextAlignmentCenter;
        lbl_zijine.font = [UIFont systemFontOfSize:13];
        lbl_zijine.textColor = [UIColor grayColor];
        
        [self.contentView  addSubview:lbl_zijine];
        x = offx * 2;
        lbl_haijine = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_haijine.textAlignment = NSTextAlignmentCenter;
        lbl_haijine.textColor = [UIColor blueColor];
        
        lbl_haijine.font = [UIFont systemFontOfSize:13];
        [self.contentView  addSubview:lbl_haijine];
        
        x = offx * 3;
        lbl_feiyong = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_feiyong.textAlignment = NSTextAlignmentCenter;
        lbl_feiyong.font = [UIFont systemFontOfSize:13];
        lbl_feiyong.textColor = [UIColor redColor];
        [self.contentView  addSubview:lbl_feiyong];
        x = offx * 4;
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.text = @"查看详情";
        [self.contentView  addSubview:lbl];
        
        
    }
    return self;
}
-(void)setTitle:(NSString *)title
{
    lbl_title.text = title;
    
}
-(void)setZijine:(NSString *)zijine{
    lbl_zijine.text =  [NSString stringWithFormat:@"￥%@",zijine] ;
}
-(void)setHaijine:(NSString *)haijine{
    lbl_haijine.text = [NSString stringWithFormat:@"￥%@",haijine] ;
}
-(void)setFeiyong:(NSString *)feiyong{
    
    lbl_feiyong.text = [NSString stringWithFormat:@"￥%@.0",feiyong] ;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
