//
//  touziTableViewCell.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "touziTableViewCell.h"

@interface touziTableViewCell (){
    
    UILabel * lbl_title;
    UILabel * lbl_shouyi;
    UILabel * lbl_lixi;
    UILabel * lbl_touzijine;


}

@end

@implementation touziTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = self.contentView.frame.size.width / 5;
        CGFloat offx = width;
        CGFloat height = self.contentView.frame.size.height;
        lbl_title = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_title.textAlignment = NSTextAlignmentCenter;
        lbl_title.font = [UIFont systemFontOfSize:13];
        lbl_title.textColor = [UIColor grayColor];
        lbl_title.adjustsFontSizeToFitWidth = YES;
        [self.contentView  addSubview:lbl_title];
        x = offx;
        lbl_touzijine = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_touzijine.textAlignment = NSTextAlignmentCenter;
        lbl_touzijine.font = [UIFont systemFontOfSize:13];
        lbl_touzijine.textColor = [UIColor grayColor];

        [self.contentView  addSubview:lbl_touzijine];
        x = offx * 2;
        lbl_shouyi = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_shouyi.textAlignment = NSTextAlignmentCenter;
        lbl_shouyi.textColor = [UIColor blueColor];

        lbl_shouyi.font = [UIFont systemFontOfSize:13];
        [self.contentView  addSubview:lbl_shouyi];

        x = offx * 3;
        lbl_lixi = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_lixi.textAlignment = NSTextAlignmentCenter;
        lbl_lixi.font = [UIFont systemFontOfSize:13];
        lbl_lixi.textColor = [UIColor redColor];
        [self.contentView  addSubview:lbl_lixi];
        x = offx * 4;
        UILabel * lbl = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.font = [UIFont systemFontOfSize:13];
        lbl.text = @"查看详情";
        [self.contentView  addSubview:lbl];

        
    }
    return self;
}
-(void)setTitle:(NSString *)title
{
    lbl_title.text = title;
    
}
-(void)setTouzijine:(NSString *)touzijine{
    
    lbl_touzijine.text =  [NSString stringWithFormat:@"￥%@",touzijine] ;
    
}
-(void)setShouyi:(NSString *)shouyi{
    lbl_shouyi.text = [NSString stringWithFormat:@"￥%@",shouyi] ;
}
-(void)setLixi:(NSString *)lixi
{
    NSString * str = @".0%";
    lbl_lixi.text = [NSString stringWithFormat:@"%@%@",lixi,str];
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
