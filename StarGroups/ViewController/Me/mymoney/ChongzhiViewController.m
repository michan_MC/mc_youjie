//
//  ChongzhiViewController.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ChongzhiViewController.h"
#import "CommeHelper.h"
#import "AA3DESManager.h"
#import "RealNameCertification.h"

@interface ChongzhiViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UITableView * _tableView;
    UIView *_headView;
    CGFloat _lblHeight;
    UITextField *_txtext;
    
}

@end

@implementation ChongzhiViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void)prepareUI{
    _headView = [self headView];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height  - 70 ) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //_tableView.bounces = NO;
    [self.view addSubview:_tableView];
    
    [self queryue];
}
-(void)chongzhi
{
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    if (![self isPureInt:_txtext.text]) {
        [self showAllTextDialog:@"请正确输入充值面额"];
        return;
    }
    NSDictionary *dic = @{
                          @"op" : @"chongzhi",
                          @"actualMoney" : _txtext.text,
                          @"userId" :username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"rechargeRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    [requestManager requestWebWithParaWithURL_NotResponseJson:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];

       RealNameCertification *rel=[[RealNameCertification alloc]init];
        rel.lodaHtml=[resultDic objectForKey:@"html"];
        rel.title = @"充值";
        [self.navigationController  pushViewController:rel animated:YES];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        

    }];
    
}
-(void)queryue
{
    [self showLoading:YES AndText:@"正在加载"];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"queryBlanace",
                          @"actualMoney" : @"",
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"rechargeRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
         _yueStr =  [AA3DESManager getDecryptWithString:resultDic[@"result"] keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
        [_tableView reloadData];
        [self showLoading:NO AndText:nil];

    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        
    }];
    
    
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
        
    }
    for (UIView* obj in cell.contentView.subviews) {
        [obj removeFromSuperview];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }

    if (indexPath.section == 0) {
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(40, 10, 150, 25)];
        lbl.text = @"当前余额(元)";
        //lbl.font = [UIFont systemFontOfSize:<#(CGFloat)#>]
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(40, 40, cell.contentView.frame.size.width - 40,40 )];
        lbl.text = _yueStr;//[NSString stringWithFormat:@"%@",_yueStr];
        lbl.font = [UIFont boldSystemFontOfSize:25];
        lbl.textColor = [UIColor redColor];
        [cell.contentView addSubview:lbl];

    }
    else
    {
        UITextField * txt = [[UITextField alloc]initWithFrame:CGRectMake(10, 2,cell.contentView.frame.size.width - 4 , 40)];
        txt.placeholder = @"我要充值";
        txt.delegate = self;
        _txtext = txt;
        //txt.font = [UIFont systemFontOfSize:20];
        [cell.contentView addSubview:txt];
    }
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    if(section == 0){
        return _lblHeight + 5;
    }
    return 10;

    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (section == 1) {
        return 60;
    }
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 100;
    }
    return 44;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return [self footerView];

    }
    return nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return _headView;
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}
-(UIView *)footerView{
  UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 40)];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, 20, Main_Screen_Width - 40, 40)];
    [btn setTitle:@"提交" forState:UIControlStateNormal];
    // [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
    [btn addTarget:self action:@selector(chongzhi) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
    
    [view addSubview:btn];
    return view;
    
}
-(UIView *)headView{
    
    UILabel* lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 5,self.view.frame.size.width, 15)];
    lbl1.text = @"提示";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.font = [UIFont systemFontOfSize:12];
    lbl1.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
//    [self.view addSubview:lbl];
    
    NSString *str = @"1.请您输入的提现金额，以及银行账号信息准确无误。\n2.如果你填写的提现信息不正确可能会导致提现失败，由此产生的提现费用将不予返还。\n3.在双休日和法定节假日期间，用户可以申请提现，汇付天下会在下一个工作日进行处理。由此造成的不便，请多多谅解!\n4.平台禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确定，将该账户的使用。";
    //获取系统版本
    // float system_Version = [[[UIDevice currentDevice] systemVersion] floatValue];
    //根据不同的系统版本使用不同的API
    //    if (system_Version >= 7.0) {
    //        _lblHeight = [str boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 10, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] context:nil].size.height;
    //
    //    }
    //    else {
    //        _lblHeight = [str sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(self.view.frame.size.width - 10, 1000)].height;
    //    }
   UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 25,self.view.frame.size.width - 10 , 0)];
    lbl.numberOfLines = 0;
    lbl.text = str;
    //lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont systemFontOfSize:12];
    lbl.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
   // [self.view addSubview:lbl];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:10];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    lbl.attributedText = attributedString;
    //[self.view addSubview:lbl];
    [lbl sizeToFit];
 
    _lblHeight = 20 + lbl.frame.size.height;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, _lblHeight)];
    [view addSubview:lbl1];
    [view addSubview:lbl];
    return view;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"充值";
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
