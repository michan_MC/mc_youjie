//
//  TixianViewController.m
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "TixianViewController.h"
#import "CommeHelper.h"
#import "AA3DESManager.h"
#import "RealNameCertification.h"

@interface TixianViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UITableView * _tableView;
        UIView *_headView;
    CGFloat _lblHeight;
    UITextField *_txtext;
    NSString * _yinhangStr;
    NSString * _yinhangnum;
    NSString * _feeStr;
    NSString * status;
    NSString * bankCardId;

}

@end

@implementation TixianViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void)prepareUI{
    _headView = [self headView];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, Main_Screen_Height - 64) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    //_tableView.bounces = NO;
    [self.view addSubview:_tableView];
    [self quertixian];

}
-(void)quertixian
{
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"query",
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"bankCardRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    //NSString * str =[AA3DESManager getDecryptWithString:@"mzetbmVMyApyLGNWIlwzbHrXzFzCloaYDaaCGySIPt7iG5iJEul9Dw==" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        NSArray * array = (NSArray*)[CommeHelper getDecryptWithString:resultDic[@"result"]];
        if (![array isEqual:[NSNull null]]) {
            NSDictionary * dic = [array lastObject];
            NSLog(@"%@",[dic objectForKey:@"name"]);
            NSLog(@"%@",[dic objectForKey:@"status"]);
            NSLog(@"%@",[dic objectForKey:@"cardNo"]);
            NSLog(@"%@",[dic objectForKey:@"id"]);
            _yinhangStr = [dic objectForKey:@"name"];
            NSString *num = [dic objectForKey:@"cardNo"];
           // NSRange  rang = NSMakeRange(num.length - 4, <#NSUInteger len#>)
            bankCardId = num;
           _yinhangnum = [NSString stringWithFormat:@"尾数%@",[num substringFromIndex:num.length - 4]];
            status = [dic objectForKey:@"status"];
            [_tableView reloadData];
        }
        
        /*
         accountName = "<null>";
         bank = "<null>";
         bankArea = "<null>";
         bankCardType = "<null>";
         bankCity = "<null>";
         bankNo = PSBC;
         bankProvince = "<null>";
         bindingprice = "<null>";
         cardNo = 6210986030005538479;
         id = 26785ef3c9c1464586ea9513c8e98e73;
         name = "\U4e2d\U56fd\U90ae\U653f\U50a8\U84c4\U94f6\U884c";
         status = "\U7ed1\U5b9a";
         time = "2015-05-13 16:37:40";

         */
        
        
       [_tableView reloadData];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        _yinhangStr = @"";
        _yinhangnum = @"";
        if (description==nil) {
            
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        

        [_tableView reloadData];
    }];
    
    
    
}
-(void)tixianfee{
    if (![self isPureInt:_txtext.text]) {
        [self showAllTextDialog:@"请正确输入提现面额"];
        return;
    }
    [self showLoading:YES AndText:nil];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    

    NSDictionary *dic = @{
                          
                          @"op" : @"tixian",
                          @"money" : _txtext.text,
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"calculateFeeRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
       NSDictionary * dic = [CommeHelper getDecryptWithString:resultDic[@"result"]];
       // NSLog(@"%@",aryy);
        _feeStr = [dic objectForKey:@"fee"];
        [self tixianfee1];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
       // [self showLoading:NO AndText:nil];
//        [self tixianfee1];
    }];
    
 
    
    
}
-(void)tixianfee1{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          
                          @"bankCardId" : bankCardId,
                          @"fee" :@"0",
                          @"money":_txtext.text,
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"withdrawRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
  //  NSString * str =[AA3DESManager getDecryptWithString:@"ljR0OWAuhx/vktiM7neyzbxVONaFuGpPNFC+yY/+UzigNPogVoXrFnvq12JzpzKypXGcog9dttc=" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        
        
        //[_tableView reloadData];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
       // [self showLoading:NO AndText:nil];
        if (description==nil) {
            
        }
        
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        

//        _yinhangStr = @"无";
//        _yinhangnum = @"无";
//        [_tableView reloadData];
    }];
    
 
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myCell";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:dentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
        
    }
    for (UIView* obj in cell.contentView.subviews) {
        [obj removeFromSuperview];
       
        
    }
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        UIImageView * imgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 40, 40)];
        imgView.image = [UIImage imageNamed:@"我的页-我的钱包icon"];
        [cell.contentView addSubview:imgView];
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(60, 15, 150, 25)];
        lbl.font = [UIFont boldSystemFontOfSize:15];
        lbl.text = [_yinhangStr isEqualToString:@""] ? @"无" :_yinhangStr ;
        [cell.contentView addSubview:lbl];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(60, 45, 150, 20)];
        lbl.font = [UIFont systemFontOfSize:15];
        lbl.textColor = [UIColor grayColor];
        lbl.text = [_yinhangnum  isEqualToString:@""] ? @"无" :_yinhangnum ;
        [cell.contentView addSubview:lbl];
        UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(cell.contentView.frame.size.width - 80 - 20, 20, 80, 35)];
        btn.layer.borderWidth = 0.5;
        btn.layer.cornerRadius = 5;
        btn.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
        btn.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
        if ([status isEqualToString:@"绑定"]) {
        [btn setTitle:@"取消绑定" forState:UIControlStateNormal];
        }
        else
        [btn setTitle:@"绑定银行卡" forState:UIControlStateNormal];
        
        [btn addTarget:self action:@selector(bangding) forControlEvents:UIControlEventTouchUpInside];
        btn.titleLabel.font = [UIFont systemFontOfSize:13];
        [cell.contentView addSubview:btn];
        return cell;
    }
    UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 44)];
    lbl2.font = [UIFont systemFontOfSize:15];
    lbl2.textColor = [UIColor grayColor];
    //lbl2.textAlignment = NSTextAlignmentCenter;
    [cell.contentView addSubview:lbl2];
    if(indexPath.section == 1 && indexPath.row == 0){
       lbl2.text = @"当前余额";
        UILabel *lbl3 = [[UILabel alloc]initWithFrame:CGRectMake(110, 0, 100, 44)];
       // lbl3.font = [UIFont systemFontOfSize:15];
        lbl3.textColor = [UIColor redColor];
        lbl3.text = _yueStr == nil ? @"￥0.0" :_yueStr ;
        //lbl2.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:lbl3];
    }
    else if(indexPath.section == 1 && indexPath.row == 1){
        lbl2.text = @"可提现";
        UILabel *lbl4 = [[UILabel alloc]initWithFrame:CGRectMake(110, 0, 100, 44)];
        // lbl3.font = [UIFont systemFontOfSize:15];
        lbl4.textColor = [UIColor redColor];
        lbl4.text =_ketixianStr == nil ? @"￥0.0" :_ketixianStr ;
        //lbl2.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:lbl4];


    }
    else if(indexPath.section == 2){
        lbl2.text = @"提现金额";
        UITextField * txt = [[UITextField alloc]initWithFrame:CGRectMake(110, 0, cell.contentView.frame.size.width - 110, 44)];
        txt.placeholder = @"我要提现";
        txt.delegate = self;
        _txtext = txt;
        //txt.font = [UIFont systemFontOfSize:20];
        [cell.contentView addSubview:txt];

    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    
    if(section == 0){
        return _lblHeight + 5;
    }
    return 10;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (section == 2) {
        return 100;
    }
    return 0.1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 80;
    }
    return 44;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 2) {
        return [self footerView];
        
    }
    return nil;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return _headView;
    }
    return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 2;
    }
    return 1;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    int val;
    return [scan scanInt:&val] && [scan isAtEnd];
}

//-()
-(void)bangding{
    
    if (![status isEqualToString:@"绑定"]) {
        [self showLoading:YES AndText:nil];
    [self showAllTextDialog:@"不能绑定带有透支功能的卡，否则将无法绑定成功"];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"binding",
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"bindingCardRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    //  NSString * str =[AA3DESManager getDecryptWithString:@"ljR0OWAuhx/vktiM7neyzbxVONaFuGpPNFC+yY/+UzigNPogVoXrFnvq12JzpzKypXGcog9dttc=" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    [requestManager requestWebWithParaWithURL_NotResponseJson:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];
        
        RealNameCertification *rel=[[RealNameCertification alloc]init];
        rel.lodaHtml=[resultDic objectForKey:@"html"];
        rel.title = @"绑定银行卡";
        [self.navigationController  pushViewController:rel animated:YES];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        
    }];

    }
    else{
        [self showAllTextDialog:@"暂时提现卡无法删除"];
        return;
        
        [self showLoading:YES AndText:nil];
        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
        NSString *username = [loginInfo objectForKey:kSDKUsername];
        
        NSDictionary *dic = @{
                              
                              @"op" : @"del",
                              @"bankcardId":bankCardId,
                              @"userId" : username
                              };
        
        
        NSDictionary *dict = @{
                               @"method" : @"bindingCardRequestHandler",
                               @"value"  :[CommeHelper getEncryptWithString:dic]
                               };
        //  NSString * str =[AA3DESManager getDecryptWithString:@"ljR0OWAuhx/vktiM7neyzbxVONaFuGpPNFC+yY/+UzigNPogVoXrFnvq12JzpzKypXGcog9dttc=" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            
            
            
            
            //[_tableView reloadData];
            
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            
            
        }];
        

//        [requestManager requestWebWithParaWithURL_NotResponseJson:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
//            [self showLoading:NO AndText:nil];
//            
//            RealNameCertification *rel=[[RealNameCertification alloc]init];
//            rel.lodaHtml=[resultDic objectForKey:@"html"];
//            rel.title = @"绑定银行卡";
//            [self.navigationController  pushViewController:rel animated:YES];
//            
//            
//        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
//            [self showLoading:NO AndText:nil];
//            if (description==nil) {
//                
//            }
//            [UIView animateWithDuration:0.5f animations:^{
//                [self showAllTextDialog:description];
//                
//            }];
//            
//            
//        }];
 
    }
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"提现";
    [self prepareUI];
    // Do any additional setup after loading the view.
}
-(UIView *)footerView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 40)];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(20, 50, Main_Screen_Width - 40, 40)];
    [btn setTitle:@"提交" forState:UIControlStateNormal];
    // [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
    [btn addTarget:self action:@selector(tixianfee) forControlEvents:UIControlEventTouchUpInside];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
    
    [view addSubview:btn];
    return view;
    
}
-(UIView *)headView{
    
    UILabel* lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 5,self.view.frame.size.width, 15)];
    lbl1.text = @"提示";
    lbl1.textAlignment = NSTextAlignmentCenter;
    lbl1.font = [UIFont systemFontOfSize:12];
    lbl1.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
    //    [self.view addSubview:lbl];
    
    NSString *str = @"1.请您输入的提现金额，以及银行账号信息准确无误。\n2.如果你填写的提现信息不正确可能会导致提现失败，由此产生的提现费用将不予返还。\n3.在双休日和法定节假日期间，用户可以申请提现，汇付天下会在下一个工作日进行处理。由此造成的不便，请多多谅解!\n4.平台禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确定，将该账户的使用。";
    //获取系统版本
    // float system_Version = [[[UIDevice currentDevice] systemVersion] floatValue];
    //根据不同的系统版本使用不同的API
    //    if (system_Version >= 7.0) {
    //        _lblHeight = [str boundingRectWithSize:CGSizeMake(self.view.frame.size.width - 10, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:12] forKey:NSFontAttributeName] context:nil].size.height;
    //
    //    }
    //    else {
    //        _lblHeight = [str sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(self.view.frame.size.width - 10, 1000)].height;
    //    }
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 25,self.view.frame.size.width - 10 , 0)];
    lbl.numberOfLines = 0;
    lbl.text = str;
    //lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont systemFontOfSize:12];
    lbl.textColor = [UIColor grayColor];
    // lbl.backgroundColor = [UIColor yellowColor];
    // [self.view addSubview:lbl];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:10];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    lbl.attributedText = attributedString;
    //[self.view addSubview:lbl];
    [lbl sizeToFit];
    
    _lblHeight = 20 + lbl.frame.size.height;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, _lblHeight)];
    [view addSubview:lbl1];
    [view addSubview:lbl];
    return view;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
