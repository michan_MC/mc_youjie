//
//  jiekuanTableViewCell.h
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface jiekuanTableViewCell : UITableViewCell
@property(nonatomic,copy)NSString *title;
@property(nonatomic,copy)NSString *zijine;
@property(nonatomic,copy)NSString *haijine;
@property(nonatomic,copy)NSString *feiyong;

@end
