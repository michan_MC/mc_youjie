//
//  LiushuiTableViewCell.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LiushuiTableViewCell.h"
@interface LiushuiTableViewCell (){
    
    UILabel * lbl_time;
    UILabel * lbl_tey;
    UILabel * lbl_jine;
    UILabel * lbl_shouming;
    
    
}

@end

@implementation LiushuiTableViewCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        CGFloat x = 0;
        CGFloat y = 0;
        CGFloat width = self.contentView.frame.size.width / 4;
        CGFloat offx = width;
        CGFloat height = self.contentView.frame.size.height;
        lbl_time = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_time.textAlignment = NSTextAlignmentCenter;
        lbl_time.font = [UIFont systemFontOfSize:13];
        lbl_time.textColor = [UIColor grayColor];
        lbl_time.adjustsFontSizeToFitWidth = YES;
        
        [self.contentView  addSubview:lbl_time];
        x = offx;
        lbl_tey = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_tey.textAlignment = NSTextAlignmentCenter;
        lbl_tey.font = [UIFont systemFontOfSize:13];
        lbl_tey.textColor = [UIColor grayColor];
        
        [self.contentView  addSubview:lbl_tey];
        x = offx * 2;
        lbl_jine = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_jine.textAlignment = NSTextAlignmentCenter;
        lbl_jine.textColor = [UIColor redColor];
        
        lbl_jine.font = [UIFont systemFontOfSize:13];
        [self.contentView  addSubview:lbl_jine];
        
        x = offx * 3;
        lbl_shouming = [[UILabel alloc]initWithFrame:CGRectMake(x, y, width, height)];
        lbl_shouming.textAlignment = NSTextAlignmentCenter;
        lbl_shouming.font = [UIFont systemFontOfSize:13];
        lbl_shouming.numberOfLines = 0;
        lbl_shouming.textColor = [UIColor grayColor];
        lbl_shouming.adjustsFontSizeToFitWidth = YES;
        [self.contentView  addSubview:lbl_shouming];
        
        
    }
    return self;
}
-(void)setTime:(NSString *)time{
    lbl_time.text = time;
}
-(void)setTey:(NSString *)tey
{
    lbl_tey.text = tey;
}
-(void)setShouming:(NSString *)shouming
{
    lbl_shouming.text = shouming;
}
-(void)setJine:(NSString *)jine{
    lbl_jine.text =  [NSString stringWithFormat:@"￥%@",jine] ;
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
