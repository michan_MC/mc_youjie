//
//  MyzhanghaoViewController.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MyzhanghaoViewController.h"
#import "CommeHelper.h"
#import "jiaoyiViewController.h"
#import "LiushuiViewController.h"
#import "ChongzhiViewController.h"
#import "TixianViewController.h"
#import "AA3DESManager.h"
#import "RealNameCertification.h"

@interface MyzhanghaoViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *_tableView;
    UILabel *_balcance;
    UILabel *_froze;
    UILabel *_balcance2;


}

@end

@implementation MyzhanghaoViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
-(void)prepareUI{
    [self addNagetionItem];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 250) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.bounces = NO;
    [self.view addSubview:_tableView];
    UIButton * btn = [[UIButton alloc]initWithFrame:CGRectMake(0, Main_Screen_Height - 64 - 44, (Main_Screen_Width - 2) / 2, 44)];
    [btn setTitle:@"充值" forState:UIControlStateNormal];
    btn.tag = 100;
    btn.titleLabel.font = [UIFont systemFontOfSize:20];
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(actionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    btn = [[UIButton alloc]initWithFrame:CGRectMake((Main_Screen_Width - 2) / 2 + 1, Main_Screen_Height - 64 - 44, (Main_Screen_Width - 2) / 2, 44)];
    [btn setTitle:@"提现" forState:UIControlStateNormal];
    btn.tag = 101;
    btn.titleLabel.font = [UIFont systemFontOfSize:20];
    btn.backgroundColor = [UIColor whiteColor];
    [btn addTarget:self action:@selector(actionBtn:) forControlEvents:UIControlEventTouchUpInside];
    [btn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.view addSubview:btn];
    [self querqianbao];
    
    
}
-(void)querqianbao{
    
    [self showLoading:YES AndText:@"正在加载"];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];

    
    
    NSDictionary *dic = @{
                          @"userId" : username
//                          @"loanId" : @"20150507000001",
//                          @"op": @"query",
                          };
    
    NSDictionary *dict = @{
                           @"method" :@"centerHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        NSDictionary * obj =[CommeHelper getDecryptWithString:resultDic[@"result"]];
        
        _balcance.text = [NSString stringWithFormat:@"￥ %@",[obj objectForKey:@"balcance"]];
        _froze.text = [NSString stringWithFormat:@"￥ %@",[obj objectForKey:@"frozen"]];
        
        _balcance2.text = [NSString stringWithFormat:@"￥ %.2f",[[obj objectForKey:@"balcance"]floatValue] -[[obj objectForKey:@"frozen"]floatValue] ];
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        
    }];
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0)
        return 3;
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid=@"mc";
    // UITableViewCell *cell=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    
    if (indexPath.section == 0){
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 100, 44)];
        lbl.font = [UIFont systemFontOfSize:16];
        lbl.textColor = [UIColor grayColor];
        [cell.contentView addSubview:lbl];
        UILabel *lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(120, 0, Main_Screen_Width - 120, 44)];
        lbl2.font = [UIFont boldSystemFontOfSize:20];
        //lbl2.textAlignment = NSTextAlignmentCenter;
        lbl2.textColor = [UIColor redColor];
        [cell.contentView addSubview:lbl2];

        if(indexPath.row == 0){
            lbl.text = @"账户总额";
            _balcance = lbl2;
            return cell;
        }
        if (indexPath.row == 1){
        lbl.text = @"冻结金额";
            //lbl2.text = @"$ 3000";
            _froze = lbl2;
        return cell;
        }
        lbl.text = @"可用金额";
        //lbl2.text = @"$ 3000";
        _balcance2 = lbl2;
        return cell;
    }
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, cell.contentView.frame.size.width, 44)];
    lbl.font = [UIFont systemFontOfSize:16];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.textColor = [UIColor grayColor];
    [cell.contentView addSubview:lbl];
    if(indexPath.row == 0){
        lbl.text = @"交易记录";
        return cell;
    }
    lbl.text = @"资金流水";
    return cell;
    
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 1 && indexPath.row == 0) {
        jiaoyiViewController * contrl = [[jiaoyiViewController alloc]init];
        [self.navigationController pushViewController:contrl animated:YES];
    }
    else if (indexPath.section == 1 && indexPath.row == 1){
        LiushuiViewController * contrl = [[LiushuiViewController alloc]init];
        [self.navigationController pushViewController:contrl animated:YES];
    }
}
//充值，提现
-(void)actionBtn:(UIButton*)btn{
    if (btn.tag == 100){//充值
        ChongzhiViewController * contrl = [[ChongzhiViewController alloc]init];
        //contrl.yueStr = _balcance2.text;
        [self.navigationController pushViewController:contrl animated:YES];

        
    }
    else if(btn.tag == 101){//提现
        TixianViewController *tixian = [[TixianViewController alloc]init];
        tixian.yueStr = _balcance.text;
        tixian.ketixianStr = _balcance2.text;
        [self.navigationController pushViewController:tixian animated:YES];
    }
}
-(void)addNagetionItem
{
    
    UIButton * btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"汇付账户" forState:0];
    btn.frame = CGRectMake(100, 0, 80, 60);
    
    
    btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentRight;
    
    btn.titleLabel.font = [UIFont systemFontOfSize:13];
    [btn setTitleColor:[UIColor whiteColor]];
    
    [btn addTarget:self action:@selector(myLoan) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *bar=[[UIBarButtonItem alloc]initWithCustomView: btn];
    
    self.navigationItem.rightBarButtonItem=bar;
    //    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithTitle:@"我要借款" style:UIBarButtonItemStyleDone target:self action:@selector(myLoan)];
    
//    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
//    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(searchLoan)];
//    [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
    
}
-(void)myLoan{
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"goHuiFu",
                          @"userId" : username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"bankCardRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    //  NSString * str =[AA3DESManager getDecryptWithString:@"UDBpa+Jw010RGV/JGZk/hspnu1PjPsNpFAi8wJI90PiuBqOx6HLMkw==" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];
    
    [requestManager requestWebWithParaWithURL_NotResponseJson:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        [self showLoading:NO AndText:nil];
        
        RealNameCertification *rel=[[RealNameCertification alloc]init];
        rel.lodaHtml=[resultDic objectForKey:@"html"];
        rel.title = @"汇付天下";
        [self.navigationController  pushViewController:rel animated:YES];
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        
    }];
 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"我的账号";
    [self prepareUI];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
