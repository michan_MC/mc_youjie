//
//  TixianViewController.h
//  StarGroups
//
//  Created by michan on 15/5/9.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface TixianViewController : BaseViewController
@property(nonatomic,copy) NSString * yueStr;
@property(nonatomic,copy)NSString * ketixianStr;

@end
