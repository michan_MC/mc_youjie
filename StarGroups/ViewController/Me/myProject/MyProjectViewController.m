//
//  MyProjectViewController.m
//  StarGroups
//
//  Created by michan on 15/5/6.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MyProjectViewController.h"
#import "HMSegmentedControl.h"
#import "ProjectListViewCell.h"
#import "CommeHelper.h"
#import "CommentsController.h"
#import "ProDetailsViewController.h"
//#import "ProjectListViewCell.h"

//#import "SearchController.h"

@interface MyProjectViewController ()<UITableViewDataSource,UITableViewDelegate,ProjectListViewCellDelegate>
{
    
    UIScrollView *mainScrollView;
    UIAlertView *alter;
    
    HMSegmentedControl *titleSegment;
    UIView *bacgroundView;
    UIButton *btnAlter;
    UIButton *btnAlter1;
    UIButton *btnAlter2;
    NSInteger curPage;
    UITableView *_myProjectTableView;
    UITableView *_myInvestTableView;
    NSMutableArray * _TempValueAryy;//项目
    NSMutableArray * _investArray;//投资
    NSInteger _MyProjectCurPage;
    NSInteger _myInvestCurPage;
    NSInteger _MyProjectSize;
    NSInteger _myInvestSize;

}
@property(strong,nonatomic)UIScrollView *dataMianScorView;//中间滚动视图


@end

@implementation MyProjectViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _TempValueAryy = [NSMutableArray array];
        _investArray = [NSMutableArray array];
        _myInvestCurPage = 0;
        _MyProjectCurPage = 0;
        _myInvestSize = 20;
        _MyProjectSize = 20;
       // _dicData = [NSDictionary dictionary];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的项目";
    //添加选择框
    [self addAllSelect];
    [self myLoanLoadeDate:0];
   // [self refesh:0];
    // Do any additional setup after loading the view.
}
-(void)addAllSelect
{
    
    mainScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,self.view.frame.size.height)];
    
    [mainScrollView setContentSize:CGSizeMake(Main_Screen_Width, Main_Screen_Height)];
    [mainScrollView setShowsVerticalScrollIndicator:NO];
   // mainScrollView.backgroundColor = [UIColor blueColor];
    [mainScrollView setBounces:NO];
    
  //  [mainScrollView addSubview:imageLogoView];
    
    
    
    
    titleSegment = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 44)];
    titleSegment.sectionTitles = @[@"我的项目", @"我的投资"];
    titleSegment.selectedSegmentIndex = 0;
    titleSegment.backgroundColor = [UIColor colorWithRed:44.0/255.0 green:45.0/255.0 blue:53.0/255.0 alpha:1];
    titleSegment.textColor = [UIColor whiteColor];
    titleSegment.selectedTextColor = [UIColor whiteColor];
    titleSegment.font = [UIFont boldSystemFontOfSize:15];
    titleSegment.selectionIndicatorColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"矩形-10"]];
    titleSegment.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    titleSegment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    
    
    
    [mainScrollView addSubview:titleSegment];
    
    _dataMianScorView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 44, Main_Screen_Width,Main_Screen_Height - 44 )];
    _dataMianScorView.delegate = self;
    _dataMianScorView.pagingEnabled = YES;
    _dataMianScorView.bounces = NO;
    //_dataMianScorView.backgroundColor = [UIColor redColor];
    __weak typeof(self) weakSelf = self;
    [titleSegment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.dataMianScorView setContentOffset:CGPointMake(Main_Screen_Width * index, 0) animated:YES];
        if (index == 1 && _investArray.count < 1){
            
            [weakSelf myInvesLoadDate:0];
        }

    }];
    
    [_dataMianScorView setContentSize:CGSizeMake(Main_Screen_Width*2, 0)];
    // [_dataMianScorView setScrollEnabled:NO];
    
    
    [mainScrollView addSubview:_dataMianScorView];
    UITableView *table=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width,_dataMianScorView.height - 64)];
    
    [table setDataSource:self];
    [table setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    table.userInteractionEnabled=YES;
    table.separatorColor=[UIColor clearColor];
    [table addFooterWithTarget:self action:@selector(myProjectFooterRefresh)];
    //table.backgroundColor = [UIColor yellowColor];
    [table addHeaderWithTarget:self action:@selector(myProjectHeaderRefresh)];

    _myProjectTableView=table;
    [_dataMianScorView addSubview:table];
//
    
    _myInvestTableView=[[UITableView alloc]initWithFrame:CGRectMake(Main_Screen_Width*1, 0, Main_Screen_Width,_dataMianScorView.height - 64)];//自己的项目
    
   [_myInvestTableView setDataSource:self];
    [_myInvestTableView setDelegate:self];
    //table.separatorStyle=UITableViewCellSeparatorStyleNone;
    _myInvestTableView.userInteractionEnabled=YES;
    [_myInvestTableView addFooterWithTarget:self action:@selector(myInvestFooterRefresh)];
    [_myInvestTableView addHeaderWithTarget:self action:@selector(myInvestHeaderRefresh)];

    _myInvestTableView.separatorColor=[UIColor clearColor];
    [_dataMianScorView addSubview:_myInvestTableView];
    
    [self.view addSubview:mainScrollView];
    
    
}
#pragma mark-上拉
-(void)myProjectFooterRefresh{
    [self myLoanLoadeDate:_MyProjectCurPage];

}
-(void)myInvestFooterRefresh{
    [self myInvesLoadDate:_myInvestCurPage];

}
#pragma mark-下拉
-(void)myProjectHeaderRefresh{
    
    _MyProjectCurPage = 0;
    _MyProjectSize = 20;

    [self myLoanLoadeDate:_MyProjectCurPage];
    
}
-(void)myInvestHeaderRefresh{
    _myInvestCurPage = 0;
    _myInvestSize = 20;

    [self myInvesLoadDate:_myInvestCurPage];
    
}

-(void)myInvesLoadDate:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];

    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSString *size=[NSString stringWithFormat:@"%ld",(long)_myInvestSize ];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    
    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : size ,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myInvestRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        NSArray * aryy =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        
        if (![aryy isEqual:[NSNull null]]) {
            _myInvestCurPage ++;
            _myInvestSize = _myInvestSize + 20;
            [_investArray removeAllObjects];
            for (NSDictionary *dict in aryy) {
            [_investArray addObject:dict];
               // NSLog(@"%@",dict);
                
            }
            [_myInvestTableView reloadData];
            [_myInvestTableView headerEndRefreshing];
            [_myInvestTableView footerEndRefreshing];


        }
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    

    
    
    
    
}
-(void)myLoanLoadeDate:(NSInteger)curPage
{
    [self showLoading:YES AndText:@"正在加载"];

    NSString *page=[NSString stringWithFormat:@"%ld",(long)curPage ];
    NSString *size=[NSString stringWithFormat:@"%ld",(long)_MyProjectSize ];

        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];

    NSDictionary *dic = @{
                          
                          @"op" : @"all",
                          @"curPage": page,
                          @"size"   : size ,
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"myLoanRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
  //  __weak typeof(self) weakSelf = self;

    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
       NSArray *aryy=[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
        if (![aryy isEqual:[NSNull null]]) {
            _MyProjectCurPage ++;
            _MyProjectSize = _MyProjectSize + 20;
            [_TempValueAryy removeAllObjects];
            for (NSDictionary *dict in aryy) {
                
                [_TempValueAryy addObject:dict];
            }
            
            
        }

        [_myProjectTableView reloadData];
        [_myProjectTableView footerEndRefreshing];
        [_myProjectTableView headerEndRefreshing];
      // [_myProjectTableView footerEndRefreshing];
        
        //[weakSelf footerEndRefreshing];
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    
    
}

#pragma mark - Table view data source

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *dentifier=@"myCell";
    static NSString *DynamicDentifier=@"DynamicDentifier";
    if (_myProjectTableView==tableView) {
        
        ProjectListViewCell *cell=(ProjectListViewCell*)[tableView dequeueReusableCellWithIdentifier:dentifier];
        
        if (cell==nil) {
            
            cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:dentifier];
            
        }
        cell.delegate = self;
        if (_TempValueAryy.count==0) {
            [cell LodaView:nil];
        }else
        {
            [cell LodaView:_TempValueAryy[indexPath.row]];
        }
        
        
        cell.backgroundColor= [  self stringTOColor:@"#E6E6E6"];
        return cell;
        
    }else if(_myInvestTableView==tableView)
    {
        ProjectListViewCell *cell=(ProjectListViewCell*)[tableView dequeueReusableCellWithIdentifier:DynamicDentifier];
        
        if (cell==nil) {
            
            cell=[[ProjectListViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:DynamicDentifier];
            
        }
        cell.delegate = self;
        if (_investArray.count==0) {
            [cell LodaView:nil];
        }else
        {
            [cell lodaInvestView:_investArray[indexPath.row]];
            //[cell LodaView:_investArray[indexPath.row]];
        }
        
        
        cell.backgroundColor= [  self stringTOColor:@"#E6E6E6"];
        return cell;
    }
    return nil;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ProDetailsViewController *pro;
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    if (tableView == _myProjectTableView){
    if (_TempValueAryy!=[NSNull null]) {
        
       
        pro=[[ProDetailsViewController alloc]initWithData:[_TempValueAryy[indexPath.row] objectForKeyedSubscript:@"id"]  withUserID:username];
        pro.isMyProject = YES;
         pro.isDetails = YES;
    }
    }
    else if (tableView == _myInvestTableView){
        if (_investArray.count!=0) {
            
            
            pro=[[ProDetailsViewController alloc]initWithData:[_investArray[indexPath.row] objectForKey:@"loanId"]  withUserID:username];
             pro.isDetails = YES;
        }
 
    }
   
    [self.navigationController pushViewController:pro animated:YES];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_myProjectTableView == tableView){
        if (![_TempValueAryy isEqual:[NSNull null]])
        {
            
            return  _TempValueAryy.count;
        }else
        {
            return 0 ;
        }
    }
    else{
        if (![_investArray isEqual:[NSNull null]])
        {

            return  _investArray.count;
        }else
        {
            return 0 ;
        }

        
    }
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 231;
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [titleSegment setSelectedSegmentIndex:page animated:YES];
    
    if (page == 1 && _investArray.count < 1){
        
        [self myInvesLoadDate:0];
    }
}

-(void)GetPraise:(NSDictionary *)praise
{
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    NSString * idStr = [praise objectForKeyedSubscript:@"id"];
    if (idStr == nil){
        idStr = [praise objectForKeyedSubscript:@"loanId"];
    }
    NSLog(@"%@",[praise objectForKey:@"isPaise2"]);
    
    //loanId
    NSDictionary *dic = @{
                          @"userId" : username,
                          @"loanId" : idStr,
                          @"op": [praise objectForKey:@"isPaise2"],
                          };
    
    NSDictionary *dict = @{
                           @"method" : @"praiseRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        if([[praise objectForKey:@"isPaise2"] isEqual:@"cancel"])
        {
            [self showHint:@"取消点赞成功"];
            
        }else
        {
            [self showHint:@"点赞成功"];
            
        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"已经点赞过了"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        
    }];
    
}
#pragma mark-评论代理实现



-(void)CommentsDidDone:(NSDictionary *)dictData
{
    
    CommentsController *comments=[[CommentsController alloc]init];
    
    comments.dataDict=dictData;
    [self.navigationController pushViewController:comments animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIColor *) stringTOColor:(NSString *)str
{
    if (!str || [str isEqualToString:@""]) {
        return nil;
    }
    unsigned red,green,blue;
    NSRange range;
    range.length = 2;
    range.location = 1;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&red];
    range.location = 3;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&green];
    range.location = 5;
    [[NSScanner scannerWithString:[str substringWithRange:range]] scanHexInt:&blue];
    UIColor *color= [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1];
    return color;
}
-(void)Share{
    [self ShareCtlView:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
