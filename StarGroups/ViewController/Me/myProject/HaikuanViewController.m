//
//  HaikuanViewController.m
//  StarGroups
//
//  Created by michan on 15/5/8.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "HaikuanViewController.h"
#import "CommeHelper.h"

@interface HaikuanViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *_tableView;
    
    
    
    
}

@end

@implementation HaikuanViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
//        _TempValueAryy = [NSMutableArray array];
//        _investArray = [NSMutableArray array];
//        _myInvestCurPage = 0;
//        _MyProjectCurPage = 0;
//        // _dicData = [NSDictionary dictionary];
    }
    return self;
}
-(void)prepareUI{
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 250) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.frame = CGRectMake(25, 300, Main_Screen_Width - 50, 40);
    btn.backgroundColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 5;
    btn.layer.borderColor = [UIColor colorWithRed:30.0/255.0 green:144.0/255.0 blue:255.0/255.0 alpha:1].CGColor;
    [btn setTitle:@"确定" forState:UIControlStateNormal];
    [self.view addSubview:btn];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self queryHaiKuan];
   // btn.addTarget(self, action: "EV_selectOnClick:", forControlEvents: UIControlEvents.TouchUpInside)
}
-(void)queryHaiKuan{
    
    //20150429000002
    //15603064130

    
    
    NSDictionary *dic = @{
                          @"userId" : @"15603064130",
                          @"loanId" : @"20150429000002",
                          @"op": @"query",
                          };
    
    NSDictionary *dict = @{
                           @"method" :@"repayMoneyRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
         NSDictionary * obj =[CommeHelper getDecryptWithString:resultDic[@"result"]][@"data"];
        
       // for (NSDictionary *obj in aryy) {
            NSLog(@"%@",[obj objectForKey:@"corpus"]);
            NSLog(@"%@",[obj objectForKey:@"id"]);
            NSLog(@"%@",[obj objectForKey:@"status"]);
           NSLog(@"%@",[obj objectForKey:@"interest"]);
            NSLog(@"%@",[obj objectForKey:@"isInRepayPeriod"]);
            NSLog(@"%@",[obj objectForKey:@"repayDay"]);
//            NSLog(@"%@",[obj objectForKey:@"interest"]);
//            NSLog(@"%@",[obj objectForKey:@"interest"]);


      //  }
        
//        
//        if([[praise objectForKey:@"isPaise2"] isEqual:@"cancel"])
//        {
//            [self showHint:@"取消点赞成功"];
//            
//        }else
//        {
//            [self showHint:@"点赞成功"];
//            
//        }
        
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
//        if (description==nil) {
//            [UIView animateWithDuration:0.5f animations:^{
//                [self showAllTextDialog:@"已经点赞过了"];
//            }];
//        }else
//        {
//            [UIView animateWithDuration:0.5f animations:^{
//                [self showAllTextDialog:description];
//            }];
//        }
        
    }];

    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellid=@"mc";
    // UITableViewCell *cell=nil;
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellid];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellid];
    }
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 0, 100, 44)];
    lbl.font = [UIFont systemFontOfSize:16];
    lbl.textColor = [UIColor grayColor];
    [cell.contentView addSubview:lbl];
    
    if (indexPath.section == 0){
        if(indexPath.row == 0){
       lbl.text = @"还款金额";
        return cell;
        }
        lbl.text = @"还款期限";
        return cell;

    }
    if(indexPath.row == 0){
        lbl.text = @"当前金额";
        return cell;
    }
    lbl.text = @"输入密码";
    return cell;
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.title = @"还款";
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
