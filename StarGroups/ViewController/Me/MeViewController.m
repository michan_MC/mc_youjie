//
//  MeTableViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MeViewController.h"
#import "MeDataViewController.h"
#import "CommeHelper.h"
#import "UIImageView+BFKit.h"
#import "UIImageView+EMWebCache.h"
#import "MF_Base64Additions.h"
#import "MyProjectViewController.h"
#import "FriendDataViewController.h"
#import "MyzhanghaoViewController.h"
#import "XiaoxiViewController.h"
#import "moreViewController.h"
#import "MCViewController.h"
@interface MeViewController () <UITableViewDataSource, UITableViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    UITableView *_tableView;
    UIImageView *_headImg;
    UILabel *lbl_name;
    UILabel *lbl_guanzhu;
    UILabel *lbl_fensi;
    UIView *_headView;
    
}
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) NSArray *titleArray;
@property (strong, nonatomic) NSArray *imageArray;
@property (nonatomic,strong) NSDictionary *dic_data;
@end

@implementation MeViewController


-(void)prepareUI{
    _headView = [self headView];
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0,0 , Main_Screen_Width, Main_Screen_Height - 46) style:UITableViewStyleGrouped];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    _titleArray = @[@"个人资料",@"我的项目", @"我的钱包", @"消息", @"更多"];
    _imageArray = @[@"我的页-个人资料icon",@"我的页-我的项目icon", @"我的页-我的钱包icon" , @"我的页-消息icon" , @"我的页-更多icon"];//, @"我的页-我的相册icon"
    [self myLoanLoadeDate:0];
    [self myguanzhu_fensi:@"query1"];
    
    
    
}
-(UIView *)headView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 200)];
    UIImageView * imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, Main_Screen_Width, 160)];
    imgView.image = [UIImage imageNamed:@"我的页-底图"];
    [view addSubview:imgView];
    
    _headImg = [[UIImageView alloc]initWithFrame:CGRectMake(20, 200 - 70 - 20, 70, 70)];
    [view addSubview:_headImg];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(pushMeData)];
    [_headImg addGestureRecognizer:tap];
    _headImg.image = [UIImage imageNamed:@"我的页-个人头像"];
    _headImg.layer.cornerRadius= 3;//(值越大，角就越圆
    _headImg.layer.masksToBounds= YES;
    _headImg.userInteractionEnabled = YES;
    
    lbl_name = [[UILabel alloc]initWithFrame:CGRectMake(90 + 20, 200 - 40 - 40, Main_Screen_Width - 90 - 10, 40)];
    lbl_name.text = @"MC";
    lbl_name.font = [UIFont boldSystemFontOfSize:20];
    lbl_name.textColor = [UIColor whiteColor];
    [view addSubview:lbl_name];
    
    lbl_guanzhu = [[UILabel alloc]initWithFrame:CGRectMake(90+20, 200 - 40, 100, 40)];
    lbl_guanzhu.font = [UIFont systemFontOfSize:15];
    lbl_guanzhu.text = @"关注: 0";
    lbl_guanzhu.textColor = [UIColor grayColor];
    [view addSubview:lbl_guanzhu];
    lbl_fensi = [[UILabel alloc]initWithFrame:CGRectMake(Main_Screen_Width - 100 -40, 200 - 40, 100, 40)];
    lbl_fensi.font = [UIFont systemFontOfSize:15];
    lbl_fensi.textAlignment =  NSTextAlignmentRight;
    lbl_fensi.textColor = [UIColor grayColor];
    lbl_fensi.text = @"粉丝: 0";
    [view addSubview:lbl_fensi];

    return view;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
    
    }


-(void)pushMeData
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    //资源类型为图片库
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    // [self presentModalViewController:picker animated:YES];
    [self presentViewController:picker animated:YES completion:nil];

}
//图像选取器的委托方法，选完图片后回调该方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    UIImage *image=[info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    //当图片不为空时显示图片并保存图片
    if (image != nil) {
        
        
        _headImg.image = image;
        [self Upload:image];
//        UIButton *btn=tempBtn;
//        
//        //图片显示在界面上
//        [btn setImage:image forState:UIControlStateNormal];
//        [self UploadGeneralLighting:image];
        
        //上传图片到友借
        
        //        [self UploadGeneralLighting:image];
        
        //        //以下是保存文件到沙盒路径下
        //        //把图片转成NSData类型的数据来保存文件
        //        NSData *data;
        //        //判断图片是不是png格式的文件
        //        if (UIImagePNGRepresentation(image)) {
        //            //返回为png图像。
        //            data = UIImagePNGRepresentation(image);
        //        }else {
        //            //返回为JPEG图像。
        //            data = UIImageJPEGRepresentation(image, 1.0);
        //        }
        //        //保存
        //        [[NSFileManager defaultManager] createFileAtPath:self.imagePath contents:data attributes:nil];
        
    }
    //关闭相册界面
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
//上传头像
-(void)Upload:(UIImage*)image
{
    
    [self showLoading:YES AndText:@"更新头像"];

    NSData *data;
    NSString *fileName;
    if (UIImagePNGRepresentation(image)) {
        //返回为png图像。
        data = UIImagePNGRepresentation(image);
        fileName=[@"123" stringByAppendingString:@".png"];
    }else {
        //返回为JPEG图像。
        data = UIImageJPEGRepresentation(image, 1.0);
        fileName=[@"123" stringByAppendingString:@".jpeg"];
    }
    NSString *base64Image=[MF_Base64Codec base64StringFromData:data];
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    NSString *strUserName=[defaults objectForKey:@"UserName"];
    
    NSDictionary *dic=@{
                        @"userId":strUserName,
                        @"image":base64Image,
                        @"fileName":fileName,
                        @"type":@"3",
                        @"op":@"uploadPic"
                        };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"appUploadPhotoRequest",
                         @"value":[CommeHelper getJson:dic]
                         };
    
    
    [self showLoading:NO AndText:nil];

    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        
        // NSDictionary *dict=[CommeHelper getDecryptWithString:[resultDic objectForKey:@"result"]];
        //         NSLog(@"resultDic=================>%@",resultDic);
        [self showAllTextDialog:@"上传照片成功"];

        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"上传图片失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
    }];
    
}
-(void)myguanzhu_fensi:(NSString *)op{
    [self showLoading:YES AndText:@"正在加载"];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
    NSDictionary *dic = @{
                          @"userId":username,
                          @"beAttentionUser" : @"",
                          @"op" :op,
                          @"index": @"1",
                          @"length" :@"10"
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"attentionRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };
    
       // NSString * str =[AA3DESManager getDecryptWithString:@"G75cA4D8k7DOTtCYam52SYG/xUJQQjp3yW3/P2OYfOw/oXoK/sVcRBmXiWur8MJH8c6A5o+Onfbpt77fHkcntyUiNoqZhKPjwnVCl4ABCZg3BrUD8ejTFaPOcxuFKGg+" keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv"];

    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
       
        if ([op isEqualToString:@"query1"]) {
             [self showLoading:NO AndText:nil];
          lbl_guanzhu.text= [NSString stringWithFormat:@"关注: %@",[[CommeHelper getDecryptWithString:resultDic[@"result"]][@"total"]stringValue]];
            [self myguanzhu_fensi:@"query2"];
        }
        else{
            lbl_fensi.text= [NSString stringWithFormat:@"粉丝: %@",[[CommeHelper getDecryptWithString:resultDic[@"result"]][@"total"]stringValue]];
            [self showLoading:NO AndText:nil];
        }
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
    }];
    

    
    
}
-(void)myLoanLoadeDate:(NSInteger)curPage
{
   // [self showLoading:YES AndText:@"正在加载"];

    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *username = [loginInfo objectForKey:kSDKUsername];
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    
//    NSString *strUserName=[defaults objectForKey:@"UserName"];

    NSDictionary *dic = @{
                          @"userId":username
                          };
    
    
    NSDictionary *dict = @{
                           @"method" : @"userInfoRequestHandler",
                           @"value"  :[CommeHelper getEncryptWithString:dic]
                           };

    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        // DLog(@"resultDic==%@",resultDic);
        [self showLoading:NO AndText:nil];
        
        
       // NSString * str = resultDic[@"result"]][@"data"];
        _dic_data = [CommeHelper getDecryptWithString:resultDic[@"result"]];
        NSLog(@"%@",_dic_data);
        
        [self loadHeadImg];//刷新头像,name
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:@"数据为空"];
            }];
        }else
        {
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
            }];
        }
        //  DLog(@"error==%@",error);
        // NSLog(@"description ==%@", description);
    }];
    
    
}
#pragma mark - 头像
-(void)loadHeadImg{
    
    UIImage *image=[UIImage imageNamed:@"首页-列表头像"];
    NSString *strUrl;
    if ([_dic_data objectForKey:@"portrait"]!=[NSNull null]) {
        strUrl =[url stringByAppendingString:[NSString stringWithFormat:@"%@",[_dic_data objectForKey:@"portrait"]]];
        
    }

    [_headImg sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:image];
    
    if ([_dic_data objectForKey:@"nickname"] != [NSNull null]){
    lbl_name.text = [_dic_data objectForKey:@"nickname"];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return section == 0 ? 4:1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"meCell"];
    UIImageView *imageViewcon;
    UILabel *labelText;
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"meCell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
//        UIView *bottonView = [[UIView alloc] initWithFrame:CGRectMake(0, 46, Main_Screen_Width, 1)];
//        [bottonView setBackgroundColor:ColorString(@"#cccccc")];
        
        imageViewcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 14, 19, 19)];
        labelText = [[UILabel alloc] initWithFrame:CGRectMake(41, 13, 80, 21)];
        labelText.font = [UIFont systemFontOfSize:13];
        [labelText setTextColor:ColorString(@"#333333")];
        [cell addSubview:imageViewcon];
        [cell addSubview:labelText];
       // [cell addSubview:bottonView];
        
    }
    if (indexPath.section == 0) {
        NSString *con = _imageArray[indexPath.row];
        [imageViewcon setImage:[UIImage imageNamed:con]];
        
        NSString *title = _titleArray[indexPath.row];
        [labelText setText:title];
    }else{
        NSString *con = [_imageArray lastObject];
        [imageViewcon setImage:[UIImage imageNamed:con]];
        NSString *title = [_titleArray lastObject];
        [labelText setText:title];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0 && indexPath.row == 0) {
//        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
//        NSString *username = [loginInfo objectForKey:kSDKUsername];
//        
//
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
//        friVC.sendToUser=username;
//        [self pushNewViewController:friVC];

//        MeDataViewController *data=[[MeDataViewController alloc] init];
//        
//        [self.navigationController pushViewController:data animated:YES];
//        
//        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
//            NSString *username = [loginInfo objectForKey:kSDKUsername];
//        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        FriendDataViewController *friVC = [story instantiateViewControllerWithIdentifier:@"FriendDataViewController"];
//        friVC.sendToUser=username;
//        [self pushNewViewController:friVC];

        MCViewController * mc = [[MCViewController alloc]init];
        // self.navigationController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //[self pushNewViewController:mc];
        [self.navigationController pushViewController:mc animated:YES];
        
    }
    else if (indexPath.section == 0 && indexPath.row == 1){
        
        MyProjectViewController * data = [[MyProjectViewController alloc]init];
        
         [self.navigationController pushViewController:data animated:YES];
        
    }
    else if (indexPath.section == 0 && indexPath.row == 2){
        
        MyzhanghaoViewController * data = [[MyzhanghaoViewController alloc]init];
        
        [self.navigationController pushViewController:data animated:YES];
        
    }
    else if (indexPath.section == 0 && indexPath.row == 3){
        
        XiaoxiViewController * data = [[XiaoxiViewController alloc]init];
        
        [self.navigationController pushViewController:data animated:YES];
        
    }
    else if (indexPath.section == 1 ){
        
        moreViewController * data = [[moreViewController alloc]init];
        
        [self.navigationController pushViewController:data animated:YES];
        
    }


    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 200;
    }
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return section == 0.1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return _headView;
    }
    return nil;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 47;
//}
@end
