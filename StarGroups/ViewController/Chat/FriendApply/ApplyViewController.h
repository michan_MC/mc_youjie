//
//  ApplyViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/25.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseTableViewController.h"

typedef enum{
    ApplyStyleFriend          = 0,
    ApplyStyleGroupInvitation,
    ApplyStyleJoinGroup,
    
}ApplyStyle;

@interface ApplyViewController : BaseTableViewController

@property (strong, nonatomic) NSMutableArray *dataSources;

+ (instancetype)shareController;

- (void)addNewApply:(NSDictionary *)dictionary;

@end
