//
//  ChatTableViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/20.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseTableViewController.h"
#import "EaseMob.h"

@interface ChatTableViewController : BaseViewController

- (void)refreshDataSource;

- (void)isConnect:(BOOL)isConnect;
- (void)networkChanged:(EMConnectionState)connectionState;
@end
