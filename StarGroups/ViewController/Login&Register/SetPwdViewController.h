//
//  SetPwdViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/25.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface SetPwdViewController : BaseViewController

@property (copy, nonatomic) NSString *userPhoneRegister;
@property (copy, nonatomic) NSString *verificationCodeRegister;

@property (nonatomic, assign) BOOL isRegister;
@end
