//
//  ForgetPwdViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "RegisterOrFindPwdViewController.h"
#import "AppDelegate.h"
#import "SetPwdViewController.h"



@interface RegisterOrFindPwdViewController ()
{

    NSTimer *_gameTimer;
    
    UIButton *_btnSender;//发送短信按钮
    
    NSDate   *_gameStartTime;
}
@property (weak, nonatomic) IBOutlet UITextField *userPhoneRegister;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeRegister;

@end

@implementation RegisterOrFindPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];

    self.title = @"注册";
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// 注册页面发送验证码按钮
- (IBAction)verificationCodeRegisterBtn:(UIButton *)sender {
    [self.userPhoneRegister resignFirstResponder];
    if (![self isPhoneNumber:self.userPhoneRegister.text]) {
        [self showAllTextDialog:@"请输入正确的电话号码"];
        return;
    }
    
//    [self showLoading:YES AndText:@"短信"];
    
    _gameStartTime=[NSDate date];
    _btnSender=sender;

    _gameTimer= [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
    
    NSDictionary *dic = @{
                          
                          @"phone" : _userPhoneRegister.text,
                          @"op" : @"zhuce",
                          @"userId"   : _userPhoneRegister.text
                          };
    
    NSString *json;
    if ([NSJSONSerialization isValidJSONObject:dic])
    {
        NSError *error;
        NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
        json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    NSString *string = [AA3DESManager getEncryptWithString:json keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv" ];

    
    NSDictionary *dict = @{
                           @"method" : @"smsRequestHandler",
                           @"value"  : string
                           };
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
    
        [self showLoading:NO AndText:nil];
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        
        [self showLoading:NO AndText:nil];
        
        if (description==nil) {
            [self showAllTextDialog:@"已经注册过了"];
        }else
        {
        [self showAllTextDialog:description];
        }
       
    }];
    
}



#pragma mark - 私有方法
// 时钟触发执行的方法
- (void)updateTimer:(NSTimer *)sender
{

    NSInteger deltaTime = [sender.fireDate timeIntervalSinceDate:_gameStartTime];
    
    NSString *text = [NSString stringWithFormat:@"%02ld:%02ld", deltaTime / 60, deltaTime % 60];
    
    if (deltaTime==60) {
        [_btnSender setTitle:@"发送短信" forState:UIControlStateNormal];
        [_btnSender setUserInteractionEnabled:YES];
        [_gameTimer invalidate];
        return;
    }else
    {
    
    
    [_btnSender setTitle:text forState:UIControlStateNormal];
    [_btnSender setUserInteractionEnabled:NO];
    
    }
    
}


#pragma mark - 注册界面注册按钮
- (IBAction)registerBtn:(UIButton *)sender {
    
    [self.userPhoneRegister resignFirstResponder];
    [self.verificationCodeRegister resignFirstResponder];
    if ([self checkTextFieldRegister])
    {
    
        [self push];
    }
    
}


- (void)push
{
    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SetPwdViewController *regVC = [storeBoard instantiateViewControllerWithIdentifier:@"Confirmation"];
    regVC.userPhoneRegister = self.userPhoneRegister.text;
    regVC.verificationCodeRegister = self.verificationCodeRegister.text;
    regVC.isRegister = YES;
    [self.navigationController pushViewController:regVC animated:YES];
}


- (BOOL)checkTextFieldRegister
{
    if (![self isPhoneNumber:self.userPhoneRegister.text]) {
        [self showAllTextDialog:@"手机格式不对"];
        return NO;
    }

    if (self.verificationCodeRegister.text.length==0) {
            [self showAllTextDialog:@"请输入验证码"];
            return NO;
    }
    
    return YES;
}

- (BOOL)isPhoneNumber:(NSString*)number {
    NSString *regex = @"^((13[0-9])|(147)|(15[0-9])|(18[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:number];
    return isMatch;
}
@end
