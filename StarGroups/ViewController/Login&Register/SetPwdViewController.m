//
//  SetPwdViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/25.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "SetPwdViewController.h"
#import "MainTableViewController.h"
#import "AppDelegate.h"
#import "CommeHelper.h"

@interface SetPwdViewController ()
// 修改密码和设置密码页
@property (weak, nonatomic) IBOutlet UITextField *onePwd;
@property (weak, nonatomic) IBOutlet UITextField *twoPwd;
@end

@implementation SetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_isRegister) {
        self.title = @"设置密码";
    }else{
        self.title = @"修改密码";
    }
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 确定密码界面确定按钮
- (IBAction)ConfirmationPwdBtn:(UIButton *)sender {
    
    if ( ![self checkTextField:_onePwd.text] && [self checkTextField:_twoPwd.text]) {
         [self showAllTextDialog:@"密码格式不正确，密码6到10位"];
        return;
    }

    if (_isRegister)
    {
       [self showLoading:YES AndText:@"正在加载..."];
        
        NSDictionary *dic = @{
                              
                              @"password" : _twoPwd.text,
                              @"mobileNumber" : _userPhoneRegister,
                              @"authCode"   : _verificationCodeRegister,
                              @"referrer"   : @""
                              };
        
        NSString *json;
        if ([NSJSONSerialization isValidJSONObject:dic])
        {
            NSError *error;
            NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&error];
            json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        }
        NSString *string = [AA3DESManager getEncryptWithString:json keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv" ];
        
        NSDictionary *dict = @{
                               @"method" : @"registRequestHandler",
                               @"value"  : string
                               };
        
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            DLog(@"resultDic==%@",resultDic);
            [self showLoading:NO AndText:nil];
            
            
            MainTableViewController *mainVC = [[MainTableViewController alloc] init];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate.window setRootViewController:mainVC];
        
           
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            [self showLoading:NO AndText:nil];
            [self showAllTextDialog:description];
            
            DLog(@"error==%@",error);
            NSLog(@"description ==%@", description);
        }];

        
    }else{

        NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
        [dictData setObject:_onePwd.text forKey:@"newPwd"];
        [dictData setObject:_twoPwd.text forKey:@"newPwd1"];
        [dictData setObject:_userPhoneRegister forKey:@"userId"];
        
        
        NSDictionary *dict=@{
                             
                             @"method" : @"pwdFind2Handler",
                             @"value"  :[CommeHelper getEncryptWithString:dictData]
                             };
        
        
        
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
            
        
            if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
            {
                
                UIWindow *window = [UIApplication sharedApplication].delegate.window;
                
               // [UIView animateWithDuration:1.0f animations:^{
                    window.alpha = 0;
                    window.frame = CGRectMake(0, window.bounds.size.width, 0, 0);
               // } completion:^(BOOL finished) {
                    exit(0);
               // }];

            }
            
            
            
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            [self showLoading:NO AndText:nil];
            if (description==nil) {
                description=@"密码修改失败了";
            }
            [UIView animateWithDuration:0.5f animations:^{
                [self showAllTextDialog:description];
                
            }];
            
            
            
        }];
    }
    
    
}



- (BOOL)checkTextField:(NSString *)pwd
{
    if ((pwd.length < 6) || (pwd.length > 10)) {
        
        return NO;
    }
    return YES;
}


@end
