//
//  ChangePwdViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/25.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "ChangePwdViewController.h"
#import "SetPwdViewController.h"
#import "CommeHelper.h"

@interface ChangePwdViewController ()
{
    
    NSTimer *_gameTimer;
    
    UIButton *_btnSender;//发送短信按钮
    
    NSDate   *_gameStartTime;
}
// 修改页面
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberChange;
@property (weak, nonatomic) IBOutlet UITextField *verificationCodeChage;

@end

@implementation ChangePwdViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
        if (_isChangePwd!=false) {
        // Do any additional setup after loading the view.
        self.title = @"忘记密码";
//     [self.phoneNumber setBorderStyle:UITextBorderStyleNone];
            
          

    }else
    {
    // Do any additional setup after loading the view.
       self.title = @"修改密码";
        self.textFile.text = _iphoneNum;
         [self.textFile setEnabled:NO];
    }
    [self.navigationController setNavigationBarHidden:NO animated:YES];
//    [_phoneNumber setBackgroundColor:[UIColor redColor]];

    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#e6e6e6"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// 修改页面发送验证码按钮
- (IBAction)verificationCodeChageBtn:(id)sender {
    
//    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
//    [dictData setObject:self.textFile.text forKey:@"phone"];
//    //[dictData setObject:self.textFile.text forKey:@"userId"];
//    [dictData setObject:@"zhaohui" forKey:@"op"];
    _gameStartTime=[NSDate date];
    _btnSender=sender;
    
    _gameTimer= [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];


    NSDictionary *dic = @{
                          //self.textFile.text
                          @"phone" : self.textFile.text,
                          @"op" : @"zhaohui",
                          @"userId" :@""
                          };
    
    NSDictionary *dict=@{
                         
                         @"method" : @"smsRequestHandler",
                         @"value"  :[CommeHelper getEncryptWithString:dic]
                         };

    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        

        if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
        {
            
            [UIView animateWithDuration:0.5f animations:^{
         
                [self showAllTextDialog:@"发送短信成功"];
        
            }];
        }

        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"发送短信失败了";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
    }];

    
}



#pragma mark - 修改密码界面按钮
- (IBAction)changPwdBtn:(id)sender {
    
    if (self.verificationCodeChage.text.length==0) {
        [self showAllTextDialog:@"请输入验证码"];
        return;
    }
    
    
    
    
    NSMutableDictionary *dictData=[[NSMutableDictionary alloc]init];
    [dictData setObject:self.textFile.text forKey:@"phone"];
    [dictData setObject:self.textFile.text forKey:@"userId"];
    [dictData setObject:self.verificationCodeChage.text forKey:@"code"];
    
    
    NSDictionary *dict=@{
                         
                         @"method" : @"pwdFindHandler",
                         @"value"  :[CommeHelper getEncryptWithString:dictData]
                         };
    
    
    
    [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dict Finish:^(NSDictionary *resultDic) {
        
        //        NSLog(@"resultDic=========>%@",resultDic);
        if ([[resultDic objectForKey:@"resultCode"]  isEqual:@"SUCCESS"])
        {
            
            [UIView animateWithDuration:2.0f animations:^{
                [self showAllTextDialog:@"验证码正确"];
                UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                SetPwdViewController *regVC = [storeBoard instantiateViewControllerWithIdentifier:@"Confirmation"];
                regVC.userPhoneRegister = _iphoneNum;
//                regVC.userPhoneRegister = self.phoneNumberChange.text;
//                regVC.verificationCodeRegister = self.verificationCodeChage.text;
                
                regVC.isRegister=NO;
                [self.navigationController pushViewController:regVC animated:YES];
                
            
            }];
            
            
           
        }
        
        
        
    } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
        [self showLoading:NO AndText:nil];
        if (description==nil) {
            description=@"验证码错误请重新输入";
        }
        [UIView animateWithDuration:0.5f animations:^{
            [self showAllTextDialog:description];
            
        }];
        
        
        
    }];



}
#pragma mark - 私有方法
// 时钟触发执行的方法
- (void)updateTimer:(NSTimer *)sender
{
    
    NSInteger deltaTime = [sender.fireDate timeIntervalSinceDate:_gameStartTime];
    
    NSString *text = [NSString stringWithFormat:@"%02d:%02d", deltaTime / 60, deltaTime % 60];
    
    if (deltaTime==120) {
        [_btnSender setTitle:@"发送短信" forState:UIControlStateNormal];
        [_btnSender setUserInteractionEnabled:YES];
        [_gameTimer invalidate];
        return;
    }else
    {
        
        
        [_btnSender setTitle:text forState:UIControlStateNormal];
        [_btnSender setUserInteractionEnabled:NO];
        
    }
    
}


@end
