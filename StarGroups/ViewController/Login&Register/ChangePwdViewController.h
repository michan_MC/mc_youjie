//
//  ChangePwdViewController.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/25.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "BaseViewController.h"

@interface ChangePwdViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *textFile;

@property (weak, nonatomic) IBOutlet UIButton *verificationCodeChageBtn;
@property(nonatomic,assign)BOOL isChangePwd;
@property(nonatomic,copy)NSString *iphoneNum;
@end
