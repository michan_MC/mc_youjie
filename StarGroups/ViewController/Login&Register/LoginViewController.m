//
//  LoginViewController.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/13.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterOrFindPwdViewController.h"
#import "MainTableViewController.h"
#import "AppDelegate.h"
#import "ChangePwdViewController.h"

@interface LoginViewController ()<UINavigationControllerDelegate, UINavigationBarDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *useName;
@property (weak, nonatomic) IBOutlet UITextField *pwd;

//@property (nonatomic, strong) RegisterOrFindPwdViewController *regVC;

@end

@implementation LoginViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//        [defaults setObject:_useName.text forKey:@"UserName"];
//        [defaults setObject :_pwd.text forKey:@"Pwd"];
    NSString * st =  [defaults objectForKey:@"UserName"]==nil?@"": [defaults objectForKey:@"UserName"];
    NSString * pw =  [defaults objectForKey:@"Pwd"]==nil?@"": [defaults objectForKey:@"Pwd"];
    _useName.text = st;
    _pwd.text = pw;
    [_pwd setSecureTextEntry:YES];
    _useName.delegate = self;
    _pwd.delegate = self;
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[IQKeyboardManager sharedManager] setKeyboardDistanceFromTextField:0];
    
    UITapGestureRecognizer* singleRecognizer;
    singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom)];
    singleRecognizer.numberOfTapsRequired = 1; // 单击
    [self.view addGestureRecognizer:singleRecognizer];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
//    _regVC = [[RegisterOrFindPwdViewController alloc] init];
}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTapFrom
{
    [self.view endEditing:YES];
}

- (IBAction)registerBtn:(UIButton *)sender {

    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RegisterOrFindPwdViewController *regVC = [storeBoard instantiateViewControllerWithIdentifier:@"register"];
    regVC.isRegister = YES;
    [self pushNewViewController:regVC];

}
//忘记密码
- (IBAction)forgetPwdBtn:(UIButton *)sender {
    
    UIStoryboard *storeBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ChangePwdViewController *changeVC = [storeBoard instantiateViewControllerWithIdentifier:@"ChangePwdViewController"];
    changeVC.isChangePwd=YES;
    [self pushNewViewController:changeVC];
}

- (IBAction)loginBtn:(UIButton *)sender {
    
    if (![self isPhoneNumber:self.useName.text]) {
        [self showAllTextDialog:@"手机格式不对"];
        return;
    }
    if (![self checkTextField:self.pwd.text]) {
        [self showAllTextDialog:@"密码错误"];
        return;
    }
    
    
#if !TARGET_IPHONE_SIMULATOR
    //弹出提示
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"login.inputApnsNickname", @"Please enter nickname for apns") delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"Cancel") otherButtonTitles:NSLocalizedString(@"ok", @"OK"), nil];
//    [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
//    UITextField *nameTextField = [alert textFieldAtIndex:0];
//    nameTextField.text = self.usernameTextField.text;
//    [alert show];
    
    
    
    [self loginWithUsername:_useName.text password:_pwd.text];
#elif TARGET_IPHONE_SIMULATOR
    [self loginWithUsername:_useName.text password:_pwd.text];
#endif
}


//点击登陆后的操作
- (void)loginWithUsername:(NSString *)username password:(NSString *)password
{
    
    [self showLoading:YES AndText:@"正在登录"];
    
    [self.view setUserInteractionEnabled:NO];

        NSDictionary *song = @{@"username" : _useName.text,
                               @"password" : _pwd.text,
                               };
    
        NSString *json;
    
        if ([NSJSONSerialization isValidJSONObject:song])
        {
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:song options:NSJSONWritingPrettyPrinted error:&error];
             json =[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSLog(@"json data:%@",json);
        }
    
    
        NSString *string = [AA3DESManager getEncryptWithString:json keyString:@"p2p_standard2_base64_key" ivString:@"p2p_s2iv" ];

    
    
        NSDictionary *dictt = @{@"method" : @"loginRequestHandler",
                                @"value" : string};
    
        [requestManager requestWebWithParaWithURL:@"/app/test" Parameter:dictt Finish:^(NSDictionary *resultDic) {
           // DLog(@"resultDic==%@",resultDic);
            [self showLoading:NO AndText:nil];
            
            // 异步方法, 使用用户名密码登录聊天服务器
            [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:username
             
                                                                password:@"YLKT2312ylkt"
                                                              completion:
             ^(NSDictionary *loginInfo, EMError *error) {
//                 NSLog(@"error  ==%@",error);
                 [self showLoading:NO AndText:nil];
                 if (loginInfo && !error) {
                     /*保存数据－－－－－－－－－－－－－－－－－begin*/
                      NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
                     [defaults setObject:_useName.text forKey:@"UserName"];
                     [defaults setObject :_pwd.text forKey:@"Pwd"];
                     //强制让数据立刻保存
                     [defaults synchronize];
                      /*保存数据－－－－－－－－－－－－－－－－－end*/
                     
                     MainTableViewController *mainVC = [[MainTableViewController alloc] init];
                     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                     [appDelegate.window setRootViewController:mainVC];
                   
                     
                     //设置是否自动登录
                     //[[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
                     
                     //将旧版的coredata数据导入新的数据库
//                     EMError *error = [[EaseMob sharedInstance].chatManager importDataToNewDatabase];
//                     if (!error) {
//                         error = [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
//                     }
                     
                     //发送自动登陆状态通知
//                     [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@YES];
                     
                 }else{
                     switch (error.errorCode) {
                         case EMErrorServerNotReachable:
                             kAlertMessage(@"连接服务器失败!");
                             break;
                         case EMErrorServerAuthenticationFailure:
                             kAlertMessage(@"用户名或密码错误");
                             break;
                         case EMErrorServerTimeout:
                             kAlertMessage(@"连接服务器超时!");
                             break;
                         default:
                             kAlertMessage(@"登录失败");
                             break;
                     }
                 }
             } onQueue:nil];
            
            

    
        } Error:^(AFHTTPRequestOperation *operation, NSError *error, NSString *description) {
            [self showLoading:NO AndText:nil];
            [self.view setUserInteractionEnabled:YES];
            if(description!=nil){
                [UIView animateWithDuration:0.5f animations:^{
                    [self showAllTextDialog:description];
                }];
            }else
            {
                [UIView animateWithDuration:0.5f animations:^{
                     [self showAllTextDialog:@"登陆失败请检查账号和密码"];
                }];
               
            }
//            DLog(@"error==%@",error);
//            NSLog(@"description ==%@", description);
        }];
    
    
   

}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)isPhoneNumber:(NSString*)number {
    NSString *regex = @"^((13[0-9])|(147)|(15[0-9])|(18[0-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:number];
    return isMatch;
}

- (BOOL)checkTextField:(NSString *)pwd
{
    if ((pwd.length < 6) || (pwd.length > 10)) {
        
        return NO;
    }
    return YES;
}

@end
