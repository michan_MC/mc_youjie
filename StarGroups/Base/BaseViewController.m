

#import "BaseViewController.h"
#import "MBProgressHUD.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import "WeiboSDK.h"

@interface BaseViewController ()
{
    MBProgressHUD *hud;
}
@property (nonatomic, copy) BarButtonItemActionBlock barbuttonItemAction;


@end

@implementation BaseViewController

- (void)clickedBarButtonItemAction {
    if (self.barbuttonItemAction) {
        self.barbuttonItemAction();
    }
}

#pragma mark - Public Method

- (void)configureBarbuttonItemStyle:(BarbuttonItemStyle)style action:(BarButtonItemActionBlock)action {
    switch (style) {
        case BarbuttonItemStyleSetting: {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"barbuttonicon_set"] style:UIBarButtonItemStyleBordered target:self action:@selector(clickedBarButtonItemAction)];
            break;
        }
        case BarbuttonItemStyleMore: {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"barbuttonicon_more"] style:UIBarButtonItemStyleBordered target:self action:@selector(clickedBarButtonItemAction)];
            break;
        }
        case BarbuttonItemStyleCamera: {
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"album_add_photo"] style:UIBarButtonItemStyleBordered target:self action:@selector(clickedBarButtonItemAction)];
            break;
        }
        default:
            break;
    }
    self.barbuttonItemAction = action;
}

- (void)setupBackgroundImage:(UIImage *)backgroundImage {
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, Main_Screen_Height, Main_Screen_Width)];
    backgroundImageView.image = backgroundImage;
    [self.view insertSubview:backgroundImageView atIndex:0];
}

- (void)pushNewViewController:(UIViewController *)newViewController {
    if (newViewController) {
        
    }
    [self.navigationController pushViewController:newViewController animated:YES];
}

#pragma mark - Loading

- (void)showLoading:(BOOL)show AndText:(NSString *)text
{
    if (show) {
        hud = [[MBProgressHUD alloc]initWithView:self.view];
        hud.labelText = text.length?text:@"正在加载中...";
        hud.dimBackground=NO;
        [self.view addSubview:hud];
        [hud show:YES];
    }
    else{
        [hud show:NO];
        [hud removeFromSuperview];
        hud=nil;
    }
}

- (void)showAllTextDialog:(NSString *)title
{
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:hud];
    
    hud.labelText = title;
    hud.mode = MBProgressHUDModeText;
    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
    [hud showAnimated:YES whileExecutingBlock:^{
        sleep(1.5);
    } completionBlock:^{
        [hud removeFromSuperview];
        hud = nil;
    }];
}

- (void)hideLoading {
    [hud removeFromSuperview];
    hud = nil;
}

- (void)showLoading
{
    
}

- (void)showSuccess
{
    
}

- (void)showError
{
    
}

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    requestManager = [NetworkManager instanceManager];
    requestManager.needSeesion = YES;
    self.view.backgroundColor = [UIColor whiteColor];
//    [self setupBackgroundImage:[UIImage imageNamed:@"首页_背景"]];
    
    
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                     [UIColor colorWithHexString:@"#fff"], NSForegroundColorAttributeName,
                                                                     [UIFont fontWithName:@"Arial-Bold" size:36.0], NSFontAttributeName,
                                                                     nil]];
    

    
    
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithHexString:@"#111111"]];
    
//    [self.navigationController.navigationBar setBackgroundColor:[UIColor colorWithRed:30/255.0f green:26/255.0f blue:29/255.0f alpha:0]];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"矩形-9"] forBarMetrics:UIBarMetricsDefault];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //所有的子界面都重写返回按钮并保存返回手势
    if (self.navigationController.viewControllers.count > 1) {
        
        [self.navigationItem setHidesBackButton:YES];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"返回icon"] style:UIBarButtonItemStylePlain target:self action:@selector(toPopVC)];
        //self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View rotation
//- (BOOL)shouldAutorotate {
//    return NO;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}
//
//- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
//    return UIInterfaceOrientationPortrait;
//}


- (void)toPopVC
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)ShareCtlView:(UIViewController*)ctl{

    //构造分享内容
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK" ofType:@"png"];
    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:@"有利可图 https://http://www.youliketoo.com"
                                       defaultContent:@"友借"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:NSLocalizedString(@"《友借》APP", @"Hello 微信好友!")
                                                  url:@"http://www.mob.com"
                                          description:@"《友借》分享"
                                            mediaType:SSPublishContentMediaTypeNews];    //以下信息为特定平台需要定义分享内容，如果不需要可省略下面的添加方法
    
    
    //定制微信好友信息
    [publishContent addWeixinSessionUnitWithType:INHERIT_VALUE
                                         content:@"有利可图 https://http://www.youliketoo.com"
                                           title:NSLocalizedString(@"《友借》APP", @"Hello 微信好友!")
                                             url:@"http://www.youliketoo.com"
                                      thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                           image:INHERIT_VALUE
                                    musicFileUrl:nil
                                         extInfo:nil
                                        fileData:nil
                                    emoticonData:nil];
    
    //定制微信朋友圈信息
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeMusic]
                                          content:@"有利可图 https://http://www.youliketoo.com"
                                            title:NSLocalizedString(@"《友借》APP", @"Hello 微信朋友圈!")
                                              url:@"http://www.youliketoo.com"
                                       thumbImage:[ShareSDK imageWithUrl:@"http://120.25.218.167/upload/20150510/28241431239707061.png"]
                                            image:nil
                                     musicFileUrl:nil
                                          extInfo:nil
                                         fileData:nil
                                     emoticonData:nil];
    
    //定制微信收藏信息
    [publishContent addWeixinFavUnitWithType:INHERIT_VALUE
                                     content:@"有利可图 https://http://www.youliketoo.com"                              title:NSLocalizedString(@"友借", @"Hello 微信收藏!")
                                         url:INHERIT_VALUE
                                  thumbImage:[ShareSDK imageWithUrl:@"http://img1.bdstatic.com/img/image/67037d3d539b6003af38f5c4c4f372ac65c1038b63f.jpg"]
                                       image:INHERIT_VALUE
                                musicFileUrl:nil
                                     extInfo:nil
                                    fileData:nil
                                emoticonData:nil];
    
    
    //定制邮件信息
    [publishContent addMailUnitWithSubject:@"Hello Mail"
                                   content:@"有利可图 https://http://www.youliketoo.com"
                                    isHTML:[NSNumber numberWithBool:YES]
                               attachments:INHERIT_VALUE
                                        to:nil
                                        cc:nil
                                       bcc:nil];
    
    //定制短信信息
    //    [publishContent addSMSUnitWithContent:@"Hello SMS"];
    [publishContent addSMSUnitWithContent:@"有利可图 https://http://www.youliketoo.com"
                                  subject:nil
                              attachments:@[[ShareSDKCoreService attachmentWithUrl:@"http://f.hiphotos.bdimg.com/album/w%3D2048/sign=df8f1fe50dd79123e0e09374990c5882/cf1b9d16fdfaaf51e6d1ce528d5494eef01f7a28.jpg"]]
                                       to:@[@"15011991178"]];
    //结束定制信息
    ////////////////////////
    
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
    [container setIPadContainerWithView:ctl.view arrowDirect:UIPopoverArrowDirectionUp];
    
    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {

                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                    [self showAllTextDialog:@"分享成功"];
                                    
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                    [self showAllTextDialog:@"分享失败"];
                                    

                                }
                                

                            }];
    
    
    
    
    
    
}


@end
