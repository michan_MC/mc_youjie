//
//  reponseData.h
//  StarGroups
//
//  Created by fenguoxl on 15/4/3.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface reponseData : NSObject
@property (nonatomic, copy) NSString *resultCode;
@property (nonatomic, copy) NSString *resultMsg;
@property (nonatomic, copy) NSString *deviceId;
@property (nonatomic, copy) NSString *requestId;
@property (nonatomic, copy) NSString *method;
@property (nonatomic, copy) NSString *result;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *sign;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *dataList;

@end
