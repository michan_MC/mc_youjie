//
//  MailListUser.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "MailListUser.h"

@implementation MailListUser
+ (instancetype)mailListUserWithDict:(NSDictionary *)dict
{
    return [[self alloc] initWithDict:dict];
}

- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

@end
