//
//  MailListUser.h
//  StarGroups
//
//  Created by fenguoxl on 15/3/18.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MailListUser : NSObject
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *userPhone;



+ (instancetype)mailListUserWithDict:(NSDictionary *)dict;

- (instancetype)initWithDict:(NSDictionary *)dict;
@end
