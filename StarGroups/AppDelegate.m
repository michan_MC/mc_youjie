//
//  AppDelegate.m
//  StarGroups
//
//  Created by fenguo on 15-1-19.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//
/*
 shareSDK-ios
 Appkey: 73abb734e781
 App Secret: eafd3705e0f40cc90bf10374f85de6aa
 
 
 微信平台
 AppID: wxe43de397e8ade567
 AppSecret: efc170e34284c856e58593c9247c7b1c
 
 
 
 
 
 ios bundle id：
 com.kufeng.youliketoo
 */
#import "AppDelegate.h"
#import <FIR/FIR.h> 
#import "MainTableViewController.h"
#import "ChatTableViewController.h"
#import "LoginViewController.h"

#import "AppDelegate+EaseMob.h"
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import "WeiboSDK.h"
@interface AppDelegate ()

@property (strong, nonatomic) MainTableViewController *mainTabelController;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    [FIR handleCrashWithKey:@"9a76b7e5e06acb5594f860172f352559"];
    [ShareSDK registerApp:@"73abb734e781"];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];

    
//    连接成功
    _connectionState = eEMConnectionConnected;

//    初始化环信SDK，详细内容在AppDelegate+EaseMob.m文件中
    [self easemobApplication:application didFinishLaunchingWithOptions:launchOptions];
   
    
//    //添加微信应用 注册网址 http://open.weixin.qq.com
    [ShareSDK connectWeChatWithAppId:WXAppID
                           wechatCls:[WXApi class]];
//
//    
//    //微信登陆的时候需要初始化
//    [ShareSDK connectWeChatWithAppId:@"wxe43de397e8ade567" appSecret:@"efc170e34284c856e58593c9247c7b1c" wechatCls:[WXApi class]];
//    
    
    [ShareSDK connectWeChatWithAppId:WXAppID   //微信APPID
                           appSecret:WXAppSecret  //微信APPSecret
                           wechatCls:[WXApi class]];
    
    //连接短信分享
    [ShareSDK connectSMS];
    //连接邮件
    [ShareSDK connectMail];
    //连接打印
    [ShareSDK connectAirPrint];
    //连接拷贝
    [ShareSDK connectCopy];
    
    
     return YES;
}




//注册了推送功能，iOS 会自动回调以下方法，得到deviceToken，您需要将deviceToken传给SDK
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [[EaseMob sharedInstance] application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

//系统方法
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //SDK方法调用
    [[EaseMob sharedInstance] application:application didFailToRegisterForRemoteNotificationsWithError:error];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"注册推送失败"
//                                                    message:error.description
//                                                   delegate:nil
//                                          cancelButtonTitle:@"确定"
//                                          otherButtonTitles:nil];
//    [alert show];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [[EaseMob sharedInstance] applicationWillResignActive:application];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[EaseMob sharedInstance] applicationDidEnterBackground:application];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[EaseMob sharedInstance] applicationWillEnterForeground:application];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[EaseMob sharedInstance] applicationDidBecomeActive:application];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[EaseMob sharedInstance] applicationWillTerminate:application];
}
- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}

#pragma mark - private
//登陆状态改变
-(void)loginStateChange:(NSNotification *)notification
{

    
    BOOL isAutoLogin = [[[EaseMob sharedInstance] chatManager] isAutoLoginEnabled];
    BOOL loginSuccess = [notification.object boolValue];
    
    if (isAutoLogin || loginSuccess) {//登陆成功加载主窗口控制器
        //加载申请通知的数据
        
        if (_mainTabelController == nil) {
            _mainTabelController = [[MainTableViewController alloc] init];
            [_mainTabelController networkChanged:_connectionState];
        }

        self.window.rootViewController = _mainTabelController;
    }else{//登陆失败加载登陆页面控制器
        _mainTabelController = nil;
        LoginViewController *loginController = [[LoginViewController alloc] init];
        
        self.window.rootViewController = loginController;
    }


}

#pragma mark - IChatManagerDelegate
// 开始自动登陆回调
- (void)willAutoLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    UIAlertView *alertView = nil;
    if (error) {
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"login.errorAutoLogin", @"Automatic logon failure") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
    }
    else{
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"login.beginAutoLogin", @"Start automatic login...") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        
        
//        if (_mainTabelController == nil) {
//            _mainTabelController = [[MainTableViewController alloc] init];
//          
//        }
//        self.window.rootViewController = _mainTabelController;
    }
    
    [alertView show];
}

// 结束自动登陆回调
- (void)didAutoLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    UIAlertView *alertView = nil;
    if (error) {
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"login.errorAutoLogin", @"Automatic logon failure") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
    }
    else{
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"login.endAutoLogin", @"End automatic login...") delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
    }
    
    [alertView show];
    
}


@end
