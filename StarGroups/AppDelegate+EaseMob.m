//
//  AppDelegate+EaseMob.m
//  StarGroups
//
//  Created by fenguoxl on 15/3/24.
//  Copyright (c) 2015年 fenguo. All rights reserved.
//

#import "AppDelegate+EaseMob.h"
#import "ApplyViewController.h"

#define KNOTIFICATION_LOGINCHANGE @"loginStateChange"



@implementation AppDelegate (EaseMob)
- (void)easemobApplication:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions ;
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginStateChange:) name:KNOTIFICATION_LOGINCHANGE object:nil];
    
    // 注册 APNS文件的名字，需要与后台上传证书时的名字一一对应
    [self registerRemoteNotification];
    //注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    //#warning SDK注册 APNS文件的名字, 需要与后台上传证书时的名字一一对应
    NSString *apnsCertName = nil;
    
#if DEBUG
    apnsCertName = @"fenguoim";
#else
    apnsCertName = @"fenguoim";
#endif
    
//        [[EaseMob sharedInstance] registerSDKWithAppKey:@"easemob-demo#chatdemo" apnsCertName:apnsCertName];
//     需要在注册sdk后写上该方法
    
    //[[EaseMob sharedInstance] registerSDKWithAppKey:@"ifenguo#fenguoim" apnsCertName:apnsCertName otherConfig:@{kSDKConfigEnableConsoleLogger:[NSNumber numberWithBool:YES]}];
    
    
//    [[EaseMob sharedInstance] registerSDKWithAppKey:@"1131301114#xlxl" apnsCertName:nil];
    [[EaseMob sharedInstance] registerSDKWithAppKey:@"youliketoo#youjieapp" apnsCertName:nil];
    

    //    登陆成功后，自动获取好友列表
    //    sdk获取结束后，会回调
    [[EaseMob sharedInstance].chatManager setIsAutoFetchBuddyList:YES];
    
    //    注册环信监听
    [self registerEaseMobNotification];

    
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    [[EaseMob sharedInstance].chatManager setAutoFetchBuddyList:YES];
    //以下一行代码的方法里实现了自动登录，异步登录,必须写!
    [[EaseMob sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
#warning ---------
    // 监听系统生命周期回调，以便将需要的事件传给SDK
      //  [self setupNotifiers];

}


#pragma mark - 注册推送
- (void)registerRemoteNotification
{

    UIApplication *application = [UIApplication sharedApplication];
    application.applicationIconBadgeNumber = 0;
    
    
    
#if !TARGET_IPHONE_SIMULATOR
    //iOS8 注册APNS
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }else{
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound |
        UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
    
#endif
}



#pragma mark - registerEaseMobNotification
- (void)registerEaseMobNotification{
    [self unRegisterEaseMobNotification];
    // 将self 添加到SDK回调中，以便本类可以收到SDK回调
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

#pragma mark - 移除代理
- (void)unRegisterEaseMobNotification{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}


// 好友申请回调
- (void)didReceiveBuddyRequest:(NSString *)username message:(NSString *)message
{
    DLog(@"%@--%@",username, message);
    
    if (!username) {
        return;
    }
    if (!message) {
                message = [NSString stringWithFormat:@"%@ 添加你为好友", username];
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"title":username, @"username":username, @"applyMessage":message, @"applyStyle":[NSNumber numberWithInteger:ApplyStyleFriend]}];
    
    [[ApplyViewController shareController] addNewApply:dic];

}


@end
